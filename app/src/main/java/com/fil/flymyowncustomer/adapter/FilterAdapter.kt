package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.FilterSubItem
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_filter.view.*


class FilterAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val masterData: ArrayList<FilterSubItem?>?,
    tabposition: Int

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        //var  masterdata:ArrayList<MasterPojo?>?=null

    var mSelection = -1
    var tabPos = -1
    /*init {

        tabPos=tabposition
        masterdata=masterData
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_filter, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder
            holder1.tv_filter_name.text=masterData!![position]!!.title
            holder1.mycart_checkbox.isChecked= masterData?.get(position)?.isSelected!!

            holder1.mycart_checkbox.setOnCheckedChangeListener { compoundButton, b ->
                if(compoundButton.isPressed) {
                    masterData?.get(holder1.adapterPosition)?.isSelected = b
                    if (onItemClick != null)
                        onItemClick.onClicklisneter(holder1.adapterPosition, "")
                }
            }
            holder1.ll_main_filter.setOnClickListener {
                holder1.mycart_checkbox.performClick()
            }
            /*holder1.mycart_checkbox.setOnClickListener(object :View.OnClickListener {
                override fun onClick(v: View?) {
                    holder1.mycart_checkbox.isChecked = holder1.mycart_checkbox.isChecked

                    if (holder1.mycart_checkbox.isChecked) {
                        onItemClick.onItemCheck(position)
                    } else {
                        onItemClick.onItemUncheck(position)
                    }
                }
            })*/

            /*when(tabPos)
            {
                1->{
                    holder1.tv_filter_name.text=masterdata!!.colors!![position]!!.colorName

                }
                2->{
                    holder1.tv_filter_name.text=masterdata!!.material!![position]!!.materialName

                }
                3->{
                    holder1.tv_filter_name.text=masterdata!!.pattern!![position]!!.patternName

                }
                4->{
                    holder1.tv_filter_name.text=masterdata!!.style!![position]!!.styleName

                }
                5->{
                    holder1.tv_filter_name.text=masterdata!!.brand!![position]!!.brandName

                }
            }*/






        }
    }

    override fun getItemCount(): Int {
        var size:Int?=null
        /*when(tabPos)
        {
            1->{
                size= masterdata?.get(0)!!.colors!!.size

            }
            2->{
                size= masterdata?.get(0)!!.material!!.size

            }
            3->{
                size= masterdata?.get(0)!!.pattern!!.size

            }
            4->{
                size= masterdata?.get(0)!!.style!!.size

            }
            5->{
                size= masterdata?.get(0)!!.brand!!.size

            }
        }*/
        return masterData!!.size


    }

    /*override fun getItemViewType(position: Int): Int {
        return if (languagelist[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }
*/
    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {
        var minteger = 0
        var mycart_checkbox=itemView.mycart_checkbox
        var tv_filter_name=itemView.tv_filter_name
        var ll_main_filter=itemView.ll_main_filter

        fun bind(
            position: Int,
            onitemClick: OnItemClick,
            tabPos: Int,
            masterdata: FilterSubItem?
        ) =
            with(itemView) {



            }


    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)

    }



}