package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.BillingAddressData
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_biilingaddress.view.*

class BillingAddressAdapter(
    val context: Activity,
    val billingAddressData:ArrayList<BillingAddressData>?,
    val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSelection = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_biilingaddress, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder

            holder1.tvBillingAddress.text=billingAddressData!![position].addressAddressLine1+" "+billingAddressData[position].addressAddressLine2+" "+billingAddressData[position].addressLandmark+" "+billingAddressData[position].addressLocality+" "+billingAddressData[position].addressPincode

            holder1.tvEdit.setOnClickListener {
                onItemClick.onClicklisneter(position,"edit")
            }
            holder1.imageViewDelete.setOnClickListener {
                onItemClick.onClicklisneter(position,"delete")
            }
           if( mSelection == position)
           {
             holder1.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_checked)

           }
           else
           {
               holder1.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_unchecked)

           }
            if(billingAddressData[position].addressIsDefault.equals("Yes",false))
            {
                holder1.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_checked)

                holder1.imageViewDelete.visibility=View.INVISIBLE
            }
            else

            {
                holder1.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_unchecked)
                holder1.imageViewDelete.visibility=View.VISIBLE
            }

            holder1.imagsetDefault.setOnClickListener {
                if (mSelection == holder1.adapterPosition) {
                    mSelection = -1
                } else {
                    mSelection = holder1.adapterPosition
                    onItemClick.onClicklisneter(position,"SetDefault")
                }
                notifyDataSetChanged()

            }





        }
    }

    override fun getItemCount(): Int {
        return billingAddressData!!.size
    }

   override fun getItemViewType(position: Int): Int {
        return if (billingAddressData!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {


        var imagsetDefault=itemView.imagsetDefault
        var imageViewDelete=itemView.imageViewDelete
        var tvBillingAddress=itemView.tvBillingAddress
        var tvEdit=itemView.tvEdit



    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)

    }

}