package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.fragment.ProductImageFullScreenFragment
import java.io.Serializable

//class ProductImagesSliderAdapter(val context: Context,val productImageName : ArrayList<String?>) : PagerAdapter() {
class StitchingServiceDetailsViewPagerAdapter(
    val context: Context,
    val productimages: List<StichingServicePojo.StichingServicData.StitchingserviceImage?>?
) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return productimages!!.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_productdetails_viewpager, view, false)!!

        val imageView = imageLayout
            .findViewById(R.id.img_productImages) as SimpleDraweeView
//        getScrennwidth()
       // imageView.setActualImageResource(R.drawable.product_image1)
        if (!productimages!![position]!!.stitchingserviceimagesName.isNullOrEmpty()){

            imageView.setImageURI (productimages[position]!!.stitchingserviceimagesName)
        }else{
            imageView.setActualImageResource(R.drawable.placehloderbanner__img)
        }

        imageView.setOnClickListener {
            var productImageFullScreenFragment= ProductImageFullScreenFragment()
            Bundle().apply {
                putString("from","StitchingDetails")
                if( !productimages.isNullOrEmpty())
                {
                    putSerializable("productImageData",productimages!! as Serializable)

                }
                productImageFullScreenFragment.arguments=this

            }
            (context as MainActivity).navigateTo(productImageFullScreenFragment, productImageFullScreenFragment::class.java.name, true)

        }
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = width + (width/2)
        heightNew = height/5

        return height
    }
}
