package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Stitching
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_sub_order_list.view.*
import kotlinx.android.synthetic.main.item_sub_order_list.view.myBookingMenuOptionIvDealer

class StitchingServiceSublistAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val stitching: List<Stitching>,
    val type: String

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSelection = -1
    var widthNew = 0
    var heightNew = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sub_order_list, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder as ViewHolder
            getScrennwidth()
            holder1.bind(
                holder.adapterPosition,
                onItemClick,
                stitching[position],
                type,
                widthNew,
                heightNew
            )


        }
    }

    override fun getItemCount(): Int {
        return stitching.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (stitching[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            position: Int,
            onitemClick: OnItemClick,
            stitching: Stitching,
            type: String,
            widthNew: Int,
            heightNew: Int
        ) =
            with(itemView) {
                var deliveryType = ""

                if (!stitching.stitchingserviceImages[0].stitchingserviceimagesName.isNullOrEmpty()) {
                    svProductImageMycart.setImageURI(
                        RestClient.imageUrlpath + stitching.stitchingserviceImages[0].stitchingserviceimagesName + "&w=$widthNew&h=$heightNew&zc=0",
                        context
                    )
                } else {
                    svProductImageMycart.setActualImageResource(R.drawable.placeholder)
                }

                llColorSizeLayout?.visibility = View.GONE

                if (!stitching.statusName.isNullOrEmpty()) {
                    llItemOrderDetailListStatus.visibility = View.VISIBLE
                    tvitemOrderDetailListStatusValue.text = "" + stitching.statusName
                } else {
                    llItemOrderDetailListStatus.visibility = View.GONE
                    tvitemOrderDetailListStatusValue.text = ""
                }

                tvitemOrderDetailListStatusReasonValue?.text =
                    "" + stitching.orderdetailsstitchingStatusRemark

                tv_ProductNameMyCart.text = stitching.stitchingserviceName
                tv_amount_to_pay_price.text =
                    resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                        java.lang.Double.valueOf(stitching.orderdetailsstitchingPrice)
                    )
                tv_StitchingStudioAddress.text = "Stitching"
                displayQutity.text =

                    MyUtils.formatPricePerCountery(java.lang.Double.valueOf(stitching.orderdetailsstitchingQty)) + " X Qty"
                tv_ExpDeliveryDate.text = "Expected Delivered by: ${MyUtils.formatDate(
                    stitching.orderdetailsstitchingExpectedDeliveryDate!!,
                    "yyyy-MM-dd",
                    "d MMM yyyy"
                )}"
                if (!stitching.orderdetailsstitchingEstimatedDeliveryDate.isNullOrEmpty() && !stitching.orderdetailsstitchingEstimatedDeliveryDate.equals(
                        "0000-00-00",
                        false
                    )
                ) {
                    tv_EstimatedDeliveryDate.text = "Estimated Delivered by: ${MyUtils.formatDate(
                        stitching.orderdetailsstitchingEstimatedDeliveryDate!!,
                        "yyyy-MM-dd",
                        "d MMM yyyy"
                    )}"
                } else {
                    tv_EstimatedDeliveryDate.visibility = View.GONE
                }
                if (stitching?.orderdetailsstitchingDeliveryType.equals(
                        "Home Delivery",
                        true
                    ) || stitching?.orderdetailsstitchingDeliveryType.equals("Home Service", true)
                ) {
                    deliveryType = "Home Service"
                } else {
                    deliveryType = "Self Service"
                }
                item_order_detail_list_excepted_delivery_type_value.text = deliveryType

                when (stitching.statusName) {
                    "Placed" -> {
                        myBookingMenuOptionIvDealer.visibility = View.VISIBLE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Confirm" -> {
                        myBookingMenuOptionIvDealer.visibility = View.VISIBLE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Fabric Received" -> {
                        myBookingMenuOptionIvDealer.visibility = View.VISIBLE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Measurement Taken" -> {
                        myBookingMenuOptionIvDealer.visibility = View.VISIBLE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Stitching" -> {
                        myBookingMenuOptionIvDealer.visibility = View.GONE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Completed" -> {
                        if (stitching.IsYouRated.equals("Yes", false)) {
                            myBookingMenuOptionIvDealer.visibility = View.GONE

                        } else if (stitching.IsYouRated.equals("No", false)) {
                            myBookingMenuOptionIvDealer.visibility = View.VISIBLE

                        }
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Cancelled" -> {
                        myBookingMenuOptionIvDealer.visibility = View.GONE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.VISIBLE

                    }
                    "Return" -> {
                        myBookingMenuOptionIvDealer.visibility = View.GONE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    "Replace" -> {
                        myBookingMenuOptionIvDealer.visibility = View.GONE
                        ll_return_replace.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE
                    }
                    else -> {
                        ll_return_replace.visibility = View.GONE
                        myBookingMenuOptionIvDealer.visibility = View.GONE
                        llItemOrderDetailListStatusReason?.visibility = View.GONE

                    }
                }

                myBookingMenuOptionIvDealer.setOnClickListener {
                    onitemClick.onClicklisneter(position, "Option", myBookingMenuOptionIvDealer)

                }
                tv_Return.setOnClickListener {
                    if (onitemClick != null) {
                        onitemClick.onClicklisneter(position, "Return", tv_Return)
                    }
                }

                tv_Replace.setOnClickListener {
                    if (onitemClick != null) {
                        onitemClick.onClicklisneter(position, "Replace", tv_Return)
                    }
                }


            }

    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String, v: View)

    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 3).toInt()
        heightNew = (height / 4).toInt()

        return height
    }
}