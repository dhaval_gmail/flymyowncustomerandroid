package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import kotlinx.android.synthetic.main.item_payment_layout.view.*


class BottomSheetListAdapter(
    var context: Context,
    var itemList: List<String>,
    var onRecyclerItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<BottomSheetListAdapter.ViewHolder>() {
    internal var mSelection = -1
    var mClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = onRecyclerItemClickListener



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_payment_layout, parent, false))
    }

    override fun getItemCount() = 10

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val layoutPayment = itemView.layoutPayment
        val tv_types = itemView.tv_types
        val radioUnselected = itemView.radioUnselected

    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, typeofOperation: String)
    }


}