package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.os.Parcelable
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.ProductListPojo




//class ProductImagesSliderAdapter(val context: Context,val productImageName : ArrayList<String?>) : PagerAdapter() {
class ProductFullViewPagerAdapter(
    val context: Context,
    val productimages: List<ProductListPojo.Data.Productimage?>?
) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return productimages!!.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_productdetails_viewpager, view, false)!!
        val imageView = imageLayout
            .findViewById(R.id.img_productImages) as SimpleDraweeView


       imageView.setImageURI (productimages?.get(position)?.prodimgName)

        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}
