package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Paint
import android.graphics.Point
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_product_listing_layout.view.*

//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class StichingServiceListingAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val productListData: ArrayList<StichingServicePojo.StichingServicData?>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    var widthNew = 0
    var heightNew = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_listing_layout, parent, false)
            return StichingStudioHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return productListData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (productListData[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is StichingStudioHolder) {
            getScrennwidth()
            mClickListener = onItemClick
            val holder1 = holder as StichingStudioHolder
            holder1.Img_Favorites.visibility = View.VISIBLE

            if (!productListData[position]!!.stitchingserviceImages.isNullOrEmpty() && !productListData[position]!!.stitchingserviceImages!![0]!!.stitchingserviceimagesName.isNullOrEmpty()) {
                holder1.svProductImage.setImageURI(Uri.parse(RestClient.imageUrlpath+productListData[position]!!.stitchingserviceImages!![0]!!.stitchingserviceimagesName+"&w=$widthNew&h=$heightNew&zc=0"),context)
            } else {
                holder1.svProductImage.setActualImageResource(R.drawable.placeholder)
            }

            holder1.tvProductName.text = productListData[position]!!.stitchingserviceName
            holder1.tv_ProductOfferPrice.text = mActivity.resources.getString(R.string.rs) +MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData[position]!!.stitchingservicePrice.toString()))

            if(!productListData[position]!!.stitchingserviceDiscount!!.equals("0.00",false))
            {
                holder1.tv_ProductActualPrice.text = mActivity.resources.getString(R.string.rs) +MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData[position]!!.stitchingserviceActualPrice.toString()))
                holder1.tv_ProductOfferPercent.text = ""+MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData[position]!!.stitchingserviceDiscount.toString())) + "% Off"

            }
            else
            {
                holder1.tv_ProductActualPrice.visibility=View.GONE
                holder1.tv_ProductOfferPercent.visibility=View.GONE

            }


            if(productListData[position]!!.isYourFavourite.equals("Yes",false))
             {
                 holder1.Img_Favorites.setImageResource(R.drawable.heart_red)
             }
             else
             {
                 holder1.Img_Favorites.setImageResource(R.drawable.heart_grey)

             }
             if(!productListData[position]!!.stitchingserviceIsPopular.isNullOrEmpty() && productListData[position]!!.stitchingserviceIsPopular.equals("Yes",false))
             {
                 holder1.tvPopular.visibility=View.VISIBLE
             }
             else
             {
                 holder1.tvPopular.visibility=View.GONE

             }

            holder1.tv_ProductActualPrice.paintFlags = holder1.tv_ProductActualPrice.paintFlags or
                    Paint.STRIKE_THRU_TEXT_FLAG

            holder1.svProductImage.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition, "click")
            }
            holder1.Img_Favorites.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition, "fav")
            }
         /* if(productListData!![position]!!.productStock.equals("1.00",false))
            {
                holder1.tv_OfferLeftTime.visibility=View.VISIBLE

            }
            else
            {
                holder1.tv_OfferLeftTime.visibility=View.GONE

            }*/


        }

    }

    class StichingStudioHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var tvPopular = itemView.tvPopular
        var tvUptoOff = itemView.tvUptoOff
        var svProductImage = itemView.svProductImage
        var tvProductName = itemView.tvProductName
        var Img_Favorites = itemView.Img_Favorites
        var tv_ProductOfferPrice = itemView.tv_ProductOfferPrice
        var tv_ProductActualPrice = itemView.tv_ProductActualPrice
        var tv_ProductOfferPercent = itemView.tv_ProductOfferPercent
        var tv_OfferLeftTime = itemView.tv_OfferLeftTime

    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int, from: String)
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 3).toInt()
        heightNew = (height / 4).toInt()

        return height
    }

}