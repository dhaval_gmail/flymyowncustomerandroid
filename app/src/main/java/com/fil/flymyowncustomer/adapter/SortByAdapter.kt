package com.fil.farmerprice.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.iterfaces.RecyclerViewItemClickListner
import com.fil.flymyowncustomer.pojo.SortingPojo

import kotlinx.android.synthetic.main.item_sorting_layout.view.*
import java.util.ArrayList

class SortByAdapter(
    var context: Context,
    var sortingList : ArrayList<SortingPojo?>,
    var recyclerViewItemClickListner: RecyclerViewItemClickListner
) : RecyclerView.Adapter<SortByAdapter.SortViewHolder>() {

    var mSelection = -1
    var mClickListner: RecyclerViewItemClickListner? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortViewHolder {

        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_sorting_layout, parent, false)

        return SortViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sortingList.size
    }

    override fun onBindViewHolder(holder: SortViewHolder, position: Int) {

        mClickListner = recyclerViewItemClickListner

        if (mSelection == position)
            holder.Img_SelectionSortType.setImageResource(R.drawable.checkbox_selected_sort_popup)
        else
            holder.Img_SelectionSortType.setImageResource(R.drawable.checkbox_unselected_sort_popup)

        holder.tv_SortingText?.text =""+ sortingList[position]?.sortingName
        
        holder.itemView.setOnClickListener {
            if (mSelection == position) {

            } else {
                mSelection = holder?.adapterPosition

                if (mClickListner != null) {
                    mClickListner!!.onItemClick(holder?.adapterPosition,  sortingList[position]?.sortingName!!)
                }
                notifyDataSetChanged()
            }
        }

        holder.itemView.tv_SortingText.setOnClickListener {
            if (mSelection == position) {

            } else {
                mSelection = holder?.adapterPosition

                if (mClickListner != null) {
                    mClickListner!!.onItemClick(holder?.adapterPosition,  sortingList[position]?.sortingName!!)
                }
                notifyDataSetChanged()
            }
        }

    }

    class SortViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_SortingText = view.tv_SortingText
        var Img_SelectionSortType = view.Img_SelectionSortType

    }
}