package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import kotlinx.android.synthetic.main.item_bottomsheet_layout.view.*


class BottomSheetStringAdapter(
    var context: Context,
    var itemList: ArrayList<String>,
    var onRecyclerItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<BottomSheetStringAdapter.ViewHolder>() {
    internal var mSelection = -1
    var mClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = onRecyclerItemClickListener

        holder.bottomsheet_checkbox.visibility = View.GONE
        holder?.bottomsheet_text.visibility = View.VISIBLE

        holder?.bottomsheet_text.text = itemList[position]

        holder?.bottomsheet_text.setOnClickListener {
            if (mSelection == position) {

            } else {

                mSelection = holder?.adapterPosition
                if (mClickListener != null) {
                    mClickListener!!.onItemClick(holder?.adapterPosition, itemList[holder.adapterPosition])
                }
                notifyDataSetChanged()
            }
        }

        holder?.itemView.setOnClickListener {
            if (mSelection == position) {

            } else {
                mSelection = holder?.adapterPosition
                if (mClickListener != null) {
                    mClickListener!!.onItemClick(holder?.adapterPosition, itemList[holder.adapterPosition])
                }
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_layout, parent, false))
    }

    override fun getItemCount() = itemList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bottomsheet_checkbox = itemView.bottomsheet_checkbox
        val bottomsheet_text = itemView.bottomsheet_text
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, selectedString : String)
    }
}