package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.WallethistoryData
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_recharge_amount_transaction_layout.view.*
import kotlinx.android.synthetic.main.item_sub_order_list.view.*

//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class TransactionHistoryAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val wallethistoryData: ArrayList<WallethistoryData>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recharge_amount_transaction_layout, parent, false)
            return TransactionHistoryHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return wallethistoryData.size
    }

  override fun getItemViewType(position: Int): Int {
        return if (wallethistoryData.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is TransactionHistoryHolder) {
            mClickListener = onItemClick
            val holder1 = holder as TransactionHistoryHolder
            holder1.tvDate.text=MyUtils.formatDate(wallethistoryData?.get(position).wallettransactonDate,"yyyy-MM-dd hh:mm:ss",
                "d MMM yyyy")
             if(wallethistoryData[position].wallettransactonMode.equals("+",false))
             {
                 holder1.tvDebit_Credit.text="+${mActivity.resources.getString(R.string.rs)}${MyUtils.formatPricePerCountery(wallethistoryData[position].wallettransactonAmount.toDouble())}"
                 holder1.tvDebit_Credit.setTextColor(mActivity.resources.getColor(R.color.credit_color))
             }else
             {
                 holder1.tvDebit_Credit.text="-${mActivity.resources.getString(R.string.rs)}${MyUtils.formatPricePerCountery(wallethistoryData[position].wallettransactonAmount.toDouble())}"
                 holder1.tvDebit_Credit.setTextColor(mActivity.resources.getColor(R.color.debit_color))

             }
            holder1.tvAvailable.text=mActivity.resources.getString(R.string.rs)+MyUtils.formatPricePerCountery(wallethistoryData[position].wallettransactonAvailableAmount.toDouble())
            holder1.itemView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }

        }

    }

    class TransactionHistoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var tvDate = itemView.tvDate
        var tvDebit_Credit = itemView.tvDebit_Credit
        var tvAvailable = itemView.tvAvailable

    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int)

    }
}