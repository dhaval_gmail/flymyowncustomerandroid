package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.retrofit.RestClient
import java.util.ArrayList
import kotlinx.android.synthetic.main.item_recent_viewed_layout.view.*

//class ProductImagesSliderAdapter(val context: Context,val productImageName : ArrayList<String?>) : PagerAdapter() {
class RecentViewedSliderAdapter(val context: Context) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return 4
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_recent_viewed_layout, view, false)!!

        val imageView = imageLayout
            .findViewById(R.id.svProductImage) as SimpleDraweeView

        val tvProductName = imageLayout .findViewById(R.id.tvProductName) as SimpleDraweeView
        val tv_ProfuctPrice = imageLayout .findViewById(R.id.tv_ProfuctPrice) as SimpleDraweeView

//        imageView.setActualImageResource(R.drawable.flash_sale_promotion)
        /*if (!productImageName[position].isNullOrEmpty()){

            imageView.setImageURI(Uri.parse(RestClient.timthumb_path1 + productImageName[position] + "&w=$widthNew&h=650"),context)
        }else{
            imageView.setActualImageResource(R.drawable.placehloderbanner__img)
        }*/
        view.addView(imageLayout)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }


}
