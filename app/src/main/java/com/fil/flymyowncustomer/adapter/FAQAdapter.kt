package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.FaqListPojo
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_faq_layout.view.*
//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class FAQAdapter(
    val context: Context,
    val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
    val onItemClick: onItemClickk
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    private var hideSelection: Int = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_faq_layout, parent, false)
            return FAQAdapter.FAQViewHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return arrayFaqDataList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (arrayFaqDataList.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is FAQViewHolder) {
            mClickListener = onItemClick
            val holder1 = holder as FAQViewHolder

            if (!arrayFaqDataList[position]?.faqQuestion.isNullOrEmpty()){
                holder1.itemFaqQuestionText?.text = arrayFaqDataList[position]?.faqQuestion
            }

            if (!arrayFaqDataList[position]?.faqAnswer.isNullOrEmpty()){
                holder1.itemFaqAnsText?.text = MyUtils.fromHtml(arrayFaqDataList[position]?.faqAnswer.toString()).trim()
            }


            holder1?.llFaqArrowLayout.setOnClickListener {
                hideSelection = holder.adapterPosition
                notifyDataSetChanged()
            }

            if (hideSelection == position) {
                holder1.itemFaqAnsText.maxLines = 100
                holder1?.llFaqArrowLayout.visibility = View.GONE
            } else {
                holder1.itemFaqAnsText.maxLines = 3
                holder1?.llFaqArrowLayout.visibility = View.VISIBLE
            }

//        holder.itemView.itemFollowUpText.text = dataList[position]
            holder1.itemView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }
        }
    }

    class FAQViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemFaqQuestion = itemView.itemFaqQuestion
        var itemFaqQuestionText = itemView.itemFaqQuestionText
        var itemFaqAnsText = itemView.itemFaqAnsText
        var llFaqArrowLayout = itemView.llFaqArrowLayout
        var imgFaq_Arrow = itemView.imgFaq_Arrow
        var itemFaqReadWholeAnswer = itemView.itemFaqReadWholeAnswer

    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int)
    }
}