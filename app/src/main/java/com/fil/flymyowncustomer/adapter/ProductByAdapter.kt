package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.FilterSubItem
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_filter.view.*
import kotlinx.android.synthetic.main.item_filter.view.mycart_checkbox
import kotlinx.android.synthetic.main.item_mycart.view.*


class ProductByAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val masterData: ArrayList<String?>?,
    tabposition: Int

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        //var  masterdata:ArrayList<MasterPojo?>?=null

    var mSelection = -1
    var tabPos = -1
    /*init {

        tabPos=tabposition
        masterdata=masterData
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_by, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder

            holder.mycart_checkbox.isChecked = mSelection == position

            holder1.tv_filter_name.text=masterData!![position]!!

            holder1.mycart_checkbox.setOnClickListener {

                if (mSelection == holder.getAdapterPosition()) {
                    mSelection = -1
                } else {
                    mSelection = holder.getAdapterPosition()

                    if (onItemClick != null)
                        onItemClick.onClicklisneter(position, masterData!![position]!!)
                }
                notifyDataSetChanged()
            }
            holder1.ll_main_filter.setOnClickListener {
                holder1.mycart_checkbox.performClick()
            }







        }
    }

    override fun getItemCount(): Int {

        return masterData!!.size


    }

    /*override fun getItemViewType(position: Int): Int {
        return if (languagelist[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }
*/
    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {
        var minteger = 0
        var mycart_checkbox=itemView.mycart_checkbox
        var tv_filter_name=itemView.tv_filter_name
        var ll_main_filter=itemView.ll_main_filter




    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)

    }



}