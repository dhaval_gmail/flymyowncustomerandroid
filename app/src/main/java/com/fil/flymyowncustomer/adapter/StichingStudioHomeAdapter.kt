package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Stitchingstudio
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.*
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.svSudioImage
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tvPopular
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tvProductRatingValue
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tvProductRatingtotalUserCount
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tvUptoOff
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tv_StitchingStudioAddress
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.tv_StitchingStudioName

//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class StichingStudioHomeAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val stitchingstudio: List<Stitchingstudio?>?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    var widthNew = 0
    var heightNew = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)
            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_stiching_studio_layout, parent, false)
            return StichingStudioHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return stitchingstudio?.size!!
    }

    /*override fun getItemViewType(position: Int): Int {
        return if (arrayFaqDataList.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }*/

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is StichingStudioHolder) {
            getScrennwidth()
            mClickListener = onItemClick
            val holder1 = holder as StichingStudioHolder
            if(!stitchingstudio!![position]!!.bannerImages.isNullOrEmpty()){

                holder1.svSudioImage.setImageURI(Uri.parse(RestClient.imageUrlpath+stitchingstudio!![position]!!.bannerImages!![0]!!.dealerimageName+"&w=$widthNew&h=$heightNew&zc=0"),context)

            }else {
                holder1.svSudioImage.setActualImageResource(R.drawable.placeholder)

            }

            if (!stitchingstudio!![position]!!.dealerRatting.isNullOrEmpty() && stitchingstudio!![position]!!.dealerRatting!!.toDouble() > 0.00 && !stitchingstudio!![position]!!.dealerReviewRattingCount.isNullOrEmpty() && stitchingstudio!![position]!!.dealerReviewRattingCount!!.toDouble() > 0.00)
            {
                holder1.tvProductRatingValue.text=stitchingstudio!![position]!!.dealerRatting

                holder1.tvProductRatingtotalUserCount.text="("+stitchingstudio!![position]!!.dealerReviewRattingCount+")"

            }else
            {
                holder1.ratingReview.visibility=View.GONE
            }

               if(stitchingstudio[position]!!.isFavourite.equals("Yes",false))
            {
                holder1.itemProductCategoryimageheart.setImageResource(R.drawable.heart_red)
            }
            else
            {
                holder1.itemProductCategoryimageheart.setImageResource(R.drawable.heart_grey)

            }
            holder1.tv_StitchingStudioName.text = stitchingstudio[position]!!.dealerCompanyName
            holder1.tv_StitchingStudioAddress.text =  stitchingstudio[position]!!.addressAddressLine1+" "+stitchingstudio[position]!!.addressLandmark+" "+stitchingstudio[position]!!.addressLocality+" "+stitchingstudio[position]!!.addressAddressLine2+" "+stitchingstudio[position]!!.stateName+" "+stitchingstudio[position]!!.addressPincode
            holder1.itemProductCategoryimageheart.visibility=View.VISIBLE

            holder1.svSudioImage.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition,"Click")
            }
            holder1.itemProductCategoryimageheart.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition,"fav")
            }
            if(!stitchingstudio[position]!!.dealerIsPopular.isNullOrEmpty() && stitchingstudio[position]!!.dealerIsPopular.equals("Yes",false))
            {
                holder1.tvPopular.visibility=View.VISIBLE
            }
            else
            {
                holder1.tvPopular.visibility=View.GONE

            }
            if(!stitchingstudio[position]!!.dealerIsPopular.isNullOrEmpty())
            {
                if(!stitchingstudio[position]!!.offerDiscountValue.isNullOrEmpty())
                {
                    holder1.tvUptoOff.visibility=View.VISIBLE
                    holder1.tvUptoOff.text="Up to ${MyUtils.formatPricePerCountery(java.lang.Double.valueOf(stitchingstudio[position]!!.offerDiscountValue.toString()))}% off"

                }
                else
                {
                    holder1.tvUptoOff.visibility=View.GONE
                }
            }
            else
            {
                holder1.tvUptoOff.visibility=View.GONE

            }


        }

    }

    class StichingStudioHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var svSudioImage = itemView.svSudioImage
        var tvProductRatingValue = itemView.tvProductRatingValue
        var tvProductRatingtotalUserCount = itemView.tvProductRatingtotalUserCount
        var tv_StitchingStudioName = itemView.tv_StitchingStudioName
        var tv_StitchingStudioAddress = itemView.tv_StitchingStudioAddress
        var itemProductCategoryimageheart = itemView.itemProductCategoryimageheart
        var tvPopular = itemView.tvPopular
        var tvUptoOff = itemView.tvUptoOff
        var ratingReview = itemView.ratingReview
    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int,from:String)
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 3 ).toInt()
        heightNew = (height / 5).toInt()

        return height
    }
}