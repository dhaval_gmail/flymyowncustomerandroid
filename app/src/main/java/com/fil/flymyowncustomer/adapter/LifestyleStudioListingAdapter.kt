package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Stitchingstudio
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_nearby_me_lifestylestore_layout.view.*

class LifestyleStudioListingAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val dealerListData: ArrayList<Stitchingstudio?>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    var widthNew = 0
    var heightNew = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_nearby_me_lifestylestore_layout, parent, false)
            return StichingStudioHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return dealerListData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (dealerListData.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is StichingStudioHolder) {
            getScrennwidth()
            mClickListener = onItemClick
            val holder1 = holder as StichingStudioHolder

            if(!dealerListData!![position]?.bannerImages.isNullOrEmpty()){
                holder1.BannerImagesRecyclerview?.visibility = View.VISIBLE
                holder1.svSudioImage?.visibility = View.GONE
                holder1.mBannerImagesSliderAdapter = BannerImagesAdapter(context,dealerListData[position]?.bannerImages,object : BannerImagesAdapter.onItemClickk{
                    override fun onClicklisneter(pos: Int) {
                        holder1?.itemView?.performClick()
                    }
                } )
                holder1?.BannerImagesRecyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                holder1?.BannerImagesRecyclerview.setHasFixedSize(true)
                holder1?.BannerImagesRecyclerview.isNestedScrollingEnabled = false
                holder1.BannerImagesRecyclerview?.adapter = holder1.mBannerImagesSliderAdapter

//                holder1.svSudioImage?.setImageURI(Uri.parse(RestClient.imageUrlpath+dealerListData!![position]!!.bannerImages!![0]!!.dealerimageName+"&w=$widthNew&h=$heightNew&zc=0"),context)
            }else {
                holder1.BannerImagesRecyclerview?.visibility = View.VISIBLE
                holder1.svSudioImage?.visibility = View.VISIBLE
                holder1.svSudioImage?.setActualImageResource(R.drawable.placeholder)
            }

            if (!dealerListData!![position]!!.dealerRatting.isNullOrEmpty() && dealerListData!![position]!!.dealerRatting!!.toDouble() > 0.00 && !dealerListData!![position]!!.dealerReviewRattingCount.isNullOrEmpty() && dealerListData!![position]!!.dealerReviewRattingCount!!.toDouble() > 0.00)
            {
                holder1.tvProductRatingValue?.text=(dealerListData[position]!!.dealerRatting)
                holder1.tvProductRatingtotalUserCount?.text="("+dealerListData[position]!!.dealerReviewRattingCount+")"

            }else
            {
                holder1.ratingReview?.visibility=View.GONE
            }

            holder1.tv_StitchingStudioName?.text=dealerListData[position]!!.dealerCompanyName
            holder1.tv_StitchingStudioAddress?.text= dealerListData[position]!!.addressAddressLine1+" "+dealerListData[position]!!.addressLandmark+" "+dealerListData[position]!!.addressLocality+" "+dealerListData[position]!!.addressAddressLine2+" "+dealerListData[position]!!.stateName+" "+dealerListData[position]!!.addressPincode


                if(!dealerListData[position]?.dealerServiceType.isNullOrEmpty()){
                    holder1?.tvServiceType.visibility = View.VISIBLE

                    if(dealerListData[position]?.dealerType.equals("Stitching Store")){
                        if(!dealerListData[position]?.dealerServiceType.isNullOrEmpty() && dealerListData[position]?.dealerServiceType.equals("Home Service",false))
                        {
                            holder1?.tvServiceType?.text="Home Delivery"
                        }
                        else if(!dealerListData[position]?.dealerServiceType.isNullOrEmpty() && dealerListData[position]?.dealerServiceType.equals("Self Service",false))
                        {
                            holder1?.tvServiceType?.text="Self Pickup"

                        }else {
                            holder1?.tvServiceType?.text="Home Delivery, Self Pickup"
                        }
                    }else if(dealerListData[position]?.dealerType.equals("Stitching Studio")){

                        if(!dealerListData[position]?.dealerServiceType.isNullOrEmpty() && dealerListData[position]?.dealerServiceType.equals("Home Service",false))
                        {
                            holder1?.tvServiceType?.text="Home Service"
                        }
                        else if(!dealerListData[position]?.dealerServiceType.isNullOrEmpty() && dealerListData[position]?.dealerServiceType.equals("Self Service",false))
                        {
                            holder1?.tvServiceType?.text="Self Service"

                        }else {
                            holder1?.tvServiceType?.text="Home Service, Self Service"
                        }
                    }

//                holder1?.tvServiceType?.text = ""+dealerListData[position]?.dealerServiceType

                }else {
                    holder1?.tvServiceType.visibility = View.GONE
                }

            holder1?.svSudioImage.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition,"click")
            }

            holder1?.itemView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition,"click")
            }

            if(dealerListData[position]!!.isFavourite.equals("Yes",false))
            {
                holder1.Img_Favorites?.setImageResource(R.drawable.heart_red)
            }
            else
            {
                holder1.Img_Favorites?.setImageResource(R.drawable.heart_grey)

            }

            if(dealerListData[position]!!.isWorking.equals("Open",false))
            {
                holder1.tv_OpenCloseStaus?.setBackgroundResource(R.drawable.open_status_green)
            }
            else
            {
                holder1.tv_OpenCloseStaus?.setBackgroundResource(R.drawable.closed_status_green)

            }

             holder1.tv_OpenCloseStaus?.text=dealerListData[position]!!.isWorking
             if(!dealerListData[position]?.distance.isNullOrEmpty()&& !dealerListData[position]?.distance.equals("0.00",false))
             {
                 holder1.tv_Distance?.text=String.format("%.2f",java.lang.Double.valueOf(dealerListData[position]?.distance?.toDouble()!!)) + " km"

             }
            else
             {
                 holder1.tv_Distance?.visibility=View.GONE
             }

            if(!dealerListData[position]!!.dealerIsPopular.isNullOrEmpty() && dealerListData[position]!!.dealerIsPopular.equals("Yes",false))
            {
                holder1.tvPopular?.visibility=View.VISIBLE
            }
            else
            {
                holder1.tvPopular?.visibility=View.GONE

            }
            if(!dealerListData[position]!!.offerDiscountValue.isNullOrEmpty() && !dealerListData[position]!!.offerDiscountValue.equals("0.00",false))
            {
                holder1.tvUptoOff.visibility=View.GONE
                holder1.tvUptoOff.text="Up to ${MyUtils.formatPricePerCountery(java.lang.Double.valueOf(dealerListData[position]!!.offerDiscountValue.toString()))}% off"
            }
            else
            {
                holder1.tvUptoOff?.visibility=View.GONE

            }
            holder1.Img_Favorites.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition,"fav")
            }




        }

    }

    class StichingStudioHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var svSudioImage = itemView.svSudioImage

        var BannerImagesRecyclerview = itemView.BannerImagesRecyclerview
        var tvProductRatingValue = itemView.tvProductRatingValue
        var tvProductRatingtotalUserCount = itemView.tvProductRatingtotalUserCount
        var tv_StitchingStudioName = itemView.tv_StitchingStudioName
        var tv_StitchingStudioAddress = itemView.tv_StitchingStudioAddress
        var tv_OpenCloseStaus = itemView.tv_OpenCloseStaus
        var tv_Distance = itemView.tv_Distance
        var Img_Favorites = itemView.Img_Favorites
        var tvPopular = itemView.tvPopular
        var tvUptoOff = itemView.tvUptoOff
        var tvServiceType = itemView.tv_serviceType
        var ratingReview = itemView.ratingReview
        var mBannerImagesSliderAdapter : BannerImagesAdapter?= null

    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int,from:String)
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 2 ).toInt()
        heightNew = (height / 4).toInt()

        return height
    }
}