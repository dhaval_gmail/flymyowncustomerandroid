package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.NotificationListPojo1
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder

import kotlinx.android.synthetic.main.fragment_notifications_adapter.view.*
import kotlinx.android.synthetic.main.fragment_order_details.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(var context: Context, val notificationListData : ArrayList<NotificationListPojo1.Datum?>, val onItemClick: OnItemClick ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: OnItemClick? = null
    private var mActivity = context
    private var deletedIndex = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_notifications_adapter, parent, false)
            return NotificationViewHolder(v)
        }
    }

    override fun getItemCount(): Int {

        return notificationListData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (notificationListData.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is LoaderViewHolder)
        {
            showLoadingView(holder, position)
//            holder.mProgressBar?.visibility = View.VISIBLE
//            return

        } else if (holder is NotificationViewHolder) {
            mClickListener = onItemClick
            val holder1 = holder as NotificationViewHolder
            if(TextUtils.isEmpty(notificationListData[position]?.notificationTitle)){
                holder1?.textViewTitleNotificationsAdapter?.visibility = View.GONE
            }else {
                holder1?.textViewTitleNotificationsAdapter?.visibility = View.VISIBLE
            }

            holder1?.textViewTitleNotificationsAdapter?.text = notificationListData[position]?.notificationTitle
            holder1?.textViewDetailNotificationsAdapter?.text = notificationListData[position]?.notificationMessageText
            try {
                holder1.textViewTimeNotificationsAdapter.text = getMyPrettyDate(notificationListData[position]?.notificationSendDate + " " + notificationListData[position]?.notificationSendTime)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            holder1?.itemView.setOnClickListener {
                var type = notificationListData[holder1.adapterPosition]?.notificationType
                when (type) {
                    "WalletRecharge" -> {
                        if (mClickListener != null) {
                            mClickListener!!.onClicklisneter(holder?.adapterPosition, ""+notificationListData[holder1.adapterPosition]?.notificationType)
                        }
                    }
                    "ReferAndEarn" -> {
                        if (mClickListener != null) {
                            mClickListener!!.onClicklisneter(holder?.adapterPosition, ""+notificationListData[holder1.adapterPosition]?.notificationType)
                        }
                    }
                    "Order" -> {
                        if (mClickListener != null) {
                            mClickListener!!.onClicklisneter(holder?.adapterPosition, ""+notificationListData[holder1.adapterPosition]?.notificationType)
                        }
                    }
                }

                notifyDataSetChanged()
            }

//            holder.bind(notificationListData[position], holder1.adapterPosition, onItemClick)

        }
    }

    class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var linearLayoutViewBackground = itemView.linearLayoutViewBackground
        var linearLayoutViewForeground = itemView.linearLayoutViewForeground
        var textViewTitleNotificationsAdapter = itemView.textViewTitleNotificationsAdapter
        var textViewDetailNotificationsAdapter = itemView.textViewDetailNotificationsAdapter
        var textViewTimeNotificationsAdapter = itemView.textViewTimeNotificationsAdapter

    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)
    }

    fun getMyPrettyDate(date: String): String {

        if(date.isNullOrEmpty())
            return "--"

        val nowTime = Calendar.getInstance()
        val neededTime = Calendar.getInstance()
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val sdfTime = SimpleDateFormat("hh:mm a")
        val sdfDate = SimpleDateFormat("dd MMM")
        var strDate: Date? = null
        try {
            strDate = sdf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        neededTime.timeInMillis = strDate!!.time

        return if (nowTime.get(Calendar.DATE) == neededTime.get(Calendar.DATE)) {
            //here return like "Today at 12:00"
            sdfTime.format(neededTime.time)

        } else {
            sdfDate.format(neededTime.time)
        }

    }

}