package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.os.Parcelable
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.LinearLayout
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.BannerImage
import com.fil.flymyowncustomer.retrofit.RestClient
import kotlinx.android.synthetic.main.item_banner_full_images_slider.view.*

class BannerImagesFullSliderAdapter(
    val context: Context,
    val banner: List<BannerImage?>?
) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return banner?.size!!
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_banner_full_images_slider, view, false)!!
        getScrennwidth()
        var linearLayout = imageLayout.findViewById<LinearLayout>(R.id.linearLayout)
        val imageView = imageLayout
            .findViewById(R.id.img_BannerImages) as SimpleDraweeView

//        imageView.setActualImageResource(R.drawable.flash_sale_promotion)
        if (!banner!![position]?.dealerimageName.isNullOrEmpty()){
            imageView?.setImageURI(Uri.parse(RestClient.imageUrlpath+banner!![position]?.dealerimageName+"&w=$widthNew&h=$heightNew&zc=0"),context)
//            imageView.setImageURI(banner!![position]!!.bannerImage)
        }else{
            imageView?.setActualImageResource(R.drawable.placehloderbanner__img)
        }

        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width/1)
        heightNew = height/5

        return height
    }
}
