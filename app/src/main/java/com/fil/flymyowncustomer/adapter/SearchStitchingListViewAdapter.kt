package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.pojo.Stitchingstudio


import java.util.ArrayList

class SearchStitchingListViewAdapter(
    val activity: Activity,
    val resource: Int,
    val searchList:ArrayList<StichingServicePojo.StichingServicData?>?
) : ArrayAdapter<String>(activity, resource) {


    override fun getCount(): Int {
        return searchList?.size!!
    }

    override fun getItem(position: Int): String? {
        return searchList?.size.toString()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        val inflater = activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.item_search_list, parent, false)
            // get all UI view
            holder = ViewHolder(convertView!!)
            // set tag for holder
            convertView.tag = holder
        } else {
            // if holder created, get tag from view
            holder = convertView.tag as ViewHolder
        }

        holder.tvSearch.text = searchList?.get(position)!!.stitchingserviceName




        return convertView
    }

    private fun getType(type: String): String {
        var type = type

        when (type) {
            "centers" -> type = "Gym"
            "foodmain" -> type = "Food"
            "userfoodmain" -> type = "Food"
            "dieticians" -> type = "Dietician"
            "post" -> type = "Post"
            "users" -> type = "User"
        }
        return type
    }

    private inner class ViewHolder(v: View) {

         var tvSearch: TextView
        init {
            tvSearch = v.findViewById<View>(R.id.tvSearch) as TextView
        }
    }
}

