package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.fragment.ProductListingFragment
import com.fil.flymyowncustomer.fragment.StichingServiceListingFragment
import com.fil.flymyowncustomer.pojo.Category
import com.fil.flymyowncustomer.pojo.Subcategory
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_lifestyle_category.view.*
import kotlin.collections.ArrayList

class LifestyleStudioCategoryAdapter(
    val context: Activity,
    val onItemClick: OnItemClickListner,
    val categoryListPojo: java.util.ArrayList<Category?>?,
    val categorytypeIDs: String,
    val from: String,
    val dealerIDs: String
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isExpandPosition: Int = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_lifestyle_category, parent, false)
            return LifestyleStudioCategoryAdapter.CategoryParentViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is CategoryParentViewHolder) {

            holder.bind(
                categoryListPojo!![position]!!,
                holder.adapterPosition,
                context,
                categorytypeIDs,
                from,
                dealerIDs
            )

            holder.itemView.setOnClickListener {
                if (holder.adapterPosition > -1) {

                    if (isExpandPosition > -1 && isExpandPosition != holder.adapterPosition && categoryListPojo.get(
                            isExpandPosition
                        )!!.isExpand
                    ) {

                        categoryListPojo!!.get(isExpandPosition)!!.isExpand = false

                        notifyItemChanged(isExpandPosition)
                    }

                    categoryListPojo.get(holder.adapterPosition)!!.isExpand =
                        !categoryListPojo.get(holder.adapterPosition)!!.isExpand
                    if (categoryListPojo.get(holder.adapterPosition)!!.isExpand)
                        isExpandPosition = holder.adapterPosition
                    else
                        isExpandPosition = -1

                    notifyItemChanged(holder.adapterPosition)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return categoryListPojo!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (categoryListPojo!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }


    class CategoryParentViewHolder(itemView: View, context: Activity) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(
            category: Category,
            position: Int,
            context: Activity,
            categorytypeIDs: String,
            from: String,
            dealerIDs: String
        ) =
            with(itemView) {
                itemProductCategoryMainCategory.text = category.categoryName

                inflateLifestoreDetailsSubcategory(
                    ll_productSubcategorey,
                    context,
                    category.subcategory,
                    categorytypeIDs,
                    category.categoryID,
                    from,
                            dealerIDs
                )

                ll_productSubcategorey.post {
                    if (category.isExpand) {
                        itemProductCategoryImageDown.setImageResource(R.drawable.arrow_up_black)
                         MyUtils.expand(ll_productSubcategorey)
                      //  ll_productSubcategorey.visibility = View.VISIBLE
                    } else {
                        MyUtils.collapse(ll_productSubcategorey)
                        //ll_productSubcategorey.visibility = View.GONE
                        itemProductCategoryImageDown.setImageResource(R.drawable.arrow_down_black)
                    }
                }
            }

        private fun inflateLifestoreDetailsSubcategory(
            llProductsubcategorey: LinearLayoutCompat,
            context: Activity,
            subcategory: List<Subcategory?>?,
            categorytypeIDs: String,
            categoryID: String?,
            from: String?,
            dealerIDs: String
        ) {
            /* var listBanner=ArrayList<String>()
             listBanner!!.add("A")
             listBanner!!.add("B")
             listBanner!!.add("C")*/
            llProductsubcategorey.removeAllViews()
            for (i in 0 until subcategory!!.size) {
                val view: View = context.layoutInflater.inflate(
                    R.layout.item_lifestyle_sub_category, // Custom view/ layout
                    llProductsubcategorey, // Root layout to attach the view
                    false // Attach with root layout or not
                )

                val itemCategorySubCategory =
                    view.findViewById<AppCompatTextView>(R.id.itemCategorySubCategory)

                itemCategorySubCategory.text = (subcategory[i]!!.subcatName)
                view.setOnClickListener {
                    when (from) {
                        "Stitching Studio" -> {
                            var stichingFragment = StichingServiceListingFragment()
                            Bundle().apply {
                               putString("from", from)
                               putString("categorytypeIDs", categorytypeIDs)
                               putString("categoryID", categoryID)
                               putString("dealerIDs", dealerIDs)
                               putSerializable("subCategoryData", subcategory[i]!!)
                                stichingFragment.arguments = this

                            }
                            (context as MainActivity).navigateTo(
                                stichingFragment,
                                stichingFragment::class.java.name,
                                true
                            )

                        }
                        else -> {
                            var productListingFragment = ProductListingFragment()
                            Bundle().apply {
                               putString("from", from)
                                putString("categorytypeIDs", categorytypeIDs)
                                putString("categoryID", categoryID)
                                putString("dealerIDs", dealerIDs)
                               putSerializable("subCategoryData", subcategory[i]!!)
                                productListingFragment.arguments = this

                            }
                            (context as MainActivity).navigateTo(
                                productListingFragment,
                                productListingFragment::class.java.name,
                                true
                            )

                        }

                    }

                }
                llProductsubcategorey.addView(view)

            }


        }


    }


    interface OnItemClickListner {
        fun onItemClick(subcatName: ArrayList<String>, pos: Int, posSubCatId: Int)
    }


}


