package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.fragment.BottomsheetWithOnlyString
import com.fil.flymyowncustomer.pojo.CountryListPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import kotlinx.android.synthetic.main.item_bottomsheet_layout.view.*


class BottomSheetCountryAdapter(
    var context: Context,
    var itemList: ArrayList<CountryListPojo.Data?>,
    var onRecyclerItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<BottomSheetCountryAdapter.ViewHolder>() {
    internal var mSelection = -1
    var mClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = onRecyclerItemClickListener

        holder.bottomsheet_checkbox.visibility = View.GONE
        holder.bottomsheet_iamgeview.visibility = View.VISIBLE
        holder.bottomsheet_text.visibility = View.VISIBLE

        if (!itemList[position]?.countryDialCode.isNullOrEmpty())
            holder.bottomsheet_text.text = itemList[position]?.countryDialCode

        var imgUri = ""
        if (!itemList[position]?.countryFlagImage.isNullOrEmpty()){
            imgUri = itemList[position]?.countryFlagImage!!
        }

        holder.bottomsheet_iamgeview.setImageURI(Uri.parse(imgUri))

        holder?.itemView.setOnClickListener {
            mSelection = holder?.adapterPosition
            if (mClickListener != null) {
                mClickListener!!.onItemClick(holder?.adapterPosition, itemList[holder.adapterPosition]?.countryDialCode!!, itemList[holder.adapterPosition]?.countryFlagImage!!, itemList[holder.adapterPosition]?.countryID!!)
            }
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_bottomsheet_layout, parent, false))
    }

    override fun getItemCount() = itemList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bottomsheet_checkbox = itemView.bottomsheet_checkbox
        val bottomsheet_iamgeview = itemView.bottomsheet_iamgeview
        val bottomsheet_text = itemView.bottomsheet_text
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, selectedCountryCode : String, selectedFlag : String, selectedCountryId : String)
    }
}