package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.MyOrderData
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_orderlist.view.*

class OrderlistAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val myOderListData: ArrayList<MyOrderData?>,
    val type: String


) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSelection = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_orderlist, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder as ViewHolder

            holder1.bind(holder.adapterPosition, onItemClick,myOderListData[position],type)


        }
    }

    override fun getItemCount(): Int {
        return myOderListData?.size
    }

   override fun getItemViewType(position: Int): Int {
        return if (myOderListData[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            position: Int,
            onitemClick: OnItemClick,
            myOderListData: MyOrderData?,
            type: String

        ) =
            with(itemView) {

                tvOrderIdValue.text=myOderListData?.orderNo
                tvOrderTypeValue.text=myOderListData?.orderType
                tvDeliveryTypeValue.visibility = View.GONE
                tvDeliveryTypeValue.text=myOderListData?.statusName

                when(type)
                {
                    "New"->{

                        myBookingMenuOptionIvDealer.visibility=View.GONE
                    }
                    "Completed"->{
                        myBookingMenuOptionIvDealer.visibility=View.GONE

                    }
                    "Cancelled"->{
                        myBookingMenuOptionIvDealer.visibility=View.GONE

                    }
                }
                myBookingMenuOptionIvDealer.setOnClickListener {
                    onitemClick.onClicklisneter(position, "Option", myBookingMenuOptionIvDealer)
                }

                itemView.setOnClickListener {
                    onitemClick.onClicklisneter(position, "itemView", itemView)

                }
            }

    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String,v:View)

    }

}