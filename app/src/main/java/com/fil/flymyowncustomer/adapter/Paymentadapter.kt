package com.fil.flymyowncustomer.adapter

import android.app.Activity

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Paymentmethod
import kotlinx.android.synthetic.main.item_biilingaddress.view.imagsetDefault
import kotlinx.android.synthetic.main.item_checkout_layout.view.*
import java.util.ArrayList


class Paymentadapter(
    internal var mActivity: Activity,
    val onItemClick: OnItemClick,
    var paymentMethodData: ArrayList<Paymentmethod>,
    val from: String
) :
    RecyclerView.Adapter<Paymentadapter.CheckoutViewHolder>() {

    override fun getItemCount(): Int {
        return paymentMethodData.size
    }


    var mSelection = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_checkout_layout, parent, false)
        return CheckoutViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull holder: CheckoutViewHolder, position: Int) {


        if (mSelection == position) {
            holder.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_checked)

        } else {
            holder.imagsetDefault.setImageResource(R.drawable.radio_button_additional_details_unchecked)

        }
        if (from.equals("add_amount", false)) {
            when {
                paymentMethodData[position].paymentmethodKeyword.equals("COD",false) -> holder.layoutPayment.visibility=View.GONE
                paymentMethodData[position].paymentmethodKeyword.equals("FlyMyOwn Wallet",false) -> holder.layoutPayment.visibility=View.GONE
                else -> holder.layoutPayment.visibility=View.VISIBLE
            }
        } else
        {
            holder.layoutPayment.visibility=View.VISIBLE
        }
        holder.paymenttextview.text = paymentMethodData[position].paymentmethodKeyword


        holder.itemView.setOnClickListener(OnClickListener {
            if (mSelection == holder.getAdapterPosition()) {
                mSelection = -1
            } else {
                mSelection = holder.getAdapterPosition()
                if (onItemClick != null)
                    onItemClick.onClicklisneter(position, "selected")
            }
            notifyDataSetChanged()
        })

    }

    class CheckoutViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imagsetDefault = itemView.imagsetDefault
        var paymenttextview = itemView.paymenttextview
        var layoutPayment = itemView.layoutPayment
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)

    }
}
