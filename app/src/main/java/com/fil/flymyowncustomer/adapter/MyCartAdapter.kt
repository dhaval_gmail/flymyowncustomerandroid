package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.graphics.Paint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.LocalCart
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_mycart.view.*


class MyCartAdapter(
    val context: Activity,
    val arrayData : ArrayList<LocalCart>,
    val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSelection = -1


    val buttonPlus = "BUTTONPLUS"
    val buttonMinus = "BUTTONMINUS"
    val buttonDeliveryType = "BUTTONDELIVERYTYPE"
    val buttonDeliveryDate = "BUTTONDELIVERYDATE"
    val buttonDelete = "BUTTONDELETE"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_mycart, parent, false)
            return ViewHolder(v, context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder
//            holder1.bind(holder.adapterPosition, onItemClick)

            if (!arrayData[position].productData.isNullOrEmpty()){

                var imgUri = ""
                if(!arrayData[position].productData!![0].productimages.isNullOrEmpty() && !arrayData[position].productData!![0].productimages!![0].prodimgName.isNullOrEmpty()){
                    imgUri = arrayData[position].productData!![0]?.productimages!![0].prodimgName!!
                }

                holder1.svProductImageMycart?.setImageURI(Uri.parse(imgUri))

                if (!arrayData[position].productData!![0]?.productName.isNullOrEmpty()){
                    holder1.tv_ProductNameMyCart.text = arrayData[position].productData!![0]?.productName
                }

                if (!arrayData[position].productData!![0]?.productType.isNullOrEmpty()){
                    holder1.tv_StitchingStudioAddress.text = arrayData[position].productData!![0]?.productType
                }
                if(!arrayData[position].addToCartDeliveryName.isNullOrEmpty() && arrayData[position].addToCartDeliveryName.equals("Home Service",false))
                {
                    holder1.tv_pickupMycart.text="Home Delivery"
                }
                else if(!arrayData[position].addToCartDeliveryName.isNullOrEmpty() && arrayData[position].addToCartDeliveryName.equals("Self Service",false))
                {
                    holder1.tv_pickupMycart.text="Self Pickup"

                }else
                {
                    holder1.tv_pickupMycart.text=arrayData[position].addToCartDeliveryName
                }

            }else if (!arrayData[position].stichingServiceData.isNullOrEmpty()){
                var imgUri = ""
                if(!arrayData[position].stichingServiceData!![0].stitchingserviceImages.isNullOrEmpty() && !arrayData[position].stichingServiceData!![0].stitchingserviceImages!![0]!!.stitchingserviceimagesName.isNullOrEmpty()){
                    imgUri = arrayData[position].stichingServiceData!![0].stitchingserviceImages!![0]!!.stitchingserviceimagesName!!
                }

                holder1.svProductImageMycart?.setImageURI(Uri.parse(imgUri))

                if (!arrayData[position].stichingServiceData!![0]?.stitchingserviceName.isNullOrEmpty()){
                    holder1.tv_ProductNameMyCart.text = arrayData[position].stichingServiceData!![0]?.stitchingserviceName
                }

//                if (!arrayData[position].stichingServiceData!![0]?.productType.isNullOrEmpty()){
                    holder1.tv_StitchingStudioAddress.text = "Stitching Studio"
//                }

                if(!arrayData[position].addToCartDeliveryName.isNullOrEmpty() && arrayData[position].addToCartDeliveryName.equals("Home Delivery",false))
                {
                    holder1.tv_pickupMycart.text="Home Service"
                }
                else if(!arrayData[position].addToCartDeliveryName.isNullOrEmpty() && arrayData[position].addToCartDeliveryName.equals("Self Pickup",false))
                {
                    holder1.tv_pickupMycart.text="Self Service"

                }else
                {
                    holder1.tv_pickupMycart.text=arrayData[position].addToCartDeliveryName
                }
            }

            if (arrayData[position]?.productDiscountedPrice != null){
                holder1.tv_amount_to_pay_price.text = context.resources.getString(R.string.rs) + " " + MyUtils.formatPricePerCountery(java.lang.Double.valueOf(arrayData[position]?.productDiscountedPrice!!))
            }

            if (arrayData[position]?.cartQuantity != null){
                holder1.displayInteger.text = arrayData[position]?.cartQuantity.toString()
            }

            /*if (!arrayData[position]?.addToCartDeliveryName.isNullOrEmpty()){
                holder1.tv_pickupMycart.text = arrayData[position]?.addToCartDeliveryName
            }*/

            if (!arrayData[position]?.addToCartExceptedDate.isNullOrEmpty()){
                holder1.tv_DeliveryDate.text = "Excepted Date: " +  MyUtils.formatDate(arrayData[position]?.addToCartExceptedDate!!, "dd-MM-yyyy", "dd MMM")
            }else{
                holder1.tv_DeliveryDate.text = "Select Except Delivery Date"
            }

            if (!arrayData[position]?.issueMsg.isNullOrEmpty()){
                holder1.tv_productError.visibility = View.VISIBLE
                holder1.tv_productError.text = arrayData[position]?.issueMsg
            }else{
                holder1.tv_productError.visibility = View.GONE
            }

            holder1.minusimg.setOnClickListener {
                if (onItemClick != null){
                    onItemClick.newOnItemClicklistner(buttonMinus, holder1.adapterPosition, arrayData[holder1.adapterPosition]!!, arrayData[holder1.adapterPosition]!!.cartID.toString())
                }
            }

            holder1.plusimg.setOnClickListener {
                if (onItemClick != null){
                    onItemClick.newOnItemClicklistner(buttonPlus, holder1.adapterPosition, arrayData[holder1.adapterPosition]!!, arrayData[holder1.adapterPosition]!!.cartID.toString())
                }
            }

            holder1.tv_pickupMycart.setOnClickListener {
                if(onItemClick !=null)
                    onItemClick.newOnItemClicklistner(buttonDeliveryType, holder1.adapterPosition, arrayData[holder1.adapterPosition]!!, "-1")
            }

            holder1.tvChangePickup.setOnClickListener {
                if(onItemClick !=null)
                    onItemClick.newOnItemClicklistner(buttonDeliveryDate, holder1.adapterPosition, arrayData[holder1.adapterPosition]!!, "-1")
            }

            holder1.tvChangePickup.paintFlags = holder1.tvChangePickup.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            holder1.mycart_checkbox.setOnClickListener(object :View.OnClickListener {
                override fun onClick(v: View?) {
                    if(onItemClick !=null)
                        onItemClick.newOnItemClicklistner(buttonDelete, holder1.adapterPosition, arrayData[holder1.adapterPosition]!!, "-1")
                }
            })

            if (arrayData[position].isDelete) holder1.mycart_checkbox.isChecked = true else holder1.mycart_checkbox.isChecked = false
        }
    }

    override fun getItemCount(): Int {
        return arrayData?.size
    }

    /*override fun getItemViewType(position: Int): Int {
        return if (languagelist[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }
*/
    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {
        var minteger = 0

        val svProductImageMycart = itemView.svProductImageMycart
        val tv_ProductNameMyCart = itemView.tv_ProductNameMyCart
        val tv_amount_to_pay_price = itemView.tv_amount_to_pay_price
        val tv_StitchingStudioAddress = itemView.tv_StitchingStudioAddress
        val minusimg = itemView.minusimg
        val plusimg = itemView.plusimg
        val displayInteger = itemView.displayInteger
        val tv_pickupMycart = itemView.tv_pickupMycart
        val tv_DeliveryDate = itemView.tv_DeliveryDate
        val tvChangePickup = itemView.tvChangePickup
        val tv_productError = itemView.tv_productError
        val mycart_checkbox = itemView.mycart_checkbox

        /*fun bind(position: Int, onitemClick: OnItemClick) =
            with(itemView) {

                tvChangePickup.paintFlags = tvChangePickup.paintFlags or Paint.UNDERLINE_TEXT_FLAG

                mycart_checkbox.setOnClickListener(object :View.OnClickListener {
                    override fun onClick(v: View?) {
                        mycart_checkbox.isChecked = mycart_checkbox.isChecked

                        if (mycart_checkbox.isChecked) {
                            onitemClick.onItemCheck(position)
                        } else {
                            onitemClick.onItemUncheck(position)
                        }
                    }
                })


            }*/
    }


    interface OnItemClick {
//        fun onClicklisneter(pos: Int, objProduct: LocalCart)
//        fun onClicklisneterDeliveryType(pos: Int, objProduct: LocalCart)
//        fun onClicklisneterSendProductData(name : String, pos: Int, productObj: LocalCart?)
//
//        fun onItemCheck(item: Int)
//        fun onItemUncheck(item: Int)

        fun newOnItemClicklistner(buttonName : String, pos : Int, objProduct: LocalCart, cartID : String)

    }
}