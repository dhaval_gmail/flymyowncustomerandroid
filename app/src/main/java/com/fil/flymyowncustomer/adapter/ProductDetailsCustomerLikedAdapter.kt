package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_stiching_studio_layout.view.*
//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class ProductDetailsCustomerLikedAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val userProductListData: ArrayList<ProductListPojo.Data?>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    var widthNew = 0
    var heightNew = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_stiching_studio_layout, parent, false)
            return StichingStudioHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return userProductListData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (userProductListData.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is StichingStudioHolder) {
            val holder1 = holder as StichingStudioHolder
            getScrennwidth()
            mClickListener = onItemClick
            holder1.itemProductCategoryimageheart.visibility=View.VISIBLE
            holder1.tv_StitchingStudioName.text=userProductListData[position]!!.productName
            holder1.tv_StitchingStudioAddress.visibility=View.GONE
            holder1.ll_popular_off.visibility=View.GONE

            if(!userProductListData!![position]!!.productimages.isNullOrEmpty()){

                holder1.svSudioImage.setImageURI(Uri.parse(RestClient.imageUrlpath+userProductListData!![position]!!.productimages!![0]!!.prodimgName+"&w=$widthNew&h=$heightNew&zc=0"),context)

            }else {
                holder1.svSudioImage.setActualImageResource(R.drawable.placeholder)

            }
            if(userProductListData[position]!!.isYourFavourite.equals("Yes",false))
            {
                holder1.itemProductCategoryimageheart.setImageResource(R.drawable.heart_red)
            }
            else
            {
                holder1.itemProductCategoryimageheart.setImageResource(R.drawable.heart_grey)
            }

            if (!userProductListData!![position]!!.productRatting.isNullOrEmpty() && userProductListData!![position]!!.productRatting!!.toDouble() > 0.00 && !userProductListData!![position]!!.productReviewRattingCount.isNullOrEmpty() && userProductListData!![position]!!.productReviewRattingCount!!.toDouble() > 0.00)
            {
                holder1.tvProductRatingValue?.text=(userProductListData[position]!!.productRatting)
                holder1.tvProductRatingtotalUserCount?.text="("+userProductListData[position]!!.productReviewRattingCount+")"

            }else
            {
                holder1.ratingReview?.visibility=View.GONE
            }

            holder1.svSudioImage.setOnClickListener {
                if(onItemClick!=null)
                    onItemClick.onClicklisneter(holder1.adapterPosition,"click")
            }
            holder1.itemProductCategoryimageheart.setOnClickListener {
                if(onItemClick!=null)
                    onItemClick.onClicklisneter(holder1.adapterPosition,"AddFav")
            }

        }

    }

    class StichingStudioHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var svSudioImage = itemView.svSudioImage
        var tvProductRatingValue = itemView.tvProductRatingValue
        var tvProductRatingtotalUserCount = itemView.tvProductRatingtotalUserCount
        var tv_StitchingStudioName = itemView.tv_StitchingStudioName
        var tv_StitchingStudioAddress = itemView.tv_StitchingStudioAddress
        var itemProductCategoryimageheart = itemView.itemProductCategoryimageheart
        var ratingReview = itemView.ratingReview
        var ll_popular_off = itemView.ll_popular_off



    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int,from:String)
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 3 ).toInt()
        heightNew = 250

        return height
    }
}