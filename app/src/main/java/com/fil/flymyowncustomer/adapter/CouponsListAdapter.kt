package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.CouponListData
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_apply_coupon.view.*

class CouponsListAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val from: String?,
    var myCouponListList: ArrayList<CouponListData?>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),Filterable {
    private var mActivity = context
    var mClickListener: OnItemClick? = null
    var CouponListOriginal = myCouponListList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)
            return LoaderViewHolder(view)
        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_apply_coupon, parent, false)
            return ViewHolder(v, context)
        }
    }
    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed

    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder,position)
        } else if (holder is ViewHolder) {
            mClickListener = onItemClick
            val holder1 = holder as ViewHolder

                holder1?.tvCoupon?.text = ""+myCouponListList[position]?.offerlogVoucherCode


            if(!myCouponListList!![position]?.offerDescription.isNullOrEmpty()){
                holder1?.tvOofferDescription?.text = ""+MyUtils.fromHtml(myCouponListList[position]?.offerDescription)
            }else {
                holder1?.tvOofferDescription?.text = "--"
            }

            if(!myCouponListList!![position]?.offerDiscountValue.isNullOrEmpty() && !myCouponListList!![position]?.offerDiscountType.isNullOrEmpty()){// Get 25% discount
                if(myCouponListList!![position]?.offerDiscountType.equals("%")){
                    holder1?.tvOfferName?.text = "Get "+ MyUtils.formatPricePerCountery(java.lang.Double.valueOf(myCouponListList!![position]?.offerDiscountValue!!)) + "%" + " Discount"
                }else {
                    holder1?.tvOfferName?.text = "Get "+MyUtils.formatPricePerCountery(java.lang.Double.valueOf(myCouponListList!![position]?.offerDiscountValue!!)) + "" + " Flat"
                }
            }else {
                holder1?.tvOfferName?.text = "--"
            }

            holder1.itemView.setOnClickListener {
                if (mClickListener != null)
                {
                    mClickListener?.onClicklisneter(holder1.adapterPosition,"ApplyCode")
               }
            }
        }
    }

    override fun getItemCount(): Int {
        return myCouponListList.size
    }

   override fun getItemViewType(position: Int): Int {
       return if (myCouponListList!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    myCouponListList = CouponListOriginal
                } else {
                    val filteredList = ArrayList<CouponListData?>()
                    for (row in CouponListOriginal) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row?.offerlogVoucherCode?.toLowerCase()!!.contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    myCouponListList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = myCouponListList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                myCouponListList = filterResults.values as ArrayList<CouponListData?>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {
        var tvCoupon = itemView.tv_coupon
        var tvOfferName = itemView.tv_offer_name
        var tvOofferDescription = itemView.tv_offer_description
        var tvApply = itemView.tvApply

//        fun bind(
//            itemListData: String,
//            position: Int,
//            onitemClick: OnItemClick
//        ) =
//            with(itemView) {
//                tv_offer_name.text = itemListData
//                itemView.setOnClickListener { onitemClick?.onClicklisneter(position, "") }
//            }
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)
    }
}