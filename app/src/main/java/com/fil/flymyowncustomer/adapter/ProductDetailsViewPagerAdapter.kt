package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.fragment.ProductImageFullScreenFragment
import com.fil.flymyowncustomer.pojo.ProductListPojo
import java.io.Serializable


//class ProductImagesSliderAdapter(val context: Context,val productImageName : ArrayList<String?>) : PagerAdapter() {
class ProductDetailsViewPagerAdapter(
    val context: Context,
    val productimages: List<ProductListPojo.Data.Productimage?>?
) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return productimages!!.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_productdetails_viewpager, view, false)!!
        val imageView = imageLayout
            .findViewById(R.id.img_productImages) as SimpleDraweeView


         imageView.setImageURI (productimages?.get(position)?.prodimgName)
         imageLayout.setOnClickListener {
             var productImageFullScreenFragment= ProductImageFullScreenFragment()
             Bundle().apply {
                 putString("from","ProductDetails")
                 if( !productimages.isNullOrEmpty())
                 {
                     putSerializable("productImageData",productimages!! as Serializable)

                 }
                 productImageFullScreenFragment.arguments=this

             }
             (context as MainActivity).navigateTo(productImageFullScreenFragment, productImageFullScreenFragment::class.java.name, true)

         }
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}
