package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.os.Parcelable
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.WindowManager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Banner

//class ProductImagesSliderAdapter(val context: Context,val productImageName : ArrayList<String?>) : PagerAdapter() {
class OfferImagesSliderAdapter(
    val context: Context,
    val banner: List<Banner?>?
) : PagerAdapter() {

    private val inflater: LayoutInflater
    var heightNew = 0
    var widthNew = 0
    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return banner?.size!!
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_offers_images_slider, view, false)!!

        val imageView = imageLayout
            .findViewById(R.id.img_OfferImages) as SimpleDraweeView
//        getScrennwidth()
        imageView.setActualImageResource(R.drawable.flash_sale_promotion)
        if (!banner!![position]!!.bannerImage.isNullOrEmpty()){

            imageView.setImageURI(banner!![position]!!.bannerImage)
        }else{
            imageView.setActualImageResource(R.drawable.placehloderbanner__img)
        }
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = width + (width/2)
        heightNew = height/5

        return height
    }
}
