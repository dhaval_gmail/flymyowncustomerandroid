package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.pojo.ProductListPojo.Data
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_recent_viewed_layout.view.*
//val arrayFaqDataList: ArrayList<FaqListPojo.Datum?>,
class RecentViewedAdapter(
    val context: Context,
    val onItemClick: onItemClickk,
    val productviews: List<ProductListPojo.Data?>?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)
            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recent_viewed_layout, parent, false)
            return RecentViewedHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return productviews!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (productviews!!.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is RecentViewedHolder) {
            mClickListener = onItemClick
            val holder1 = holder as RecentViewedHolder

            if(!productviews!![position]!!.productimages.isNullOrEmpty() && !productviews!![position]!!.productimages!![0]!!.prodimgName.isNullOrEmpty()){

                holder1?.svProductImage?.setImageURI(Uri.parse(RestClient.imageUrlpath+productviews!![position]!!.productimages!![0]!!.prodimgName+"&w=100&h=100&zc=0"),context)
         
            }else {
                holder1?.svProductImage?.setActualImageResource(R.drawable.placeholder)

            }
            holder1?.tvProductName?.text=(productviews!![position]!!.productName)
//            holder1.tv_ProfuctPrice.text=mActivity.resources.getString(R.string.rs)+productviews!![position]!!.productprice!![0]!!.productpricePrice

            if(!productviews!![position]!!.productprice.isNullOrEmpty() && !productviews!![position]!!.productprice!![0]!!.productpricePrice.isNullOrEmpty()){

                holder1?.tv_ProfuctPrice?.text=mActivity.resources.getString(R.string.rs)+ MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productviews!![position]!!.productprice!![0]!!.productpricePrice.toString())).toString()
            }else {
                holder1?.tv_ProfuctPrice?.text=mActivity.resources.getString(R.string.rs)+ "0"
            }

            holder1?.itemView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }

        }

    }

    class RecentViewedHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var svProductImage = itemView.svProductImage
        var tvProductName = itemView.tvProductName
        var tv_ProfuctPrice = itemView.tv_ProfuctPrice
    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int)
    }
}