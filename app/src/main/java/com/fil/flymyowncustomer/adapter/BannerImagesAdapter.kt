package com.fil.flymyowncustomer.adapter

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.BannerImage
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_banner_images_slider.view.*

class BannerImagesAdapter(
    val context: Context, val banner: List<BannerImage?>?,val onItemClick: onItemClickk
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: onItemClickk? = null
    var mActivity = context
    var heightNew = 0
    var widthNew = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_banner_images_slider, parent, false)
            return TransactionHistoryHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return banner!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (banner?.get(position) == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is TransactionHistoryHolder) {
            getScrennwidth()
            mClickListener = onItemClick
            val holder1 = holder as TransactionHistoryHolder

            if (!banner!![position]?.dealerimageName.isNullOrEmpty()){
                holder1.imageView?.setImageURI(Uri.parse(RestClient.imageUrlpath+banner!![position]?.dealerimageName+"&w=$widthNew&h=$heightNew&zc=0"),context)
//            imageView.setImageURI(banner!![position]!!.bannerImage)
            }else{
                holder1.imageView?.setActualImageResource(R.drawable.placehloderbanner__img)
            }

            holder1.linearLayout.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }

            holder1.imageView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }

            holder1.linearLayout?.layoutParams = LinearLayout.LayoutParams((widthNew+180), LinearLayout.LayoutParams.MATCH_PARENT)
//        imageView?.layoutParams = LinearLayout.LayoutParams(
//            widthNew+10, LinearLayout.LayoutParams.MATCH_PARENT)
            holder1.itemView.setOnClickListener {
                if (onItemClick != null)
                    onItemClick?.onClicklisneter(holder1.adapterPosition)
            }

        }

    }

    class TransactionHistoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imageView = itemView.img_BannerImages
        val linearLayout = itemView.linearLayout

    }

    interface onItemClickk {
        fun onClicklisneter(pos: Int)
    }

    private fun getScrennwidth(): Int {

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width/2)
        heightNew = height/5

        return height
    }
}