package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.Orderdetail
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_returned_replaced_layout.view.*
import kotlinx.android.synthetic.main.item_returned_replaced_layout.view.item_order_detail_list_excepted_delivery_type_value
import kotlinx.android.synthetic.main.item_returned_replaced_layout.view.llItemOrderDetailListStatus
import kotlinx.android.synthetic.main.item_returned_replaced_layout.view.tvitemOrderDetailListStatusValue
import kotlinx.android.synthetic.main.item_sub_order_list.view.*

class ReturnReplaceOrderListingAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val orderdetails: ArrayList<Orderdetail>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSelection = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_returned_replaced_layout, parent, false)
            return ViewHolder(v, context)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {

        } else if (holder is ViewHolder) {
            val holder1 = holder as ViewHolder
            holder1.bind(holder.adapterPosition, onItemClick,orderdetails[position])


        }
    }

    override fun getItemCount(): Int {
        return orderdetails?.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (orderdetails[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    class ViewHolder(itemView: View, context: Activity) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            position: Int,
            onitemClick: OnItemClick,
            orderdetail: Orderdetail
        ) =
            with(itemView) {
                var deliveryType=""
                if(!orderdetail.productImage[0].prodimgName.isNullOrEmpty()){

                    svProductImage.setImageURI(RestClient.imageUrlpath+orderdetail.productImage[0].prodimgName+"&w=250&h=250&zc=0",context)
                }else {

                    svProductImage.setActualImageResource(R.drawable.placeholder)
                }

                tvProductName.text=orderdetail.productName

                if(!orderdetail.statusName.isNullOrEmpty()){
                    llItemOrderDetailListStatus.visibility = View.VISIBLE
                    tvitemOrderDetailListStatusValue.text = ""+orderdetail.statusName
                }else {
                    llItemOrderDetailListStatus.visibility = View.GONE
                    tvitemOrderDetailListStatusValue.text = ""
                }

                tv_ProductPrice.text =
                    resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                        java.lang.Double.valueOf(orderdetail.orderdetailsPrice)
                    )
                tvCategoryProductType.text=orderdetail.productType
                if (orderdetail?.orderdetailsDeliveryType.equals("Home Delivery", true) || orderdetail?.orderdetailsDeliveryType.equals("Home Service", true)){
                    deliveryType= "Home Delivery"
                }else{
                    deliveryType= "Self Pickup"
                }
                item_order_detail_list_excepted_delivery_type_value.text=deliveryType

                tv_ProductQty.text=   MyUtils.formatPricePerCountery(java.lang.Double.valueOf(orderdetail.orderdetailsQty)) +
                        " X Qty"
                tvProductReturnReplaceInfotext.text=orderdetail.reasonInfo

            }

    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String,v:View)

    }

}