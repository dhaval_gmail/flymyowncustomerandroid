package com.fil.flymyowncustomer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.ProductReviewListData
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_review_adapter.view.*

class ReviewListAdapter(
    val context: Activity,
    val onItemClick: OnItemClick,
    val productReviewList: ArrayList<ProductReviewListData?>?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)
            return LoaderViewHolder(view)
        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_review_adapter, parent, false)
            return ReviewViewHolder(v)
        }
    }
    private fun showLoadingView(viewHolder: LoaderViewHolder, position: Int) {
        //ProgressBar would be displayed

    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            showLoadingView(holder, position)
        } else if (holder is ReviewViewHolder) {

            val holder1 = holder as ReviewViewHolder

            holder1.bind(productReviewList?.get(position),holder1.adapterPosition,onItemClick)




        }
    }

    override fun getItemCount(): Int {
        return productReviewList?.size!!
    }

    override fun getItemViewType(position: Int): Int {
        return if (productReviewList!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }

    class ReviewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemUserFriendsImage = itemView.reviewIconImageview
        var tvReviewTitle = itemView.tvReviewTitle
        var tvreviewDes = itemView.tvreviewDes
        var ratingBarYourRating = itemView.ratingBarYourRating

       fun bind(
           itemListData: ProductReviewListData?,
           position: Int,
           onitemClick: OnItemClick
        ) =
            with(itemView) {
                reviewIconImageview.setImageURI(itemListData?.userProfilePicture)
                tvreviewDes.text=itemListData?.reviewReview
                ratingBarYourRating.rating=itemListData?.reviewRatting?.toFloat()!!
                tvReviewTitle.text=itemListData?.userFullName!!

                itemView.setOnClickListener { onitemClick?.onClicklisneter(position, "") }
            }
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, buttonName: String)
    }
}