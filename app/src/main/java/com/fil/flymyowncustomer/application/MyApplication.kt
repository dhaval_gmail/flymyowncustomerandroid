package com.fil.flymyowncustomer.application

import android.app.Application

import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.facebook.drawee.backends.pipeline.Fresco
import com.fil.flymyowncustomer.util.AppSignatureHelper


// testlast3232@gmail.com/fipl3178 Google account

class MyApplication : Application() {

    companion object {
        lateinit var instance: MyApplication
                        private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        MultiDex.install(this)
        Fresco.initialize(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        var appSignature = AppSignatureHelper(this)
        appSignature.appSignatures
    }
}