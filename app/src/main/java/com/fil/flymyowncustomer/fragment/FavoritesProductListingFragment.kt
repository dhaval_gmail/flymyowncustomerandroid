package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ProductListingAdapter
import com.fil.flymyowncustomer.model.FavuriteProductListViewModel
import com.fil.flymyowncustomer.model.ProductListModel
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.pojo.ProductListPojo.Data
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class FavoritesProductListingFragment : Fragment() {


    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = ProductListingFragment::class.java.name

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var sessionManager: SessionManager? = null

    var mProductListingAdapter: ProductListingAdapter? = null

    var productListData: ArrayList<ProductListPojo.Data?>? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_favorites_product_listing, container, false)
        }
        return v

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = false

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(mActivity!!)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.account_favorites)
        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
//        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.back_arrow)
        toolbar_back.menuNotification.visibility = View.GONE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        (mActivity as MainActivity).setDrawerSwipe(false)

        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        linearLayoutManager = GridLayoutManager(mActivity, 2)
        notificationRecyclerview?.layoutManager = linearLayoutManager

        bindData()

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.childCount
                totalItemCount = linearLayoutManager.itemCount
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getProductListApi()
                    }
                }
            }
        })

        btnRetry?.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
            getProductListApi()
        }


    }

    fun getProductListApi() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            productListData!!.clear()
            mProductListingAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            productListData!!.add(null)
            mProductListingAdapter?.notifyItemInserted(productListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var productListModel =
            ViewModelProviders.of(this@FavoritesProductListingFragment)
                .get(ProductListModel::class.java)
        productListModel.getProductListList(mActivity!!, false, jsonArray.toString(),"favList")
            .observe(this@FavoritesProductListingFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        notificationRecyclerview.visibility = (View.VISIBLE)

                        if (pageNo > 0) {
                            productListData!!.removeAt(productListData!!.size - 1)
                            mProductListingAdapter?.notifyItemRemoved(productListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                productListData!!.clear()

                            productListData!!.addAll(masterPojo[0].data!!)
                            mProductListingAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (productListData.isNullOrEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE

                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })


    }


    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun bindData() {

        if (productListData == null) {
            productListData = ArrayList()
            mProductListingAdapter = ProductListingAdapter(
              mActivity!!,
                object : ProductListingAdapter.onItemClickk {
                    override fun onClicklisneter(pos: Int, from: String) {
                        when (from) {
                            "click" -> {
                                var productDetailsFragment = ProductDetailsFragment()
                                Bundle().apply {
                                    this.putString("from", "Stitching Store")
                                    this.putSerializable(
                                        "productListData",
                                        productListData!![pos]!!
                                    )
                                    productDetailsFragment.arguments = this

                                }
                                (activity as MainActivity).navigateTo(
                                    productDetailsFragment,
                                    productDetailsFragment::class.java.name,
                                    true
                                )



                            }
                            "fav" -> {
                                getAddProduct(pos)
                            }

                        }
                    }

                },
                productListData!!
            )

            notificationRecyclerview?.isNestedScrollingEnabled = false
            notificationRecyclerview?.setHasFixedSize(true)
            notificationRecyclerview?.adapter = mProductListingAdapter

            getProductListApi()
        }
    }

    private fun getAddProduct(pos: Int) {

        if (productListData!![pos]!!.isYourFavourite.equals("No", false)) {
            productListData!![pos]!!.isYourFavourite = "Yes"
            setAddFavourite(productListData!![pos]!!.productID, pos, "AddFav")
        } else {
            productListData!![pos]!!.isYourFavourite = "No"
            setAddFavourite(productListData!![pos]!!.productID, pos, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, pos: Int, type: String) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@FavoritesProductListingFragment).get(
            FavuriteProductListViewModel::class.java)
        addFavDoctorModel.apiFunction(activity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@FavoritesProductListingFragment,
                Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("add", false)) {
                                productListData!![pos]!!.isYourFavourite = "Yes"
                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                productListData!![pos]!!.isYourFavourite = "No"
                                productListData!!.removeAt(pos)
                                mProductListingAdapter !!.notifyItemRemoved(pos)
                                mProductListingAdapter!!.notifyItemChanged(pos, productListData!!.size)
                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))
                            }
                            mProductListingAdapter!!.notifyDataSetChanged()
                        }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

}
