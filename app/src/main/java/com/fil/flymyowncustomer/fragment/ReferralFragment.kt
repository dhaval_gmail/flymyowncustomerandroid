package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.RegisterAddAddressActivity
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.RegisterPojo
import com.fil.flymyowncustomer.util.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_billing_address.*
import kotlinx.android.synthetic.main.fragment_referral.*
import kotlinx.android.synthetic.main.toolbar_back.*





class ReferralFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = ReferralFragment::class.java.name
    lateinit var sessionManager: SessionManager
    var userdata: RegisterNewPojo.Datum? = null
    var userid : String?= ""
    var moneyEarnedByCustomerThroughReferAndEarn : String? = "0"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_referral, container, false)
        }

        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        (activity as MainActivity).updateNavigationBarState(R.id.menu_ReferAndEarn)
        (activity as MainActivity).setDrawerSwipe(false)

        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        tvToolbarTitel.text = getString(R.string.refer_and_earn)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        try {
            sessionManager = SessionManager(mActivity!!)
            if(sessionManager.isLoggedIn()){

                userdata = sessionManager.get_Authenticate_User()
                if(userdata != null){
                    userid = userdata?.userID
                    tvCodeText?.text = ""+userdata?.userReferCode
                    moneyEarnedByCustomerThroughReferAndEarn = "" + userdata?.settings!![0]?.settingsMoneyEarnedByCustomerThroughReferAndEarn
                }else {
                    userid = "0"
                    tvCodeText?.text = ""
                    moneyEarnedByCustomerThroughReferAndEarn = "0"
                }
            }else {
                userid = "0"
                tvCodeText?.text = ""
                moneyEarnedByCustomerThroughReferAndEarn = "0"
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        btnInviteAFriendsShare?.setOnClickListener {
            val appPackageName = (activity as MainActivity).packageName // getPackageName() from Context or Activity object
            val applicationName = mActivity!!.resources.getString(R.string.app_name)
//            Join me on flyMyOwn. Enter my code "COde" to earn xx points back on your first order
            val shareUrl = "Join me on "+ applicationName +". Enter my code "+tvCodeText?.text.toString() + " to earn "+moneyEarnedByCustomerThroughReferAndEarn +" points back on your first order. Refer this link to get app "+" \n https://play.google.com/store/apps/details?id=$appPackageName"
            Log.e("System out", "Print values := " + shareUrl.toString())
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareUrl)
            shareIntent.type = "text/plain"
            //shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            (mActivity as MainActivity).startActivity(Intent.createChooser(shareIntent, "Share Referral Code..."))
        }

    }


}
