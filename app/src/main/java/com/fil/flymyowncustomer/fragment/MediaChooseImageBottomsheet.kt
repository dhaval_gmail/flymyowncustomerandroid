package com.fil.flymyowncustomer.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R
import kotlinx.android.synthetic.main.mediachoose_images_bottomsheet.*

import kotlinx.android.synthetic.main.mediachoose_images_bottomsheet.view.*
import java.io.File
import java.util.ArrayList

class MediaChooseImageBottomsheet() : DialogFragment(){
    private var file = File("")
    private var timeForImageName: Long = 0
    private var imgName = ""
    private var mIntent = Intent()
    private var pictureUri = Uri.fromFile(file)

    private val TAG = MediaChooseImageBottomsheet::class.java.name
    private var mActivity = AppCompatActivity()
    var v:View?= null
//    var OnSelectionItemClickListener : OnItemClickListener?=null;

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.mediachoose_images_bottomsheet, null)
        dialog.setContentView(contentView)
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false)
        dialog.llCameraLayout.setOnClickListener {

            startCameraActivity()

        }

        dialog.tvCancel.setOnClickListener {
            if (dialog != null) {
                dismiss()
            }
        }

        dialog.llGalleryLayout.setOnClickListener {

            openGallery()

        }
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    /*fun setListener(listener: OnItemClickListener) {
        this.OnSelectionItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onSellerSelectionClick(position: Int,typeofOperation: String)
    }
*/
    private fun openGallery() {
        mIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        mIntent.type = "image/*"
        activity!!.startActivityForResult(Intent.createChooser(mIntent, "Select Picture"),
            MediaChooseImageBottomsheet.SELECT_PICTURE
        )
        if (dialog != null){
            dismiss()
        }
    }

    private fun startCameraActivity() {
        mIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        timeForImageName = System.currentTimeMillis()
        imgName = "img$timeForImageName.jpeg"
        file = File(activity!!.getExternalFilesDir(null), imgName)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            mIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file))
        } else {
            mIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity!!, "com.fil.flymyowncustomer.provider", file))
        }

        pictureUri = Uri.fromFile(file)
        activity!!.startActivityForResult(mIntent, TAKE_PICTURE)
        if(dialog != null){
            dismiss()
        }
    }

    fun selectedImage(): Uri? {
        return pictureUri
    }

    companion object {
        private const val REQUEST_CODE_CAMERA_PERMISSIONS = 11
        private const val REQUEST_CODE_READ_PERMISSIONS = 12
        private const val REQUEST_CODE_WRITE_PERMISSIONS = 13
        private const val TAKE_PICTURE = 1
        private const val SELECT_PICTURE = 2
    }
}