package com.fil.flymyowncustomer.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.LoginActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.UserChangePasswordModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo

import com.fil.flymyowncustomer.pojo.getChangePassword
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager

import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_changepassword.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject

// https://github.com/vikramkakkar/SublimeNavigationView
class ChangePasswordFragment : Fragment(), View.OnClickListener {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    lateinit var sessionManager: SessionManager
    var userdata: RegisterNewPojo.Datum? = null
    var userid : String?= ""
    val TAG = ChangePasswordFragment::class.java.name

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_changepassword, container, false)
        }
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*(mActivity as MainActivity).toolbar_title!!.visibility = View.VISIBLE
        (mActivity as MainActivity).toolbar_Imageview?.visibility = View.GONE*/
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel!!.visibility=View.VISIBLE
        tvToolbarTitel.text=mActivity!!.resources.getString(R.string.changepassword)
        menuNotification.visibility=View.GONE

        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE
        (mActivity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        /*(mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.changepassword))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.back_arrow)
        (mActivity as MainActivity).bottom_navigation!!.setVisibility(View.GONE)
        (mActivity as MainActivity).setDrawerSwipe(false)*/

        sessionManager = SessionManager(mActivity!!)
        if(sessionManager.isLoggedIn()){

            userdata = sessionManager.get_Authenticate_User()
            if(userdata != null){
                userid = userdata!!.userID
            }else {
                userid = "0"
            }
        }else {
            userid = "0"
        }


        btnChangePasswordSubmit?.setOnClickListener {
            MyUtils.hideKeyboardFrom(mActivity!!, btnChangePasswordSubmit)
            if (checkValidation()) {
                if (MyUtils.isInternetAvailable(mActivity!!)) {
                    getUserChangePwdAPI()
                }else {
                    MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                }

            }
        }

    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btnChangePasswordSubmit -> {
                MyUtils.hideKeyboardFrom(mActivity!!, btnChangePasswordSubmit)
                if (checkValidation()) {
                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        getUserChangePwdAPI()
                    }else {
                        MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }

                }
            }
        }

    }

    private fun checkValidation(): Boolean {
        var checkFlag = true

        if (changePasswordEditTextOldPassword?.text.toString().isEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.textfield_ch_old_eror_pwd)
            )
        } else if (changePasswordEditTextOldPassword?.text.toString().length < 8) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.shr_error_password)
            )
        }else if(MyUtils.isValidPassword(changePasswordEditTextOldPassword?.text.toString().trim()) == false){
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                mActivity!!.resources.getString(R.string.validerror_password)
            )
            checkFlag = false
        }
        else if (changePasswordEditTexteNewPassword?.text.toString().isEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.textfield_ch_new_eror_pwd)
            )
        } else if (changePasswordEditTexteNewPassword?.text.toString().length < 8) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.shr_error_password)
            )
        }else if(MyUtils.isValidPassword(changePasswordEditTexteNewPassword?.text.toString().trim()) == false){
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                mActivity!!.resources.getString(R.string.validerror_password)
            )
            checkFlag = false
        }
        else if (changePasswordEditTexteReEnterPassword!!.text.toString().isEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.textfield_ch_confirm_eror_pwd)
            )
        } else if (changePasswordEditTexteReEnterPassword!!.text.toString().length < 8) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.shr_error_password)
            )
        }else if(MyUtils.isValidPassword(changePasswordEditTexteNewPassword?.text.toString().trim()) == false){
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                mActivity!!.resources.getString(R.string.validerror_password)
            )
            checkFlag = false
        }
        else if (!changePasswordEditTexteNewPassword?.text.toString().equals(changePasswordEditTexteReEnterPassword?.text.toString())) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootChangePasswordLayout,
                this@ChangePasswordFragment.resources.getString(R.string.shr_error_password_not_match)
            )
        }

        return checkFlag
    }

    private fun getUserChangePwdAPI() {
        btnChangePasswordSubmit.startAnimation()
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            // users/change-password
            /*[{
                "languageID": "1",
                "loginuserID": "1",
                "userCurrentPassword": "ankit@12345",
                "userNewPassword": "ankit@1234",
                "apiType": "Android",
                "apiVersion": "1.0"
            }]*/
            jsonObject.put("loginuserID", userid)
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("userCurrentPassword", changePasswordEditTextOldPassword.text.toString().trim())
            jsonObject.put("userNewPassword", changePasswordEditTexteNewPassword.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
//            Log.d(TAG, "ChangePassword api call := " + jsonArray.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var getUserChangePwdModel = ViewModelProviders.of(this@ChangePasswordFragment).get(UserChangePasswordModel::class.java)
        getUserChangePwdModel.getUserUpdate(mActivity!!, false, jsonArray.toString())
            .observe(this@ChangePasswordFragment, Observer<List<getChangePassword>> { changePwdPojo ->

                    if (changePwdPojo != null) {
                        btnChangePasswordSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootChangePasswordLayout, true)
                        if (changePwdPojo[0].status.equals("true", true)) {
                            if(!changePwdPojo[0].message!!.isNullOrEmpty()){

                                MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, changePwdPojo[0].message!!)
                            }
                            Handler().postDelayed({
                                sessionManager.clear_login_session()
                                val myIntent = Intent(mActivity!!, LoginActivity::class.java)
                                mActivity!!.startActivity(myIntent)
                                mActivity!!.finishAffinity()
                            }, 2000)
                        } else {
                            btnChangePasswordSubmit.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(rootChangePasswordLayout, true)
                            MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, changePwdPojo[0].message!!)
                        }
                    } else {
                        btnChangePasswordSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootChangePasswordLayout, true)
                        errorMethod()
                    }
            })

    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(mActivity!!)) {
                MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))

            } else {
                MyUtils.showSnackbarkotlin(mActivity!!, rootChangePasswordLayout, mActivity!!.resources.getString(R.string.error_crash_error_message))

            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

    }
}