package com.fil.flymyowncustomer.fragment

import android.content.ActivityNotFoundException
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.OrderSublistAdapter
import com.fil.flymyowncustomer.adapter.StitchingServiceSublistAdapter
import com.fil.flymyowncustomer.model.CancelOrderModel
import com.fil.flymyowncustomer.model.OrderDetailsPojoModel
import com.fil.flymyowncustomer.model.ReturnReplaceRequestModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.notificationRecyclerview
import kotlinx.android.synthetic.main.fragment_order_details.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 */
class OrderDetailsFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var orderListSublistAdapter: OrderSublistAdapter? = null
    var stitChingServiceAdapter: StitchingServiceSublistAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var mSelection: Int = -1
    var type: String = ""
    private val REQ_CODE_DRAW_OVER = 1004
    var navigationLatLong: LatLng? = null
    var sessionManager: SessionManager? = null
    var orderData: MyOrderData? = null
    var list: ArrayList<String>? = null
    var orderdetails: ArrayList<Orderdetail>? = null
    var orderDetailsData: ArrayList<OrderDetailsData>? = null
    var mOrderDetailsData: OrderDetailsData? = null
    var stitching: ArrayList<Stitching>? = null
    var widthNew = 0
    var heightNew = 0
    var isConfirm = false
    var isCancelled = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_order_details, container, false)

        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        sessionManager = SessionManager(mActivity!!)
        if (arguments != null) {
            orderData = arguments!!.getSerializable("orderData") as MyOrderData?
            type = arguments!!.getString("type")!!
        }


        getScrennwidth()
        if (orderData != null) {
            getOrderDetails(orderData?.orderID)
        }

        menuShare.visibility = View.VISIBLE
        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
        (activity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        menuShare.setOnClickListener {
            val appPackageName = (activity as MainActivity).packageName // getPackageName() from Context or Activity object
            try {
//                val share = Intent.createChooser(Intent().apply {
//                    action = Intent.ACTION_SEND
//                    putExtra(Intent.EXTRA_TEXT, ""+"market://details?id=$appPackageName")
//                }, null)
//                (activity as MainActivity).startActivity(Intent.createChooser(share,"Share using"))

                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, ""+"market://details?id=$appPackageName")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Share using")
                (activity as MainActivity).startActivity(shareIntent)

            } catch (anfe: ActivityNotFoundException) {
                (activity as MainActivity).startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }

        img_call_store?.setOnClickListener {

            if (mOrderDetailsData != null) {

                if (!mOrderDetailsData?.dealerinfo.isNullOrEmpty() && !mOrderDetailsData?.dealerinfo!![0].dealerMobile.isNullOrEmpty()) {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data =
                        Uri.parse("tel:" + mOrderDetailsData?.dealerinfo!![0].dealerCountryCode + " " + mOrderDetailsData?.dealerinfo!![0].dealerMobile)
                    startActivity(intent)
                } else {
                    MyUtils.showSnackbarkotlin(
                        (activity as MainActivity),
                        rootOrderDetailsMainLayout!!,
                        mActivity!!.resources.getString(R.string.err_no_cus_mobile)
                    )
                }
            }

        }

        when (type) {
            "New" -> {
                btnCancelOrder.progressText = "Cancel Order"
            }
            "Completed" -> {
                btnCancelOrder.visibility = View.GONE
//                btnCancelOrder.progressText = "Rate"
            }
            "Cancelled" -> {
                btnCancelOrder.visibility = View.GONE
            }
        }

        btnCancelOrder.setOnClickListener {
            when (type) {
                "New" -> {
                    if (orderData != null) {
                        openDialog(orderData?.orderID!!, "", "cancel", -1)
                    }
                }
                "Completed" -> {

                }
                "Cancelled" -> {

                }
            }


        }

        //Google Map display App Icon
        LocalBroadcastManager.getInstance(mActivity!!)
            .registerReceiver(receiver, IntentFilter(FloatingOverMapIconService.BROADCAST_ACTION))

        tv_getDirection.setOnClickListener {
            navigationPermission()
        }

        btnRetry.setOnClickListener {
            if (orderData != null) {
                getOrderDetails(orderData?.orderID)

            }
        }

    }

    private fun getScrennwidth(): Int {

        val wm = mActivity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 3).toInt()
        heightNew = (height / 4).toInt()

        return height
    }

    private fun getOrderDetails(orderID: String?) {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        relativeprogressBar!!.visibility = View.VISIBLE
        ll_main.visibility = View.GONE

        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("orderID", orderID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var orderDetailsModel =
            ViewModelProviders.of(this@OrderDetailsFragment).get(OrderDetailsPojoModel::class.java)
        orderDetailsModel.getOrderDetails(mActivity!!, false, jsonArray.toString(), "cancel")
            .observe(this@OrderDetailsFragment,
                Observer<List<OrderDetailsPojo>> { orderData ->
                    run {
                        if (orderData != null && orderData.isNotEmpty()) {
                            if (orderData[0].status.equals("true", true)) {
                                ll_main.visibility = View.VISIBLE
                                noDatafoundRelativelayout.visibility = View.GONE
                                nointernetMainRelativelayout.visibility = View.GONE
                                relativeprogressBar!!.visibility = View.GONE
                                orderDetailsData = ArrayList()
                                orderDetailsData?.clear()
                                orderDetailsData?.addAll(orderData[0].data)
                                orderdetails = ArrayList()
                                orderdetails?.clear()
                                orderdetails?.addAll(orderData[0].data[0].orderdetails)
                                stitching = ArrayList()
                                stitching?.clear()
                                stitching?.addAll(orderData[0].data[0].stitching)
                                mOrderDetailsData = orderData[0].data[0]
                                setData(orderData[0].data[0], orderID)

                            } else {
                                relativeprogressBar!!.visibility = View.GONE

                                ll_main.visibility = View.GONE
                                noDatafoundRelativelayout.visibility = View.VISIBLE

                            }

                        } else {
                            ll_main.visibility = View.GONE

                            nodatafound()
                        }

                    }
                })


    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setData(orderData: OrderDetailsData, orderID: String?) {
        if (orderData != null) {
            tvToolbarTitel.text = "Order Id:${this.orderData?.orderNo}"
            if (!orderData.dealerinfo[0].bannerImages.isNullOrEmpty()) {
                svProductImageMycart.setImageURI(
                    RestClient.imageUrlpath + orderData.dealerinfo[0].bannerImages[0].dealerimageName + "&w=$widthNew&h=$heightNew&zc=0",
                    mActivity!!
                )

            }

            tv_ProductStoreName.text = orderData.dealerinfo[0].dealerCompanyName
            tv_ProductStoreAddress.text =
                orderData.dealerinfo[0].addressAddressLine1 + " " + orderData.dealerinfo[0].addressLandmark + " " + orderData.dealerinfo[0].addressLocality + " " + orderData.dealerinfo[0].addressAddressLine2 + " " + orderData.dealerinfo[0].stateName + " " + orderData.dealerinfo[0].addressPincode
//            tvNetAmountValue.text =resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(java.lang.Double.valueOf(orderData.orderNetAmount))

            /*if(!orderData.orderWalletRechargeAmount.isNullOrEmpty() && java.lang.Double.valueOf(orderData.orderWalletRechargeAmount) > 0){
                tvNetAmountValue.text =
                    resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                        java.lang.Double.valueOf(orderData.orderNetAmount))
            }else {*/
                tvNetAmountValue.text =
                    resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                        java.lang.Double.valueOf(orderData.orderGrossAmt)+java.lang.Double.valueOf(orderData.orderIGST))
//            }

            if(!orderData.orderDiscount.isNullOrEmpty())
            {
                if(orderData.orderDiscount.toDouble()==0.0)
                {
                    llCouponDiscountLayout?.visibility = View.GONE

                }else
                {
                    llCouponDiscountLayout?.visibility = View.VISIBLE

                    tvCouponDiscountValue.text ="- " +
                        resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                            java.lang.Double.valueOf(orderData.orderDiscount)
                        )
                }
            }

            if(!orderData.orderWalletRechargeAmount.isNullOrEmpty() && java.lang.Double.valueOf(orderData.orderWalletRechargeAmount) == 0.0){
                tvFlyMyOwnMoneyValue.text = resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                    java.lang.Double.valueOf(orderData.orderWalletRechargeAmount))

            }else {
                tvFlyMyOwnMoneyValue.text = "- "+ resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                    java.lang.Double.valueOf(orderData.orderWalletRechargeAmount))

            }


//            tvFlyMyOwnMoneyValue.text =
//                resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
//                    java.lang.Double.valueOf(orderData.orderWalletFlyMyOwnAmount)
//                )

            tvDeliveryChargesValue.text =
                resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                    java.lang.Double.valueOf(orderData.orderDeliveryCharges)
                )

            tvTotalAmountValue.text =
                resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                    java.lang.Double.valueOf(orderData.orderNetAmount)
                )
            tvIGSTAmountValue.text =
                resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                    java.lang.Double.valueOf(orderData.orderIGST)
                )

            linearLayoutManager = LinearLayoutManager(mActivity)

            when (orderData.orderType) {
                "Stitching" ->{

                    if(!stitching.isNullOrEmpty()){
                        for (i in 0 until stitching!!.size!!) {
                            if(stitching!![i]?.statusNameDisplay.equals("Stitching") || stitching!![i]?.statusNameDisplay.equals("Cancelled")){
                                isConfirm = true
                                break
                            }
                        }

                        if(isConfirm == true){

                            btnCancelOrder.visibility = View.GONE
                        }
                    }


                }
                "Buy Fabric" -> {

                    if(!orderdetails.isNullOrEmpty()){
                        for (i in 0 until orderdetails!!.size!!) {
                            if(orderdetails!![i]?.statusName.equals("Confirmed") || orderdetails!![i]?.statusName.equals("Cancelled") ){
                                isConfirm = true
                                break
                            }
                        }

                        if(isConfirm == true){

                            btnCancelOrder.visibility = View.GONE
                        }
                    }


                }
            }

            when (orderData.orderType) {
                "Stitching" -> {
                    stitChingServiceAdapter = StitchingServiceSublistAdapter(
                        mActivity!!,
                        object : StitchingServiceSublistAdapter.OnItemClick {
                            override fun onClicklisneter(pos: Int, name: String, v: View) {
                                when (name) {
                                    "Option" -> {

                                        list = java.util.ArrayList()
                                        list!!.clear()
                                        if(type.equals("New",false))
                                        {
                                            list!!.add("Cancel")

                                        }else if (type.equals("Completed",false))
                                        {
                                            list!!.add("Rate")
                                        }
                                        PopupMenu(activity!!, v, list!!).showPopUp(object :
                                            PopupMenu.OnMenuSelectItemClickListener {
                                            override fun onItemClick(item: String) {
                                                if(item.equals("Cancel",false)) {
                                                    if (pos == 0) {
                                                        openDialog(
                                                            orderData?.orderID!!,
                                                            orderData.stitching!![pos]!!.orderdetailsstitchingID,
                                                            "cancel",
                                                            -1
                                                        )

                                                    } else {
                                                        openDialog(
                                                            orderID!!,
                                                            orderData.stitching!![pos]!!.orderdetailsstitchingID,
                                                            "cancelSingle",
                                                            pos
                                                        )

                                                    }
                                                }
                                                else  if(item.equals("Rate",false)){
                                                    var writeReviewFragment=WrireReviewFragment()
                                                    var bundle=Bundle()
                                                    bundle.putString("from","stitching")
                                                    bundle.putString("orderId",orderData.orderID)
                                                    bundle.putString("orderNo",orderData?.orderNo)
                                                    bundle.putSerializable("orderData",stitching?.get(pos))
                                                    writeReviewFragment.arguments=bundle
                                                    (activity as MainActivity).navigateTo(writeReviewFragment,writeReviewFragment::class.java.name,true)
                                                }

                                            }

                                        })
                                    }
                                    "Return" -> {

                                        openReplaceDialog(
                                            orderData?.orderID!!,
                                            orderData.stitching!![pos]!!.orderdetailsstitchingID,
                                            name,
                                            pos,
                                            orderData.stitching!![pos]!!.orderdetailsstitchingQty
                                        )
                                    }
                                    "Replace" -> {

                                        openReplaceDialog(
                                            orderData?.orderID!!,
                                            orderData.stitching!![pos]!!.orderdetailsstitchingID,
                                            name,
                                            pos,
                                            orderData.stitching!![pos]!!.orderdetailsstitchingQty
                                        )
                                    }

//                                    "return" -> {
//
//                                        openReplaceDialog(
//                                            orderData?.orderID!!,
//                                            orderData.stitching!![pos]!!.orderdetailsstitchingID,
//                                            name,
//                                            pos)

                                    /*val SortingDialogFragment = PopUpAddStock()
                                    val bundle = Bundle()
                                    bundle.putInt("mSelection", mSelection)

                                    SortingDialogFragment.arguments = bundle
                                    SortingDialogFragment.isCancelable = false
                                    SortingDialogFragment.show(childFragmentManager, "dialog")
                                    SortingDialogFragment.setListener(object :
                                        PopUpAddStock.OnItemClickListener {
                                        override fun onItemSelectionClick(
                                            typeButton: String,
                                            flag: Int,
                                            qty: String
                                        ) {

                                        }

                                        *//*override fun onItemSelectionClick(
                                                typeButton: String,
                                                position: Int
                                            ) {
                                                mSelection = position
                                            }*//*

                                        })*/
//                                    }
                                }

                            }
                        }
                        , stitching!!, type)

                    notificationRecyclerview.layoutManager = linearLayoutManager
                    notificationRecyclerview.adapter = stitChingServiceAdapter
                }
                "Buy Fabric" -> {
                    orderListSublistAdapter = OrderSublistAdapter(
                        mActivity!!,
                        object : OrderSublistAdapter.OnItemClick {
                            override fun onClicklisneter(pos: Int, name: String, v: View) {
                                when (name) {
                                    "Option" -> {

                                        list = java.util.ArrayList()
                                        list!!.clear()
                                        if(type.equals("New",false))
                                        {
                                            list!!.add("Cancel")

                                        }else if (type.equals("Completed",false))
                                        {
                                            list!!.add("Rate")
                                        }
                                        PopupMenu(activity!!, v, list!!).showPopUp(object :
                                            PopupMenu.OnMenuSelectItemClickListener {
                                            override fun onItemClick(item: String) {
                                                if(item.equals("Cancel",false)) {


                                                if (pos == 0) {
                                                    openDialog(
                                                        orderData?.orderID!!,
                                                        orderData.orderdetails!![pos]!!.orderdetailsID,
                                                        "cancel",
                                                        -1
                                                    )

                                                } else {
                                                    openDialog(
                                                        orderID!!,
                                                        orderData.orderdetails!![pos]!!.orderdetailsID,
                                                        "cancelSingle",
                                                        pos
                                                    )

                                                 }
                                                }
                                                else  if(item.equals("Rate",false)) {
                                                    var writeReviewFragment=WrireReviewFragment()
                                                    var bundle=Bundle()
                                                    bundle.putString("from","product")
                                                    bundle.putString("orderId",orderData.orderID)
                                                    bundle.putString("orderNo",orderData?.orderNo)

                                                    bundle.putSerializable("orderData",orderdetails?.get(pos))
                                                    writeReviewFragment.arguments=bundle
                                                    (activity as MainActivity).navigateTo(writeReviewFragment,writeReviewFragment::class.java.name,true)

                                                }

                                            }

                                        })
                                    }
                                    "Return" -> {

                                        openReplaceDialog(
                                            orderData?.orderID!!,
                                            orderData.orderdetails!![pos]!!.orderdetailsID,
                                            name,
                                            pos,
                                            orderData.orderdetails!![pos]!!.orderdetailsQty
                                        )
                                    }
                                    "Replace" -> {

                                        openReplaceDialog(
                                            orderData?.orderID!!,
                                            orderData.orderdetails!![pos]!!.orderdetailsID,
                                            name,
                                            pos,
                                            orderData.orderdetails!![pos]!!.orderdetailsQty
                                        )
                                    }
                                }

                            }
                        }, orderdetails!!, type
                    )

                    notificationRecyclerview.layoutManager = linearLayoutManager
                    notificationRecyclerview.adapter = orderListSublistAdapter
                }
            }


        }


    }

    fun openReplaceDialog(
        orderID: String,
        orderdetailsID: String,
        from: String,
        pos: Int,
        qty: String
    ) {

        var list: ArrayList<String>
        list = ArrayList()
        list.clear()
        if (PrefDb(mActivity!!).getMasterData() != null) {
            if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty()) {
                for (i in 0 until PrefDb(mActivity!!).getMasterData()?.reason?.size!!) {
                    list.add(PrefDb(mActivity!!).getMasterData()?.reason?.get(i)?.reasonInfo!!)
                }
            }
        }

        val SortingDialogFragment = PopUpAddStock()
        val bundle = Bundle()
        bundle.putInt("mSelection", mSelection)
        bundle.putSerializable("list", list)
        bundle.putString("from", from)
        bundle.putString("qty", qty)
        SortingDialogFragment.arguments = bundle
        SortingDialogFragment.isCancelable = false
        SortingDialogFragment.show(childFragmentManager, "dialog")
        SortingDialogFragment.setListener(object :
            PopUpAddStock.OnItemClickListener {
            override fun onItemSelectionClick(typeButton: String, flag: Int, qty: String) {

                mSelection = flag
                if(!typeButton.equals("cancel",false))
                {
                    if (PrefDb(mActivity!!).getMasterData() != null) {
                        if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty()) {
                            MyUtils.showMessageOKCancel1(mActivity!!,
                                "Are you sure you want to ${typeButton} Order?",
                                typeButton,
                                DialogInterface.OnClickListener { dialog, which ->
                                    dialog.dismiss()
                                    getReturnReplaceRequest(
                                        orderID,
                                        orderdetailsID,
                                        PrefDb(mActivity!!).getMasterData()?.reason!![flag]?.reasonID!!,
                                        typeButton,
                                        from,
                                        pos, qty
                                    )
                                })
                        }
                    }
                }

            }

            /*override fun onItemSelectionClick(
                typeButton: String,
                position: Int
            ) {
                mSelection = position
            }*/

        })
    }

    fun openDialog(
        orderID: String,
        orderdetailsID: String,
        from: String,
        pos: Int
    ) {

        var list: ArrayList<String>
        list = ArrayList()
        list.clear()
        if (PrefDb(mActivity!!).getMasterData() != null) {
            if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty()) {
                for (i in 0 until PrefDb(mActivity!!).getMasterData()?.reason?.size!!) {
                    list.add(PrefDb(mActivity!!).getMasterData()?.reason?.get(i)?.reasonInfo!!)
                }
            }
        }
        val cancelReasonFragment = CancelReasonFragment()
        val bundle = Bundle()
        bundle.putInt("mSelection", mSelection)
        bundle.putString("from", "cancel_order")
        bundle.putSerializable("list", list)
        cancelReasonFragment.arguments = bundle
        cancelReasonFragment.isCancelable = true
        cancelReasonFragment.show(childFragmentManager, "dialog")
        cancelReasonFragment.setListener(object :
            CancelReasonFragment.OnItemClickListener {
            override fun onSellerSelectionClick(position: Int, typeofOperation: String) {
                mSelection = position
                if (PrefDb(mActivity!!).getMasterData() != null) {
                    if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty()) {
                        MyUtils.showMessageOKCancel1(mActivity!!,
                            "Are you sure you want to Cancel Order?",
                            "Cancel",
                            DialogInterface.OnClickListener { dialog, which ->
                                dialog.dismiss()
                                getCancelOrderApi(
                                    orderID,
                                    orderdetailsID,
                                    PrefDb(mActivity!!).getMasterData()?.reason!![position]?.reasonID!!,
                                    typeofOperation,
                                    from,
                                    pos
                                )
                            })
                    }
                }
            }
        })
    }


    private fun navigationPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // if OS is pre-marshmallow then create the floating icon, no permission is needed
            createFloatingBackButton()
        } else {
            if (!Settings.canDrawOverlays(activity)) {
                // asking for DRAW_OVER permission in settings
                var intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + mActivity!!.applicationContext?.packageName)
                )
                mActivity!!.startActivityForResult(intent, REQ_CODE_DRAW_OVER)
                mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            } else {
                createFloatingBackButton()
            }
        }
    }

    @SuppressLint("NewApi")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == REQ_CODE_DRAW_OVER) {

            if (Settings.canDrawOverlays(activity)) {
                createFloatingBackButton()
            } else {
                Toast.makeText(activity, "Navigation permission denied", Toast.LENGTH_LONG).show()
            }
        }

    }


    private fun createFloatingBackButton() {
        var latLng: Array<String>? = null
        if (!orderDetailsData.isNullOrEmpty())
            if (!orderDetailsData!![0].dealerinfo.isNullOrEmpty()) {
                if (!orderDetailsData!![0].dealerinfo[0].addressLatitude.isNullOrEmpty() && !orderDetailsData!![0].dealerinfo[0].addressLongitude.isNullOrEmpty())
                {
                    latLng =
                        "${orderDetailsData!![0].dealerinfo[0].addressLatitude},${orderDetailsData!![0].dealerinfo[0].addressLongitude}".split(
                            ",".toRegex()
                        ).dropLastWhile { it.isEmpty() }.toTypedArray()


                    val latitude = java.lang.Double.parseDouble(latLng!![0])
                    val longitude = java.lang.Double.parseDouble(latLng!![1])
                    navigationLatLong = LatLng(latitude, longitude)

                    var gmmIntentUri =
                        Uri.parse("google.navigation:q=" + latitude + "," + longitude)

                    if (navigationLatLong?.latitude!! <= 0) {
                        //gmmIntentUri = Uri.parse("geo:" + currentLocation?.latitude + "," + currentLocation?.longitude)
                        gmmIntentUri =
                            Uri.parse("geo:" + MyUtils.currentLattitude + "," + MyUtils.currentLongitude)
                    }

                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    if (mapIntent.resolveActivity(mActivity!!.packageManager) != null) {
                        mActivity!!.startService(
                            Intent(
                                mActivity!!,
                                FloatingOverMapIconService::class.java
                            )
                        )


                        mActivity!!.startActivityForResult(mapIntent, 1234)
                        mActivity!!.overridePendingTransition(
                            R.anim.slide_in_right,
                            R.anim.slide_out_left
                        )


                    } else {
                        (activity as MainActivity).showSnackBar("Please install Google Map application.")
                    }
                }
            }
    }


    private fun getCancelOrderApi(
        orderID: String,
        orderdetailsID: String,
        reasonID: String,
        remark: String,
        from: String,
        pos: Int
    ) {
        MyUtils.showProgressDialog(mActivity!!)
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userData!!.userID)
            jsonObject.put("languageID", userData!!.languageID)
            jsonObject.put("orderID", orderID)
            if (orderData?.orderType.equals("Buy Fabric", false)) {
                jsonObject.put("orderdetailsID", orderdetailsID)
            } else if (orderData?.orderType.equals("Stitching", false)) {
                jsonObject.put("orderdetailsstitchingID", orderdetailsID)

            }
            jsonObject.put("reasonID", reasonID)
            jsonObject.put("Remark", remark)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        var cancelOrderModel =
            ViewModelProviders.of(this@OrderDetailsFragment).get(CancelOrderModel::class.java)
        cancelOrderModel.getCancelOrder(mActivity!!, false, jsonArray.toString(), "cancel")
            .observe(this@OrderDetailsFragment,
                Observer<List<CommonPojo>> { appointmentListPojo ->
                    run {
                        if (appointmentListPojo != null && appointmentListPojo.isNotEmpty()) {
                            if (appointmentListPojo[0].status.equals("true", true)) {
                                MyUtils.closeProgress()
                                if (from.equals("cancel", false)) {
                                    mActivity!!.onBackPressed()

                                } else if (from.equals("cancelSingle", false)) {
                                    orderdetails!!.removeAt(pos)
                                    orderListSublistAdapter!!.notifyItemRemoved(pos)
                                    orderListSublistAdapter!!.notifyItemChanged(
                                        pos,
                                        orderdetails!!.size
                                    )

                                }
                                if (!appointmentListPojo[0].message!!.isNullOrEmpty()) {
                                    (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                                }
//                                (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)

                            } else {
                                MyUtils.closeProgress()
                                if (!appointmentListPojo[0].message!!.isNullOrEmpty()) {
                                    (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                                }
//                                (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                            }
                        } else {
                            MyUtils.closeProgress()
                            (activity as MainActivity).errorMethod()
                        }

                    }
                })

    }

    fun getReturnReplaceRequest(
        orderID: String,
        orderdetailsID: String,
        reasonID: String,
        remark: String,
        from: String,
        pos: Int,
        ReturnReplaceQty: String
    ) {
        /*orders/return-replace-request

        REq:

        [
            {
                "loginuserID": "139",
                "languageID": "1",
                "orderID": "21",
                "statusID": "6",
                "orderdetailsID": "0",
                "orderdetailsstitchingID": "9",
                "reasonID": "2",
                "Remark": "The order was fraudulent",
                "RequestedForReturnReplaceQty": "1.00",
                "apiType": "Android",
                "apiVersion": "1.0"
            }
        ]

        Res:

        "status": "true",
        "message": "Order status updated successfully."



        Ankit, 7:10 PM

        statusID

        "6" => Return
        "7" => Replace*/
        MyUtils.showProgressDialog(mActivity!!)
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userData!!.userID)
            jsonObject.put("languageID", userData!!.languageID)
            jsonObject.put("orderID", orderID)
            if (orderData?.orderType.equals("Buy Fabric", false)) {
                jsonObject.put("orderdetailsID", orderdetailsID)
            } else if (orderData?.orderType.equals("Stitching", false)) {
                jsonObject.put("orderdetailsstitchingID", orderdetailsID)

            }

            if (from.equals("Return")) {
//                jsonObject.put("statusID", "6")
                jsonObject.put("statusID", "13")
            } else {
//                jsonObject.put("statusID", "7")
                jsonObject.put("statusID", "16")
            }
            jsonObject.put("reasonID", reasonID)
            jsonObject.put("Remark", "This is not good " + from)
            jsonObject.put("RequestedForReturnReplaceQty", ReturnReplaceQty)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        Log.e("System out", "Print jjj :=  " + jsonArray.toString())

        var returnReplaceRequestModel =
            ViewModelProviders.of(this@OrderDetailsFragment)
                .get(ReturnReplaceRequestModel::class.java)
        returnReplaceRequestModel.getCancelOrder(mActivity!!, false, jsonArray.toString(), "cancel")
            .observe(this@OrderDetailsFragment,
                Observer<List<OrderDetailsPojo>> { appointmentListPojo ->
                    run {
                        if (appointmentListPojo != null && appointmentListPojo.isNotEmpty()) {
                            if (appointmentListPojo[0].status.equals("true", true)) {
                                MyUtils.closeProgress()

                                ll_main.visibility = View.VISIBLE
                                noDatafoundRelativelayout.visibility = View.GONE
                                nointernetMainRelativelayout.visibility = View.GONE
                                relativeprogressBar!!.visibility = View.GONE
                                orderdetails = ArrayList()
                                orderdetails?.clear()
                                orderdetails?.addAll(appointmentListPojo[0].data[0].orderdetails)
                                stitching = ArrayList()
                                stitching?.clear()
                                stitching?.addAll(appointmentListPojo[0].data[0].stitching)
                                mOrderDetailsData = appointmentListPojo[0].data[0]
                                setData(appointmentListPojo[0].data[0], orderID)

                                if (!appointmentListPojo[0].message!!.isNullOrEmpty()) {
                                    (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                                }

                            } else {
                                MyUtils.closeProgress()
//                                (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)

                                /*relativeprogressBar!!.visibility = View.GONE

                                ll_main.visibility = View.GONE
                                noDatafoundRelativelayout.visibility = View.VISIBLE*/
                                if (!appointmentListPojo[0].message!!.isNullOrEmpty()) {
                                    (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                                }

                            }
                        } else {
                            MyUtils.closeProgress()
//                            (activity as MainActivity).errorMethod()
//                            ll_main.visibility = View.GONE

//                            nodatafound()
                            (activity as MainActivity).errorMethod()
                        }

                    }
                })


    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action!!.equals(
                    FloatingOverMapIconService.BROADCAST_ACTION,
                    ignoreCase = true
                ) && activity != null
            ) {
                val selfIntent = Intent(activity, MainActivity::class.java)
                selfIntent.flags =
                    (Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                mActivity!!.startActivity(selfIntent)
            }
        }

    }
}
