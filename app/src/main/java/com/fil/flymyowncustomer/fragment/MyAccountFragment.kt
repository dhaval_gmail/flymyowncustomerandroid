package com.fil.flymyowncustomer.fragment


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.ApplyCouponsActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.UpdateProfileModel
import com.fil.flymyowncustomer.pojo.FiluploadResponse
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.RegisterPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.BottomNavigationViewBehavior
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.Gson
import com.google.gson.JsonParseException
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_my_account.*

import kotlinx.android.synthetic.main.nav_toolbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class MyAccountFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = MyAccountFragment::class.java.name

    var sessionManager: SessionManager? = null
    var userData: RegisterNewPojo.Datum? = null
    val keyFrom = "FROM"
    val keyAccount = "ACCOUNT"
    private var mediaChooseBottomSheet = MediaChooseImageBottomsheet()
    private val TAKE_PICTURE = 1
    private val SELECT_PICTURE = 2
    private var pictureUri: Uri? = null
    private var timeForImageName: Long = 0
    private var imgName: String? = null
    private var picturePath: String? = null
    internal var pbDialog: Dialog? = null
    private var actualImage: File? = null
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    var compressedImage: File? = null
    var selectedProfilePicture: String? = ""

    fun getVersionInfo() {
        var versionName: String = ""
        var versionCode: Int = -1
        try {
            var packageInfo =
                mActivity!!.packageManager.getPackageInfo(mActivity!!.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace();
        }
        Log.e("System Out", "Version Code: " + String.format("V. %s", versionName))
        Log.d("System Out", "Version Code: " + String.format("V. %s", versionName))


        tvVersionCodeName?.text = "" + String.format("V. %s", versionName).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_my_account, container, false)
        }

        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = true

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility = View.VISIBLE
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text = mActivity!!.resources.getString(R.string.account_myAccount)
        menuNotification.visibility = View.GONE
        toolbar_Imageview!!.visibility = View.GONE

//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.VISIBLE
        /*  (mActivity as MainActivity).handleSelection()
          (mActivity as MainActivity).selectBottomNavigationOption(0)*/
        (mActivity as MainActivity).setDrawerSwipe(true)
        BottomNavigationViewBehavior().slideUp((activity as MainActivity).bottom_navigation)
        try {
            tvVersionCodeName?.visibility = View.GONE
            getVersionInfo()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        menuNotification.setOnClickListener {
            (activity as MainActivity).navigateTo(
                NotificationFragment(),
                NotificationFragment::class.java.name,
                true
            )
        }

        sessionManager = SessionManager(mActivity!!)

        if (sessionManager != null) {
            if (sessionManager!!.isLoggedIn()) {
                userData = sessionManager?.get_Authenticate_User()
                if (userData != null) {
                    svUserProfile?.setImageURI(userData?.userProfilePicture, this@MyAccountFragment)
                    if (userData?.userPassword.isNullOrEmpty() && (!userData?.userFacebookID.isNullOrEmpty() || !userData?.userGoogleID.isNullOrEmpty())) {

                        tvAccountChange_Password?.visibility = View.GONE
                    } else {
                        tvAccountChange_Password?.visibility = View.VISIBLE

                    }
                }
            }
        }

        tvAccountMyProfile?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                MyProfileFragment(),
                MyProfileFragment::class.java!!.name,
                true
            )
        }


        tvAccountMyWallet?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(MyWalletFragment(), MyWalletFragment::class.java.name, true)

        }

        tvAccountMyAddresses?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                BillingAddressFragment(),
                BillingAddressFragment::class.java.name,
                true
            )

        }

        tvAccountFavorites?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                FavoritesProductListingFragment(),
                FavoritesProductListingFragment::class.java!!.getName(),
                true
            )
//            MyUtils.startActivity(mActivity!!, ApplyCouponsActivity::class.java,false,true)
        }

        tvAccountFavoritestore_stitching_studio?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                FavoritesStore_StichingStoreFragment(),
                FavoritesStore_StichingStoreFragment::class.java!!.getName(),
                true
            )
        }

        tvAccountSettings?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                SettingsFragment(),
                SettingsFragment::class.java!!.getName(),
                true
            )
        }

        tvAccountChange_Password?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                ChangePasswordFragment(),
                ChangePasswordFragment::class.java!!.getName(),
                true
            )
        }

        tvAccountFavorites_stitching_service?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                FavoritesStitchingServiceListingFragment(),
                FavoritesStitchingServiceListingFragment::class.java!!.getName(),
                true
            )
        }

        img_profileImage?.setOnClickListener {
            val currentapiVersion = Build.VERSION.SDK_INT
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                getWriteStoragePermissionOther()
            } else {
                mediaChooseBottomSheet.show(
                    mActivity!!.supportFragmentManager,
                    "BottomSheet demoFragment"
                )
            }
        }

        svUserProfile.setOnClickListener {
            var profilePhotoFullScreenFragment = ProfilePhotoFullScreenFragment()
            Bundle().apply {
                this.putString("profileImg", userData?.userProfilePicture)
                profilePhotoFullScreenFragment.arguments = this

            }
            (mActivity as MainActivity).navigateTo(
                profilePhotoFullScreenFragment,
                profilePhotoFullScreenFragment::class.java!!.getName(),
                true
            )

        }


    }

    fun getWriteStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(
                mActivity as MainActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getReadStoragePermissionOther()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getReadStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(
                mActivity as MainActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getCameraPermissionOther()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getCameraPermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(mActivity as MainActivity, Manifest.permission.CAMERA)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            mediaChooseBottomSheet.show(
                mActivity!!.supportFragmentManager,
                "BottomSheet demoFragment"
            )
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), MyUtils.Per_REQUEST_CAMERA_1)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getReadStoragePermissionOther()
            } else {
                getWriteStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCameraPermissionOther()
            } else {
                getReadStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_CAMERA_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mediaChooseBottomSheet.show(
                    mActivity!!.supportFragmentManager,
                    "BottomSheet demoFragment"
                )
            } else {
                getCameraPermissionOther()
            }
        }
    }

    fun uploadImage(file: File) {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val surveyImagesParts = arrayOfNulls<MultipartBody.Part>(1)

        for (i in 0 until 1) {
            val fileNew = File(file.absolutePath)
            val surveyBody = RequestBody.create(MediaType.parse("image/*"), fileNew)
            surveyImagesParts[i] =
                MultipartBody.Part.createFormData("MultipleFiles[]", fileNew.name, surveyBody)
        }

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userData?.userID)
            jsonObject.put("foldername", "users")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val call = RestClient.get()!!.uploadAttachment(
            surveyImagesParts, RequestBody.create(
                MediaType.parse("text/plain"), jsonArray.toString()
            )
        )

        Log.i("System out", "json " + jsonArray.toString())

        pbDialog = MyUtils.showProgressDialog(mActivity!!)
        pbDialog!!.show()

        call.enqueue(object : Callback<List<FiluploadResponse>> {
            override fun onResponse(
                call: Call<List<FiluploadResponse>>,
                response: Response<List<FiluploadResponse>>
            ) {

                if (response.body() != null) {
                    if (pbDialog != null)
                        pbDialog!!.dismiss()
                    if (response.body()!![0].status.equals("true")) {

                        if (!response.body()!![0].data.isNullOrEmpty()) {
                            if (!response.body()!![0].data!![0].filename.isNullOrEmpty()) {
                                selectedProfilePicture = response.body()!![0].data!![0].filename!!

                                /* try {
                                     actualImage?.delete()
                                     compressedImage?.delete()
                                 } catch (e: Exception) {
                                     e.printStackTrace()
                                 }*/

                                svUserProfile?.setImageURI(
                                    Uri.parse(RestClient.image_base_url + "users/" + selectedProfilePicture),
                                    mActivity
                                )
                                editProfileAPI()
                            }
                        }
                    } else {
                        try {
                            if (MyUtils.isInternetAvailable(mActivity!!)) {
                                MyUtils.showSnackbarkotlin(
                                    mActivity!!,
                                    rootMyaccuntLayout!!,
                                    mActivity!!.resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    mActivity!!,
                                    rootMyaccuntLayout!!,
                                    mActivity!!.resources.getString(R.string.error_common_network)
                                )
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } else {

                }
            }

            override fun onFailure(call: Call<List<FiluploadResponse>>, t: Throwable) {
                // Log error here since request failed
                Log.e("System out", t.message)
                if (pbDialog != null)
                    pbDialog!!.dismiss()
            }
        })

    }

    private fun editProfileAPI() {

        if (selectedProfilePicture!!.contains("/")) {
            val fileName =
                selectedProfilePicture?.substring(selectedProfilePicture!!.lastIndexOf('/') + 1)
            selectedProfilePicture = fileName
        }

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {

//            [
//                {
//                    "languageID": "1",
//                    "loginuserID": "1",
//                    "userFullName": "Ankit Patel",
//                    "userProfilePicture": "API20190830070741746455.jpg",
//                    "userMobile": "9979517447",
//                    "userEmail": "patelankit1516@gmail.com",
//                    "apiType": "Android",
//                    "apiVersion": "1.0"
//                }
//            ]

            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userData?.userID)
            jsonObject.put("userFullName", userData?.userFullName.toString().trim())
            jsonObject.put("userProfilePicture", selectedProfilePicture)
            jsonObject.put("userMobile", userData?.userMobile)
            jsonObject.put("userEmail", userData?.userEmail)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        Log.e("System Out", "Login Request $jsonArray")

        val countryList = ViewModelProviders.of(mActivity!!).get(UpdateProfileModel::class.java)
        countryList.apiFunction(mActivity!!, true, jsonArray.toString()).observe(mActivity!!,
            object : Observer<List<RegisterNewPojo>> {
                override fun onChanged(response: List<RegisterNewPojo>?) {
                    if (!response.isNullOrEmpty()) {
                        if (response.size > 0) {
                            if (response[0].status.equals("true", true)) {

                                /*if (!response[0].message.isNullOrEmpty()){
                                    MyUtils.showSnackbarkotlin(mActivity!!, rootMyaccuntLayout!!, response[0].message!!)
                                }else{
                                    MyUtils.showSnackbarkotlin(mActivity!!, rootMyaccuntLayout!!, mActivity!!.resources.getString(R.string.update_profile_success))
                                }*/
                                sessionManager?.clear_login_session()
                                storeSessionManager(response[0]!!.data!!)

                                (mActivity as MainActivity).tvUserName?.text =
                                    response[0]!!.data!![0].userFullName
                                (mActivity as MainActivity).profileImageview?.setImageURI(
                                    Uri.parse(
                                        response[0]!!.data!![0].userProfilePicture
                                    ), mActivity
                                )

                            } else {
                                if (MyUtils.isInternetAvailable(mActivity!!)) {
                                    MyUtils.showSnackbarkotlin(
                                        mActivity!!,
                                        rootMyaccuntLayout!!,
                                        mActivity!!.resources.getString(R.string.error_crash_error_message)
                                    )
                                } else {
                                    MyUtils.showSnackbarkotlin(
                                        mActivity!!,
                                        rootMyaccuntLayout!!,
                                        mActivity!!.resources.getString(R.string.error_common_network)
                                    )
                                }
                            }
                        }
                    } else {
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(mActivity!!)) {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                rootMyaccuntLayout!!,
                                mActivity!!.resources.getString(R.string.error_crash_error_message)
                            )
                        } else {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                rootMyaccuntLayout!!,
                                mActivity!!.resources.getString(R.string.error_common_network)
                            )
                        }
                    }
                }
            })
    }

    private fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(mActivity!!)) {
                MyUtils.showSnackbarkotlin(
                    mActivity!!,
                    rootMyaccuntLayout,
                    mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                )
            } else {
                MyUtils.showSnackbarkotlin(
                    mActivity!!,
                    rootMyaccuntLayout,
                    mActivity!!.resources.getString(R.string.error_crash_error_message)
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()
        val json = gson.toJson(driverdata[0]!!)
        sessionManager?.create_login_session(
            json,
            driverdata[0]!!.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            timeForImageName = System.currentTimeMillis()
            imgName = "img$timeForImageName.jpg"

            when (requestCode) {
                TAKE_PICTURE -> if (mediaChooseBottomSheet.selectedImage() != null) {
                    pictureUri = mediaChooseBottomSheet.selectedImage()
                    picturePath = pictureUri?.path
                    svUserProfile?.setImageURI(
                        Uri.fromFile(File(picturePath)),
                        this@MyAccountFragment
                    )
                    actualImage = File(picturePath)
                    Log.e("System out","after size : "+ String.format("Size : %s",getReadableFileSize(actualImage!!.length())))
                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        compressedImage = customCompressImage(actualImage!!)
                        Log.e("System out","after  compress Image size : "+ String.format("Size : %s",getReadableFileSize(compressedImage!!.length())))
//                        Log.e("System out","compressedImage file :  "+compressedImage?.absolutePath)
                        uploadImage(compressedImage!!)

                    } else {
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            rootMyaccuntLayout,
                            mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                        )
                    }

                }
                SELECT_PICTURE -> {
                    var isBool = false
                    pictureUri = data!!.data
                    picturePath = MyUtils.getPath5(pictureUri!!, this.mActivity!!)
                    if (picturePath != null) {
                        if (picturePath?.contains("https:")!!) {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                rootMyaccuntLayout!!,
                                "Please, select another profile pic."
                            )
                        } else {
                            svUserProfile?.setImageURI(
                                Uri.fromFile(File(picturePath)),
                                this@MyAccountFragment
                            )
                            actualImage = File(picturePath)
//                            Log.e("System out","after size : "+ String.format("Size : %s",getReadableFileSize(actualImage!!.length())))
                            try {
                                picturePath = MyUtils.getPath(pictureUri!!, this.mActivity!!)
                                actualImage = File(picturePath)
                                compressedImage = customCompressImage(actualImage!!)

//                                Log.e("System out","after  compress Image size : "+ String.format("Size : %s",getReadableFileSize(compressedImage!!.length())))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            if (MyUtils.isInternetAvailable(mActivity!!)) {
                                uploadImage(compressedImage!!)
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    mActivity!!,
                                    rootMyaccuntLayout,
                                    mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                                )
                            }
                        }
                    } else {
                        picturePath = MyUtils.getPathFromInputStreamUri(mActivity!!, data!!.data)
                        svUserProfile?.setImageURI(
                            Uri.fromFile(File(picturePath)),
                            this@MyAccountFragment
                        )
                        actualImage = File(picturePath)
//                        Log.e("System out","after size : "+ String.format("Size : %s",getReadableFileSize(actualImage!!.length())))
                        if (MyUtils.isInternetAvailable(mActivity!!)) {
//                            compressedImage = customCompressImage(actualImage!!)
                            uploadImage(actualImage!!)


                        } else {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                rootMyaccuntLayout,
                                mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                            )
                        }

                    }

//
                }
            }
        }
    }

    fun customCompressImage(actualImage: File): File? {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        var compressedImage: File? = null
        try {
            compressedImage = Compressor(mActivity)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    ).absolutePath
                )
                .compressToFile(actualImage, "IMG_" + timeStamp + ".jpg")
        } catch (e: IOException) {
            e.printStackTrace()
//            showError(e.message)
        }

        return compressedImage

    }

    fun getReadableFileSize(size: Long): String {
        if (size <= 0) {
            return "0"
        }
        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(
            size / Math.pow(1024.0, digitGroups.toDouble())) + " " + units[digitGroups]
    }

}