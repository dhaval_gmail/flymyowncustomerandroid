package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ReturnReplacelistAdapter
import com.fil.flymyowncustomer.model.MyOrderListModel
import com.fil.flymyowncustomer.pojo.MyOrderData
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PopupMenu
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class ReturnReplaceListFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    private var tabposition: Int = 0
    var type = ""
    var mReturnReplacelistAdapter: ReturnReplacelistAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var list: ArrayList<String>? = null
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var sessionManager: SessionManager? = null
    var myOderListData: ArrayList<MyOrderData?>?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_return_replace_list, container, false)

        }
        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (arguments != null) {
            tabposition = arguments!!.getInt("position", -1)
            type = arguments!!.getString("type", "")
        }
        sessionManager=SessionManager(mActivity!!)
        noDatafoundRelativelayout.visibility=View.GONE
        relativeprogressBar.visibility=View.GONE
        nointernetMainRelativelayout.visibility=View.GONE


        myOderListData= ArrayList()
        linearLayoutManager = LinearLayoutManager(mActivity!!)
        mReturnReplacelistAdapter = ReturnReplacelistAdapter(
            mActivity!!,
            object : ReturnReplacelistAdapter.OnItemClick {
                override fun onClicklisneter(pos: Int, name: String) {
                    when(name){
                        "Option"->
                        {
                            /*list = java.util.ArrayList()
                            list!!.clear()
                            if(type.equals("New",false))
                            {
                                list!!.add("Cancel")
                            }else if (type.equals("Completed",false))
                            {
                                list!!.add("Rate")
                            }
                            PopupMenu(activity!!, v!!, list!!).showPopUp(object :
                                PopupMenu.OnMenuSelectItemClickListener {
                                override fun onItemClick(item: String) {
                                    openDialog(myOderListData!![pos]!!.orderID,"")

                                }

                            })*/
                        }
                        "itemView"->
                        {
                            var returnReplaceDetailsFragment= ReturnReplaceDetailsFragment()
                            Bundle().apply {
                                putSerializable("orderData", myOderListData!![pos])
                                putSerializable("type",type)
                                returnReplaceDetailsFragment.arguments=this

                            }
                            (activity as MainActivity).navigateTo(returnReplaceDetailsFragment,returnReplaceDetailsFragment::class.java.name,true)
                        }
                    }
                }

            },myOderListData!!,type
        )
        notificationRecyclerview.layoutManager = linearLayoutManager
        notificationRecyclerview.adapter = mReturnReplacelistAdapter
        if (myOderListData.isNullOrEmpty() || pageNo == 0) {
            pageNo = 0
            isLastpage = false
            isLoading = false
            getMyOrderList()
        }
        btnRetry.setOnClickListener {
            pageNo = 0
            isLastpage = false
            isLoading = false
            getMyOrderList()
        }

    }

    private fun getMyOrderList()
    {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            myOderListData!!.clear()
            mReturnReplacelistAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            myOderListData!!.add(null)
            mReturnReplacelistAdapter?.notifyItemInserted(myOderListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "1")
            jsonObject.put("type",type)
            jsonObject.put("orderType","")
            jsonObject.put("IsRequestedForReturnReplace","Yes")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var myOrderListModel =
            ViewModelProviders.of(this@ReturnReplaceListFragment).get(MyOrderListModel::class.java)
        myOrderListModel.getMyOrderList(mActivity!!, false, jsonArray.toString(),"")
            .observe(this@ReturnReplaceListFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE

                        if (pageNo > 0) {
                            myOderListData!!.removeAt(myOderListData!!.size - 1)
                            mReturnReplacelistAdapter?.notifyItemRemoved(myOderListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                myOderListData!!.clear()

                            myOderListData!!.addAll(masterPojo[0].data!!)
                            mReturnReplacelistAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (myOderListData!!.isEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE


                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })

    }


    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!))
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            }
            else
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
