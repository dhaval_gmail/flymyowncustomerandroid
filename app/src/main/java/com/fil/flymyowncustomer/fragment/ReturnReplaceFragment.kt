package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import java.util.ArrayList
import kotlinx.android.synthetic.main.fragment_return_replace.*

/**
 * A simple [Fragment] subclass.
 *
 */
class ReturnReplaceFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var adapter: ViewPagerAdapter? = null
    var tabposition = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v=inflater.inflate(R.layout.fragment_return_replace, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility=View.VISIBLE
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text=mActivity!!.resources.getString(R.string.return_replace)
        menuNotification.visibility=View.GONE
        toolbar_Imageview!!.visibility = View.GONE

//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE

        (mActivity as MainActivity).setDrawerSwipe(true)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        menuNotification.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(),NotificationFragment::class.java.name,true)
        }

        setupViewPager(ReturnPreplaceViewPager!!)
        ReturnPreplaceTablayout!!.setupWithViewPager(ReturnPreplaceViewPager)

    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(childFragmentManager)
        adapter?.addFragment(ReturnReplaceListFragment(), "Pending")
        adapter?.addFragment(ReturnReplaceListFragment(), "Completed")
        viewPager.adapter = adapter
        viewPager.currentItem = tabposition
    }

    inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
//            val bundle = Bundle()
//            bundle.putInt("position", position)
//            val fragment = mFragmentList[position]
//            fragment.arguments = bundle


            val bundle = Bundle()
            bundle.putInt("position", position)

            if (position == 0)
                bundle.putString("type", "New")
            else if (position == 1)
                bundle.putString("type", "Completed")

            val fragment = mFragmentList[position]
            fragment.arguments = bundle

            return fragment
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun saveState(): Parcelable? {
            return null
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }

    }

}
