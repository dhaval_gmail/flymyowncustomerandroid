package com.fil.flymyowncustomer.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.farmerprice.adapter.ReplaceResoneListingAdapter
import com.fil.farmerprice.adapter.SortByAdapter
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.iterfaces.RecyclerViewItemClickListner

import com.fil.flymyowncustomer.pojo.SortingPojo
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PrefDb

import kotlinx.android.synthetic.main.popup_add_stock.view.*
import java.util.ArrayList


class PopUpAddStock : DialogFragment(), View.OnClickListener {

    var view1: View? = null
    var mActivity: AppCompatActivity? = null
    var OnSelectionItemClickListener: OnItemClickListener? = null
    var mSelection :Int = 0
    var mSortByAdapter: ReplaceResoneListingAdapter?= null;

    var linearLayoutManager : LinearLayoutManager? = null
    var list:ArrayList<String>?=null
    var from:String=""
    var qty:String=""
    var editReplaceItemText : AppCompatEditText?= null
    var popup_title_add_stock : AppCompatTextView?= null
    var popup_layout_main : LinearLayoutCompat?= null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
//        val bundle = arguments
    }

    fun setMargins(dialog: Dialog, marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int): Dialog {
        val window = dialog.window
            ?: // dialog window is not available, cannot apply margins
            return dialog
        val context = dialog.context

        // set dialog to fullscreen
        val root = RelativeLayout(context)
        root.layoutParams =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        // set background to get rid of additional margins
//        window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        // apply left and top margin directly
        window.setGravity(Gravity.CENTER)
        val attributes = window.attributes
        attributes.x = marginLeft
        attributes.y = marginBottom
        window.attributes = attributes

        // set right and bottom margin implicitly by calculating width and height of dialog
        val displaySize = getDisplayDimensions(context)
        val width = displaySize.x - marginLeft - marginRight
        val height = displaySize.y - marginTop - marginBottom
        window.setLayout(width,  ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(resources.getDrawable(R.drawable.background_inset1))

        return dialog
    }

    fun getDisplayDimensions(context: Context): Point {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay

        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val screenWidth = metrics.widthPixels
        var screenHeight = metrics.heightPixels

        // find out if status bar has already been subtracted from screenHeight
        display.getRealMetrics(metrics)
        val physicalHeight = metrics.heightPixels
        val statusBarHeight = getStatusBarHeight(context)
        val navigationBarHeight = getNavigationBarHeight(context)
        val heightDelta = physicalHeight - screenHeight
        if (heightDelta == 0 || heightDelta == navigationBarHeight) {
            screenHeight -= statusBarHeight
        }

        return Point(screenWidth, screenHeight)
    }

    fun getStatusBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
    }

    fun getNavigationBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val bundle = arguments
//        if (bundle != null)
//            dialogKey = bundle!!.getString(keyBundleFromWhere, "")

        if (arguments != null) {
            mSelection = arguments?.getInt("mSelection", 0)!!
            list = (arguments?.getSerializable("list") as ArrayList<String>?)!!
            from=arguments?.getString("from")!!

            qty=arguments?.getString("qty","0")!!
        }


        val style = DialogFragment.STYLE_NO_FRAME
        val theme = R.style.DialogTheme
        setStyle(style, theme)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
                mActivity = context as MainActivity
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view1 = inflater!!.inflate(R.layout.popup_add_stock, container, false)

//        view1?.imv_add_bnk_statement.setImageURI()
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        setMargins(dialog!!, 20, 0, 20, 0)

        var arraySortingList: java.util.ArrayList<SortingPojo?>? = null
        arraySortingList = java.util.ArrayList<SortingPojo?>()
        var data1 = SortingPojo()
        data1.isSelection = false
        data1.sortingName = "Item is defected"
        arraySortingList.add(data1)
        var data2 = SortingPojo()
        data2.isSelection = false
        data2.sortingName = "Wrong product"
        arraySortingList.add(data2)
        var data3 = SortingPojo()
        data3.isSelection = false
        data3.sortingName = "Wrong Size"
        arraySortingList.add(data3)
        editReplaceItemText = view1?.findViewById<AppCompatEditText>(R.id.editReplaceItemText)
        popup_layout_main = view1?.findViewById<LinearLayoutCompat>(R.id.popup_layout_main)
        popup_title_add_stock = view1?.findViewById<AppCompatTextView>(R.id.popup_title_add_stock)
        linearLayoutManager = LinearLayoutManager(mActivity)

        if(!from.isNullOrEmpty())
        {
            popup_title_add_stock?.text=from
        }


        view1?.popup_recyclerview_size?.layoutManager = linearLayoutManager
        view1?.popup_recyclerview_size?.hasFixedSize()
        mSortByAdapter = ReplaceResoneListingAdapter(mActivity!!, list!!,object : RecyclerViewItemClickListner {
            override fun onItemClick(position: Int, typeofOperation: String) {
                if(mSortByAdapter!!.mSelection > -1){
                    /*if(OnSelectionItemClickListener != null){
                        OnSelectionItemClickListener!!.onItemSelectionClick(typeofOperation,position)
                        mSelection = position
                        dismiss()
                    }*/

                }
            }

        })
        view1?.popup_recyclerview_size?.adapter = mSortByAdapter
        mSortByAdapter?.mSelection = mSelection
        mSortByAdapter?.notifyDataSetChanged()
        view1?.tv_popup_done?.setOnClickListener(this)
        view1?.tv_popup_cancel?.setOnClickListener(this)
        return view1
    }

    companion object {
        fun newInstance(): PopUpAddStock {
            val f = PopUpAddStock()
            // Supply num input as an argument.
            var args = Bundle()
//           args = bundle
//           Log.d("argsbundle", args.toString())

            f.arguments = args
            return f
        }
    }

    fun setListener(listener: OnItemClickListener) {
        this.OnSelectionItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onItemSelectionClick(typeButton: String, flag: Int,qty : String)
    }

    override fun onClick(v: View?) {
        /*
            0 -> For women fragment
            1 -> For men fragmnet
        */
        when (v?.id) {
            R.id.tv_popup_cancel ->{
                if (OnSelectionItemClickListener != null){
                    OnSelectionItemClickListener!!.onItemSelectionClick("cancel", 1,"")
                    dismiss()
                }
            }
            R.id.tv_popup_done -> {
                MyUtils.hideKeyboard1(mActivity!!)
                if(editReplaceItemText?.text.toString().trim().isNullOrEmpty()){

                    MyUtils.showSnackbarkotlin(
                        mActivity,
                        popup_layout_main!!,
                        "Please Enter Qty to ${from}"
                    )

                }else if(!editReplaceItemText?.text.toString().trim().isNullOrEmpty() && editReplaceItemText?.text.toString().trim().toInt()<=0){

                    MyUtils.showSnackbarkotlin(
                        mActivity,
                        popup_layout_main!!,
                        "Please Enter Valid Qty to ${from}"
                    )

                }else if(!editReplaceItemText?.text.toString().trim().isNullOrEmpty() && editReplaceItemText?.text.toString().trim().toDouble()> qty.toDouble()){

                    MyUtils.showSnackbarkotlin(
                        mActivity,
                        popup_layout_main!!,
                        "Quantity more than your order"
                    )

                }else {
                    if(mSortByAdapter!!.mSelection > -1){
                        if(OnSelectionItemClickListener != null){
                            OnSelectionItemClickListener!!.onItemSelectionClick(from,mSortByAdapter!!.mSelection,editReplaceItemText?.text.toString().trim())
                            mSelection = mSortByAdapter!!.mSelection
                            dismiss()
                        }

                    }else {
                        MyUtils.showSnackbarkotlin(
                            mActivity,
                            popup_layout_main!!,
                            "Please Select any one reason"
                        )
                    }
                }

//                dismiss()
            }
        }
    }

    data class MeasurmentSize(var isSelected : Boolean, val name : String)
    data class MeasurmentSizeWithPrize(val name : String, var price : String, var discount : String, var discountType : String)
}