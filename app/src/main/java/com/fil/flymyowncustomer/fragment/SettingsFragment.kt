package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log

import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.RegisterAddAddressActivity
import com.fil.flymyowncustomer.model.UserUpdateSettingsModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.fil.flymyowncustomer.util.SwitchButton
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_billing_address.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class SettingsFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = SettingsFragment::class.java.name
    var isSwitchBtnOrdersRelatedPushNotifi = false
    var isSwitchBtnOrdersRelatedEmailNotifi = false
    var isSwitchBtnOffersRelatedPushNotifi = false
    var isSwitchBtnOffersRelatedEmailNotifi = false
    lateinit var sessionManager: SessionManager
    var userData : RegisterNewPojo.Datum? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_settings, container, false)
        }

        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        tvToolbarTitel.text = getString(R.string.account_settings)
        
        (mActivity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        try {
            sessionManager = SessionManager(mActivity!!)

            if (sessionManager != null){
                if (sessionManager!!.isLoggedIn()){
                    userData = sessionManager?.get_Authenticate_User()
                }
            }

            setUserSettings()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        SwitchBtnOrdersRelatedPushNotifi?.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {
                if(isChecked){
                    isSwitchBtnOrdersRelatedPushNotifi = true
                }else {
                    isSwitchBtnOrdersRelatedPushNotifi = false
                }

                updateUserSettingAPI()
            }
        })

        SwitchBtnSwitchOrdersRelatedEmailNotifi?.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {
                if(isChecked){
                    isSwitchBtnOrdersRelatedEmailNotifi = true
                }else {
                    isSwitchBtnOrdersRelatedEmailNotifi = false
                }
                updateUserSettingAPI()
            }
        })

        SwitchBtnOffersRelatedPushNotifi?.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {
                if(isChecked){
                    isSwitchBtnOffersRelatedPushNotifi = true
                }else {
                    isSwitchBtnOffersRelatedPushNotifi = false
                }
                updateUserSettingAPI()
            }
        })

        SwitchBtnSwitchOffersRelatedEmailNotifi?.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {
                if(isChecked){
                    isSwitchBtnOffersRelatedEmailNotifi = true
                }else {
                    isSwitchBtnOffersRelatedEmailNotifi = false
                }
                updateUserSettingAPI()
            }
        })

    }

    private fun updateUserSettingAPI() {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

//            [{
//                "languageID": "1",
//                "loginuserID": "1",
//                "userOrderEmailNotification": "Yes",
//                "userOrderPushNotification": "Yes",
//                "userOfferEmailNotification": "Yes",
//                "userOfferPushNotification": "Yes",
//                "apiType": "Android",
//                "apiVersion": "1.0"
//            }]

            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userData!!.userID)
            jsonObject.put("userOrderPushNotification", if (isSwitchBtnOrdersRelatedPushNotifi) "Yes" else "No")
            jsonObject.put("userOrderEmailNotification", if (isSwitchBtnOrdersRelatedEmailNotifi) "Yes" else "No")
            jsonObject.put("userOfferPushNotification", if (isSwitchBtnOffersRelatedPushNotifi) "Yes" else "No")
            jsonObject.put("userOfferEmailNotification", if (isSwitchBtnOffersRelatedEmailNotifi) "Yes" else "No")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.d(TAG,"Register api call := "+jsonArray.toString())

        }catch (e: java.lang.Exception){
            e.printStackTrace()
        }

        val getuserUpdateSettingsModel = ViewModelProviders.of(this@SettingsFragment).get(UserUpdateSettingsModel::class.java)
        getuserUpdateSettingsModel.apiFunction(mActivity!!, true, jsonArray.toString()).observe(this@SettingsFragment,
            Observer<List<RegisterNewPojo>> { response ->
                if (!response.isNullOrEmpty()){
                    if (response[0].status.equals("true", true)){

                        if (!response[0].message.isNullOrEmpty()){
                            MyUtils.showSnackbarkotlin(mActivity!!, rootNotificationSettingsLayout, response[0].message!!)

                        }else {
                            MyUtils.showSnackbarkotlin(mActivity!!, rootNotificationSettingsLayout, mActivity!!.resources.getString(R.string.settingchanged))
                        }

                        storeSessionManager(response[0]!!.data!!)
                        sessionManager = SessionManager(mActivity!!)
                        userData = sessionManager.get_Authenticate_User()

                        setUserSettings()
                    }else{
                        MyUtils.showSnackbarkotlin(mActivity!!, rootNotificationSettingsLayout, response[0].message!!)
                    }
                }else{
                    if (MyUtils.isInternetAvailable(mActivity!!)){
                        MyUtils.showSnackbarkotlin(mActivity!!, rootNotificationSettingsLayout, mActivity!!.resources.getString(R.string.error_crash_error_message))
                    }else{
                        MyUtils.showSnackbarkotlin(mActivity!!, rootNotificationSettingsLayout, mActivity!!.resources.getString(R.string.error_common_network))
                    }
                }
            })
    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()

        val json = gson.toJson(driverdata[0]!!)

        sessionManager?.create_login_session(
            json,
            driverdata[0]!!.userEmail!!,
            "",
            true,
            sessionManager.isEmailLogin())

    }

    private fun setUserSettings() {

        if (sessionManager!!.isLoggedIn()){
            userData = sessionManager?.get_Authenticate_User()
        }
//        "userOrderEmailNotification": "Yes",
//        "userOrderPushNotification": "Yes",
//        "userOfferEmailNotification": "Yes",
//        "userOfferPushNotification": "Yes",
        if (userData?.userOrderPushNotification.equals("Yes", true)){
            SwitchBtnOrdersRelatedPushNotifi?.isChecked = true
            isSwitchBtnOrdersRelatedPushNotifi = true
        }else{
            SwitchBtnOrdersRelatedPushNotifi?.isChecked = false
            isSwitchBtnOrdersRelatedPushNotifi = false
        }

        if (userData?.userOrderEmailNotification.equals("Yes", true)){
            SwitchBtnSwitchOrdersRelatedEmailNotifi?.isChecked = true
            isSwitchBtnOrdersRelatedEmailNotifi = true
        }else{
            SwitchBtnSwitchOrdersRelatedEmailNotifi?.isChecked = false
            isSwitchBtnOrdersRelatedEmailNotifi = false
        }

        if (userData?.userOfferPushNotification.equals("Yes", true)){
            SwitchBtnOffersRelatedPushNotifi?.isChecked = true
            isSwitchBtnOffersRelatedPushNotifi = true
        }else{
            SwitchBtnOffersRelatedPushNotifi?.isChecked = false
            isSwitchBtnOffersRelatedPushNotifi = false
        }

        if (userData?.userOfferEmailNotification.equals("Yes", true)){
            SwitchBtnSwitchOffersRelatedEmailNotifi?.isChecked = true
            isSwitchBtnOffersRelatedEmailNotifi = true
        }else{
            SwitchBtnSwitchOffersRelatedEmailNotifi?.isChecked = false
            isSwitchBtnOffersRelatedEmailNotifi = false
        }
    }

}
