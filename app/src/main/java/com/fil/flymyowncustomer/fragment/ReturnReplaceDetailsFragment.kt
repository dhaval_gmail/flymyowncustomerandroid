package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ReturnReplaceOrderListingAdapter
import com.fil.flymyowncustomer.adapter.ReturnReplaceOrderStitchingListingAdapter
import com.fil.flymyowncustomer.model.OrderDetailsPojoModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_return_replace_details.*
import kotlinx.android.synthetic.main.fragment_return_replace_details.notificationRecyclerview
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class ReturnReplaceDetailsFragment : Fragment() {

    private var v: View? = null
    var mActivity: Activity? = null
    lateinit var sessionManager: SessionManager
    var userdata: RegisterNewPojo.Datum? = null
    var userid: String? = ""
    val TAG = ReturnReplaceDetailsFragment::class.java.name
    var mReturnReplaceOrderListingAdapter: ReturnReplaceOrderListingAdapter? = null
    var mReturnReplaceOrderStitchingListingAdapter: ReturnReplaceOrderStitchingListingAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var mOrderDetailsData : OrderDetailsData?= null
    var type: String = ""
    var orderdetails: ArrayList<Orderdetail>? = null
    var stitching: ArrayList<Stitching>? = null

    var orderData: MyOrderData? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_return_replace_details, container, false)
        }
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*(mActivity as MainActivity).toolbar_title!!.visibility = View.VISIBLE
        (mActivity as MainActivity).toolbar_Imageview?.visibility = View.GONE*/
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel!!.visibility = View.VISIBLE
        tvToolbarTitel.text = mActivity!!.resources.getString(R.string.return_replace)
        menuNotification.visibility = View.GONE

        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE
        (mActivity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        sessionManager = SessionManager(mActivity!!)
        if (arguments != null) {
            orderData = arguments!!.getSerializable("orderData") as MyOrderData?
            type = arguments!!.getString("type")!!
        }
        if (orderData != null) {
            getOrderDetails(orderData?.orderID)
        }



        /*sessionManager = SessionManager(mActivity!!)
        if(sessionManager.isLoggedIn()){

            userdata = sessionManager.get_Authenticate_User()
            if(userdata != null){
                userid = userdata!!.userID
            }else {
                userid = "0"
            }
        }else {
            userid = "0"
        }*/


    }
    private fun getOrderDetails(orderID: String?) {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        relativeprogressBar!!.visibility = View.VISIBLE
        rootOrderDetailsMainLayout.visibility = View.GONE

        var userData = sessionManager.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("orderID", orderID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var orderDetailsModel =
            ViewModelProviders.of(this@ReturnReplaceDetailsFragment).get(OrderDetailsPojoModel::class.java)
        orderDetailsModel.getOrderDetails(mActivity!!, false, jsonArray.toString(), "cancel")
            .observe(this@ReturnReplaceDetailsFragment,
                Observer<List<OrderDetailsPojo>> { orderData ->
                    run {
                        if (orderData != null && orderData.isNotEmpty()) {
                            if (orderData[0].status.equals("true", true)) {
                                rootOrderDetailsMainLayout.visibility = View.VISIBLE
                                noDatafoundRelativelayout.visibility = View.GONE
                                nointernetMainRelativelayout.visibility = View.GONE
                                relativeprogressBar!!.visibility = View.GONE
                                orderdetails = ArrayList()
                                orderdetails?.clear()
                                orderdetails?.addAll(orderData[0].data[0].orderdetails)
                                stitching = ArrayList()
                                stitching?.clear()
                                stitching?.addAll(orderData[0].data[0].stitching)
                                mOrderDetailsData = orderData[0].data[0]
                                bindData(orderdetails!!,stitching!!)
                            } else {
                                relativeprogressBar!!.visibility = View.GONE

                                rootOrderDetailsMainLayout.visibility = View.GONE
                                noDatafoundRelativelayout.visibility = View.VISIBLE

                            }

                        } else {
                            rootOrderDetailsMainLayout.visibility = View.GONE

                            nodatafound()
                        }

                    }
                })


    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun bindData(
        orderdetails: ArrayList<Orderdetail>,
        stitching: ArrayList<Stitching>) {

//        if (notificationListData != null) {
        if(orderData!=null) {
            tvOrderIdValue.text = orderData?.orderNo
            tvOrderTypes.text = orderData?.statusName
            tvOrderCompleted.text = orderData?.statusName
            tvOrderTotalAmount.text = resources.getString(R.string.rs) + orderData?.orderNetAmount
            linearLayoutManager = LinearLayoutManager(mActivity)

            when (orderData?.orderType) {
                "Buy Fabric"->{
                    mReturnReplaceOrderListingAdapter = ReturnReplaceOrderListingAdapter(
                        mActivity!!,
                        object : ReturnReplaceOrderListingAdapter.OnItemClick {
                            override fun onClicklisneter(pos: Int, name: String, v: View) {

                            }

                        },orderdetails)

                    notificationRecyclerview?.isNestedScrollingEnabled = false
                    notificationRecyclerview?.layoutManager = linearLayoutManager
                    notificationRecyclerview ?. setHasFixedSize (true)
                    notificationRecyclerview ?. adapter = mReturnReplaceOrderListingAdapter
                }
                "Stitching"->{
                    mReturnReplaceOrderStitchingListingAdapter = ReturnReplaceOrderStitchingListingAdapter(
                        mActivity!!,
                        object : ReturnReplaceOrderStitchingListingAdapter.OnItemClick {
                            override fun onClicklisneter(pos: Int, name: String, v: View) {

                            }

                        },stitching)

                    notificationRecyclerview?.isNestedScrollingEnabled = false
                    notificationRecyclerview?.layoutManager = linearLayoutManager
                    notificationRecyclerview ?. setHasFixedSize (true)
                    notificationRecyclerview ?. adapter = mReturnReplaceOrderStitchingListingAdapter
                }

            }
        }
//        }
    }


}
