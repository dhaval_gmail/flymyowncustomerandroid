package com.fil.flymyowncustomer.fragment


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ParseException
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.GetCmspageModel
import com.fil.flymyowncustomer.model.GetUserSettingsModel
import com.fil.flymyowncustomer.pojo.GetCmspagePojo
import com.fil.flymyowncustomer.pojo.GetUserSettingsPojo
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.RegisterPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.BottomNavigationViewBehavior
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_termsand_conditions.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.webview.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


//https://www.mkyong.com/android/how-to-make-a-phone-call-in-android/
/**
 * A simple [Fragment] subclass.
 *
 */
class ContactUsFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    var userId: String? = "0"
    var sessionManager : SessionManager? = null
    var userData : RegisterNewPojo.Datum? = null
    var mGetUserSettingslist = ArrayList<GetUserSettingsPojo.Datum?>()
    var mGetUserSettingsPojo : RegisterNewPojo.Datum.Setting?= null
    var fragmentType: String?=null
    var WhichCMSPage: String?=""
    val mTerms = "ContactUS"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null)
        {
//            v = inflater!!.inflate(R.layout.fragment_contact_us, container, false)
            v = inflater!!.inflate(R.layout.fragment_termsand_conditions, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility=View.VISIBLE
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text=mActivity!!.resources.getString(R.string.contact_us)
        menuNotification.visibility=View.GONE
        toolbar_Imageview!!.visibility = View.GONE

//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE
//        (mActivity as MainActivity).handleSelection()
        /*  (mActivity as MainActivity).selectBottomNavigationOption(0)*/
        (mActivity as MainActivity).setDrawerSwipe(true)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        try {
            sessionManager = SessionManager(mActivity!!)
            if (sessionManager!!.isLoggedIn()) {
                userData = sessionManager?.get_Authenticate_User()
                if (userData != null) {
                    userId = userData?.userID
//                    if(!userData!!.settings.isNullOrEmpty() && userData!!.settings!!.size>0){
//                        mGetUserSettingsPojo = userData?.settings!![0]
//                        if(mGetUserSettingsPojo != null){
//                            setContactUsData(mGetUserSettingsPojo)
//                        }
//                    }
                } else {
                    userId = "0"
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        fragmentType = arguments?.getString("Type", "")

        relativeprogressBar?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        webViewMainRelativeLayout?.visibility = View.VISIBLE
        llMainTermconfitions?.visibility = View.GONE

        webData()

        when(fragmentType){

            mTerms->{
                toolbar_title.text=(mActivity!!.resources.getString(R.string.contact_us))
                WhichCMSPage = "000005"
                commonMethod()
            }


        }

        btnRetry?.setOnClickListener {
            commonMethod()
        }

        /*tvforsupportrelatedValue?.setOnClickListener {

            if(tvforsupportrelatedValue?.text.toString().trim().equals("--")){

            }else {
                if(!tvforsupportrelatedValue?.text.toString().trim().isNullOrEmpty()){
                    sendEmail(tvforsupportrelatedValue?.text.toString().trim(),"Hello There","Add Message here")
                }
            }
        }
        tvTalkwithValue.setOnClickListener {

            if(isPermissionGranted()){
                call_action()
            }
        }*/

        /*if (MyUtils.internetConnectionCheck(mActivity!!)) {
            getUserSettingsAPI()
        } else {
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                rootContactUsLayout,
                mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
            )
        }*/

    }

    private fun webData(){

        val settings = webView.settings
        // Enable java script in web view
        settings.javaScriptEnabled = true
        // Enable zooming in web view
        settings.setSupportZoom(false)
        settings.builtInZoomControls = false
        settings.displayZoomControls = false
        settings.setDefaultFontSize(40);
        // Zoom web view text
//        settings.textZoom = 125
        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true
        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false

        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        webView.fitsSystemWindows = true
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY

        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        // Set web view client
        webView.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
            }
        }
        //  Case 2 .. Create your own html page...


    }

    private fun commonMethod(){

        if (MyUtils.internetConnectionCheck(mActivity!!) == true) {
            getCMSPages()

//            var mHtmlText: String =
//                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
//
//            webView.loadDataWithBaseURL(null, mHtmlText, "text/HTML", "UTF-8", null)
        } else {

            relativeprogressBar?.visibility = View.GONE
            llMainTermconfitions?.visibility = View.GONE

            nointernetImageview.visibility = View.VISIBLE
            nointernetMainRelativelayout?.visibility = View.VISIBLE
            noDatafoundRelativelayout?.visibility = View.GONE
            webViewMainRelativeLayout?.visibility = View.GONE
            nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
            nointernettextview1.text = (this.getString(R.string.internetmsg1))
            nointernettextview.text = (getString(R.string.error_common_network))
        }

    }

    private fun getCMSPages() {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
// cmspage/get-cms
//            [{
//                "cmspageConstantCode": "000001",
//                "cmspageFor": "Customer",
//                "apiType": "Android",
//                "apiVersion": "1.0"
//            }]
            jsonObject.put("cmspageConstantCode", WhichCMSPage)
            jsonObject.put("cmspageFor", "Customer")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.d("System out","Terms api call := "+jsonArray.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        relativeprogressBar?.visibility = View.VISIBLE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        llMainTermconfitions?.visibility = View.GONE

        var getCmspageModel = ViewModelProviders.of(this@ContactUsFragment).get(GetCmspageModel::class.java)
        getCmspageModel.getCMSPage(mActivity!!, false,jsonArray.toString()).observe(this@ContactUsFragment,
            Observer<List<GetCmspagePojo>> { cmspagePoJos ->
                if (cmspagePoJos != null) {

                    if (cmspagePoJos[0].status.equals("true",true)) {
                        relativeprogressBar?.visibility = View.GONE
                        llMainTermconfitions?.visibility = View.VISIBLE
                        webViewMainRelativeLayout?.visibility = View.VISIBLE

                        webView.loadDataWithBaseURL(null, cmspagePoJos!![0]!!.data!![0]!!.cmspageContents!!,"text/HTML", "UTF-8", null)
                    } else {
                        noDatafoundRelativelayout?.visibility = View.VISIBLE
//                        nodatafound()
                    }
                } else {
                    nodatafound()
                }
            })

    }

    private fun nodatafound() {

        try {
            relativeprogressBar?.visibility = View.GONE
            llMainTermconfitions?.visibility = View.GONE
            webViewMainRelativeLayout?.visibility = View.GONE
            nointernetMainRelativelayout?.visibility = View.VISIBLE
            noDatafoundRelativelayout?.visibility = View.GONE
            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }







    }

    fun call_action() {
        val phnum =  mGetUserSettingsPojo?.settingsSupportMobile
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:$phnum")
        startActivity(callIntent)
    }


    private fun getUserSettingsAPI() {
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            // settings/get-user-settings
            /*[{
                "loginuserID": "1",
                "languageID": "1",
                "apiType": "Android",
                "apiVersion": "1.0"
            }]*/
            jsonObject.put("loginuserID", userId)
            jsonObject.put("languageID", "1")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System out", "get-user-settings api call := " + jsonArray.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var getUserSettingsModel = ViewModelProviders.of(this@ContactUsFragment).get(GetUserSettingsModel::class.java)
        getUserSettingsModel.UserSettingsApi(mActivity!!, true, jsonArray.toString())
            .observe(this@ContactUsFragment, Observer<List<GetUserSettingsPojo>> { changePwdPojo ->

                if (changePwdPojo != null) {
                    mGetUserSettingslist.clear()
                    if (changePwdPojo[0].status.equals("true", true)) {
//                        MyUtils.showSnackbarkotlin(mActivity!!, rootContactUsLayout, changePwdPojo[0].message!!)
                        if(!changePwdPojo[0].data.isNullOrEmpty()){

                            mGetUserSettingslist.addAll(changePwdPojo[0].data!!)

                            if(!mGetUserSettingslist.isNullOrEmpty()){
//                                mGetUserSettingsPojo = mGetUserSettingslist[0]
//                                setContactUsData(mGetUserSettingsPojo)
                            }

                        }

                    } else {
                        if(!changePwdPojo[0].message!!.isNullOrEmpty()){
                            MyUtils.showSnackbarkotlin(mActivity!!, rootHelpLayout, changePwdPojo[0].message!!)
                        }

                    }
                } else {
                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            rootHelpLayout,
                            resources.getString(R.string.error_crash_error_message)
                        )
                    } else {
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            rootHelpLayout,
                            resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                        )
                    }
                }
            })

    }

    fun setContactUsData(mGetUserSettingsPojo: RegisterNewPojo.Datum.Setting?){

        if(!mGetUserSettingsPojo?.settingsSupportEmail.isNullOrEmpty()){
//            tvforsupportrelatedValue?.text = "" + mGetUserSettingsPojo?.settingsSupportEmail
        }else {
//            tvforsupportrelatedValue?.text = "--"
        }

        if(!mGetUserSettingsPojo?.settingsSupportMobile.isNullOrEmpty()){
//            tvTalkwithValue?.text = ""+mGetUserSettingsPojo?.settingsSupportMobile
        }else {
//            tvTalkwithValue?.text = "--"
        }

    }

    private fun sendEmail(recipient: String, subject: String, message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)

        try {
            //start email intent
            mActivity!!.startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        }catch (ex : android.content.ActivityNotFoundException) {
            Toast.makeText(mActivity!!,
                "No email clients installed.",
                Toast.LENGTH_SHORT).show();
        }catch (e: Exception){
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(mActivity!!, e.message, Toast.LENGTH_LONG).show()
        }

    }

    private fun isPermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(mActivity!!,android.Manifest.permission.CALL_PHONE) === PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted")
                return true
            } else {

                Log.v("TAG", "Permission is revoked")
                ActivityCompat.requestPermissions(   mActivity!!, arrayOf(Manifest.permission.CALL_PHONE), 1)
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted")
            return true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {

            1 -> {

                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    call_action()
                } else {
                    Toast.makeText(mActivity!!, "Permission denied", Toast.LENGTH_SHORT)
                        .show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }


}

//[
//{
//    "data": [
//    {
//                        "settingsSupportEmail": "support@flymyowncustomer.com",
//                        "settingsSupportMobile": "8787878787",
//                        "settingsPaymentUrl": "http://13.234.187.191/frontend/web/payment.php?txn=",
//                        "settingsPaymentSuccessUrl": "http://13.234.187.191/frontend/web/paymentsuccess.php?id=",
//                        "settingsPaymentErrorUrl": "http://13.234.187.191/frontend/web/error.php?msg=",
//                        "settingsGSTOnFabric": "5.00",
//                        "settingsGSTOnStitchingservice": "5.00",
//                        "settingsGSTOnReadymadeGarments": "5.00",
//                        "settingsGSTOnOtherService": "5.00",
//                        "settingsMoneyToBeUsedFromWalletForPayment": "0.00",
//                        "settingsMinimumOrderValue": "1.00"
//    }
//    ],
//    "status": "true",
//    "message": ""
//}
//]
