package com.fil.flymyowncustomer.fragment

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.fragment_add_amount.*
import kotlinx.android.synthetic.main.toolbar_back.*

// TODO: Rename parameter arguments, choose names that match



class AddAmountFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_add_amount, container, false)

        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
        (activity as MainActivity).setDrawerSwipe(false)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        tvToolbarTitel.text=resources.getString(R.string.add_amount)


        btnadd_amount.setOnClickListener {
            MyUtils.hideKeyboard1(mActivity!!)
            if(add_amount_edit_text.text.toString().trim().isNullOrEmpty())
            {
                (activity as MainActivity).showSnackBar(resources.getString(R.string.please_enter_amount))
            }
            else{
                onDetach()
                val paymentFragment=PaymentFragment()

                Bundle().apply {
                    putDouble("amount",add_amount_edit_text.text.toString().toDouble())
                    putString("from","add_amount" )
                    paymentFragment.arguments=this

                }
                (mActivity as MainActivity).navigateTo(
                    paymentFragment,
                    paymentFragment::class.java.name,
                    true
                )
            }

        }
    }





    override fun onDetach() {
        super.onDetach()
    }


}
