package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler

import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.OfferImagesSliderAdapter
import com.fil.flymyowncustomer.adapter.RecentViewedAdapter
import com.fil.flymyowncustomer.adapter.StichingStudioHomeAdapter
import com.fil.flymyowncustomer.adapter.StoreStudioHomeAdapter
import com.fil.flymyowncustomer.model.DealerFavouriteViewModel
import com.fil.flymyowncustomer.model.StitchingStoreFavouriteViewModel
import com.fil.flymyowncustomer.model.UserhomeModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.BottomNavigationViewBehavior
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PrefDb
import com.fil.flymyowncustomer.util.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : Fragment() {
    var mActivity: AppCompatActivity? = null
    private var v: View? = null
    var page_position = 0
    private var dotsCount: Int = 0
    private lateinit var dots: Array<ImageView?>
    var mOfferImagesSliderAdapter :OfferImagesSliderAdapter?= null
    var mStichingStudioHomeAdapter : StichingStudioHomeAdapter?= null
    var mStoreStudioHomeAdapter : StoreStudioHomeAdapter?= null
    var mRecentViewedAdapter : RecentViewedAdapter?= null
    private  var linearLayoutManager: LinearLayoutManager?=null
    private  var linearLayoutManagerStore: LinearLayoutManager?=null
    private  var linearLayoutManagerRecentViewed: LinearLayoutManager?=null
    private var userHomePojo :ArrayList<UserHomePojo?>?=null

    private var banner:ArrayList<Banner?>?=null
    private var productviews : ArrayList<ProductListPojo.Data?>?=null
    private var stitchingstudio :ArrayList<Stitchingstudio?>?=null
    private var stichingStore : ArrayList<Stitchingstudio?>?=null

    var userHomeModel: UserhomeModel? = null
    var sessionManager:SessionManager?=null
    public var menuNotification: AppCompatImageView?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_main, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar!!)
        (activity as MainActivity).updateNavigationBarState(R.id.menu_home)
        menuNotification = v?.findViewById<AppCompatImageView>(R.id.menuNotification)
          toolbar!!.visibility=View.VISIBLE
          toolbar_title!!.visibility = View.GONE
          menuNotification?.visibility=View.GONE
          toolbar_Imageview!!.visibility = View.VISIBLE
          setNotificationIcon(0)
//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.VISIBLE
      /*  (mActivity as MainActivity).handleSelection()
        (mActivity as MainActivity).selectBottomNavigationOption(0)*/
        (mActivity as MainActivity).setDrawerSwipe(true)
        BottomNavigationViewBehavior().slideUp((activity as MainActivity).bottom_navigation)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        sessionManager=SessionManager(mActivity!!)
        menuNotification?.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(),NotificationFragment::class.java.name,true)
        }
        if(userHomePojo.isNullOrEmpty())
        {
            userHomePojo= ArrayList()
            banner= ArrayList()
            productviews= ArrayList()
            stitchingstudio= ArrayList()
            stichingStore=ArrayList()

            getHomeData(!userHomePojo.isNullOrEmpty())

        }
        else
        {

            try {
                if(userHomePojo!![0]!!.banner.isNullOrEmpty())
                {
                    noBannerAvailableTv.visibility = View.VISIBLE
                    viewPager_main_fragment.visibility = View.GONE
                    layoutDotsTutorial.visibility = View.GONE
                }
                else
                {
                    noBannerAvailableTv.visibility = View.GONE
                    viewPager_main_fragment.visibility = View.VISIBLE
                    layoutDotsTutorial.visibility = View.VISIBLE
                    OfferAdapter(userHomePojo!![0]!!.banner)
                }
                if(userHomePojo!![0]!!.productviews.isNullOrEmpty())
                {
                    noProductViewAvailableTv.visibility = View.VISIBLE
                    rv_main_receentViewed.visibility = View.INVISIBLE
                }
                else
                {
                    RecentViewdAdapter(userHomePojo!![0]!!.productviews)


                    noProductViewAvailableTv.visibility = View.GONE
                    rv_main_receentViewed.visibility = View.VISIBLE
                    tv_main_recent.visibility=View.VISIBLE
                }
                 if(userHomePojo!![0]!!.stitchingstudio.isNullOrEmpty())
                 {
                     tv_main_studio.visibility = View.GONE
                     noStichingStudioAvailableTv.visibility = View.GONE
                     rv_main_Stiching_studio.visibility=View.GONE
                     tv_main_studio_view_all.visibility=View.GONE
                 }
                else
                 {
                     StichingStudioAdapter(userHomePojo!![0]!!.stitchingstudio)

                     tv_main_studio.visibility = View.VISIBLE
                     noStichingStudioAvailableTv.visibility = View.GONE
                     rv_main_Stiching_studio.visibility=View.VISIBLE
                     tv_main_studio_view_all.visibility=View.VISIBLE
                 }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_main_lifestylestore?.setOnClickListener {
           var lifestyleStoreListFragment= LifestyleStoreListingFragment()
                Bundle().apply {
                    this.putString("from","Stitching Store")
                    lifestyleStoreListFragment.arguments=this

                }
            (mActivity as MainActivity).navigateTo(lifestyleStoreListFragment, lifestyleStoreListFragment::class.java!!.getName(), true)

        }

        tv_main_LifeStylestudio_view_all.setOnClickListener {
            var lifestyleStoreListFragment= LifestyleStoreListingFragment()
            Bundle().apply {
                this.putString("from","Stitching Store")
                lifestyleStoreListFragment.arguments=this

            }
            (mActivity as MainActivity).navigateTo(lifestyleStoreListFragment, lifestyleStoreListFragment::class.java!!.getName(), true)

        }

        tv_main_studio_view_all.setOnClickListener {
            var lifestyleStoreListFragment= LifestyleStoreListingFragment()
            Bundle().apply {
                this.putString("from","Stitching Studio")
                
                lifestyleStoreListFragment.arguments=this

            }
            (mActivity as MainActivity).navigateTo(lifestyleStoreListFragment, lifestyleStoreListFragment::class.java!!.getName(), true)

        }

        btnRetry.setOnClickListener {
            getHomeData(!userHomePojo.isNullOrEmpty())
        }
    }

    fun setNotificationIcon(type : Int){

        if(type == 0){
            menuNotification?.setImageResource(R.drawable.notification_icon)
        }else {
            menuNotification?.setImageResource(R.drawable.notification_with_red_dot)
        }
    }

    private fun getHomeData(isRefreshData: Boolean) {


        if (!isRefreshData) {
            relativeprogressBar.visibility = View.VISIBLE
            nointernetMainRelativelayout.visibility = View.GONE
            mainLayoutMain.visibility = View.GONE
        }
        userHomeModel = ViewModelProviders.of(this@MainFragment).get(UserhomeModel::class.java)
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()

        jsonObject.put("loginuserID", sessionManager!!.get_Authenticate_User().userID)
        jsonObject.put("languageID", sessionManager!!.get_Authenticate_User().languageID)
        jsonObject.put("userLatitude", ""+ PrefDb(mActivity!!).getString("LOCATIONLATE"))
        jsonObject.put("userLongitude", ""+ PrefDb(mActivity!!).getString("LOCATIONLONG"))
        jsonObject.put("apiType", RestClient.apiType)
        jsonObject.put("apiVersion", RestClient.apiVersion)

        jsonArray.put(jsonObject)
        userHomeModel?.getUserHome(mActivity!!, false, jsonArray.toString())?.observe(this,
            androidx.lifecycle.Observer { languageLiatPojo ->
                if (languageLiatPojo != null && languageLiatPojo.isNotEmpty())
                {
                    relativeprogressBar.visibility = View.GONE
                    if (mainLayoutMain.visibility != View.VISIBLE)
                        mainLayoutMain.visibility = View.VISIBLE
                    if (languageLiatPojo[0].status.equals("true", true)) {
                        userHomePojo!!.clear()
                        userHomePojo!!.addAll(languageLiatPojo)

                        banner!!.clear()
                        productviews!!.clear()
                        stitchingstudio!!.clear()
                        stichingStore!!.clear()

                        banner!!.addAll(languageLiatPojo[0].banner!!)
                        productviews!!.addAll(languageLiatPojo[0].productviews!!)
                        stitchingstudio!!.addAll(languageLiatPojo[0].stitchingstudio!!)
                        stichingStore!!.addAll(languageLiatPojo[0].stitchingstore!!)

                        if (productviews.isNullOrEmpty()) {
                            noProductViewAvailableTv.visibility = View.VISIBLE
                            rv_main_receentViewed.visibility = View.INVISIBLE
                        } else {
                            noProductViewAvailableTv.visibility = View.GONE
                            rv_main_receentViewed.visibility = View.VISIBLE
                            tv_main_recent.visibility=View.VISIBLE
                            RecentViewdAdapter(userHomePojo!![0]!!.productviews)
                        }
                        if (banner.isNullOrEmpty()) {
                            noBannerAvailableTv.visibility = View.VISIBLE
                            viewPager_main_fragment.visibility = View.GONE
                            layoutDotsTutorial.visibility = View.GONE

                        } else {
                            noBannerAvailableTv.visibility = View.GONE
                            viewPager_main_fragment.visibility = View.VISIBLE
                            layoutDotsTutorial.visibility=View.VISIBLE
                            OfferAdapter(banner)
                        }

                        if (stitchingstudio.isNullOrEmpty()) {
                            tv_main_studio.visibility = View.GONE
                            noStichingStudioAvailableTv.visibility = View.GONE
                            rv_main_Stiching_studio.visibility=View.GONE
                            tv_main_studio_view_all.visibility=View.GONE
                        } else {
                            tv_main_studio.visibility = View.VISIBLE
                            noStichingStudioAvailableTv.visibility = View.GONE
                            rv_main_Stiching_studio.visibility=View.VISIBLE
                            tv_main_studio_view_all.visibility=View.VISIBLE
                            StichingStudioAdapter(stitchingstudio)
                        }

                        relativelifeStyleStore.visibility = View.GONE
                        if (stichingStore.isNullOrEmpty()) {
                            tv_main_LifeStylestudio.visibility = View.GONE
                            nLifeStyleStudioAvailableTv.visibility = View.VISIBLE
                            tv_main_LifeStylestudio_view_all.visibility = View.GONE
                            btn_main_lifestylestore.visibility = View.GONE
                            btn_main_lifestylestore.text=resources.getString(R.string.no_data_found)
                            tv_main_studiobackground.visibility = View.GONE

                        } else {
                            tv_main_LifeStylestudio.visibility = View.VISIBLE
                            nLifeStyleStudioAvailableTv.visibility = View.GONE
                            tv_main_LifeStylestudio_view_all.visibility = View.VISIBLE
                            btn_main_lifestylestore.visibility = View.GONE
                            tv_main_studiobackground.visibility = View.GONE
                            btn_main_lifestylestore.text=resources.getString(R.string.lifestore)
                            tv_main_studiobackground.visibility = View.GONE
//                            if(!languageLiatPojo[0].stitchingstore!![0]!!.bannerImages.isNullOrEmpty())
//                            {
//                                tv_main_studiobackground.setImageURI(languageLiatPojo[0].stitchingstore!![0]!!.bannerImages!![0]!!.dealerimageName)
//
//                            }
                            StitchingStore(stichingStore)
                        }
                    } else {
                        (activity as MainActivity).showSnackBar(languageLiatPojo[0].message!!)
                    }


                } else {
                    if (mActivity != null) {

                        try {
                            nointernetMainRelativelayout.visibility = View.VISIBLE
                            relativeprogressBar.visibility = View.GONE
                            mainLayoutMain.visibility = View.GONE

                            if (MyUtils.isInternetAvailable(activity!!)) {
                                nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.something_went_wrong))
                                nointernettextview.text = (activity!!.getString(R.string.error_something))
                                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
                            } else {
                                nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.no_internet_connection))
                                nointernettextview1.text = (this.getString(R.string.internetmsg1))


                                nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }
            })

    }

    fun OfferAdapter(banner: List<Banner?>?) {
        mOfferImagesSliderAdapter = OfferImagesSliderAdapter(mActivity as MainActivity,banner)
        viewPager_main_fragment.clipToPadding = false
        viewPager_main_fragment.setPadding(35, 0, 35, 0)
        viewPager_main_fragment?.adapter = mOfferImagesSliderAdapter
        viewPager_main_fragment?.addOnPageChangeListener(viewPagerChangeListener)
//        if (prodimgNameStr!!.size > 1) {
            addBottomDots(0)
            sliderBanner()
//        } else {
//            addBottomDots(0)
//        }
    }

    fun RecentViewdAdapter(productviews: List<ProductListPojo.Data?>?) {
        linearLayoutManagerRecentViewed = LinearLayoutManager(mActivity!!, LinearLayoutManager.HORIZONTAL, false)
        rv_main_receentViewed?.layoutManager = linearLayoutManagerRecentViewed
        mRecentViewedAdapter = RecentViewedAdapter(mActivity!!, object : RecentViewedAdapter.onItemClickk {

            override fun onClicklisneter(pos: Int) {
                var productDetailsFragment= ProductDetailsFragment()
                Bundle().apply {
                    this.putString("from","Stitching Store")
                    this.putSerializable("productListData",productviews!![pos]!!)
                    productDetailsFragment.arguments=this

                }
                (activity as MainActivity).navigateTo(productDetailsFragment, productDetailsFragment::class.java.name, true)
            }

        },productviews)
        rv_main_receentViewed?.isNestedScrollingEnabled = false
        rv_main_receentViewed?.setHasFixedSize(true)
        rv_main_receentViewed?.adapter = mRecentViewedAdapter

    }

    fun StitchingStore(stichingStore : ArrayList<Stitchingstudio?>?) {
        linearLayoutManagerStore = LinearLayoutManager(mActivity!!, LinearLayoutManager.HORIZONTAL, false)
        rv_main_LifeStyle_studio?.layoutManager = linearLayoutManagerStore

        mStoreStudioHomeAdapter = StoreStudioHomeAdapter(mActivity!!, object : StoreStudioHomeAdapter.onItemClickk {

            override fun onClicklisneter(pos: Int,from:String) {
                when(from){
                    "Click"->{
                        var lifestyleStoredetailsFragment= LifestyleStoredetailsFragment()
                        Bundle().apply {
                            this.putString("from","Stitching Store")
                            this.putSerializable("dealerListData",stichingStore!![pos]!!)
                            lifestyleStoredetailsFragment.arguments=this

                        }
                        (activity as MainActivity).navigateTo(lifestyleStoredetailsFragment, LifestyleStoredetailsFragment::class.java.name, true)

                    }
                    "fav"->{
                        getAddProductStoreStudio(pos,stichingStore)
                    }
                }

            }
        },stichingStore)

        rv_main_LifeStyle_studio?.isNestedScrollingEnabled = false
        rv_main_LifeStyle_studio?.setHasFixedSize(true)
        rv_main_LifeStyle_studio?.adapter = mStoreStudioHomeAdapter


    }

    fun StichingStudioAdapter(stitchingstudio: List<Stitchingstudio?>?) {
        linearLayoutManager = LinearLayoutManager(mActivity!!, LinearLayoutManager.HORIZONTAL, false)
        rv_main_Stiching_studio?.layoutManager = linearLayoutManager

        mStichingStudioHomeAdapter = StichingStudioHomeAdapter(mActivity!!, object : StichingStudioHomeAdapter.onItemClickk {

            override fun onClicklisneter(pos: Int,from:String) {
                when(from){
                    "Click"->{
                        var lifestyleStoredetailsFragment= LifestyleStoredetailsFragment()
                        Bundle().apply {
                            this.putString("from","Stitching Studio")
                            this.putSerializable("dealerListData",stitchingstudio!![pos]!!)
                            lifestyleStoredetailsFragment.arguments=this

                        }
                        (activity as MainActivity).navigateTo(lifestyleStoredetailsFragment, LifestyleStoredetailsFragment::class.java.name, true)

                    }
                    "fav"->{
                        getAddProduct(pos,stitchingstudio)
                    }
                }

            }

        },stitchingstudio)

        rv_main_Stiching_studio?.isNestedScrollingEnabled = false
        rv_main_Stiching_studio?.setHasFixedSize(true)
        rv_main_Stiching_studio?.adapter = mStichingStudioHomeAdapter
    }

    fun addBottomDots(currentPage: Int) {
        dotsCount = banner!!.size

        layoutDotsTutorial?.removeAllViews()
        dots = arrayOfNulls(dotsCount)

        for (i in 0 until dotsCount) {

            dots[i] = ImageView(mActivity)
            dots[i]?.setImageResource(R.drawable.shape_deselected_dot51)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            dots[i]?.setPadding(0, 0, 0, 0)
            dots[i]?.layoutParams = params
            layoutDotsTutorial?.addView(dots[i]!!, params)
            layoutDotsTutorial?.bringToFront()

        }
        if (dots.size > 0 && dots.size > currentPage) {
            dots[currentPage]?.setImageResource(R.drawable.shape_selected_dot)
        }

    }

    fun sliderBanner() {
        val handler = Handler()
        val update = Runnable {
            try {
//                if (images != null && images!!.size > 0) {
                if (banner!!.size > 0) {
                    if (page_position == banner!!.size ) {
                        page_position = 0

                    } else {
                        page_position = page_position + 1
                    }
                    viewPager_main_fragment?.setCurrentItem(page_position, true)
                }
            } catch (e: NullPointerException) {

            }
        }
        Timer().schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 15000)
    }

    internal var viewPagerChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            page_position = position
        }

        override fun onPageSelected(position: Int) {
            page_position = position
            addBottomDots(position)

        }
    }

    private fun getAddProductStoreStudio(pos: Int,stichingStore : ArrayList<Stitchingstudio?>?) {

        if (stichingStore!![pos]!!.isFavourite.equals("No", false)) {
            stichingStore!![pos]!!.isFavourite = "Yes"
            setAddFavouriteStore(stichingStore!![pos]!!.dealerID, pos, "AddFav",stichingStore)
        } else {
            stichingStore!![pos]!!.isFavourite = "No"
            setAddFavouriteStore(stichingStore!![pos]!!.dealerID, pos, "RemoveFav",stichingStore)
        }

    }

    private fun setAddFavouriteStore(productID: String?, pos: Int, type: String,stitchingstudio: List<Stitchingstudio?>?) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@MainFragment).get(
            DealerFavouriteViewModel::class.java)
        addFavDoctorModel.apiFunction(mActivity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@MainFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                stitchingstudio!![pos]!!.isFavourite = "Yes"
                                // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                stitchingstudio!![pos]!!.isFavourite = "No"

                                // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            mStoreStudioHomeAdapter!!.notifyDataSetChanged()
                        }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    private fun getAddProduct(pos: Int,stitchingstudio: List<Stitchingstudio?>?) {

        if (stitchingstudio!![pos]!!.isFavourite.equals("No", false)) {
            stitchingstudio!![pos]!!.isFavourite = "Yes"
            setAddFavourite(stitchingstudio!![pos]!!.dealerID, pos, "AddFav",stitchingstudio)
        } else {
            stitchingstudio!![pos]!!.isFavourite = "No"
            setAddFavourite(stitchingstudio!![pos]!!.dealerID, pos, "RemoveFav",stitchingstudio)
        }

    }



    private fun setAddFavourite(productID: String?, pos: Int, type: String,stitchingstudio: List<Stitchingstudio?>?) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@MainFragment).get(
            DealerFavouriteViewModel::class.java)
        addFavDoctorModel.apiFunction(mActivity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@MainFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                stitchingstudio!![pos]!!.isFavourite = "Yes"
                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                stitchingstudio!![pos]!!.isFavourite = "No"

                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            mStichingStudioHomeAdapter!!.notifyDataSetChanged()
                        }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }
}
