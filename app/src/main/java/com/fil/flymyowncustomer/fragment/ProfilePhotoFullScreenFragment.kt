package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile_photo.*


import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList




/**
 * A simple [Fragment] subclass.
 */
class ProfilePhotoFullScreenFragment : Fragment() {


    private var isLastpage = false
    private var userId = ""
    private var action = ""
    var mActivity: AppCompatActivity? = null
    private var v: View? = null
     var profileImg:String?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_profile_photo, container, false)
        }
        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        MyUtils.changeStatusBarColor(mActivity!!, R.color.black)

        if(arguments!=null)
        {
            profileImg=arguments?.getString("profileImg")
            svProfileImage.setImageURI(profileImg)
        }
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        ivClose.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

    }





}// Required empty public constructor
