package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ReviewListAdapter
import com.fil.flymyowncustomer.adapter.StitchingserviceReviewListAdapter
import com.fil.flymyowncustomer.model.ProductReviewListModel
import com.fil.flymyowncustomer.model.StitchingServiceReviewListModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_review_list.*
import kotlinx.android.synthetic.main.item_review_adapter.view.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class ReviewListFragment : Fragment() {

    var reviewListAdapter1: ReviewListAdapter? = null
    var stitchingserviceReviewListAdapter: StitchingserviceReviewListAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var v: View? = null
    var mActivity: Activity? = null
    var productListData: ProductListPojo.Data? = null
    var stichingServicData: StichingServicePojo.StichingServicData? = null

    var productReviewList: ArrayList<ProductReviewListData?>?=ArrayList()
    var stitchingServiesDataList: ArrayList<StitchingServiesData?>?=ArrayList()
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var sessionManager:SessionManager?=null
    var from=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_review_list, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.reviews)

        tvRemoveitemCart.visibility=View.GONE
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        sessionManager= SessionManager(mActivity!!)
        if (arguments != null) {
            from=arguments?.getString("from")!!
            if(from.equals("product",false))
            {
                productListData =
                    arguments!!.getSerializable("productListData") as ProductListPojo.Data?

            }
            else if(from.equals("stiching",false))
            {
                stichingServicData =
                    arguments!!.getSerializable("productListData") as StichingServicePojo.StichingServicData?
            }

        }

        if(from.equals("product",false)) {
            if (productListData != null) {
                tvOverallRating.text = productListData?.productRatting
                ratingBarYourRating.rating=productListData?.productRatting?.toFloat()!!
                tvReviewsCount.text=  "${productListData!!.productReviewRattingCount} Reviews"
                tvBasedonReviewstitle.text =
                    "Based on ${productListData!!.productReviewRattingCount} reviews"
                if (!productListData?.productratting.isNullOrEmpty()) {
                    if (!productListData?.productratting?.get(0)?.Excellent.isNullOrEmpty()) {
                        progressBar.progress =
                            productListData?.productratting?.get(0)?.Excellent?.toInt()!!
                        tvExcellentReviews.text="Excellent(${productListData?.productratting?.get(0)?.Excellent})"
                    }
                    if (!productListData?.productratting?.get(0)?.Good.isNullOrEmpty()) {
                        goodprogressBar.progress =
                            productListData?.productratting?.get(0)?.Good?.toInt()!!
                        tvgoodReviews.text="Good(${productListData?.productratting?.get(0)?.Good})"

                    }
                    if (!productListData?.productratting?.get(0)?.Average.isNullOrEmpty()) {
                        avrageProgressBar.progress =
                            productListData?.productratting?.get(0)?.Average?.toInt()!!
                        tvAverageReviews.text="Average(${productListData?.productratting?.get(0)?.Average?.toInt()})"

                    }
                    if (!productListData?.productratting?.get(0)?.BelowAverage.isNullOrEmpty()) {
                        belowAverageprogressBar.progress =
                            productListData?.productratting?.get(0)?.BelowAverage?.toInt()!!
                        tvBelowAverageReviews.text="Below Average(${productListData?.productratting?.get(0)?.BelowAverage?.toInt()})"

                    }
                    if (!productListData?.productratting?.get(0)?.Poor.isNullOrEmpty()) {
                        poorprogressBar.progress =
                            productListData?.productratting?.get(0)?.Poor?.toInt()!!
                        tvPoorReviews.text="Poor(${productListData?.productratting?.get(0)?.Poor?.toInt()})"


                    }
                }

            }
            noDatafoundRelativelayout.visibility=View.GONE
            relativeprogressBar.visibility=View.GONE
            nointernetMainRelativelayout.visibility=View.GONE
            productReviewList=ArrayList()
            linearLayoutManager = LinearLayoutManager(mActivity)
            reviewListAdapter1 = ReviewListAdapter(
                mActivity!!,
                object : ReviewListAdapter.OnItemClick {
                    override fun onClicklisneter(pos: Int, name: String) {
                        when(name){

                        }
                    }
                }
                ,productReviewList)
            notificationRecyclerview.layoutManager = linearLayoutManager
            notificationRecyclerview.adapter = reviewListAdapter1
            if (productReviewList.isNullOrEmpty() || pageNo == 0) {
                pageNo = 0
                isLastpage = false
                isLoading = false
                getReviewList(productListData?.productID!!)
            }
        }else if(from.equals("stiching",false))
        {
            if (stichingServicData != null) {
                tvOverallRating.text = stichingServicData?.stitchingserviceRatting
                tvBasedonReviewstitle.text =
                    "Based on ${stichingServicData?.stitchingserviceReviewRattingCount} reviews"

                ratingBarYourRating.rating=stichingServicData?.stitchingserviceRatting?.toFloat()!!
                tvReviewsCount.text=  "${stichingServicData!!.stitchingserviceReviewRattingCount} Reviews"


               if (!stichingServicData?.stitchingratting.isNullOrEmpty()) {
                    if (!stichingServicData?.stitchingratting?.get(0)?.Excellent.isNullOrEmpty()) {
                        progressBar.progress =
                            stichingServicData?.stitchingratting?.get(0)?.Excellent?.toInt()!!
                        tvExcellentReviews.text="Excellent(${  stichingServicData?.stitchingratting?.get(0)?.Excellent})"

                    }
                    if (!stichingServicData?.stitchingratting?.get(0)?.Good.isNullOrEmpty()) {
                        goodprogressBar.progress =
                            stichingServicData?.stitchingratting?.get(0)?.Good?.toInt()!!
                        tvgoodReviews.text="Good(${  stichingServicData?.stitchingratting?.get(0)?.Good})"

                    }
                    if (!stichingServicData?.stitchingratting?.get(0)?.Average.isNullOrEmpty()) {
                        avrageProgressBar.progress =
                            stichingServicData?.stitchingratting?.get(0)?.Average?.toInt()!!
                        tvAverageReviews.text="Average(${stichingServicData?.stitchingratting?.get(0)?.Average?.toInt()})"

                    }
                    if (!stichingServicData?.stitchingratting?.get(0)?.BelowAverage.isNullOrEmpty()) {
                        belowAverageprogressBar.progress =
                            stichingServicData?.stitchingratting?.get(0)?.BelowAverage?.toInt()!!
                        tvBelowAverageReviews.text="Below Average(${stichingServicData?.stitchingratting?.get(0)?.BelowAverage?.toInt()})"

                    }
                    if (!stichingServicData?.stitchingratting?.get(0)?.Poor.isNullOrEmpty()) {
                        poorprogressBar.progress =
                            stichingServicData?.stitchingratting?.get(0)?.Poor?.toInt()!!
                        tvPoorReviews.text="Poor(${stichingServicData?.stitchingratting?.get(0)?.Poor?.toInt()})"

                    }
                }
            }

            noDatafoundRelativelayout.visibility=View.GONE
            relativeprogressBar.visibility=View.GONE
            nointernetMainRelativelayout.visibility=View.GONE

            linearLayoutManager = LinearLayoutManager(mActivity)
            stitchingserviceReviewListAdapter = StitchingserviceReviewListAdapter(
                mActivity!!,
                object : StitchingserviceReviewListAdapter.OnItemClick {
                    override fun onClicklisneter(pos: Int, name: String) {
                        when(name){

                        }
                    }
                }
                ,stitchingServiesDataList)
            notificationRecyclerview.layoutManager = linearLayoutManager
            notificationRecyclerview.adapter = stitchingserviceReviewListAdapter


            if (stitchingServiesDataList.isNullOrEmpty() || pageNo == 0) {
                pageNo = 0
                isLastpage = false
                isLoading = false
                getStitchingReviewList(stichingServicData?.stitchingserviceID!!)
            }
        }


        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager?.childCount!!
                totalItemCount = linearLayoutManager?.itemCount!!
                firstVisibleItemPosition = linearLayoutManager?.findFirstVisibleItemPosition()!!
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10) {
                        isLoading = true
                        if(from.equals("product",false)) {
                            getReviewList(productListData?.productID!!)
                        }else if(from.equals("product",false))
                        {
                            getStitchingReviewList(stichingServicData?.stitchingserviceID!!)
                        }
                    }
                }
            }
        })


        btnRetry.setOnClickListener {
            pageNo = 0
            isLastpage = false
            isLoading = false
            getReviewList(productListData?.productID!!)
        }
    }

    private fun getStitchingReviewList(stitchingserviceID: String) {


        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            stitchingServiesDataList!!.clear()
            stitchingserviceReviewListAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            stitchingServiesDataList!!.add(null)
            stitchingserviceReviewListAdapter?.notifyItemInserted(stitchingServiesDataList!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("stitchingserviceID", stitchingserviceID)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var stitchingServiceReviewListModel =
            ViewModelProviders.of(this@ReviewListFragment).get(StitchingServiceReviewListModel::class.java)
        stitchingServiceReviewListModel.getStitchingServiceReviewList(mActivity!!, false, jsonArray.toString(),"")
            .observe(this@ReviewListFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE

                        if (pageNo > 0) {
                            stitchingServiesDataList!!.removeAt(stitchingServiesDataList!!.size - 1)
                            stitchingserviceReviewListAdapter?.notifyItemRemoved(stitchingServiesDataList!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                productReviewList!!.clear()

                            stitchingServiesDataList!!.addAll(masterPojo[0].data!!)
                            stitchingserviceReviewListAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (stitchingServiesDataList!!.isEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE


                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })
    }

    private fun getReviewList(productID:String) {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            productReviewList!!.clear()
            reviewListAdapter1?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            productReviewList!!.add(null)
            reviewListAdapter1?.notifyItemInserted(productReviewList!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("productID", productID)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var productReviewModel =
            ViewModelProviders.of(this@ReviewListFragment).get(ProductReviewListModel::class.java)
        productReviewModel.getProductReviewList(mActivity!!, false, jsonArray.toString(),"")
            .observe(this@ReviewListFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE

                        if (pageNo > 0) {
                            productReviewList!!.removeAt(productReviewList!!.size - 1)
                            reviewListAdapter1?.notifyItemRemoved(productReviewList!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                productReviewList!!.clear()

                            productReviewList!!.addAll(masterPojo[0].data!!)
                            reviewListAdapter1?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (productReviewList!!.isEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE


                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })
    }
    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!))
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            }
            else
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
