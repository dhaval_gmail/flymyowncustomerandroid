package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.FilterActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.LifestyleStudioListingAdapter
import com.fil.flymyowncustomer.model.DelearListModel
import com.fil.flymyowncustomer.model.MasterListModel
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.pojo.Stitchingstudio
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_lifestyle_store_listing.*
import kotlinx.android.synthetic.main.fragment_termsand_conditions.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import kotlinx.android.synthetic.main.webview.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.widget.RelativeLayout
import com.fil.flymyowncustomer.model.DealerFavouriteViewModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.util.PrefDb
import kotlinx.android.synthetic.main.nointernetconnection.view.*


/**
 * A simple [Fragment] subclass.
 *
 */
class LifestyleStoreListingFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = LifestyleStoreListingFragment::class.java.name

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0

    var mLifestyleStudioListingAdapter : LifestyleStudioListingAdapter?= null
    var sessionManager: SessionManager? = null
    var masterData: ArrayList<MasterPojo?>?=null
    var dealerListData: ArrayList<Stitchingstudio?>?=null

    var colorsIDs2:String=""
    var materialsIDs2:String=""
    var patternsIDs2:String=""
    var stylesIDs2:String=""
    var brandsIDs2:String=""

    var maxValue2:String=""
    var minValue2:String=""
    var from1:String=""
    var filterby:String=""
    var sortBy:String=""
    var mSelection :Int = 0
    var mSelection1 :Int = 0
    var SearchKeyWord :String = ""
    var relativeprogressBar : RelativeLayout? = null
    var noDatafoundRelativelayout : RelativeLayout? = null
    var nointernetMainRelativelayout : RelativeLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_lifestyle_store_listing, container, false)
        }
        return v

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.menu_notification).isVisible = true
        super.onPrepareOptionsMenu(menu)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        toolbar_back.menuNotification.visibility=View.GONE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        toolbar_back.menuNotification.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(), NotificationFragment::class.java.name, true)

        }

        relativeprogressBar = view?.findViewById(R.id.relativeprogressBar) as RelativeLayout
        noDatafoundRelativelayout = view?.findViewById(R.id.noDatafoundRelativelayout) as RelativeLayout
        nointernetMainRelativelayout = view?.findViewById(R.id.nointernetMainRelativelayout) as RelativeLayout

        if(arguments!=null)
        {
            from1=arguments!!.getString("from")!!
        }
        if(from1.equals("Stitching Studio",false))
        {
            tvToolbarTitel.text = getString(R.string.stitching_studio)
            editSerchSales.hint = "Search stitching studio, city, location"
        }
        else if(from1.equals("Stitching Store",false))
        {
            tvToolbarTitel.text = getString(R.string.lifestyle_store)
            editSerchSales.hint = "Search store, city, location"

        }

        sessionManager=SessionManager(mActivity!!)
        masterData =ArrayList()

        linearLayoutManager = LinearLayoutManager(mActivity)
        notificationRecyclerview?.layoutManager = linearLayoutManager

        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        editSerchSales.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {


            }

            override fun afterTextChanged(s: Editable) {
                if(editSerchSales.isPressed)
                {
                    return
                }
                if (editSerchSales.text.toString().length > 0) {
                    img_clear_edit_text.visibility=View.VISIBLE
                    notificationRecyclerview.visibility = View.VISIBLE
                    SearchKeyWord = editSerchSales.text.toString()
                    pageNo=0
                    getDealerList(editSerchSales.text.toString(),"","")
                } else {
                    img_clear_edit_text.visibility=View.GONE
                    SearchKeyWord = ""
                    notificationRecyclerview.visibility = View.VISIBLE
                    relativeprogressBar?.visibility = View.GONE
                    pageNo=0
                    getDealerList("","","")

                }
            }
        })

        img_clear_edit_text.setOnClickListener {
            editSerchSales.setText("")
        }

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.childCount
                totalItemCount = linearLayoutManager.itemCount
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                            && totalItemCount >= 10) {

                        isLoading = true
                        getDealerList(SearchKeyWord,sortBy,filterby)
                    }
                }
            }
        })

        bindData()

        btnRetry?.setOnClickListener {
            pageNo=0
//            getDealerList("","","")
            getDealerList(SearchKeyWord,sortBy,filterby)
        }

        ImgFilter.setOnClickListener {
            openFilter()
        }

        tv_FilterText.setOnClickListener {
            openFilter()
        }

        ImgSort.setOnClickListener {
            openDialog()
        }

        tv_SortText.setOnClickListener {
            openDialog()
        }


    }

    private fun getDealerList(toString: String,sortby:String,filterby:String) {
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        ll_near_me_title.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            dealerListData!!.clear()
            mLifestyleStudioListingAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            dealerListData!!.add(null)
            mLifestyleStudioListingAdapter?.notifyItemInserted(dealerListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("searchkeyword", toString)
            jsonObject.put("userLatitude", ""+ PrefDb(mActivity!!).getString("LOCATIONLATE"))
            jsonObject.put("userLongitude", ""+ PrefDb(mActivity!!).getString("LOCATIONLONG"))
            jsonObject.put("dealerType",from1)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("sortby", sortby)
            jsonObject.put("filterby", filterby)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
//            Log.e("System out","Print With SearchKeyword json Api :"+ jsonArray.toString())

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var delearListModel =
            ViewModelProviders.of(this@LifestyleStoreListingFragment).get(DelearListModel::class.java)
        delearListModel.getDealer(mActivity!!, false, jsonArray.toString(),"")
            .observe(this@LifestyleStoreListingFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout?.visibility = View.GONE
                        nointernetMainRelativelayout?.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        notificationRecyclerview.visibility = (View.VISIBLE)
                        searchView?.visibility= View.VISIBLE
                        ll_near_me_title.visibility = View.VISIBLE

                        if (pageNo > 0) {
                            dealerListData!!.removeAt(dealerListData!!.size - 1)
                            mLifestyleStudioListingAdapter?.notifyItemRemoved(dealerListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                dealerListData!!.clear()

                            dealerListData!!.addAll(masterPojo[0].data!!)
                            mLifestyleStudioListingAdapter
                                ?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                        }

                        } else {
                            if (dealerListData!!.isEmpty()) {
                                noDatafoundRelativelayout?.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE
                                ll_near_me_title.visibility = View.GONE

                            } else {
                                noDatafoundRelativelayout?.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                                searchView?.visibility = View.VISIBLE
                                ll_near_me_title.visibility = View.VISIBLE
                            }
                        }

                    } else {

                        nodatafound()
                    }
                })
    }

    private fun bindData() {

       if (dealerListData == null) {
           dealerListData=ArrayList()

            mLifestyleStudioListingAdapter = LifestyleStudioListingAdapter(
              mActivity!!,
            object : LifestyleStudioListingAdapter.onItemClickk {
                override fun onClicklisneter(pos: Int,from:String) {
                    when(from)
                    {
                        "click"->{
                            MyUtils.hideKeyboard1(mActivity!!)
                            var lifestyleStoredetailsFragment= LifestyleStoredetailsFragment()
                            Bundle().apply {
                                this.putString("from",from1)
                                this.putSerializable("dealerListData",dealerListData!![pos]!!)
                                lifestyleStoredetailsFragment.arguments=this

                            }
                            (activity as MainActivity).navigateTo(lifestyleStoredetailsFragment, LifestyleStoredetailsFragment::class.java.name, true)

                        }
                        "fav"->{
                            getAddProduct(pos)
                        }
                    }
                       }

            },dealerListData!!)


        notificationRecyclerview?.isNestedScrollingEnabled = false
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview?.adapter = mLifestyleStudioListingAdapter
        getDealerList("","","")
       }
    }


    fun openFilter(data: List<MasterPojo?>?)
    {
       Intent(mActivity!!,FilterActivity::class.java).apply {
           this.putExtra("masterList",data as Serializable)
           this.putExtra("colorsIDs2",colorsIDs2)
           this.putExtra("materialsIDs2",materialsIDs2)
           this.putExtra("patternsIDs2",patternsIDs2)
           this.putExtra("stylesIDs2",stylesIDs2)
           this.putExtra("brandsIDs2",brandsIDs2)
           this.putExtra("maxValue2",maxValue2)
           this.putExtra("minValue2",minValue2)
           startActivityForResult(this,105)
       }
      mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //   super.onActivityResult(requestCode, resultCode, data)
        when(requestCode)
        {
            105->if(data!=null)
            {
                colorsIDs2=data.getStringExtra("colorsIDs1")
                brandsIDs2=data.getStringExtra("brandsIDs1")
                stylesIDs2=data.getStringExtra("stylesIDs1")
                patternsIDs2=data.getStringExtra("patternsIDs1")
                materialsIDs2=data.getStringExtra("materialsIDs1")
                maxValue2=data.getStringExtra("maxValue1")
                minValue2=data.getStringExtra("minValue1")

            }

        }




    }


    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            ll_near_me_title.visibility = View.GONE
            nointernetMainRelativelayout?.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun openDialog() {
        var list:ArrayList<String>
        list= ArrayList()
        list.clear()
        list.add("ASC")
        list.add("DESC")

        val cancelReasonFragment = CancelReasonFragment()
        val bundle = Bundle()
        bundle.putInt("mSelection", mSelection)
        bundle.putString("from", "Sort")
        bundle.putSerializable("list", list)
        cancelReasonFragment.arguments = bundle
        cancelReasonFragment.isCancelable = true
        cancelReasonFragment.show(childFragmentManager, "dialog")
        cancelReasonFragment.setListener(object :
            CancelReasonFragment.OnItemClickListener {
            override fun onSellerSelectionClick(position: Int, typeofOperation: String) {
                 mSelection = position
                 sortBy=typeofOperation
                 notificationRecyclerview.visibility = View.GONE
                 pageNo=0
                 getDealerList("",typeofOperation,filterby)
            }
        })

        /* val mBuilder = MaterialAlertDialogBuilder(mActivity)
         val mView = mActivity.getLayoutInflater().inflate(R.layout.cancel_orderdialog, null)
         val reoprt_edittext = mView.findViewById(R.id.reoprt_edittext) as EditText
         val report_textview = mView.findViewById(R.id.report_textview) as TextView
         val closedialog = mView.findViewById(R.id.closedialog) as ImageView
         val tvTitle = mView.findViewById(R.id.tv) as TextView


         val cancel = mView.findViewById(R.id.cancel_textview) as TextView
         if (statusID.equals("15", ignoreCase = true))
             tvTitle.setText(R.string.reason_return)
         val reMain = mView.findViewById(R.id.reMain) as RelativeLayout
         mBuilder.setView(mView)
         val dialog = mBuilder.create()

         cancel.setOnClickListener { MyUtils.dismissDialog(mActivity!!, dialog) }

         report_textview.setOnClickListener {
             if (!reoprt_edittext.text.toString().isEmpty()) {
                 MyUtils().dismissDialog(mActivity, dialog)
                 reopetforCancelOrder(
                     orderID,
                     statusID,
                     statusName,
                     reoprt_edittext.text.toString(),
                     pos
                 )
             } else {
                 MyUtils.showSnackbar(mActivity, "Please enter reason.", reMain)
             }
         }
         dialog.show()*/
    }

    fun openFilter() {
        var list:ArrayList<String>
        list= ArrayList()
        list.clear()
        list.add("Popular")
        list.add("Open")
        list.add("Closed")

        val cancelReasonFragment = CancelReasonFragment()
        val bundle = Bundle()
        bundle.putInt("mSelection", mSelection1)
        bundle.putString("from", "Filter")
        bundle.putSerializable("list", list)
        cancelReasonFragment.arguments = bundle
        cancelReasonFragment.isCancelable = true
        cancelReasonFragment.show(childFragmentManager, "dialog")
        cancelReasonFragment.setListener(object :
            CancelReasonFragment.OnItemClickListener {
            override fun onSellerSelectionClick(position: Int, typeofOperation: String) {
                 mSelection1 = position
                 filterby=typeofOperation
                 pageNo=0
                 notificationRecyclerview.visibility = View.GONE
                 getDealerList("",sortBy,typeofOperation)
            }
        })
        /* val mBuilder = MaterialAlertDialogBuilder(mActivity)
         val mView = mActivity.getLayoutInflater().inflate(R.layout.cancel_orderdialog, null)
         val reoprt_edittext = mView.findViewById(R.id.reoprt_edittext) as EditText
         val report_textview = mView.findViewById(R.id.report_textview) as TextView
         val closedialog = mView.findViewById(R.id.closedialog) as ImageView
         val tvTitle = mView.findViewById(R.id.tv) as TextView


         val cancel = mView.findViewById(R.id.cancel_textview) as TextView
         if (statusID.equals("15", ignoreCase = true))
             tvTitle.setText(R.string.reason_return)
         val reMain = mView.findViewById(R.id.reMain) as RelativeLayout
         mBuilder.setView(mView)
         val dialog = mBuilder.create()

         cancel.setOnClickListener { MyUtils.dismissDialog(mActivity!!, dialog) }

         report_textview.setOnClickListener {
             if (!reoprt_edittext.text.toString().isEmpty()) {
                 MyUtils().dismissDialog(mActivity, dialog)
                 reopetforCancelOrder(
                     orderID,
                     statusID,
                     statusName,
                     reoprt_edittext.text.toString(),
                     pos
                 )
             } else {
                 MyUtils.showSnackbar(mActivity, "Please enter reason.", reMain)
             }
         }
         dialog.show()*/
    }


    private fun getAddProduct(pos: Int) {

        if (dealerListData!![pos]!!.isFavourite.equals("No", false)) {
            dealerListData!![pos]!!.isFavourite = "Yes"
            setAddFavourite(dealerListData!![pos]!!.dealerID, pos, "AddFav")
        } else {
            dealerListData!![pos]!!.isFavourite = "No"
            setAddFavourite(dealerListData!![pos]!!.dealerID, pos, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, pos: Int, type: String) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@LifestyleStoreListingFragment).get(
            DealerFavouriteViewModel::class.java)
        addFavDoctorModel.apiFunction(mActivity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@LifestyleStoreListingFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                dealerListData!![pos]!!.isFavourite = "Yes"
                                //(activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                dealerListData!![pos]!!.isFavourite = "No"
                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            mLifestyleStudioListingAdapter?.notifyDataSetChanged()

                        }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }


}
