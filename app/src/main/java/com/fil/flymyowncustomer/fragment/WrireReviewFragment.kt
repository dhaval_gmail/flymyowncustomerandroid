package com.fil.flymyowncustomer.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.service.autofill.UserData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.AddAddressModel
import com.fil.flymyowncustomer.model.AddReviewModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import kotlinx.android.synthetic.main.fragment_write_review.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class WrireReviewFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = WrireReviewFragment::class.java.name
    var orderData: Orderdetail?=null
    var stitching: Stitching?=null


    var sessionManager:SessionManager?=null
    var userData: RegisterNewPojo.Datum ?=null
    var from=""
    var orderId=""
    var orderNo=""
    var widthNew:Int?=null
    var heightNew:Int?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_write_review, container, false)
        }

        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvToolbarTitel.text = getString(R.string.rate)
        tvRemoveitemCart.visibility=View.GONE
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        if(arguments!=null)
        {
            from = arguments?.getString("from")!!
            orderId = arguments?.getString("orderId")!!
            orderNo = arguments?.getString("orderNo")!!

            if(from.equals("product",false))
            {
                orderData= arguments!!.getSerializable("orderData") as Orderdetail?

            }
            else  if(from.equals("stitching",false))
            {
                stitching=arguments!!.getSerializable("orderData") as Stitching?
            }
        }

        tv_product_orderID.text= "Order ID: $orderNo"

        if(from.equals("product",false)) {
            if (orderData != null) {
                setProductData()
            }
            tv_review_product.text="Review Product"

        }else if(from.equals("stitching",false))
        {
            if (stitching != null) {
                setProductData()
            }
            tv_review_product.text="Review Stitching"
        }
        btnSubmit.setOnClickListener {
            MyUtils.hideKeyboardFrom(mActivity!!, btnSubmit)
            var ratingValue = rating_product?.rating!!

            if (ratingValue == 0f) {
                (activity as MainActivity).showSnackBar(mActivity!!.resources.getString(R.string.ratingMsg))
            }
            /*else if (edt_like_dislike!!.text.toString().trim().isEmpty()) {
                (activity as MainActivity).showSnackBar(mActivity!!.resources.getString(R.string.err_productReview))

            }*/
            else {
                if (MyUtils.isInternetAvailable(mActivity!!)) {
                    AddProductReviewsApiCall(ratingValue.toString(), edt_like_dislike?.text.toString().trim())
                } else {
                    (activity as MainActivity).showSnackBar(mActivity!!.resources.getString(R.string.error_common_network))

                }
            }
        }

         try {
             sessionManager = SessionManager(mActivity!!)
             if (sessionManager != null) {
                 if (sessionManager!!.isLoggedIn()) {
                     userData = sessionManager?.get_Authenticate_User()
                 }
             }
             getScrennwidth()

         } catch (e: Exception) {
             e.printStackTrace()
         }



    }

    private fun setProductData() {
        if(from.equals("product",false)) {
            if (orderData != null ) {
                if( !orderData?.productImage!![0].prodimgName.isNullOrEmpty())
                {
                    img_product?.setImageURI(RestClient.timthumb_path1 + orderData?.productImage!![0].prodimgName +"&w=$widthNew&h=$heightNew&zc=0",mActivity!!)
                }
                if (!orderData?.productName.isNullOrEmpty()){
                    tv_product_name.text = orderData?.productName
                }
            }
        }else if(from.equals("stitching",false)) {
            if (stitching != null) {
                if (!stitching?.stitchingserviceImages!![0].stitchingserviceimagesName.isNullOrEmpty()) {
                    img_product?.setImageURI(RestClient.timthumb_path1 + stitching?.stitchingserviceImages!![0].stitchingserviceimagesName+"&w=$widthNew&h=$heightNew&zc=0",mActivity!!)

                }
                if (!stitching?.stitchingserviceName.isNullOrEmpty()){
                    tv_product_name.text = stitching?.stitchingserviceName
                }
            }
        }
    }

    /*fun setSelectionWiseData(productImage : ProductMasterPojo.ProductImage?){
        if (productImage != null) {

            if (!productImage.prodimgName.isNullOrEmpty()) {
                prodimgNameStr?.clear()

                for (i in 0 until productImage.prodimgName!!.size) {

                    prodimgNameStr.add(productImage.prodimgName!![i].prodimgName)

                }
            }
        } else {
            prodimgNameStr?.clear()
        }

        if (!prodimgNameStr.isNullOrEmpty() && prodimgNameStr!!.size > 0) {
            if (!prodimgNameStr[0].isNullOrEmpty()){

                img_product?.setImageURI(Uri.parse(RestClient.timthumb_path1 + prodimgNameStr[0] + "&w=$widthNew&h=650"),mActivity!!)
            }else{
                img_product?.setActualImageResource(R.drawable.product_placeholder_img)
            }
        }
    }*/

    /*fun setColorData(productData: ProductMasterPojo?) {

        if (productData != null) {

            productColorImageList?.clear()

            if (!productData?.productImage.isNullOrEmpty()) {
                for (i in 0 until productData?.productImage!!.size) {
                    productColorImageList.add(productData?.productImage[i]!!)
                }
            } else {
                productColorImageList?.clear()
            }

            if (productColorImageList!!.size > 0) {
                setSelectionWiseData(productColorImageList!![0])
            }


        }

    }*/

    fun AddProductReviewsApiCall(reviewRatting: String?, reviewReview: String?) {


        var userDate = sessionManager!!.get_Authenticate_User()
        btnSubmit.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootWriteReview, false)
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userDate.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("orderID",orderId)
             if(from.equals("product",false))
             {
                 if(orderData!=null)
                 {
                     jsonObject.put("productID",orderData?.productID)
                     jsonObject.put("orderdetailsID",orderData?.orderdetailsID)
                     jsonObject.put("reviewRatting",reviewRatting)
                     jsonObject.put("reviewReview",reviewReview)
                 }

             }
            else if(from.equals("stitching",false))
            {
                if(stitching!=null) {
                    jsonObject.put("stitchingserviceID", stitching?.stitchingserviceID)
                    jsonObject.put(
                        "orderdetailsstitchingID",
                        stitching?.orderdetailsstitchingID
                    )
                }
                 jsonObject.put("stitchingservicereviewRatting",reviewRatting)
                jsonObject.put("stitchingservicereviewReview",reviewReview)
            }
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        val addReviewModel = ViewModelProviders.of(this@WrireReviewFragment).get(
            AddReviewModel::class.java
        )
        addReviewModel.addReview(
            mActivity!!,
            false,
            jsonArray.toString(),
            from
        )
            .observe(this@WrireReviewFragment,
                androidx.lifecycle.Observer<List<CommonPojo>> { addAddresspojo ->
                    if (addAddresspojo != null && addAddresspojo.isNotEmpty()) {
                        if (addAddresspojo[0].status.equals("true", true)) {
                            btnSubmit.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(rootWriteReview, true)
                            try {
                                (activity as MainActivity).showSnackBar(addAddresspojo[0].message!!)
                                Handler().postDelayed({
                                    (activity as MainActivity).onBackPressed()
                                }, 1000)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                            btnSubmit.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(
                                rootWriteReview,
                                true
                            )
                            (activity as MainActivity).showSnackBar(addAddresspojo[0].message!!)

                        }

                    } else {
                        btnSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootWriteReview, true)
                        (activity as MainActivity).errorMethod()
                    }
                })

    }

    private fun getScrennwidth(): Int {

        val wm = mActivity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

       widthNew = width + (width / 2)
       heightNew = height / 5
        return height
    }
}
