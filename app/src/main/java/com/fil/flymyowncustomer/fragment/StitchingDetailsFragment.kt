package com.fil.flymyowncustomer.fragment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ProductDetailsCustomerLikedAdapter
import com.fil.flymyowncustomer.adapter.StitchingServiceDetailsViewPagerAdapter
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_stitching_details.*

import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import java.util.*
import kotlin.collections.ArrayList

class StitchingDetailsFragment : Fragment() {

    var v: View? = null
    var mActivity: AppCompatActivity? = null
    var page_position = 0
    private var dotsCount: Int = 0
    var listColor: ArrayList<Int>? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var mProductDetailsCustomerLikedAdapter: ProductDetailsCustomerLikedAdapter? = null

    private lateinit var dots: Array<ImageView?>
    var stichingServicData: StichingServicePojo.StichingServicData? = null
    var isHaveAddress = false
    var selectedDeliveryTypeName = ""
    var cartCalculate : CartCalculate? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_stitching_details, container, false)

        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppCompatActivity) {
            mActivity = context
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.visibility = View.GONE
        (activity as MainActivity).bottom_navigation?.visibility = View.GONE

        toolbar_back.menu_Cart.visibility = View.GONE
        toolbar_back.menuHeartlike.visibility = View.GONE
        toolbar_back.menuShare.visibility = View.VISIBLE

        toolbar_back.menuShare.setOnClickListener {
            val appPackageName = (activity as MainActivity).packageName // getPackageName() from Context or Activity object
            try {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, ""+"market://details?id=$appPackageName")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Share using")
                (activity as MainActivity).startActivity(shareIntent)
            } catch (anfe: ActivityNotFoundException) {
                (activity as MainActivity).startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }

        }

        (activity as MainActivity).setDrawerSwipe(false)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        if (arguments != null) {
            stichingServicData =
                    arguments?.getSerializable("stichingServicData") as StichingServicePojo.StichingServicData?

        }
        cartCalculate = CartCalculate

        val userData = SessionManager(mActivity!!).get_Authenticate_User()
        if (userData != null) isHaveAddress = !userData?.billing.isNullOrEmpty() else isHaveAddress = false


        listColor = ArrayList()
        if (stichingServicData != null) {
            setData()
        }

        tv_ProductOriginalPrice.paintFlags = tv_ProductOriginalPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        try {
            OfferAdapter(stichingServicData?.stitchingserviceImages)
        } catch (e: Exception) {
        }

        btnBookService.setOnClickListener {

            if (isHaveAddress) {
                if (!cartCalculate?.checkDealerServiceAlreadyItemInCartOrNotAndClear(stichingServicData!!, false)!!) {
                    MyUtils.showMessageOKCancel(
                        mActivity!!,
                        "Your cart will be clear and add this service. In cart you can only add product of same studio and store.",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {

                                if (cartCalculate?.checkDealerServiceAlreadyItemInCartOrNotAndClear(stichingServicData!!, true)!!) {
                                        showDialog()

                                }
                            }
                        })
                }else{
                    showDialog()
                }
            } else {
                MyUtils.showMessageOKCancel(
                    mActivity!!,
                    "Please add your delivery address.",
                    object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            (mActivity as MainActivity).navigateTo(
                                BillingAddressFragment(),
                                BillingAddressFragment::class.java.name,
                                true
                            )
                        }
                    })
            }

        }

        ll_rating.setOnClickListener {
            if(stichingServicData?.stitchingserviceRatting!!.toDouble()>0.0&&stichingServicData?.stitchingserviceReviewRattingCount!!.toDouble()>0.0)
            {
                var reviewListFragment=ReviewListFragment()
                var bundle=Bundle()
                bundle.putString("from","stiching")
                bundle.putSerializable("productListData",stichingServicData)
                reviewListFragment.arguments=bundle
                (activity as MainActivity).navigateTo(reviewListFragment,reviewListFragment::class.java.name,true)

            }
        }
        ll_review.setOnClickListener {
            if(stichingServicData?.stitchingserviceRatting!!.toDouble()>0.0&&stichingServicData?.stitchingserviceReviewRattingCount!!.toDouble()>0.0)
            {
                var reviewListFragment = ReviewListFragment()
                var bundle = Bundle()
                bundle.putString("from","stiching")
                bundle.putSerializable("productListData", stichingServicData)
                reviewListFragment.arguments = bundle
                (activity as MainActivity).navigateTo(
                    reviewListFragment,
                    reviewListFragment::class.java.name,
                    true
                )
            }
        }
    }

    private fun getRequiredDataForAddToCart() {

        val userData = SessionManager(mActivity!!).get_Authenticate_User()
        if (userData != null && !userData.billing.isNullOrEmpty()) {

            for (i in 0 until userData.billing!!.size) {
                if (userData.billing!![i].addressIsDefault.equals(
                        "true",
                        true
                    ) || userData.billing!![i].addressIsDefault.equals("Yes", true)
                ) {
                    if (!userData.billing!![i].addressGST.isNullOrEmpty() && !stichingServicData?.dealerGST.isNullOrEmpty()) {
                        if (stichingServicData?.dealerGST!!.substring(
                                0,
                                2
                            ) == userData.billing!![i].addressGST?.substring(0, 2)
                        ) {
                            stichingServicData?.isIGST = false
                            break
                        } else {
                            stichingServicData?.isIGST = true
                            break
                        }
                    } else {
                        stichingServicData?.isIGST = true
                        break
                    }
                } else {
                    stichingServicData?.isIGST = true
                    break
                }
            }
        } else {
            stichingServicData?.isIGST = true
        }

        stichingServicData?.addToCartColorId = ""
        stichingServicData?.addToCartMaterialId = ""
        stichingServicData?.addToCartPatternId = ""
        stichingServicData?.addToCartStyleId = ""
        stichingServicData?.addToCartBrandId = ""
        stichingServicData?.addToCartSizeId = ""
        stichingServicData?.productDiscountedPrice = java.lang.Double.valueOf(stichingServicData?.stitchingservicePrice!!)

        stichingServicData?.addToCartDeliveryName = selectedDeliveryTypeName

        val data: RegisterNewPojo.Datum = SessionManager(mActivity!!).get_Authenticate_User()
        if (!data?.settings.isNullOrEmpty() && !data?.settings!![0].settingsGSTOnFabric.isNullOrEmpty() && !data?.settings!![0].settingsGSTOnStitchingservice.isNullOrEmpty()
        ) {
            stichingServicData?.categoryGST =
                    java.lang.Double.valueOf(data?.settings!![0].settingsGSTOnStitchingservice!!)
        }
    }

    fun setData() {
        tv_StitchingStudioName.text = stichingServicData!!.stitchingserviceName
        if(stichingServicData?.stitchingserviceDescription != null && !stichingServicData?.stitchingserviceDescription!!.isNullOrEmpty()){
            tv_ProductDescription.text = HtmlCompat.fromHtml(
                stichingServicData!!.stitchingserviceDescription!!,
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            tv_ProductDescription?.visibility = View.VISIBLE
            tv_ProductDescription_title?.visibility = View.VISIBLE

        }else {
            tv_ProductDescription?.visibility = View.GONE
            tv_ProductDescription_title?.visibility = View.GONE
            tv_ProductDescription?.text = ""
        }

        tv_ProductPrice.text = resources.getString(R.string.rs) +
                MyUtils.formatPricePerCountery((stichingServicData!!.stitchingservicePrice!!.toDouble()))

         if (!stichingServicData!!.stitchingserviceDiscount!!.isNullOrEmpty() && !stichingServicData!!.stitchingserviceDiscount.equals(
                "0.00",
                false
            )
        ) {
             tv_ProductOriginalPrice.text = resources.getString(R.string.rs) +
                     MyUtils.formatPricePerCountery(stichingServicData!!.stitchingserviceActualPrice!!.toDouble())

             tv_ProductDisscountOffer.text = resources.getString(R.string.rs) +
                    stichingServicData!!.stitchingserviceDiscount!! + "% Off"
        } else {
            tv_ProductDisscountOffer.visibility = View.GONE
             tv_ProductOriginalPrice.visibility = View.GONE
        }

        if (stichingServicData?.stitchingserviceIsPopular.equals("Yes", false)) {
            tvPopular.visibility = View.VISIBLE
        } else {
            tvPopular.visibility = View.GONE

        }

        tv_ProductShipng.visibility = View.VISIBLE
        ll_free_shipping.visibility = View.GONE

        //tv_ProductRating.text=stichingServicData!!.productRatting
        // tv_ProductReview.text=stichingServicData!!.productReviewRattingCount
        if (!stichingServicData!!.stitchingserviceRatting!!.isNullOrEmpty())
        {
            tv_ProductRating.text = stichingServicData!!.stitchingserviceRatting!!

        }
        if (!stichingServicData!!.stitchingserviceReviewRattingCount!!.isNullOrEmpty())
        {
            tv_ProductReview.text = stichingServicData!!.stitchingserviceReviewRattingCount!!

        }
        /*if( stichingServicData!!.productIsPopular.equals("Yes",false))
        {
            tvPopular.visibility=View.VISIBLE
        }
        else
        {
            tvPopular.visibility=View.GONE

        }*/


        /* if(stichingServicData!!.dealerFreeServiceIsAvailable.equals("Yes",false))
         {
             tv_ProductShipng.visibility=View.VISIBLE
             ll_free_shipping.visibility=View.VISIBLE

         }
         else
         {
             tv_ProductShipng.visibility=View.GONE
             ll_free_shipping.visibility=View.GONE

         }*/

        //  getAddProductView(productListData!!.productID,"AddView")

        try {
            if (!stichingServicData?.stitchingserviceImages.isNullOrEmpty()) {
                OfferAdapter(stichingServicData?.stitchingserviceImages)

            }
        } catch (e: Exception) {
        }

    }

    private fun showDialog() {
        var deliveryType = ""
        var deliveryFreeService = ""
        var deliveryServiceCharges = ""

        var isHome = false
        var isService = false

        if (!stichingServicData?.dealerServiceType.isNullOrEmpty()) {
            deliveryType = stichingServicData?.dealerServiceType!!
        }

        if (!stichingServicData?.dealerFreeServiceIsAvailable.isNullOrEmpty()) {
            deliveryFreeService = stichingServicData?.dealerFreeServiceIsAvailable!!
        }

        if (!stichingServicData?.dealerDeliveryCharges.isNullOrEmpty()) {
            deliveryServiceCharges = stichingServicData?.dealerDeliveryCharges!!
        }


        if (deliveryType.equals("Home Service", true)) {
            isHome = true
            isService = false
//            selectedDeliveryTypeName = "Home Service"
        } else if (deliveryType.equals("Self Service", true)) {
            isService = true
            isHome = false
//            selectedDeliveryTypeName = "Self Service"
        } else if (deliveryType.equals("Both", true)) {
            isHome = true
            isService = true
        }


        val dialogs = MaterialAlertDialogBuilder(mActivity!!)

        //   dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)

        val dialogView = mActivity!!.layoutInflater.inflate(R.layout.popup_delivery, null)

        val ll_home_delivery = dialogView.findViewById<LinearLayout>(R.id.ll_home_delivery)
        val ll_self_pickup = dialogView.findViewById<LinearLayout>(R.id.ll_self_pickup)
        val img_home_delivery = dialogView.findViewById<ImageView>(R.id.img_home_delivery)
        val img_self_pickup = dialogView.findViewById<ImageView>(R.id.img_self_pickup)
        val tv_Cancel = dialogView.findViewById<AppCompatTextView>(R.id.tv_Cancel)
        val tv_Done = dialogView.findViewById<AppCompatTextView>(R.id.tv_Done)
        val tv_homedelivery = dialogView.findViewById<AppCompatTextView>(R.id.tv_homedelivery)
        val tv_self_pickup = dialogView.findViewById<AppCompatTextView>(R.id.tv_self_pickup)

        tv_homedelivery.text="Home Service"
        tv_self_pickup.text="Self Service"

        if (isHome && !isService) {
            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.GONE
        } else if (!isHome && isService) {
            ll_home_delivery.visibility = View.GONE
            ll_self_pickup.visibility = View.VISIBLE
        } else if (isHome && isService) {
            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.VISIBLE
        }

        dialogs.setView(dialogView)
        val alertDialog = dialogs.create()
        alertDialog.show()
        tv_Cancel.setOnClickListener {
            alertDialog.dismiss()
        }

        tv_Done.setOnClickListener {
            if (selectedDeliveryTypeName.isNullOrEmpty()) {
                (activity as MainActivity).showSnackBar( "Please select delivery type.")
            } else {
                alertDialog.dismiss()
                addTocartProduct()
            }
        }
        ll_home_delivery.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_checked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            selectedDeliveryTypeName = "Home Service"
            if (deliveryFreeService.equals("Yes", true)) {
                stichingServicData?.addToCartDeliveryCharges = 0.0
            } else {
                stichingServicData?.addToCartDeliveryCharges = java.lang.Double.valueOf(stichingServicData?.dealerDeliveryCharges!!)
            }
        }
        ll_self_pickup.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_checked)
            selectedDeliveryTypeName = "Self Service"
            stichingServicData?.addToCartDeliveryCharges = 0.0
        }
    }

    private fun addTocartProduct() {
        getRequiredDataForAddToCart()

        /*if (!cartCalculate?.checkDealerServiceAlreadyItemInCartOrNotAndClear(stichingServicData!!, false)!!) {
            MyUtils.showMessageOKCancel(
                mActivity!!,
                "Your cart will be clear and add this service. In cart you can only add product of same studio and store.",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {

                        if (selectedDeliveryTypeName.isNullOrEmpty()) {
                            showDialog()
                        } else {
                            if (cartCalculate?.checkDealerServiceAlreadyItemInCartOrNotAndClear(
                                    stichingServicData!!,
                                    true
                                )!!
                            ) {

                                }
                            }
                        }
                    }
                })
        }*/



        val oldCartId =  cartCalculate?.getServiceCartId(stichingServicData!!)
        if (cartCalculate?.AddToCartService(stichingServicData!!, true, false, oldCartId!!)!!) {
            (mActivity as MainActivity).navigateTo(MyCartFragment(), MyCartFragment::class.java.name, true)
        } else {
            (activity as MainActivity).showSnackBar("Please try again later.")
        }

    }


    fun OfferAdapter(stitchingserviceImages: List<StichingServicePojo.StichingServicData.StitchingserviceImage?>?) {
        var stitchingServiceDetailsViewPagerAdapter = StitchingServiceDetailsViewPagerAdapter(
            mActivity as MainActivity,
            stitchingserviceImages!!
        )
        viewPager_main_fragment.clipToPadding = false
        viewPager_main_fragment.setPadding(0, 0, 0, 0)
        viewPager_main_fragment?.adapter = stitchingServiceDetailsViewPagerAdapter
        viewPager_main_fragment?.addOnPageChangeListener(viewPagerChangeListener)
        if (stitchingserviceImages!!.size > 1) {
            addBottomDots(0)
            sliderBanner(stitchingserviceImages)
        } else {
            addBottomDots(0)
        }
    }

    fun addBottomDots(currentPage: Int) {
        dotsCount = stichingServicData?.stitchingserviceImages!!.size

        layoutDotsTutorial?.removeAllViews()
        dots = arrayOfNulls(dotsCount)

        for (i in 0 until dotsCount) {

            dots[i] = ImageView(mActivity)
            dots[i]?.setImageResource(R.drawable.slider_dot_unselected)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            dots[i]?.setPadding(0, 0, 0, 0)
            dots[i]?.layoutParams = params
            layoutDotsTutorial?.addView(dots[i]!!, params)
            layoutDotsTutorial?.bringToFront()

        }
        if (dots.size > 0 && dots.size > currentPage) {
            dots[currentPage]?.setImageResource(R.drawable.slider_dot_selected)
        }

    }

    fun sliderBanner(stitchingserviceImages: List<StichingServicePojo.StichingServicData.StitchingserviceImage?>) {
        val handler = Handler()
        val update = Runnable {
            try {
//                if (images != null && images!!.size > 0) {
                if (stitchingserviceImages.size > 0) {
                    if (page_position == stitchingserviceImages.size) {
                        page_position = 0

                    } else {
                        page_position = page_position + 1
                    }
                    viewPager_main_fragment?.setCurrentItem(page_position, true)
                }
            } catch (e: NullPointerException) {

            }
        }
        Timer().schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 15000)
    }

    internal var viewPagerChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            page_position = position
        }

        override fun onPageSelected(position: Int) {
            page_position = position
            addBottomDots(position)

        }
    }


}
