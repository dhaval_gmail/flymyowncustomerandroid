package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.util.RangeSeekBar2
import kotlinx.android.synthetic.main.fragment_price_filter.*
import kotlin.math.max

/**
 * A simple [Fragment] subclass.
 */
class PriceFilterFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    private var Selectedstar = false
    private var minrate: String? = "0"
    private var maxrate: String? = "25000"
    var onMRangeValuePass: OnMRangeValuePass? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_price_filter, container, false)

        }
        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
            onMRangeValuePass = activity as OnMRangeValuePass

        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         if(arguments!=null)
         {
             maxrate=arguments!!.getString("maxValue","0")
             minrate=arguments!!.getString("minValue","25000")
         }
        if(!maxrate.isNullOrEmpty())
        {
            range_seekbar_fees.absoluteMaxValue=maxrate?.toInt()
            textMin1.text=maxrate
        }
        if(!minrate.isNullOrEmpty())
        {
            range_seekbar_fees.absoluteMaxValue=minrate?.toInt()
            textMax1.text=minrate

        }

        range_seekbar_fees.setOnRangeSeekBarChangeListener(object : RangeSeekBar2.OnRangeSeekBarChangeListener<Any> {
            override fun onRangeSeekBarValuesChanged(bar: RangeSeekBar2<*>, minValue: Any, maxValue: Any) {
                Selectedstar = true
                minrate = minValue.toString()
                maxrate = maxValue.toString()
                Log.d("system", minrate)
                Log.d("system", maxrate)
                onMRangeValuePass!!.onMRangeValuePass(maxrate!!,minrate!!)
            }
        })
    }


    interface OnMRangeValuePass{

        fun onMRangeValuePass( maxValue: String,
                               minValue: String)
    }



}
