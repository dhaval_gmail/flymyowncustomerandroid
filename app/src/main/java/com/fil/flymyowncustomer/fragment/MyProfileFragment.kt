package com.fil.flymyowncustomer.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.LoginActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.UpdateProfileModel
import com.fil.flymyowncustomer.pojo.FiluploadResponse
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import java.io.File
import java.io.IOException
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.fragment_my_account.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class MyProfileFragment : Fragment(), View.OnClickListener {

    var mActivity: AppCompatActivity? = null
    private var v: View? = null
    var sessionManager : SessionManager? = null
    var userData : RegisterNewPojo.Datum? = null
    var profileLayoutMain: LinearLayoutCompat? = null
    private var mediaChooseBottomSheet = MediaChooseImageBottomsheet()
    private val TAKE_PICTURE = 1
    private val SELECT_PICTURE = 2
    private var pictureUri: Uri? = null
    private var timeForImageName: Long = 0
    private var imgName: String? = null
    private var picturePath: String? = null
    internal var pbDialog: Dialog? = null
    private var actualImage: File? = null
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    var compressedImage: File? = null
    var selectedProfilePicture :String?= ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_my_profile, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.myprofile)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        profileLayoutMain = v?.findViewById<LinearLayoutCompat>(R.id.profileLayoutMain)

        try {
            sessionManager = SessionManager(mActivity!!)

            if (sessionManager != null){
                if (sessionManager!!.isLoggedIn()){
                    userData = sessionManager?.get_Authenticate_User()

                }
            }

            enableOrDisible(false)

            setData()

        } catch (e: Exception) {
            e.printStackTrace()
        }
        profile_edit_name1.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        profile_edit_name1?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub
            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (profile_edit_name1?.text.toString().isNotEmpty()) {
                    try {
                        var x: Char
                        val t = IntArray(profile_edit_name1?.text.toString().length)

                        for (i in 0 until profile_edit_name1?.text.toString().length) {
                            x = profile_edit_name1?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..91 || z in 97..122 || z == 32 || 1 in 48..57) {

                            } else {
                                Toast.makeText(mActivity!!, "" + "Special Character not allowed", Toast.LENGTH_SHORT)
                                    .show()
                                val ss = profile_edit_name1?.text.toString()
                                    .substring(0, profile_edit_name1?.text.toString().length - 1)
                                profile_edit_name1?.setText(ss)
                                profile_edit_name1?.setSelection(profile_edit_name1?.text.toString().length)
                            }
                        }
                    } catch (e: IndexOutOfBoundsException) {
                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        profileUpdate?.setOnClickListener {
            MyUtils.hideKeyboard(mActivity!!,profileUpdate)
            if (checkValidation()){

                if(actualImage == null){
                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        editProfileAPI()

                    } else {
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }
                }else {
                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        uploadImage(actualImage!!)
                    } else {
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }
                }
            }
        }
        profileEdit.setOnClickListener(this)

        profile_imv_edit?.setOnClickListener {
            val currentapiVersion = Build.VERSION.SDK_INT
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                getWriteStoragePermissionOther()
            } else {
                mediaChooseBottomSheet.show(mActivity!!.supportFragmentManager, "BottomSheet demoFragment")
            }
        }


    }

    private fun setData() {

        if(userData != null){
            if (!userData?.userProfilePicture.isNullOrEmpty()){
                profile_imv_dp?.setImageURI(Uri.parse(userData?.userProfilePicture),mActivity)
                selectedProfilePicture = userData?.userProfilePicture!!
            }

            profile_imv_dp.setOnClickListener{
                var profilePhotoFullScreenFragment= ProfilePhotoFullScreenFragment()
                Bundle().apply {
                    this.putString("profileImg",userData?.userProfilePicture)
                    profilePhotoFullScreenFragment.arguments=this

                }
                (mActivity as MainActivity).navigateTo(profilePhotoFullScreenFragment, profilePhotoFullScreenFragment::class.java!!.getName(), true)

            }

            if (!userData?.userFullName.isNullOrEmpty()){
                profile_edit_name1?.setText(userData?.userFullName)
//                oldDealerName = userData?.userFullName!!
            }

            if (!userData?.countryFlagImage.isNullOrEmpty()){
                profile_recyclerview_contact_detail?.setImageURI(Uri.parse(userData?.countryFlagImage),mActivity)
//                selectedProfilePicture = userData?.userProfilePicture!!
            }

            if (!userData?.userCountryCode.isNullOrEmpty()){
                profile_tv_country_code?.setText(userData?.userCountryCode)
            }

            if ( !userData?.userMobile.isNullOrEmpty()){
                profile_edit_mobile1?.setText(userData?.userMobile)
            }

            if (!userData?.userEmail.isNullOrEmpty()){
                profile_edit_email1?.setText(userData?.userEmail)
            }
        }

//        mActivity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    fun checkValidation(): Boolean {
        var checkFlag = true

        if (profile_edit_name1.text.toString().isNullOrEmpty() || profile_edit_name1.text.toString().isNullOrBlank()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                profileLayoutMain!!,
                resources.getString(R.string.err_empty_full_name)
            )
        } else if (profile_edit_name1.text.toString().length < 3) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                mActivity!!,
                profileLayoutMain!!,
                resources.getString(R.string.err_lnth_full_name_3)
            )
        }
        return checkFlag
    }

    fun enableOrDisible(visible: Boolean) {

        profile_country_selection.isClickable = false
        profile_country_selection.isEnabled = false

        profile_recyclerview_contact_detail.isClickable = false
        profile_recyclerview_contact_detail.isEnabled = false

        profile_tv_country_code.isClickable = false
        profile_tv_country_code.isEnabled = false

        profile_countery_drpd.isClickable = false
        profile_countery_drpd.isEnabled = false

        profile_edit_mobile1.isClickable = false
        profile_edit_mobile1.isEnabled = false

        profile_edit_email1.isClickable = false
        profile_edit_email1.isEnabled = false

        profile_edit_name1.isClickable = visible
        profile_edit_name1.isEnabled = visible

        profile_imv_edit?.isClickable = visible
        profile_imv_edit.isEnabled = visible

        profileUpdate.isEnabled = visible
        profileUpdate.isClickable = visible
        profileFrameLayoutImage.isClickable = visible

        if (visible) {
            profileUpdate.visibility = View.VISIBLE
            profileEdit.visibility = View.GONE


            profileFrameLayoutImage.setOnClickListener(this)
        } else {
            profileUpdate.visibility = View.GONE
            profileEdit.visibility = View.VISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.profileUpdate -> {
                if (checkValidation()){

                    if(actualImage == null){
                        if (MyUtils.isInternetAvailable(mActivity!!)) {
                            editProfileAPI()

                        } else {
                            MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                        }
                    }else {
                        if (MyUtils.isInternetAvailable(mActivity!!)) {
                            uploadImage(actualImage!!)

                        } else {
                            MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                        }
                    }
                }
            }

            R.id.profileEdit -> {
                enableOrDisible(true)
            }
        }
    }

    fun getWriteStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(mActivity as MainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getReadStoragePermissionOther()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getReadStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(mActivity as MainActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getCameraPermissionOther()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getCameraPermissionOther() {
        val permissionCheck = ContextCompat.checkSelfPermission(mActivity as MainActivity, Manifest.permission.CAMERA)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            mediaChooseBottomSheet.show(mActivity!!.supportFragmentManager, "BottomSheet demoFragment")
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), MyUtils.Per_REQUEST_CAMERA_1)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getReadStoragePermissionOther()
            } else {
                getWriteStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCameraPermissionOther()
            } else {
                getReadStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_CAMERA_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mediaChooseBottomSheet.show(mActivity!!.supportFragmentManager, "BottomSheet demoFragment")
            } else {
                getCameraPermissionOther()
            }
        }
    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()
        val json = gson.toJson(driverdata[0]!!)
        sessionManager?.create_login_session(
            json,
            driverdata[0]!!.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            timeForImageName = System.currentTimeMillis()
            imgName = "img$timeForImageName.jpg"

            when (requestCode) {
                TAKE_PICTURE -> if (mediaChooseBottomSheet.selectedImage() != null) {
                    pictureUri = mediaChooseBottomSheet.selectedImage()
                    picturePath = pictureUri?.path
                    profile_imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@MyProfileFragment)
                    actualImage = File(picturePath)
                    compressedImage = customCompressImage(actualImage!!)

                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        uploadImage(compressedImage!!)

                    } else {
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }

                }
                SELECT_PICTURE -> {
                    pictureUri = data!!.data
                    picturePath = MyUtils.getPath5(pictureUri!!, this.mActivity!!)
                    if (picturePath != null) {
                        if (picturePath?.contains("https:")!!) {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                profileLayoutMain!!,
                                "Please, select another profile pic."
                            )
                        } else {
                            profile_imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@MyProfileFragment)
                            actualImage = File(picturePath)
                            try {
                                picturePath = MyUtils.getPath(pictureUri!!, this.mActivity!!)
                                actualImage = File(picturePath)
                                compressedImage = customCompressImage(actualImage!!)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }else {
                        picturePath = MyUtils.getPathFromInputStreamUri(mActivity!!, data!!.data)

                        profile_imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@MyProfileFragment)
                        actualImage = File(picturePath)
//                        compressedImage = customCompressImage(actualImage!!)
                    }

                    if (MyUtils.isInternetAvailable(mActivity!!)) {
                        uploadImage(actualImage!!)

                    } else {
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }
//
                }
            }
        }
    }

    fun customCompressImage(actualImage: File): File? {
        var compressedImage: File? = null
        try {
            compressedImage = Compressor(mActivity)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    ).absolutePath
                )
                .compressToFile(actualImage,"IMG_" + timeStamp + ".jpg")
        } catch (e: IOException) {
            e.printStackTrace()
//            showError(e.message)
        }

        return compressedImage

    }

    fun uploadImage(file: File){
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val surveyImagesParts = arrayOfNulls<MultipartBody.Part>(1)

        for (i in 0 until 1) {
            val fileNew = File(file.absolutePath)
            val surveyBody = RequestBody.create(MediaType.parse("image/*"), fileNew)
            surveyImagesParts[i] = MultipartBody.Part.createFormData("MultipleFiles[]", fileNew.name, surveyBody)
        }

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userData?.userID)
            jsonObject.put("foldername", "users")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val call = RestClient.get()!!.uploadAttachment(
            surveyImagesParts, RequestBody.create(
                MediaType.parse("text/plain"), jsonArray.toString()
            )
        )

        Log.i("System out", "json $jsonArray")

        pbDialog = MyUtils.showProgressDialog(mActivity!!)
        pbDialog!!.show()

        call.enqueue(object : Callback<List<FiluploadResponse>> {
            override fun onResponse(call: Call<List<FiluploadResponse>>, response: Response<List<FiluploadResponse>>) {

                if (response.body() != null) {
                    if (pbDialog != null)
                        pbDialog!!.dismiss()
                    if (response.body()!![0].status.equals("true")) {

                        if (!response.body()!![0].data.isNullOrEmpty()){
                            if (!response.body()!![0].data!![0].filename.isNullOrEmpty()){
                                selectedProfilePicture = response.body()!![0].data!![0].filename!!
                                svUserProfile?.setImageURI(Uri.parse(RestClient.imageBaseUrlUser+ selectedProfilePicture),mActivity)
                                /*try {
                                    actualImage?.delete()
                                    compressedImage?.delete()
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }*/
                                editProfileAPI()
                            }
                        }
                    } else {
                        try {
                            if (MyUtils.isInternetAvailable(mActivity!!)) {
                                MyUtils.showSnackbarkotlin(
                                    mActivity!!,
                                    profileLayoutMain!!,
                                    mActivity!!.resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    mActivity!!,
                                    profileLayoutMain!!,
                                    mActivity!!.resources.getString(R.string.error_common_network)
                                )
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } else {

                }
            }

            override fun onFailure(call: Call<List<FiluploadResponse>>, t: Throwable) {
                // Log error here since request failed
                Log.e("System out", t.message)
                if (pbDialog != null)
                    pbDialog!!.dismiss()
            }
        })

    }

    private fun editProfileAPI() {

        if(selectedProfilePicture!!.contains("/")) {
            val fileName = selectedProfilePicture?.substring(selectedProfilePicture!!.lastIndexOf('/') + 1)
            selectedProfilePicture = fileName
        }

        profileUpdate.startAnimation()
        MyUtils.setViewAndChildrenEnabled(profileLayoutMain!!, false)
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {

            /*[
                {
                    "languageID": "1",
                    "loginuserID": "1",
                    "userFullName": "Ankit Patel",
                    "userProfilePicture": "API20190830070741746455.jpg",
                    "userMobile": "9979517447",
                    "userEmail": "patelankit1516@gmail.com",
                    "apiType": "Android",
                    "apiVersion": "1.0"
                }
            ]*/

            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userData?.userID)
            jsonObject.put("userFullName", profile_edit_name1.text.toString())
            jsonObject.put("userProfilePicture", selectedProfilePicture)
            jsonObject.put("userMobile", userData?.userMobile)
            jsonObject.put("userEmail", userData?.userEmail)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }
        Log.e("System Out", "Login Request $jsonArray")

        val countryList = ViewModelProviders.of(mActivity!!).get(UpdateProfileModel::class.java)
        countryList.apiFunction(mActivity!!, false, jsonArray.toString()).observe(mActivity!!,
            Observer<List<RegisterNewPojo>> { response ->
                if (!response.isNullOrEmpty()){
                    if (response.isNotEmpty()){
                        profileUpdate.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(profileLayoutMain!!, true)
                        if (response[0].status.equals("true", true)){

                            if (!response[0].message.isNullOrEmpty()){
                                MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, response[0].message!!)
                            }else{
                                MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.update_profile_success))
                            }
                            sessionManager?.clear_login_session()
                            storeSessionManager(response[0]!!.data!!)
                            (mActivity as MainActivity).tvUserName?.text = response[0]!!.data!![0].userFullName
                            (mActivity as MainActivity).profileImageview?.setImageURI(Uri.parse(response[0]!!.data!![0].userProfilePicture),mActivity)
                            Handler().postDelayed({
                                (mActivity as MainActivity).onBackPressed()
                            }, 2000)
                        }else{
                            profileUpdate.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(profileLayoutMain!!, true)
                            if (MyUtils.isInternetAvailable(mActivity!!)){
                                MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_crash_error_message))
                            }else{
                                MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_network))
                            }
                        }
                    }
                }else{
                    //No internet and somting went rong
                    profileUpdate.endAnimation()
                    MyUtils.setViewAndChildrenEnabled(profileLayoutMain!!, true)
                    if (MyUtils.isInternetAvailable(mActivity!!)){
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_crash_error_message))
                    }else{
                        MyUtils.showSnackbarkotlin(mActivity!!, profileLayoutMain!!, mActivity!!.resources.getString(R.string.error_common_network))
                    }
                }
            })
    }
}