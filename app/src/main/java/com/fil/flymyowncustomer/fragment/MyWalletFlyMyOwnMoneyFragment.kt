package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.RechargeWalletHistoryModel
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException

import kotlinx.android.synthetic.main.fragment_my_wallet_fly_my_own_money.*
import kotlinx.android.synthetic.main.fragment_my_wallet_recharge_amount.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Double

/**
 * A simple [Fragment] subclass.
 *
 */
class MyWalletFlyMyOwnMoneyFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var sessionManager:SessionManager?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_my_wallet_fly_my_own_money, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager= SessionManager(mActivity!!)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.my_wallet)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        rechargeWalletHistory()

        btnReferandEarn?.setOnClickListener {

            (activity as MainActivity).selectBottomNavigationOption()
        }
    }


    private  fun rechargeWalletHistory()
    {
        relativeprogressBar!!.visibility = View.VISIBLE
        main_Layout.visibility=View.GONE
        nointernetMainRelativelayout.visibility=View.GONE
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("wallettransactonType","Referral")
            jsonObject.put("page", "0")
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var myWallethistory =
            ViewModelProviders.of(this@MyWalletFlyMyOwnMoneyFragment).get(
                RechargeWalletHistoryModel::class.java)
        myWallethistory.rechargeWalletHistory(mActivity!!, false, jsonArray.toString())
            .observe(this@MyWalletFlyMyOwnMoneyFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {

                        relativeprogressBar!!.visibility = View.GONE
                        main_Layout.visibility=View.VISIBLE
                        nointernetMainRelativelayout.visibility=View.GONE

                        if (masterPojo[0].status.equals("true", false)) {
                            tvReferandEarnBalance.text= resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(
                                Double.valueOf(masterPojo[0].userWalletReferAndEarnAmount))

                        } else {
                            tvReferandEarnBalance.text= resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(0.00)
                            main_Layout.visibility=View.VISIBLE
                            nointernetMainRelativelayout.visibility=View.GONE



                        }

                    } else {
                        main_Layout.visibility=View.GONE
                        nodatafound()
                    }
                })

    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!))
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            }
            else
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
