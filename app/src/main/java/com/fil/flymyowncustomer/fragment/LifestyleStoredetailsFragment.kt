package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.BannerImagesFullSliderAdapter

import com.fil.flymyowncustomer.adapter.LifestyleStudioCategoryAdapter
import com.fil.flymyowncustomer.adapter.SearchProductListViewAdapter
import com.fil.flymyowncustomer.adapter.SearchStitchingListViewAdapter
import com.fil.flymyowncustomer.model.CategoryTypeListModel
import com.fil.flymyowncustomer.model.DealerFavouriteViewModel
import com.fil.flymyowncustomer.model.ProductListModel
import com.fil.flymyowncustomer.model.StichingServiceListModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_lifestylestoreproductdetails.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class LifestyleStoredetailsFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var listBanner: ArrayList<Int>? = null
    var lifestyleStudioCategoryAdapter: LifestyleStudioCategoryAdapter? = null
    var arrayCategoryParent: java.util.ArrayList<FaqListPojo.Datum?>? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var dealerListData: Stitchingstudio? = null
    var widthNew = 0
    var heightNew = 0
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var sessionManager: SessionManager? = null
    private var categoryListPojo: java.util.ArrayList<CategoryTypeListPojo?>? = null
    private var categoryTypeListPojo: java.util.ArrayList<CategoryTypeData?>? = null
    private var categoryListData: java.util.ArrayList<Category?>? = null
    private var subCategoryListData: java.util.ArrayList<Subcategory?>? = null
    var categorytypeIDs: String = ""

    var from: String = ""
    var dealerIDs: String = "0"
    var mBannerImagesSliderAdapter: BannerImagesFullSliderAdapter? = null
    var productListData: ArrayList<ProductListPojo.Data?>? = ArrayList()
    var stitchingListData: ArrayList<StichingServicePojo.StichingServicData?>? = ArrayList()

    var searchListViewAdapter: SearchProductListViewAdapter? = null
    var searchStitchingListViewAdapter: SearchStitchingListViewAdapter? = null
    var searchView: SearchView? = null
    var menuSearch: MenuItem? = null
    var menuNotification1: MenuItem? = null
    var productListModel: ProductListModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_lifestylestoreproductdetails, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)

        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
        (activity as MainActivity).setDrawerSwipe(false)
        (activity as MainActivity).menuNotification?.isVisible = false
        menuNotification?.visibility = View.GONE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        noDatafoundRelativelayout.visibility = View.GONE
        relativeprogressBar.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        sessionManager = SessionManager(mActivity!!)
        getScrennwidth()
        if (arguments != null) {
            dealerListData = arguments!!.getSerializable("dealerListData") as Stitchingstudio?
            from = arguments!!.getString("from")!!
            toolbar_back.tvToolbarTitel.text = dealerListData!!.dealerCompanyName
        }

        if (dealerListData != null) {
            dealerIDs = dealerListData!!.dealerID!!

            setData()
        }
        if (categoryListPojo.isNullOrEmpty()) {
            categoryListPojo = ArrayList()
            categoryTypeListPojo = ArrayList()
            categoryListData = ArrayList()
            categoryListPojo?.clear()
            categoryTypeListPojo?.clear()
            categoryListData?.clear()
            getCategoryTypeList(!categoryListPojo.isNullOrEmpty())

            btnRetry.setOnClickListener {
                categoryListPojo = ArrayList()
                categoryTypeListPojo = ArrayList()
                categoryListData = ArrayList()
                categoryListPojo?.clear()
                categoryTypeListPojo?.clear()
                categoryListData?.clear()
                getCategoryTypeList(!categoryListPojo.isNullOrEmpty())
            }
        } else {
            if (!categoryTypeListPojo.isNullOrEmpty()) {
                inflateLifestoreDetails(categoryTypeListPojo!!)

                //  tv_StitchingStudioCategoryTypeName.text = categoryTypeListPojo!![0]!!.categorytypeName
                //  categorytypeIDs = categoryTypeListPojo!![0]!!.categorytypeID!!
            }
            if (!categoryListData.isNullOrEmpty()) {

                setAdapaterData(categoryListData!!, categorytypeIDs)
            }
        }

        productListModel =
            ViewModelProviders.of(this@LifestyleStoredetailsFragment)
                .get(ProductListModel::class.java)

        when (from) {
            "Stitching Store" -> {
                searchListViewAdapter =
                    SearchProductListViewAdapter(
                        mActivity!!,
                        R.layout.item_search_list,
                        productListData!!
                    )
                list_Search.adapter = searchListViewAdapter


            }
            "Stitching Studio" -> {
                searchStitchingListViewAdapter =
                    SearchStitchingListViewAdapter(
                        mActivity!!,
                        R.layout.item_search_list,
                        stitchingListData!!
                    )
                list_Search.adapter = searchStitchingListViewAdapter
            }
        }

        list_Search.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i2, l1 ->

                when (from) {
                    "Stitching Store" -> {
                        var productDetailsFragment = ProductDetailsFragment()
                        Bundle().apply {
                            this.putString("from", from)
                            this.putSerializable("productListData", productListData!![i2]!!)
                            productDetailsFragment.arguments = this

                        }
                        (activity as MainActivity).navigateTo(
                            productDetailsFragment,
                            productDetailsFragment::class.java.name,
                            true
                        )
                        productListData?.clear()
                        searchListViewAdapter?.notifyDataSetChanged()


                    }
                    "Stitching Studio" -> {
                        var stiDetailsFragment = StitchingDetailsFragment()
                        Bundle().apply {
                            putString("from", from)
                            putSerializable("stichingServicData", stitchingListData!![i2]!!)
                            stiDetailsFragment.arguments = this

                        }
                        (activity as MainActivity).navigateTo(
                            stiDetailsFragment,
                            stiDetailsFragment::class.java.name,
                            true
                        )

                        stitchingListData?.clear()
                        searchStitchingListViewAdapter?.notifyDataSetChanged()

                    }

                }

                if (searchView != null) {
                    if (!searchView?.isIconified!!) {
                        searchView?.isIconified = false
                    }
                    if (menuSearch != null)
                        menuSearch?.collapseActionView()

                }

            }


    }

    private fun getScrennwidth(): Int {

        val wm = mActivity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        widthNew = (width / 2).toInt()
        heightNew = (height / 4).toInt()

        return height
    }

    private fun setData() {
        if (!dealerListData?.bannerImages.isNullOrEmpty()) {
            svSudioImage?.visibility = View.GONE
            viewPager_main_bannerImagesDetails?.visibility = View.VISIBLE
            mBannerImagesSliderAdapter =
                BannerImagesFullSliderAdapter(mActivity!!, dealerListData?.bannerImages)
            viewPager_main_bannerImagesDetails?.clipToPadding = true
//            viewPager_main_bannerImagesDetails?.setPadding(15, 0, 45, 0)
            viewPager_main_bannerImagesDetails?.adapter = mBannerImagesSliderAdapter

            Log.e(
                "System out",
                RestClient.imageUrlpath + dealerListData!!.bannerImages!![0]!!.dealerimageName + "&w=$widthNew&h=$heightNew&zc=0"
            )
//            svSudioImage?.setImageURI(Uri.parse(RestClient.imageUrlpath+dealerListData!!.bannerImages!![0]!!.dealerimageName+"&w=$widthNew&h=$heightNew&zc=0"),context)
        } else {
            svSudioImage?.visibility = View.VISIBLE
            viewPager_main_bannerImagesDetails?.visibility = View.GONE
            svSudioImage?.setActualImageResource(R.drawable.placeholder)
        }

        if (!dealerListData!!.dealerServiceType.isNullOrEmpty()) {
            tv_serviceType?.visibility = View.VISIBLE

            if (dealerListData?.dealerType.equals("Stitching Store")) {
                if (!dealerListData?.dealerServiceType.isNullOrEmpty() && dealerListData?.dealerServiceType.equals(
                        "Home Service",
                        false
                    )
                ) {
                    tv_serviceType?.text = "Home Delivery"
                } else if (!dealerListData?.dealerServiceType.isNullOrEmpty() && dealerListData?.dealerServiceType.equals(
                        "Self Service",
                        false
                    )
                ) {
                    tv_serviceType?.text = "Self Pickup"

                } else {
                    tv_serviceType?.text = "Home Delivery, Self Pickup"
                }
            } else if (dealerListData?.dealerType.equals("Stitching Studio")) {
                if (!dealerListData?.dealerServiceType.isNullOrEmpty() && dealerListData?.dealerServiceType.equals(
                        "Home Service",
                        false
                    )
                ) {
                    tv_serviceType?.text = "Home Service"
                } else if (!dealerListData?.dealerServiceType.isNullOrEmpty() && dealerListData?.dealerServiceType.equals(
                        "Self Service",
                        false
                    )
                ) {
                    tv_serviceType?.text = "Self Service"

                } else {
                    tv_serviceType?.text = "Home Service, Self Service"
                }
            }

//            tv_serviceType?.text = ""+dealerListData?.dealerServiceType
        } else {
            tv_serviceType?.visibility = View.GONE
        }

        if (!dealerListData!!.dealerRatting.isNullOrEmpty() && dealerListData!!.dealerRatting!!.toDouble() > 0.00 && !dealerListData!!.dealerReviewRattingCount.isNullOrEmpty() && dealerListData!!.dealerReviewRattingCount!!.toDouble() > 0.00) {
            tvProductRatingValue.text = (dealerListData!!.dealerRatting)
            tvProductRatingtotalUserCount.text =
                "(" + dealerListData!!.dealerReviewRattingCount + ")"

        } else {
            ratingReview.visibility = View.GONE
        }
        tv_StitchingStudioName.text = dealerListData!!.dealerCompanyName
        tv_StitchingStudioAddress.text =
            dealerListData!!.addressAddressLine1 + " " + dealerListData!!.addressLandmark + " " + dealerListData!!.addressLocality + " " + dealerListData!!.addressAddressLine2 + " " + dealerListData!!.stateName + " " + dealerListData!!.addressPincode

        setFavData()

        if (dealerListData!!.isWorking.equals("Open", false)) {
            tv_OpenCloseStaus.setBackgroundResource(R.drawable.open_status_green)
        } else {
            tv_OpenCloseStaus.setBackgroundResource(R.drawable.closed_status_green)

        }

        tv_OpenCloseStaus.text = dealerListData!!.isWorking
        if (!dealerListData?.distance.isNullOrEmpty() && !dealerListData?.distance.equals(
                "0.00",
                false
            )
        ) {
            tv_Distance.text = String.format(
                "%.2f",
                java.lang.Double.valueOf(dealerListData!!.distance?.toDouble()!!)
            ) + " km"

        } else {

            tv_Distance.visibility = View.GONE
        }

        if (!dealerListData!!.dealerIsPopular.isNullOrEmpty() && dealerListData!!.dealerIsPopular.equals(
                "Yes",
                false
            )
        ) {
            tvPopular.visibility = View.VISIBLE
        } else {
            tvPopular.visibility = View.GONE

        }
        if (!dealerListData!!.offerDiscountValue.isNullOrEmpty() && !dealerListData!!.offerDiscountValue.equals(
                "0.00",
                false
            )
        ) {
            tvOffer.visibility = View.GONE

            tvOffer.text =
                "Up to ${MyUtils.formatPricePerCountery(java.lang.Double.valueOf(dealerListData!!.offerDiscountValue.toString()))}% off"
        } else {
            tvOffer.visibility = View.GONE

        }


        itemProductCategoryImageDown.setOnClickListener {
            getAddProduct()
        }


    }

    private fun setFavData() {
        if (dealerListData!!.isFavourite.equals("Yes", false)) {
            itemProductCategoryImageDown.setImageResource(R.drawable.heart_red)
        } else {
            itemProductCategoryImageDown.setImageResource(R.drawable.heart_grey)

        }
    }

    private fun inflateLifestoreDetails(data: ArrayList<CategoryTypeData?>?) {
        categoryTypeListPojo = ArrayList()
        categoryTypeListPojo!!.addAll(data!!)
        ll_productcategorey.removeAllViews()
        for (i in 0 until categoryTypeListPojo!!.size) {
            val view: View = layoutInflater.inflate(
                R.layout.item_category_banner, // Custom view/ layout
                ll_productcategorey, // Root layout to attach the view
                false // Attach with root layout or not
            )

            val svLifeStyleDetails = view.findViewById<SimpleDraweeView>(R.id.svLifeStyleDetails)
            val tv_StitchingStudioCategoryTypeName1 =
                view.findViewById<AppCompatTextView>(R.id.tv_StitchingStudioCategoryTypeName1)

            svLifeStyleDetails.setImageURI(categoryTypeListPojo!![i]!!.categorytypeImage)
            tv_StitchingStudioCategoryTypeName1.text = categoryTypeListPojo!![i]!!.categorytypeName
            // svLifeStyleDetails.tag=i
            /*if (categoryTypeListPojo!![i]!!.categorytypeID.equals(categoryListPojo!![0]!!.data!![0]!!.categorytypeID)) {
                tv_StitchingStudioCategoryTypeName.text =
                    categoryTypeListPojo!![i]!!.categorytypeName
                categorytypeIDs = categoryTypeListPojo!![i]!!.categorytypeID!!
                categoryListData!!.clear()
                categoryListData?.addAll(categoryTypeListPojo!![i]!!.category!!)
                setAdapaterData(
                    (categoryTypeListPojo!![i]!!.category as ArrayList<Category?>?)!!,
                    categorytypeIDs
                )
            }*/
            svLifeStyleDetails.setOnClickListener {
                for (i2 in 0 until categoryTypeListPojo?.size!!) {

                    if (categoryTypeListPojo!![i]!!.categorytypeID.equals(categoryListPojo!![0]!!.data!![i2]!!.categorytypeID)) {
                        tv_StitchingStudioCategoryTypeName.text =
                            categoryTypeListPojo!![i]!!.categorytypeName
                        categorytypeIDs = categoryTypeListPojo!![i]!!.categorytypeID!!
                        categoryListData!!.clear()
                        categoryListData?.addAll(categoryTypeListPojo!![i]!!.category!!)
                        setAdapaterData(
                            (categoryTypeListPojo!![i]!!.category as ArrayList<Category?>?)!!,
                            categorytypeIDs
                        )
                        break
                    }

                }

            }
            ll_productcategorey.addView(view)
        }
    }


    private fun getCategoryTypeList(isRefreshData: Boolean) {
        if (!isRefreshData) {
            relativeprogressBar.visibility = View.VISIBLE
            nointernetMainRelativelayout.visibility = View.GONE

        }

        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
//            jsonObject.put("logindealerID", "0")
            jsonObject.put("logindealerID", dealerIDs)
            jsonObject.put("dealerType", dealerListData?.dealerType)
            jsonObject.put("languageID", userData.languageID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }


        var categoryTypeListModel = ViewModelProviders.of(this@LifestyleStoredetailsFragment)
            .get(CategoryTypeListModel::class.java)
        categoryTypeListModel.getCategoryTypeList(mActivity!!, false, jsonArray.toString())
            .observe(this@LifestyleStoredetailsFragment, Observer {
                if (it != null && it.isNotEmpty()) {
                    relativeprogressBar.visibility = View.GONE

                    if (it[0].status.equals("true", true)) {
                        categoryListPojo?.clear()
                        categoryListPojo?.addAll(it)
                        if (!it[0].data.isNullOrEmpty()) {
                            inflateLifestoreDetails(it[0].data!!)
                            tv_StitchingStudioCategoryTypeName.text =
                                it[0].data!![0]!!.categorytypeName
                            categorytypeIDs = it[0].data!![0]!!.categorytypeID!!
                        }
                        if (!it[0].data?.get(0)?.category.isNullOrEmpty()) {
                            categoryListData = ArrayList()
                            categoryListData!!.clear()
                            categoryListData!!.addAll(it[0].data?.get(0)?.category!!)
                            setAdapaterData(categoryListData!!, categorytypeIDs)
                        } else {
                            noDatafoundRelativelayout.visibility = View.VISIBLE
                            notificationRecyclerview.visibility = View.GONE
                        }
                    } else {

                        (activity as MainActivity).showSnackBar(it[0].message!!)
                    }

                } else {
                    try {
                        nointernetMainRelativelayout.visibility = View.VISIBLE
                        relativeprogressBar.visibility = View.GONE

                        if (MyUtils.isInternetAvailable(activity!!)) {
                            nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.something_went_wrong))
                            nointernettextview.text =
                                (activity!!.getString(R.string.error_something))
                            nointernettextview1.text = (this.getString(R.string.somethigwrong1))
                        } else {
                            nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.no_internet_connection))
                            nointernettextview1.text = (this.getString(R.string.internetmsg1))


                            nointernettextview.text =
                                (activity!!.getString(R.string.error_common_network))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            })
    }

    fun setAdapaterData(
        category: ArrayList<Category?>,
        categorytypeIDs: String
    ) {
        /* arrayCategoryParent= ArrayList()
       arrayCategoryParent!!.clear()
        for (i in 0 until 5)
        {
            var datum=FaqListPojo.Datum()

            datum!!.faqQuestion="Ethinic Wear"

            arrayCategoryParent?.add(datum)

        }*/
        if (!category.isNullOrEmpty()) {
            noDatafoundRelativelayout.visibility = View.GONE
            notificationRecyclerview.visibility = View.VISIBLE

            linearLayoutManager = LinearLayoutManager(activity)
            lifestyleStudioCategoryAdapter = LifestyleStudioCategoryAdapter(
                mActivity!!, object : LifestyleStudioCategoryAdapter.OnItemClickListner {
                    override fun onItemClick(
                        subcatName: ArrayList<String>,
                        pos: Int,
                        posSubCatId: Int
                    ) {


                    }
                }, category
                , categorytypeIDs, from, dealerIDs
            )
            // Removes blinks
            ((notificationRecyclerview.itemAnimator) as SimpleItemAnimator).supportsChangeAnimations =
                false
            notificationRecyclerview.layoutManager = linearLayoutManager
            notificationRecyclerview.adapter = lifestyleStudioCategoryAdapter
        } else {
            noDatafoundRelativelayout.visibility = View.VISIBLE
            notificationRecyclerview.visibility = View.GONE
        }

    }


    private fun getAddProduct() {

        if (dealerListData!!.isFavourite.equals("No", false)) {
            dealerListData!!.isFavourite = "Yes"
            setAddFavourite(dealerListData!!.dealerID, "AddFav")
        } else {
            dealerListData!!.isFavourite = "No"
            setAddFavourite(dealerListData!!.dealerID, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, type: String) {
        var data: RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@LifestyleStoredetailsFragment).get(
            DealerFavouriteViewModel::class.java
        )
        addFavDoctorModel.apiFunction(
            mActivity!!,
            false,
            data.userID!!,
            data.languageID!!,
            type,
            productID!!
        )
            .observe(this@LifestyleStoredetailsFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                dealerListData!!.isFavourite = "Yes"
                                //  (activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                dealerListData!!.isFavourite = "No"
                                //  (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            setFavData()


                        } else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu, inflater)

        var menuInflater = mActivity?.menuInflater
        menuInflater?.inflate(R.menu.header_menu, menu)
//        (activity as MainActivity).menuNotification?.isVisible = false
        menuSearch = menu.findItem(R.id.menu_search)

        var notification = menu.findItem(R.id.menu_notification)
        notification.isVisible = false
        var searchManager =
            mActivity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        if (menuSearch != null) {
            searchView = menuSearch?.actionView as SearchView
            searchView?.queryHint = "Search..."
        }
        searchView?.setSearchableInfo(searchManager.getSearchableInfo(mActivity!!.componentName))
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // Toast like print
                //   UserFeedback.show( "SearchOnQueryTextSubmit: " + query);

                if (query.length <= 1) {
                    when (from) {
                        "Stitching Store" -> {
                            productListData?.clear()
                            searchListViewAdapter?.notifyDataSetChanged()
                        }
                        "Stitching Studio" -> {
                            stitchingListData?.clear()
                            searchStitchingListViewAdapter?.notifyDataSetChanged()
                        }
                    }
                    list_Search.visibility = View.GONE
                } else {
                    if (list_Search.visibility != View.VISIBLE)
                        list_Search.visibility = View.VISIBLE

                    if (!productListData.isNullOrEmpty() || !stitchingListData.isNullOrEmpty()) {

                        when (from) {
                            "Stitching Store" -> {
                                var productDetailsFragment = ProductDetailsFragment()
                                Bundle().apply {
                                    this.putString("from", from)
                                    this.putSerializable("productListData", productListData!![0]!!)
                                    productDetailsFragment.arguments = this

                                }
                                (activity as MainActivity).navigateTo(
                                    productDetailsFragment,
                                    productDetailsFragment::class.java.name,
                                    true
                                )
                                productListData?.clear()
                                searchListViewAdapter?.notifyDataSetChanged()


                            }
                            "Stitching Studio" -> {
                                var stiDetailsFragment = StitchingDetailsFragment()
                                Bundle().apply {
                                    putString("from", from)
                                    putSerializable("stichingServicData", stitchingListData!![0]!!)
                                    stiDetailsFragment.arguments = this

                                }
                                (activity as MainActivity).navigateTo(
                                    stiDetailsFragment,
                                    stiDetailsFragment::class.java.name,
                                    true
                                )

                                stitchingListData?.clear()
                                searchStitchingListViewAdapter?.notifyDataSetChanged()

                            }

                        }

                    }
//                    when(from){
//                        "Stitching Store" -> {
//                            productListData?.clear()
//                            if (!stitchingListData.isNullOrEmpty()) {
//                                getSearchProduct(query)
//                            }
//                        }
//                        "Stitching Studio"->{
//                            stitchingListData?.clear()
//                            if (!stitchingListData.isNullOrEmpty()) {
//                                getSearchStitching(query)
//                            }
//                        }
//                    }
                }
                MyUtils.hideKeyboard1(mActivity!!)
//                if (!searchView?.isIconified!!) {
//                    searchView?.isIconified = false
//                }
                menuSearch?.collapseActionView()
                return true
            }

            override fun onQueryTextChange(s: String): Boolean {

                if (s.length <= 1) {
                    when (from) {
                        "Stitching Store" -> {
                            productListData?.clear()
                            searchListViewAdapter?.notifyDataSetChanged()
                        }
                        "Stitching Studio" -> {
                            stitchingListData?.clear()
                            searchStitchingListViewAdapter?.notifyDataSetChanged()
                        }
                    }
                    list_Search.visibility = View.GONE
                } else {
                    if (list_Search.visibility != View.VISIBLE)
                        list_Search.visibility = View.VISIBLE
                    when (from) {
                        "Stitching Store" -> {
                            productListData?.clear()
                            if (s.length > 2) {
                                getSearchProduct(s)
                            }
                        }
                        "Stitching Studio" -> {
                            stitchingListData?.clear()
                            if (s.length > 2) {
                                getSearchStitching(s)
                            }
                        }

                    }


                }
                return false
            }
        })

        return super.onCreateOptionsMenu(menu, menuInflater!!)

    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_search).isVisible = true
        menu.findItem(R.id.menu_notification).isVisible = false

    }

    private fun getSearchProduct(s: String) {
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "1")
            jsonObject.put("productIDs", "")
            jsonObject.put("categorytypeIDs", "")
            jsonObject.put("categoryIDs", "")
            jsonObject.put("subcatIDs", "")
            jsonObject.put("brandIDs", "")
            jsonObject.put("materialIDs", "")
            jsonObject.put("colorIDs", "")
            jsonObject.put("patternIDs", "")
            jsonObject.put("styleIDs", "")
            jsonObject.put("minPrice", "")
            jsonObject.put("maxPrice", "")
            jsonObject.put("dealerIDs", dealerIDs)
            jsonObject.put("searchkeyword", s)
            jsonObject.put("page", "0")
            jsonObject.put("pagesize", "100")
            jsonObject.put("sortby", "")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        productListModel?.getProductListList(mActivity!!, false, jsonArray.toString(), "")!!
            .observe(this@LifestyleStoredetailsFragment,
                Observer<List<ProductListPojo>> { t ->
                    if (t != null && t.isNotEmpty()) {
                        productListData?.clear()

                        if (t[0].status.equals("true")) {
                            list_Search.visibility = View.VISIBLE
                            productListData?.addAll(t.get(0).data!!)

                        } else {
                            list_Search.visibility = View.GONE
                        }
                        searchListViewAdapter?.notifyDataSetChanged()


                    } else {


                    }
                })
    }

    private fun getSearchStitching(s: String) {
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("dealerID", dealerIDs)
            jsonObject.put("languageID", "1")
            jsonObject.put("productIDs", "")
            jsonObject.put("categorytypeIDs", "0")
            jsonObject.put("categoryIDs", "")
            jsonObject.put("subcatIDs", "")
            jsonObject.put("stitchingserviceID", "0")
            jsonObject.put("sortby", "")
            jsonObject.put("minPrice", "")
            jsonObject.put("maxPrice", "")
            jsonObject.put("page", "0")
            jsonObject.put("pagesize", "100")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        var stichingServiceListModel =
            ViewModelProviders.of(this@LifestyleStoredetailsFragment)
                .get(StichingServiceListModel::class.java)
        stichingServiceListModel.getStichingListList(mActivity!!, false, jsonArray.toString(), "")
            .observe(this@LifestyleStoredetailsFragment,
                Observer<List<StichingServicePojo>> { t ->
                    if (t != null && t.isNotEmpty()) {
                        stitchingListData?.clear()

                        if (t[0].status.equals("true")) {
                            list_Search.visibility = View.VISIBLE
                            stitchingListData?.addAll(t.get(0).data!!)

                        } else {
                            list_Search.visibility = View.GONE
                        }
                        searchStitchingListViewAdapter?.notifyDataSetChanged()


                    } else {


                    }
                })
    }

}
