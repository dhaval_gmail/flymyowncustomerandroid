package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.OrderlistAdapter
import com.fil.flymyowncustomer.model.CancelOrderModel
import com.fil.flymyowncustomer.model.MyOrderListModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.MyOrderData
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PopupMenu
import com.fil.flymyowncustomer.util.PrefDb
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class OrdersListFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    private var tabposition: Int = 0
    var type = ""
    var orderlistAdapterer: OrderlistAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var list: ArrayList<String>? = null
    var mSelection :Int = -1

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var orderID :String=""
    var orderdetailsID :String=""
    var sessionManager: SessionManager? = null
    var myOderListData: ArrayList<MyOrderData?>?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

            v= inflater.inflate(R.layout.fragment_orders_list, container, false)


        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager= SessionManager(mActivity!!)
        if (arguments != null) {
            tabposition = arguments!!.getInt("position", -1)
            type = arguments!!.getString("type", "")
        }

        noDatafoundRelativelayout.visibility=View.GONE
        relativeprogressBar.visibility=View.GONE
        nointernetMainRelativelayout.visibility=View.GONE


            myOderListData= ArrayList()
            linearLayoutManager = LinearLayoutManager(mActivity!!)
            orderlistAdapterer = OrderlistAdapter(
                mActivity!!,
                object : OrderlistAdapter.OnItemClick {
                    override fun onClicklisneter(pos: Int, name: String, v: View) {
                        when(name){
                            "Option"->
                            {
                                list = java.util.ArrayList()
                                list!!.clear()
                                if(type.equals("New",false))
                                {
                                    list!!.add("Cancel")

                                }
                                else if (type.equals("Completed",false))
                                {
                                    list!!.add("Rate")
                                }

                                PopupMenu(activity!!, v, list!!).showPopUp(object :
                                    PopupMenu.OnMenuSelectItemClickListener {
                                    override fun onItemClick(item: String) {
                                        if(item.equals("Cancel",false)){

                                            openDialog(myOderListData!![pos]!!.orderID,"")
                                        }else if(item.equals("Rate",false)){
                                             var writeReviewFragment=WrireReviewFragment()
                                             var bundle=Bundle()
                                             bundle.putSerializable("orderData",myOderListData?.get(pos))
                                             writeReviewFragment.arguments=bundle
                                            (activity as MainActivity).navigateTo(WrireReviewFragment(),WrireReviewFragment::class.java.name,false)
                                        }

                                    }

                                })

                            }
                            "itemView"->
                            {
                                var orderDetailsFragment= OrderDetailsFragment()
                                Bundle().apply {
                                    putSerializable("orderData", myOderListData!![pos])
                                    putSerializable("type",type)
                                    orderDetailsFragment.arguments=this

                                }
                                (activity as MainActivity).navigateTo(orderDetailsFragment,orderDetailsFragment::class.java.name,true)
                            }
                        }
                    }

                },myOderListData!!,type
            )
            notificationRecyclerview.layoutManager = linearLayoutManager
            notificationRecyclerview.adapter = orderlistAdapterer



        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager?.childCount!!
                totalItemCount = linearLayoutManager?.itemCount!!
                firstVisibleItemPosition = linearLayoutManager?.findFirstVisibleItemPosition()!!
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10) {
                        isLoading = true
                        getMyOrderList()
                    }
                }
            }
        })
        if (myOderListData.isNullOrEmpty() || pageNo == 0) {
            pageNo = 0
            isLastpage = false
            isLoading = false
            getMyOrderList()
        }
        btnRetry.setOnClickListener {
            pageNo = 0
            isLastpage = false
            isLoading = false
            getMyOrderList()
        }
    }


    fun openDialog(orderID:String,orderdetailsID:String) {
        var list:ArrayList<String>
        list= ArrayList()
        list.clear()
        if(PrefDb(mActivity!!).getMasterData()!=null){
            if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty())
            {
               for(i in 0 until  PrefDb(mActivity!!).getMasterData()?.reason?.size!!)
               {
                   list.add( PrefDb(mActivity!!).getMasterData()?.reason?.get(i)?.reasonInfo!!)
               }
            }
        }
        val cancelReasonFragment = CancelReasonFragment()
        val bundle = Bundle()
        bundle.putInt("mSelection", mSelection)
        bundle.putString("from","cancel_order")
        bundle.putSerializable("list", list)
        cancelReasonFragment.arguments = bundle
        cancelReasonFragment.isCancelable = true
        cancelReasonFragment.show(childFragmentManager, "dialog")
        cancelReasonFragment.setListener(object :
            CancelReasonFragment.OnItemClickListener {
            override fun onSellerSelectionClick(position: Int, typeofOperation: String) {
               mSelection = position
                if(PrefDb(mActivity!!).getMasterData()!=null) {
                    if (!PrefDb(mActivity!!).getMasterData()?.reason.isNullOrEmpty()) {
                        MyUtils.showMessageOKCancel1(mActivity!!,
                    "Are you sure you want to Cancel Order?",
                    "Cancel",
                    DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                        getCancelOrderApi(orderID,orderdetailsID ,PrefDb(mActivity!!).getMasterData()?.reason!![position]?.reasonID!!,typeofOperation)
                    })
                      }
                }
            }
        })
    }


    private fun getMyOrderList()
    {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            myOderListData!!.clear()
            orderlistAdapterer?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            myOderListData!!.add(null)
            orderlistAdapterer?.notifyItemInserted(myOderListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "1")
            jsonObject.put("type",type)
            jsonObject.put("orderType","")
            jsonObject.put("IsRequestedForReturnReplace","No")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var myOrderListModel =
            ViewModelProviders.of(this@OrdersListFragment).get(MyOrderListModel::class.java)
        myOrderListModel.getMyOrderList(mActivity!!, false, jsonArray.toString(),"")
            .observe(this@OrdersListFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE

                        if (pageNo > 0) {
                            myOderListData!!.removeAt(myOderListData!!.size - 1)
                            orderlistAdapterer?.notifyItemRemoved(myOderListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                myOderListData!!.clear()

                            myOderListData!!.addAll(masterPojo[0].data!!)
                            orderlistAdapterer?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (myOderListData!!.isEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE


                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })

    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!))
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            }
            else
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun getCancelOrderApi(orderID:String,orderdetailsID:String,reasonID:String,remark:String)
    {
        MyUtils.showProgressDialog(mActivity!!)
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userData!!.userID)
            jsonObject.put("languageID", userData!!.languageID)
            jsonObject.put("orderID", orderID)
            jsonObject.put("orderdetailsID", orderdetailsID)
            jsonObject.put("orderdetailsstitchingID", "")
            jsonObject.put("reasonID", reasonID)
            jsonObject.put("Remark", remark)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        var cancelOrderModel =
            ViewModelProviders.of(this@OrdersListFragment).get(CancelOrderModel::class.java)
        cancelOrderModel.getCancelOrder(mActivity!!, false, jsonArray.toString(), "cancel")
            .observe(this@OrdersListFragment,
                Observer<List<CommonPojo>> { appointmentListPojo ->
                    run {
                        if (appointmentListPojo != null && appointmentListPojo.isNotEmpty()) {
                            if (appointmentListPojo[0].status.equals("true", true)) {
                                MyUtils.closeProgress()
                                (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                                if (parentFragment != null && parentFragment is MyOrderFragment) {
                                    pageNo = 0
                                    (parentFragment as MyOrderFragment).updateViewPager()
                                }
                            } else {
                                MyUtils.closeProgress()
                                (activity as MainActivity).showSnackBar(appointmentListPojo[0].message!!)
                            }
                        } else {
                            MyUtils.closeProgress()
                            (activity as MainActivity).errorMethod()
                        }

                    }
                })

    }


   /* override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        // super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && v != null) {

            pageNo = 0
            isLastpage = false
            isLoading = false
            getMyOrderList()

        }

    }*/



}
