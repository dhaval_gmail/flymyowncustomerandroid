package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.RegisterAddAddressActivity
import com.fil.flymyowncustomer.adapter.MyCartAdapter
import com.fil.flymyowncustomer.adapter.OrderlistAdapter
import com.fil.flymyowncustomer.util.PrefDb
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.fragment_my_order.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 *
 */
class MyOrderFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null

    var adapter: ViewPagerAdapter? = null
    var tabposition = 0
    public var menuNotification: AppCompatImageView?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null) {
            v = inflater.inflate(R.layout.fragment_my_order, container, false)
        }
        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = true

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility=View.VISIBLE
        menuNotification = v?.findViewById<AppCompatImageView>(R.id.menuNotification)
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text=mActivity!!.resources.getString(R.string.my_orders)
        menuNotification?.visibility=View.GONE
        toolbar_Imageview!!.visibility = View.GONE

        (mActivity as MainActivity).updateNavigationBarState(R.id.menu_MyOrder)
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.VISIBLE
        (mActivity as MainActivity).setDrawerSwipe(true)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        menuNotification?.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(),NotificationFragment::class.java.name,true)
        }

        setupViewPager(myOrdersViewPager!!)

    }

    private fun setupViewPager(viewPager: ViewPager) {
        if (adapter == null) {
            adapter = ViewPagerAdapter(childFragmentManager)
            adapter?.addFragment(OrdersListFragment(), "New")
            adapter?.addFragment(OrdersListFragment(), "Completed")
            adapter?.addFragment(OrdersListFragment(), "Cancelled")
            viewPager.adapter = adapter
            myOrdersTablayout!!.setupWithViewPager(myOrdersViewPager)
        }

    }

    inner class ViewPagerAdapter(manager: FragmentManager) :FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
//            val bundle = Bundle()
//            bundle.putInt("position", position)
//            val fragment = mFragmentList[position]
//            fragment.arguments = bundle


            val bundle = Bundle()
            bundle.putInt("position", position)

            if (position == 0)
                bundle.putString("type", "New")
            else if (position == 1)
                bundle.putString("type", "Completed")
            else if (position == 2)
                bundle.putString("type", "Cancelled")


            val fragment = mFragmentList[position]
            fragment.arguments = bundle

            return fragment
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun saveState(): Parcelable? {
            return null
        }
        override fun getItemPosition(`object`: Any): Int {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return PagerAdapter.POSITION_NONE
        }
        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }

    }

    fun updateViewPager() {
        myOrdersViewPager.adapter!!.notifyDataSetChanged()
    }
}
