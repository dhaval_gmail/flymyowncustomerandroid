package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.FilterActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.StichingServiceListingAdapter
import com.fil.flymyowncustomer.model.*
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_product_listing.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * A simple [Fragment] subclass.
 *
 */
class StichingServiceListingFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = StichingServiceListingFragment::class.java.name

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private  var linearLayoutManager: LinearLayoutManager?=null
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0

    var mStichingServiceListingAdapter: StichingServiceListingAdapter? = null
    var mSelection: Int = -1
    var masterData: ArrayList<MasterPojo?>? = null
    var sessionManager: SessionManager? = null

    /*var colorsIDs2: String = ""
    var materialsIDs2: String = ""
    var patternsIDs2: String = ""
    var stylesIDs2: String = ""
    var brandsIDs2: String = ""*/

    var maxValue2: String = ""
    var minValue2: String = ""

    var from: String = ""
    var sortBy: String = ""
    var subcategory: Subcategory? = null
    var productListData: ArrayList<StichingServicePojo.StichingServicData?>? = null
    var categorytypeIDs:String=""

    var categoryID:String=""
    var dealerIDs:String=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_product_listing, container, false)
        }
        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)

        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
//        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.back_arrow)
        toolbar_back.menuNotification.visibility = View.GONE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        ll_sort_filter.visibility=View.VISIBLE
        if (arguments != null) {
            from = arguments!!.getString("from")!!
            subcategory = arguments!!.getSerializable("subCategoryData") as Subcategory?
            categorytypeIDs=arguments!!.getString("categorytypeIDs")!!
            categoryID=arguments!!.getString("categoryID")!!
            dealerIDs=arguments!!.getString("dealerIDs")!!

            tvToolbarTitel.text = subcategory?.subcatName
        }


        sessionManager = SessionManager(mActivity!!)
        masterData = ArrayList()

        (mActivity as MainActivity).setDrawerSwipe(false)

        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        linearLayoutManager = GridLayoutManager(mActivity, 2)
        notificationRecyclerview?.layoutManager = linearLayoutManager

        bindData()

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager!!.getChildCount()
                totalItemCount = linearLayoutManager!!.getItemCount()
                firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                      getProductListApi("")
                    }
                }
            }
        })

        btnRetry?.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
            getProductListApi("")
        }

        llSortIngLayout?.setOnClickListener {
            val SortingDialogFragment = SortingFragment()
            val bundle = Bundle()
            bundle.putInt("mSelection", mSelection)

            SortingDialogFragment.arguments = bundle
            SortingDialogFragment.isCancelable = true
            SortingDialogFragment.show(childFragmentManager, "dialog")
            SortingDialogFragment.setListener(object :
                SortingFragment.OnItemClickListener {
                override fun onSellerSelectionClick(position: Int, typeofOperation: String) {
                    mSelection = position
                    sortBy=typeofOperation
                    pageNo=0
                    getProductListApi("")
                }
            })


        }
        llFilterLayout.setOnClickListener {
           /*if (masterData.isNullOrEmpty()) {
                getMastersList()
            } else {*/
                openFilter()

           // }
        }


    }

    private fun bindData() {

        if (productListData == null) {
            productListData = ArrayList()
            mStichingServiceListingAdapter = StichingServiceListingAdapter(
                activity as MainActivity,
                object : StichingServiceListingAdapter.onItemClickk {
                    override fun onClicklisneter(pos: Int,from:String) {
                        when(from)
                        {
                            "click"->{
                                var stiDetailsFragment= StitchingDetailsFragment()
                                Bundle().apply {
                                    putString("from",from)
                                    putSerializable("stichingServicData",productListData!![pos]!!)
                                    stiDetailsFragment.arguments=this

                                }
                                (activity as MainActivity).navigateTo(stiDetailsFragment, stiDetailsFragment::class.java.name, true)


                            }
                          "fav"->
                            {
                                getAddProduct(pos)
                            }
                        }


                    }

                },productListData!!)

            notificationRecyclerview?.setNestedScrollingEnabled(false)
            notificationRecyclerview?.setHasFixedSize(true)
            notificationRecyclerview?.adapter = mStichingServiceListingAdapter
            getProductListApi("")
        }
    }

    fun getMastersList() {
        MyUtils.showProgressDialog(mActivity!!)
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var masterListModel =
            ViewModelProviders.of(this@StichingServiceListingFragment).get(MasterListModel::class.java)
        masterListModel.getMasterList(mActivity!!, false, jsonArray.toString())
            .observe(this@StichingServiceListingFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {

                        if (masterPojo[0].status.equals("true", false)) {
                            MyUtils.closeProgress()
                            masterData!!.clear()
                            masterData!!.addAll(masterPojo)
                            openFilter()

                        } else {
                            MyUtils.closeProgress()
                            (activity as MainActivity).showSnackBar(masterPojo[0].message!!)
                        }

                    } else {
                        MyUtils.closeProgress()
                        (activity as MainActivity).errorMethod()
                    }
                })
    }


    fun openFilter() {
        Intent(mActivity!!, FilterActivity::class.java).apply {
             // putExtra("masterList", data as Serializable)
              putExtra("from","StitchingService")
              /*putExtra("colorsIDs2", colorsIDs2)
              putExtra("materialsIDs2", materialsIDs2)
              putExtra("patternsIDs2", patternsIDs2)
              putExtra("stylesIDs2", stylesIDs2)
              putExtra("brandsIDs2", brandsIDs2)*/
              putExtra("maxValue2", maxValue2)
              putExtra("minValue2", minValue2)
            startActivityForResult(this, 105)
        }
        mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


    }


    fun getProductListApi(searchKeyWord: String) {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        ll_sort_filter.visibility=View.GONE
        if (pageNo == 0) {

            relativeprogressBar!!.visibility = View.VISIBLE
            productListData!!.clear()
            mStichingServiceListingAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            productListData!!.add(null)
            mStichingServiceListingAdapter?.notifyItemInserted(productListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("dealerID", dealerIDs)
            jsonObject.put("languageID", "1")
            jsonObject.put("productIDs", "")
            jsonObject.put("categorytypeIDs", categorytypeIDs)
            jsonObject.put("categoryIDs", categoryID)
            jsonObject.put("subcatIDs", subcategory?.subcatID)
            jsonObject.put("stitchingserviceID", "0")
            jsonObject.put("sortby", sortBy)
            jsonObject.put("minPrice", minValue2)
            jsonObject.put("maxPrice", maxValue2)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        var stichingServiceListModel =
            ViewModelProviders.of(this@StichingServiceListingFragment).get(StichingServiceListModel::class.java)
        stichingServiceListModel.getStichingListList(mActivity!!, false, jsonArray.toString(), "")
            .observe(this@StichingServiceListingFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        notificationRecyclerview.visibility = (View.VISIBLE)
                        ll_sort_filter.visibility=View.VISIBLE

                        if (pageNo > 0) {
                            productListData!!.removeAt(productListData!!.size - 1)
                            mStichingServiceListingAdapter?.notifyItemRemoved(productListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                productListData!!.clear()

                            productListData!!.addAll(masterPojo[0].data!!)
                            mStichingServiceListingAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (productListData.isNullOrEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE
                                ll_sort_filter.visibility=View.GONE

                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE
                                ll_sort_filter.visibility=View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })


    }
    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE
            ll_sort_filter.visibility=View.GONE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //   super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            105 -> if (data != null) {
                /*colorsIDs2 = data.getStringExtra("colorsIDs1")
                brandsIDs2 = data.getStringExtra("brandsIDs1")
                stylesIDs2 = data.getStringExtra("stylesIDs1")
                patternsIDs2 = data.getStringExtra("patternsIDs1")
                materialsIDs2 = data.getStringExtra("materialsIDs1")*/
                maxValue2 = data.getStringExtra("maxValue1")
                minValue2 = data.getStringExtra("minValue1")
                pageNo=0
                getProductListApi("")

            }

        }


    }

    private fun getAddProduct(pos: Int) {

        if (productListData!![pos]!!.isYourFavourite.equals("No", false)) {
            productListData!![pos]!!.isYourFavourite = "Yes"
            setAddFavourite(productListData!![pos]!!.stitchingserviceID, pos, "AddFav")
        } else {
            productListData!![pos]!!.isYourFavourite = "No"
            setAddFavourite(productListData!![pos]!!.stitchingserviceID, pos, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, pos: Int, type: String) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@StichingServiceListingFragment).get(
            StitchingStoreFavouriteViewModel::class.java)
        addFavDoctorModel.apiFunction(activity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@StichingServiceListingFragment,
                Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                productListData!![pos]!!.isYourFavourite = "Yes"
                                //(activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                               /* if (where.equals("MyDoctor", false)) {
                                    searchDoctordata!![pos]!!.isFavorite = "No"
                                    searchDoctordata!!.removeAt(pos)
                                    searchDoctorAdapter!!.notifyItemRemoved(pos)
                                    searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                    (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                } else {*/
                                productListData!![pos]!!.isYourFavourite = "No"

                                  //  (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                                }
                               mStichingServiceListingAdapter!!.notifyDataSetChanged()
                            }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }
}
