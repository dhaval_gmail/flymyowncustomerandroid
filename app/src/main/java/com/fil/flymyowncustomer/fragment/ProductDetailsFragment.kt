package com.fil.flymyowncustomer.fragment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.ProductByActivity
import com.fil.flymyowncustomer.adapter.ProductDetailsCustomerLikedAdapter
import com.fil.flymyowncustomer.adapter.ProductDetailsViewPagerAdapter
import com.fil.flymyowncustomer.model.FavuriteProductListViewModel
import com.fil.flymyowncustomer.model.ProductInventoryModel
import com.fil.flymyowncustomer.model.ProductListModel
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_product_details.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class ProductDetailsFragment : Fragment() {

    var v: View? = null
    var mActivity: AppCompatActivity? = null
    var page_position = 0
    private var dotsCount: Int = 0
    var listColor: ArrayList<Int>? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var mProductDetailsCustomerLikedAdapter: ProductDetailsCustomerLikedAdapter? = null

    private  var dots: Array<ImageView?>?=null
    var productDetailsViewPagerAdapter: ProductDetailsViewPagerAdapter? = null
    var productListData: ProductListPojo.Data? = null
    var sessionManager: SessionManager? = null
    var selectedPos: Int = 0
    var selectedPos1: Int = 0
    var userProductListData: ArrayList<ProductListPojo.Data?>? = null
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var list: ArrayList<String>? = null
    var materialID: String = "0"
    var materialName: String = ""
    var brandID: String = "0"
    var brandName: String = ""
    var patternID: String = "0"
    var patternName: String = ""
    var categorytypeID: String = "0"
    var categoryID: String = "0"
    var subcatID: String = "0"
    var colorID: String = "0"
    var styleID: String = "0"
    var styleName: String = "0"
    var standardmeasurementID: String = "0"
    var productPriceId: String = "0"
    var cartCalculate: CartCalculate? = null
    var selectedDeliveryTypeName = ""
    var addButtonType = ""
    var haveAddress = false
    var tv_ProductStyle : AppCompatTextView? = null
    var tv_ProductMaterial : AppCompatTextView? = null
    var tv_ProductPatten : AppCompatTextView? = null
    var tv_ProductBrand : AppCompatTextView? = null
    var relativeprogressBar : RelativeLayout? = null
    var noDatafoundRelativelayout : RelativeLayout? = null
    var nointernetMainRelativelayout : RelativeLayout? = null
    var adToCartProductPrice = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        if (v == null) {
            v = inflater.inflate(R.layout.fragment_product_details, container, false)
//        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppCompatActivity) {
            mActivity = context
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(mActivity!!)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.visibility = View.GONE

        (activity as MainActivity).bottom_navigation?.visibility = View.GONE

        toolbar_back.menu_Cart.visibility = View.VISIBLE
        toolbar_back.menuHeartlike.visibility = View.VISIBLE

        toolbar_back.menuShare.visibility = View.VISIBLE

        (activity as MainActivity).setDrawerSwipe(false)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        toolbar_back.menu_Cart.setOnClickListener {
            (activity as MainActivity).navigateTo(MyCartFragment(), MyCartFragment::class.java.name, true)
        }

        relativeprogressBar = view?.findViewById(R.id.relativeprogressBar) as RelativeLayout
        noDatafoundRelativelayout = view?.findViewById(R.id.noDatafoundRelativelayout) as RelativeLayout
        nointernetMainRelativelayout = view?.findViewById(R.id.nointernetMainRelativelayout) as RelativeLayout

        tv_ProductStyle = v?.findViewById(R.id.tv_ProductStyle) as AppCompatTextView
        tv_ProductMaterial = v?.findViewById(R.id.tv_ProductMaterial) as AppCompatTextView
        tv_ProductPatten = v?.findViewById(R.id.tv_ProductPatten) as AppCompatTextView
        tv_ProductBrand = v?.findViewById(R.id.tv_ProductBrand) as AppCompatTextView

        toolbar_back.menuShare.setOnClickListener {
            val appPackageName = (activity as MainActivity).packageName // getPackageName() from Context or Activity object
            try {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, ""+"market://details?id=$appPackageName")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Share using")
                (activity as MainActivity).startActivity(shareIntent)
            } catch (anfe: ActivityNotFoundException) {
                (activity as MainActivity).startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }

        }




        cartCalculate = CartCalculate
        if(cartCalculate?.getCaSize()!! > 0)
        {
            toolbar_back.textNotify.visibility = View.VISIBLE
            toolbar_back.textNotify.text=cartCalculate?.getCaSize().toString()

        }
        else
        {
            toolbar_back.textNotify.visibility = View.GONE

        }
        if (arguments != null) {
            productListData =
                arguments!!.getSerializable("productListData") as ProductListPojo.Data?
        }

        tv_ProductOriginalPrice.paintFlags =
            tv_ProductOriginalPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        list = ArrayList()
        listColor = ArrayList()
        userProductListData = ArrayList()
        if (productListData != null) {
            setDataProductData()

        }

        tv_ProductCustomerliked_title.visibility = View.GONE
        notificationRecyclerview?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        val userData = SessionManager(mActivity!!).get_Authenticate_User()
        if (userData != null) haveAddress = !userData?.billing.isNullOrEmpty() else haveAddress =
            false

        btnByNow.setOnClickListener {
            if (haveAddress) {
                addButtonType = "Buy"
//                showDialog()
                if (!cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(
                        productListData!!,
                        false
                    )!!
                ) {
                    MyUtils.showMessageOKCancel(
                        mActivity!!,
                        "Your cart will be clear and add this product. In cart you can only add product of same studio and store.",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                if (cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(
                                        productListData!!,
                                        true
                                    )!!
                                ) {
                                    showDialog()

                                }
                            }
                        })
                } else showDialog()
            } else {
                MyUtils.showMessageOKCancel(
                    mActivity!!,
                    "Please add your delivery address.",
                    object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            (mActivity as MainActivity).navigateTo(
                                BillingAddressFragment(),
                                BillingAddressFragment::class.java.name,
                                true
                            )
                        }
                    })
            }
        }
        btnAddToCart.setOnClickListener {
            if (haveAddress) {
                addButtonType = "Cart"
                if (!cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(
                        productListData!!,
                        false
                    )!!
                ) {
                    MyUtils.showMessageOKCancel(
                        mActivity!!,
                        "Your cart will be clear and add this product. In cart you can only add product of same studio and store.",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                if (cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(
                                        productListData!!,
                                        true
                                    )!!
                                ) {
                                    showDialog()
                                }

                            }
                        })
                } else {
                    showDialog()
                }
            } else {
                MyUtils.showMessageOKCancel(
                    mActivity!!,
                    "Please add your delivery address.",
                    object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            (mActivity as MainActivity).navigateTo(
                                BillingAddressFragment(),
                                BillingAddressFragment::class.java.name,
                                true
                            )
                        }
                    })
            }
        }

        toolbar_back.menuHeartlike.setOnClickListener {
            getAddProduct()
        }
        toolbar_back.textNotify.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                MyCartFragment(),
                MyCartFragment::class.java.name,
                true
            )
        }
        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager!!.getChildCount()
                totalItemCount = linearLayoutManager!!.getItemCount()
                firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getUserProductListApi()
                    }
                }
            }
        })

        ll_rating.setOnClickListener {
             if(productListData?.productRatting!!.toDouble()>0.0&&productListData?.productReviewRattingCount!!.toDouble()>0.0)
             {
                 var reviewListFragment=ReviewListFragment()
                 var bundle=Bundle()
                 bundle.putString("from","product")
                 bundle.putSerializable("productListData",productListData)
                 reviewListFragment.arguments=bundle
                 (activity as MainActivity).navigateTo(reviewListFragment,reviewListFragment::class.java.name,true)

             }
         }
        ll_review.setOnClickListener {
            if(productListData?.productRatting!!.toDouble()>0.0&&productListData?.productReviewRattingCount!!.toDouble()>0.0) {
                var reviewListFragment = ReviewListFragment()
                var bundle = Bundle()
                bundle.putString("from","product")
                bundle.putSerializable("productListData", productListData)
                reviewListFragment.arguments = bundle
                (activity as MainActivity).navigateTo(
                    reviewListFragment,
                    reviewListFragment::class.java.name,
                    true
                )
            }
        }
    }

    private fun getRequiredDataForAddToCart() {

        var customerGst = ""
        val userData = SessionManager(mActivity!!).get_Authenticate_User()
        if (userData != null && !userData.billing.isNullOrEmpty()) {

            for (i in 0 until userData.billing!!.size) {
                if (userData.billing!![i].addressIsDefault.equals(
                        "true",
                        true
                    ) || userData.billing!![i].addressIsDefault.equals("Yes", true)
                ) {
                    if (!userData.billing!![i].addressGST.isNullOrEmpty() && !productListData?.dealerGST.isNullOrEmpty()) {
                        if (productListData?.dealerGST!!.substring(
                                0,
                                2
                            ) == userData.billing!![i].addressGST?.substring(0, 2)
                        ) {
                            productListData?.isIGST = false
                            break
                        } else {
                            productListData?.isIGST = true
                            break
                        }
                    } else {
                        productListData?.isIGST = true
                        break
                    }
                } else {
                    productListData?.isIGST = true
                    break
                }
            }
        } else {
            productListData?.isIGST = true
        }

        productListData?.addToCartColorId = colorID
        productListData?.addToCartMaterialId = materialID
        productListData?.addToCartPatternId = patternID
        productListData?.addToCartStyleId = styleID
        productListData?.addToCartBrandId = brandID
        productListData?.productPriceId = productPriceId.toDouble()
        productListData?.addToCartSizeId = standardmeasurementID
        productListData?.addToCartDeliveryName = selectedDeliveryTypeName
        productListData?.productDiscountedPrice = java.lang.Double.valueOf(adToCartProductPrice)

        val data: RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()
        if (!data?.settings.isNullOrEmpty() && !data?.settings!![0].settingsGSTOnFabric.isNullOrEmpty() &&
            !data?.settings!![0].settingsGSTOnReadymadeGarments.isNullOrEmpty() && !data?.settings!![0].settingsGSTOnOtherService.isNullOrEmpty()
        ) {

            if (productListData?.productType.equals("Readymade Garment", true)) {
                productListData?.categoryGST =
                    java.lang.Double.valueOf(data?.settings!![0].settingsGSTOnReadymadeGarments!!)
            } else if (productListData?.productType.equals(
                    "Others",
                    true
                ) || productListData?.productType.equals("Other", true)
            ) {
                productListData?.categoryGST =
                    java.lang.Double.valueOf(data?.settings!![0].settingsGSTOnOtherService!!)
            } else if (productListData?.productType.equals("Fabric", true)) {
                productListData?.categoryGST =
                    java.lang.Double.valueOf(data?.settings!![0].settingsGSTOnFabric!!)
            }
        }
//        productListData?.productDiscountedPrice = java.lang.Double.valueOf(standardmeasurementID)
    }

    private fun getProductData(
        categoryID: String,
        categorytypeID: String,
        brandID: String,
        subcatID: String,
        styleID: String,
        materialID: String,
        patternID: String,
        standardmeasurementID: String,
        colorID: String,
        s: String
    ) {

        if (s.isEmpty()) {
            rvMain.visibility = View.GONE
            rvOvrLayer.visibility = View.GONE
            nestedScrollviewMain.visibility = View.GONE
            progressDetails.visibility = View.VISIBLE
        } else if (s.equals("click", false)) {
            rvMain.visibility = View.VISIBLE
            rvOvrLayer.visibility = View.VISIBLE
            nestedScrollviewMain.visibility = View.VISIBLE
            progressDetails.visibility = View.VISIBLE
        }


        val userData = sessionManager!!.get_Authenticate_User()
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "1")
            jsonObject.put("productID", productListData!!.productID)
            jsonObject.put("categorytypeID", categorytypeID)
            jsonObject.put("categoryID", categoryID)
            jsonObject.put("subcatID", subcatID)
            jsonObject.put("brandID", brandID)
            jsonObject.put("materialID", materialID)
            jsonObject.put("colorID", colorID)
            jsonObject.put("patternID", patternID)
            jsonObject.put("styleID", styleID)
            jsonObject.put("standardmeasurementID", standardmeasurementID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        Log.e("stock", jsonArray.toString())

        val productInventoryModel =
            ViewModelProviders.of(this@ProductDetailsFragment)
                .get(ProductInventoryModel::class.java)
        productInventoryModel.ProductInventory(mActivity!!, false, jsonArray.toString(), "")
            .observe(this@ProductDetailsFragment,
                androidx.lifecycle.Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        if (masterPojo[0].status.equals("true", false)) {
                            if (s.isEmpty()) {
                                rvMain.visibility = View.VISIBLE
                                rvOvrLayer.visibility = View.GONE
                                nestedScrollviewMain.visibility = View.VISIBLE
                                progressDetails.visibility = View.GONE
                            } else if (s.equals("click", false)) {
                                rvMain.visibility = View.VISIBLE
                                rvOvrLayer.visibility = View.GONE
                                nestedScrollviewMain.visibility = View.VISIBLE
                                progressDetails.visibility = View.GONE
                            }
                            if (masterPojo[0].data?.get(0)!!.inventoryStock!!.toDouble() > 0.00) {
                                btnAddToCart.visibility = View.VISIBLE
                                btnByNow.visibility = View.VISIBLE
                                btnOutOfStock.visibility = View.GONE
                            } else {
                                btnAddToCart.visibility = View.GONE
                                btnByNow.visibility = View.GONE
                                btnOutOfStock.visibility = View.VISIBLE
                            }

                            if (!masterPojo[0].data.isNullOrEmpty() && !masterPojo[0].data!![0]?.inventoryStock.isNullOrEmpty()) {
                                productListData?.cartStock =
                                    java.lang.Double.valueOf(masterPojo[0].data!![0]?.inventoryStock!!)
                            }
                        } else {
                            if (s.isEmpty()) {
                                rvMain.visibility = View.VISIBLE
                                nestedScrollviewMain.visibility = View.VISIBLE
                                progressDetails.visibility = View.GONE
                            } else if (s.equals("click", false)) {
                                rvMain.visibility = View.VISIBLE
                                rvOvrLayer.visibility = View.GONE
                                nestedScrollviewMain.visibility = View.VISIBLE
                                progressDetails.visibility = View.GONE
                            }
                            btnAddToCart.visibility = View.GONE
                            btnByNow.visibility = View.GONE
                            btnOutOfStock.visibility = View.VISIBLE
                        }


                    } else {
                        if (s.isEmpty()) {
                            rvMain.visibility = View.VISIBLE
                            nestedScrollviewMain.visibility = View.VISIBLE
                            progressDetails.visibility = View.GONE
                        } else if (s.equals("click", false)) {
                            rvMain.visibility = View.VISIBLE
                            rvOvrLayer.visibility = View.GONE
                            nestedScrollviewMain.visibility = View.VISIBLE
                            progressDetails.visibility = View.GONE
                        }

                        (activity as MainActivity).errorMethod()
                    }
                })


    }

    private fun setDataProductData() {
        categoryID = productListData?.categoryID!!
        categorytypeID = productListData?.categorytypeID!!
        subcatID = productListData?.subcatID!!


        tv_StitchingStudioName.text = productListData!!.productName
        if(productListData?.productDescription != null && !productListData?.productDescription.isNullOrEmpty()){
            tv_ProductDescription.text = HtmlCompat.fromHtml(
                productListData?.productDescription!!,
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            tv_ProductDescription.visibility = View.VISIBLE
            tv_ProductDescription_title.visibility = View.VISIBLE

        }else {
            tv_ProductDescription.visibility = View.GONE
            tv_ProductDescription_title.visibility = View.GONE
            tv_ProductDescription.text = ""
        }

        tv_ProductRating.text = productListData!!.productRatting
        tv_ProductReview.text = productListData!!.productReviewRattingCount


        if (productListData!!.productIsPopular.equals("Yes", true)) {
            tvPopular.visibility = View.VISIBLE
        }else if (productListData!!.productIsPopular.equals("No", true)) {
            tvPopular.visibility = View.GONE
        }
        else {
            tvPopular.visibility = View.GONE
        }

        if (productListData!!.productStock.equals("1.00", false)) {
            tv_OfferLeftTime.visibility = View.GONE

        } else {
            tv_OfferLeftTime.visibility = View.GONE

        }
        if(productListData!!.productType.equals("Others",false))
        {
            ll_productcolor.visibility=View.GONE
            tv_Color_title.visibility=View.GONE
            ll_horizonatal_color.visibility=View.GONE
            tv_ProductDetails_title.visibility=View.GONE
            ll_product_details.visibility=View.GONE
        }
        else
        {
            ll_productcolor.visibility=View.VISIBLE
            tv_Color_title.visibility=View.VISIBLE
            ll_horizonatal_color.visibility=View.VISIBLE
            tv_ProductDetails_title.visibility=View.VISIBLE
            ll_product_details.visibility=View.VISIBLE
        }

        if (productListData!!.isYourFavourite.equals("Yes", false)) {
            toolbar_back.menuHeartlike.setImageResource(R.drawable.heart_red)
        } else {
            toolbar_back.menuHeartlike.setImageResource(R.drawable.heart_grey)
        }

        if (!productListData!!.productprice.isNullOrEmpty()) {

            tv_ProductPrice.text = resources.getString(R.string.rs) +
                    MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![0]!!.productpricePrice!!)).toString()
            adToCartProductPrice = productListData!!.productprice!![0]!!.productpricePrice!!
            if (!productListData!!.productprice!![0]!!.productpriceDiscount!!.isNullOrEmpty() && !productListData!!.productprice!![0]!!.productpriceDiscount!!.equals(
                    "0.00",
                    false
                )
            ) {
                tv_ProductOriginalPrice.text = resources.getString(R.string.rs) +
                        MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![0]!!.productpriceActualPrice!!)).toString()



                tv_ProductDisscountOffer.text = "" +
                        MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![0]!!.productpriceDiscount!!)).toString() +
                        "% Off"
            } else {
                tv_ProductOriginalPrice.visibility = View.GONE
                tv_ProductDisscountOffer.visibility = View.GONE
            }
            standardmeasurementID = productListData!!.productprice!![0]!!.standardmeasurementID!!
            productPriceId= productListData!!.productprice!![0]!!.productpriceID!!
        }

        if (!productListData!!.material.isNullOrEmpty()) {
            tv_ProductMaterial?.text = productListData!!.material!![0]!!.materialName
            materialID = productListData!!.material?.get(0)?.materialID!!
            ll_material.visibility = View.VISIBLE

        } else {
            ll_material.visibility = View.GONE
        }

        if (!productListData!!.pattern.isNullOrEmpty()) {
            tv_ProductPatten?.text = productListData!!.pattern!![0]!!.patternName
            ll_pattern.visibility = View.VISIBLE
            patternID = productListData!!.pattern!![0]!!.patternID!!

        } else {
            ll_pattern.visibility = View.GONE

        }
        if (!productListData!!.brand.isNullOrEmpty()) {
            tv_ProductBrand?.text = productListData!!.brand!![0]!!.brandName
            ll_brand.visibility = View.VISIBLE
            brandID = productListData!!.brand!![0]!!.brandID!!

        } else {
            ll_brand.visibility = View.GONE

        }

        if (!productListData!!.style.isNullOrEmpty()) {
            tv_ProductStyle?.text = productListData!!.style!![0].styleName
            ll_Style.visibility = View.VISIBLE
            styleID = productListData!!.style!![0]!!.styleID!!

        } else {
            ll_Style.visibility = View.GONE

        }
        ll_free_shipping.visibility = View.GONE

        if (productListData!!.dealerFreeServiceIsAvailable.equals("Yes", false)) {
            tv_ProductShipng.visibility = View.VISIBLE
            ll_free_shipping.visibility = View.GONE

        } else {
            tv_ProductShipng.visibility = View.GONE
            ll_free_shipping.visibility = View.GONE

        }

        getAddProductView(productListData!!.productID, "AddView")

        try {
            if (!productListData?.productimages.isNullOrEmpty()) {
                OfferAdapter(productListData?.productimages)

            }
            StichingStudioAdapter()
            if (!productListData!!.colors.isNullOrEmpty()) {
                if (!productListData?.colors.isNullOrEmpty() && !productListData?.colors!![0].colorID.isNullOrEmpty()) {
                    colorID = productListData?.colors!![0]!!.colorID!!
                }
                inflateProductColor()
            }
            if (!productListData!!.productprice.isNullOrEmpty()) {

                if (!productListData?.colors.isNullOrEmpty() && !productListData?.colors!![0].colorID.isNullOrEmpty()) {
                    colorID = productListData?.colors!![0]!!.colorID!!
                }

                inflateProductSize()
            }
        } catch (e: Exception) {
        }

        if (!productListData!!.style.isNullOrEmpty()) {
            styleID = productListData!!.style!![0]!!.styleID!!
        }

        getProductData(
            categoryID,
            categorytypeID,
            brandID,
            subcatID,
            styleID,
            materialID,
            patternID,
            standardmeasurementID,
            colorID,
            ""
        )

        getUserProductListApi()

        ll_material.setOnClickListener {
            list!!.clear()
            for (i in 0 until productListData?.material?.size!!) {
                list?.add(productListData?.material?.get(i)?.materialName!!)
            }
            Intent(mActivity!!, ProductByActivity::class.java)
                .apply {
                    putExtra("from", "By Material")
                    putExtra("List", list)
                    startActivityForResult(this, 107)
                }
            mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


        }
        ll_brand.setOnClickListener {
            list!!.clear()
            for (i in 0 until productListData?.brand?.size!!) {
                list?.add(productListData?.brand?.get(i)?.brandName!!)
            }
            Intent(mActivity!!, ProductByActivity::class.java)
                .apply {
                    putExtra("from", "By Brand")
                    putExtra("List", list)
                    startActivityForResult(this, 107)
                }
            mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


        }
        ll_pattern.setOnClickListener {
            list!!.clear()
            for (i in 0 until productListData?.pattern?.size!!) {
                list?.add(productListData?.pattern?.get(i)?.patternName!!)
            }
            Intent(mActivity!!, ProductByActivity::class.java)
                .apply {
                    putExtra("from", "By Pattern")
                    putExtra("List", list)
                    startActivityForResult(this, 107)
                }
            mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


        }
        ll_Style.setOnClickListener {
            list!!.clear()
            for (i in 0 until productListData?.style?.size!!) {
                list?.add(productListData?.style?.get(i)?.styleName!!)
            }
            Intent(mActivity!!, ProductByActivity::class.java)
                .apply {
                    putExtra("from", "By Style")
                    putExtra("List", list)
                    startActivityForResult(this, 107)
                }
            mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


        }


    }

    private fun inflateProductSize() {
        ll_productsize.removeAllViews()
        for (i in 0 until productListData?.productprice!!.size) {

            val view: View = layoutInflater.inflate(
                R.layout.item_product_size, // Custom view/ layout
                ll_productsize, // Root layout to attach the view
                false // Attach with root layout or not
            )

            val tvProductSize = view.findViewById<AppCompatTextView>(R.id.tvProductSize)
            val ll_size = view.findViewById<LinearLayout>(R.id.ll_size)

            tvProductSize.setText(productListData?.productprice!![i]!!.standardmeasurementName)

            productListData?.productDiscountedPrice =
                java.lang.Double.valueOf(productListData!!.productprice!![0]!!.productpricePrice!!)



            if (selectedPos1 >= 0 && selectedPos1 == i) {
                ll_size.background = resources.getDrawable(R.drawable.bg_square_trans_red)


            } else {
                ll_size.background =
                    resources.getDrawable(R.drawable.bg_square_trans_with_broder_black)
            }

            ll_size.tag = i
            ll_size.setOnClickListener {
                val pos: Int = ll_size.tag as Int
                tv_ProductPrice.text = resources.getString(R.string.rs) +
                        MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![pos]!!.productpricePrice!!)).toString()

                productListData?.productDiscountedPrice =
                    java.lang.Double.valueOf(productListData!!.productprice!![pos]!!.productpricePrice!!)

                adToCartProductPrice = productListData!!.productprice!![pos]!!.productpricePrice!!

                tv_ProductOriginalPrice.text = resources.getString(R.string.rs) +
                        MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![pos]!!.productpriceActualPrice!!)).toString()
                standardmeasurementID =
                    productListData!!.productprice!![pos]!!.standardmeasurementID!!
                productPriceId= productListData!!.productprice!![pos]!!.productpriceID!!

                getProductData(
                    categoryID,
                    categorytypeID,
                    brandID,
                    subcatID,
                    styleID,
                    materialID,
                    patternID,
                    standardmeasurementID,
                    colorID,
                    "click"
                )

                if (!productListData!!.productprice!![i]!!.productpriceDiscount!!.isNullOrEmpty() && !productListData!!.productprice!![pos]!!.productpriceDiscount!!.equals(
                        "0.00",
                        false
                    )
                ) {

                    tv_ProductDisscountOffer.text = "" +
                            MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData!!.productprice!![pos]!!.productpriceDiscount!!)).toString() +
                            "% Off"
                } else {
                    tv_ProductDisscountOffer.visibility = View.GONE
                }
                selectedPos1 = pos
                inflateProductSize()
            }
            ll_productsize.addView(view)

        }
    }

    private fun getAddProductView(productID: String?, type: String) {

        var data: RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@ProductDetailsFragment).get(
            FavuriteProductListViewModel::class.java
        )
        addFavDoctorModel.apiFunction(
            activity!!,
            false,
            data.userID!!,
            data.languageID!!,
            type,
            productID!!
        )
            .observe(this@ProductDetailsFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {


                        } else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })


    }

    private fun inflateProductColor() {
        /* listColor!!.clear()
         listColor!!.add(R.drawable.radio_button_additional_details_checked)
         listColor!!.add(R.drawable.radio_button_additional_details_checked)
         listColor!!.add(R.drawable.radio_button_additional_details_checked)
         listColor!!.add(R.drawable.radio_button_additional_details_checked)*/

        ll_productcolor.removeAllViews()
        for (i in 0 until productListData?.colors!!.size) {

            val view: View = layoutInflater.inflate(
                R.layout.item_product_color, // Custom view/ layout
                ll_productcolor, // Root layout to attach the view
                false // Attach with root layout or not
            )

            val svLifeStyleDetails = view.findViewById<SimpleDraweeView>(R.id.svLifeStyleDetails)
            val ll_colors = view.findViewById<LinearLayout>(R.id.ll_colors)

            svLifeStyleDetails.setImageURI(productListData?.colors!![i]!!.colorImage)

            if (selectedPos >= 0 && selectedPos == i) {
                ll_colors.background = resources.getDrawable(R.drawable.border_selected_color)


            } else {
                ll_colors.background = null
            }

            ll_colors.tag = i
            ll_colors.setOnClickListener {
                val pos: Int = ll_colors.tag as Int
                colorID = productListData?.colors!![pos]!!.colorID!!
                getProductData(
                    categoryID,
                    categorytypeID,
                    brandID,
                    subcatID,
                    styleID,
                    materialID,
                    patternID,
                    standardmeasurementID,
                    colorID,
                    "click"
                )
                selectedPos = pos
                inflateProductColor()
            }
            ll_productcolor.addView(view)

        }


    }

    fun OfferAdapter(productimages: List<ProductListPojo.Data.Productimage?>?) {
        productDetailsViewPagerAdapter =
            ProductDetailsViewPagerAdapter(mActivity as MainActivity, productimages)
        viewPager_main_fragment.clipToPadding = false
        viewPager_main_fragment?.adapter = productDetailsViewPagerAdapter
        viewPager_main_fragment?.addOnPageChangeListener(viewPagerChangeListener)
//        if (prodimgNameStr!!.size > 1) {
        addBottomDots(0)
        sliderBanner(productimages!!)
//        } else {
//            addBottomDots(0)
//        }
    }

    fun addBottomDots(currentPage: Int) {
        dotsCount = productListData?.productimages!!.size

        layoutDotsTutorial?.removeAllViews()
        dots = arrayOfNulls(dotsCount)

        for (i in 0 until dotsCount) {

            dots!![i] = ImageView(mActivity)
            dots!![i]?.setImageResource(R.drawable.slider_dot_unselected)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            dots!![i]?.setPadding(0, 0, 0, 0)
            dots!![i]?.layoutParams = params
            layoutDotsTutorial?.addView(dots!![i]!!, params)
            layoutDotsTutorial?.bringToFront()

        }
        if (dots!!.size > 0 && dots?.size!! > currentPage) {
            dots!![currentPage]?.setImageResource(R.drawable.slider_dot_selected)
        }

    }

    fun sliderBanner(productimages: List<ProductListPojo.Data.Productimage?>?) {
        val handler = Handler()
        val update = Runnable {
            try {
//                if (images != null && images!!.size > 0) {
                if (productimages?.size!! > 0) {
                    if (page_position == productimages?.size) {
                        page_position = 0

                    } else {
                        page_position = page_position + 1
                    }
                    viewPager_main_fragment?.setCurrentItem(page_position, true)
                }
            } catch (e: NullPointerException) {

            }
        }
        Timer().schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 15000)
    }

    internal var viewPagerChangeListener: ViewPager.OnPageChangeListener =
        object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                page_position = position
            }

            override fun onPageSelected(position: Int) {
                page_position = position
                addBottomDots(position)

            }
        }


    fun StichingStudioAdapter() {
        linearLayoutManager =
            LinearLayoutManager(mActivity!!, LinearLayoutManager.HORIZONTAL, false)
        notificationRecyclerview.layoutManager = linearLayoutManager
        mProductDetailsCustomerLikedAdapter = ProductDetailsCustomerLikedAdapter(
            mActivity!!,
            object : ProductDetailsCustomerLikedAdapter.onItemClickk {

                override fun onClicklisneter(pos: Int, from: String) {
                    when (from) {
                        "click" -> {
                            var productDetailsFragment = ProductDetailsFragment()
                            Bundle().apply {
                                putSerializable("productListData", userProductListData!![pos]!!)
                                productDetailsFragment.arguments = this

                            }
                            (activity as MainActivity).navigateTo(
                                productDetailsFragment,
                                productDetailsFragment::class.java.name,
                                true
                            )

                        }
                        "AddFav" -> {
                            getAddUserLikedProduct(pos)
                        }
                    }

                }

            },
            userProductListData!!
        )
        notificationRecyclerview?.isNestedScrollingEnabled = false
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview?.adapter = mProductDetailsCustomerLikedAdapter
    }


    private fun showDialog() {
        var deliveryType = ""
        var deliveryFreeService = ""
        var deliveryServiceCharges = ""

        var isHome = false
        var isService = false

        if (!productListData?.dealerServiceType.isNullOrEmpty()) {
            deliveryType = productListData?.dealerServiceType!!
        }

        if (!productListData?.dealerFreeServiceIsAvailable.isNullOrEmpty()) {
            deliveryFreeService = productListData?.dealerFreeServiceIsAvailable!!
        }

        if (!productListData?.dealerDeliveryCharges.isNullOrEmpty()) {
            deliveryServiceCharges = productListData?.dealerDeliveryCharges!!
        }


        if (deliveryType.equals("Home Service", true)) {
            isHome = true
            isService = false
//            selectedDeliveryTypeName = "Home Service"
        } else if (deliveryType.equals("Self Service", true)) {
            isService = true
            isHome = false
//            selectedDeliveryTypeName = "Self Service"
        } else if (deliveryType.equals("Both", true)) {
            isHome = true
            isService = true
        }


        val dialogs = MaterialAlertDialogBuilder(mActivity!!)

        //   dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)

        val dialogView = mActivity!!.layoutInflater.inflate(R.layout.popup_delivery, null)

        val ll_home_delivery = dialogView.findViewById<LinearLayout>(R.id.ll_home_delivery)
        val ll_self_pickup = dialogView.findViewById<LinearLayout>(R.id.ll_self_pickup)
        val img_home_delivery = dialogView.findViewById<ImageView>(R.id.img_home_delivery)
        val img_self_pickup = dialogView.findViewById<ImageView>(R.id.img_self_pickup)
        val tv_Cancel = dialogView.findViewById<AppCompatTextView>(R.id.tv_Cancel)
        val tv_Done = dialogView.findViewById<AppCompatTextView>(R.id.tv_Done)
        val tv_self_pickup = dialogView.findViewById<AppCompatTextView>(R.id.tv_self_pickup)
        if (isHome && !isService) {
            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.GONE
        } else if (!isHome && isService) {
            ll_home_delivery.visibility = View.GONE
            ll_self_pickup.visibility = View.VISIBLE
        } else if (isHome && isService) {
            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.VISIBLE
        }

        dialogs.setView(dialogView)
        val alertDialog = dialogs.create()
        alertDialog.show()
        tv_Cancel.setOnClickListener {
            alertDialog.dismiss()
        }

        tv_Done.setOnClickListener {
            if (selectedDeliveryTypeName.isNullOrEmpty()) {
                MyUtils.showSnackbarkotlin(
                    mActivity!!,
                    productDetailLayoutMain,
                    "Please select delivery type."
                )
            } else {
                alertDialog.dismiss()
                addTocartProduct(addButtonType)
            }
        }
        ll_home_delivery.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_checked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            selectedDeliveryTypeName = "Home Service"
            if (deliveryFreeService.equals("Yes", true)) {
                productListData?.addToCartDeliveryCharges = 0.0
            } else {
                productListData?.addToCartDeliveryCharges =
                    java.lang.Double.valueOf(productListData?.dealerDeliveryCharges!!)
            }
        }
        ll_self_pickup.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_checked)
            selectedDeliveryTypeName = "Self Service"
            productListData?.addToCartDeliveryCharges = 0.0
        }
    }

    private fun addTocartProduct(from: String) {
        getRequiredDataForAddToCart()
        when (from) {
            "Cart" -> {
                val cartId = cartCalculate?.getCartId(productListData!!)
                if (cartCalculate?.AddToCart(productListData!!, true, false, cartId!!)!!) {
                    toolbar_back.textNotify.visibility = View.VISIBLE
                    toolbar_back.textNotify.text=cartCalculate?.getCaSize().toString()
                    (activity as MainActivity).setCartBadgeCount()
                } else {
                    if (cartCalculate?.getCartQuantityPerCartId(cartId!!)!!.toInt() >= productListData?.cartStock!!) {
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            productDetailLayoutMain!!,
                            "Available  stock is " + MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData?.cartStock!!)) + "."
                        )
                    }else{
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            productDetailLayoutMain!!,
                            "Please try again later."
                        )
                    }
                }
            }

            "Buy" -> {
//                if (!cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(productListData!!, false)!!) {
//                    MyUtils.showMessageOKCancel(
//                        mActivity!!,
//                        "Your cart will be clear and add this product. In cart you can only add product of same seller.",
//                        object : DialogInterface.OnClickListener {
//                            override fun onClick(dialog: DialogInterface?, which: Int) {
//
//                                if (selectedDeliveryTypeName.isNullOrEmpty()) {
//                                    showDialog()
//                                } else {
//                                    if (cartCalculate?.checkDealerItemAlreadyItemInCartOrNotAndClear(
//                                            productListData!!,
//                                            true
//                                        )!!
//                                    ) {
//                                        if (cartCalculate?.AddToCart(productListData!!, true, false)!!) {
//                                            (mActivity as MainActivity).navigateTo(
//                                                MyCartFragment(),
//                                                MyCartFragment::class.java.name,
//                                                true
//                                            )
//                                        } else {
//                                            if (!productListData!!.issueMsg.isNullOrEmpty()) {
//                                                MyUtils.showSnackbarkotlin(
//                                                    mActivity!!,
//                                                    productDetailLayoutMain!!,
//                                                    productListData!!.issueMsg
//                                                )
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        })
//                } else {

//                    if (selectedDeliveryTypeName.isNullOrEmpty()) {
//                        showDialog()
//                    } else {
                val cartId = cartCalculate?.getCartId(productListData!!)
                if (cartCalculate?.AddToCart(productListData!!, true, false, cartId!!)!!) {
                    (mActivity as MainActivity).navigateTo(
                        MyCartFragment(),
                        MyCartFragment::class.java.name,
                        true
                    )
                } else {
                    if (cartCalculate?.getCartQuantityPerCartId(cartId!!)!!.toInt() <= productListData?.cartStock!!) {
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            productDetailLayoutMain!!,
                            "Available  stock is " + MyUtils.formatPricePerCountery(java.lang.Double.valueOf(productListData?.cartStock!!)) + "."
                        )
                    }else{
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            productDetailLayoutMain!!,
                            "Please try again later."
                        )
                    }
                }
//                    }
//                }
            }

        }


    }

    fun getUserProductListApi() {
        tv_ProductCustomerliked_title.visibility = View.GONE
        notificationRecyclerview?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            userProductListData!!.clear()
            mProductDetailsCustomerLikedAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            userProductListData!!.add(null)
            mProductDetailsCustomerLikedAdapter?.notifyItemInserted(userProductListData!!.size - 1)
        }

        var userData = sessionManager!!.get_Authenticate_User()
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()

        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "1")
            jsonObject.put("productIDs", "")
            jsonObject.put("categorytypeIDs", productListData?.categorytypeID)
            jsonObject.put("categoryIDs", productListData?.categoryID)
            jsonObject.put("subcatIDs", productListData?.subcatID)
            jsonObject.put("brandIDs", "")
            jsonObject.put("materialIDs", "")
            jsonObject.put("colorIDs", "")
            jsonObject.put("patternIDs", "")
            jsonObject.put("styleIDs", "")
            jsonObject.put("minPrice", "")
            jsonObject.put("maxPrice", "")
            jsonObject.put("dealerIDs", "")
            jsonObject.put("searchkeyword", "")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("sortby", "")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var productListModel =
            ViewModelProviders.of(this@ProductDetailsFragment).get(ProductListModel::class.java)
        productListModel.getProductListList(mActivity!!, false, jsonArray.toString(), "UserLike")
            .observe(this@ProductDetailsFragment,
                androidx.lifecycle.Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        noDatafoundRelativelayout?.visibility = View.GONE
                        nointernetMainRelativelayout?.visibility = View.GONE
                        relativeprogressBar?.visibility = View.GONE

                        if (pageNo > 0) {
                            userProductListData!!.removeAt(userProductListData!!.size - 1)
                            mProductDetailsCustomerLikedAdapter?.notifyItemRemoved(
                                userProductListData!!.size
                            )
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                userProductListData!!.clear()
                            for (i in 0 until masterPojo[0].data!!.size) {
                                if (masterPojo[0].data!![i]!!.productID!!.equals(
                                        productListData?.productID,
                                        false
                                    )
                                ) {
                                    masterPojo[0].data!!.removeAt(i)
                                    break
                                }
                            }
                            userProductListData!!.addAll(masterPojo[0].data!!)
                            mProductDetailsCustomerLikedAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }
                            tv_ProductCustomerliked_title.visibility = View.VISIBLE
                            notificationRecyclerview.visibility = View.VISIBLE


                        } else {
                            relativeprogressBar?.visibility = View.GONE
                            if (userProductListData.isNullOrEmpty()) {
                                tv_ProductCustomerliked_title.visibility = View.GONE
                                notificationRecyclerview.visibility = View.GONE

                            } else {
                                tv_ProductCustomerliked_title.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.VISIBLE
                            }
                        }

                    } else {
                        relativeprogressBar?.visibility = View.GONE
                        tv_ProductCustomerliked_title.visibility = View.GONE
                        notificationRecyclerview.visibility = View.GONE
                    }
                })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //   super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            107 -> if (data != null) {


                ProductByData(data.getStringExtra("from"), data.getStringExtra("name"))
            }


        }
    }

    public fun ProductByData(from: String, selectedName: String) {


        when (from) {
            "By Material" -> {
                if (!productListData!!.material.isNullOrEmpty()) {
                    for (i in 0 until productListData!!.material?.size!!) {
                        if (selectedName.equals(
                                productListData!!.material?.get(i)?.materialName!!,
                                false
                            )
                        ) {
                            materialID = productListData!!.material?.get(i)?.materialID!!
                            materialName = productListData!!.material?.get(i)?.materialName!!
                            tv_ProductMaterial?.text = materialName
                            break
                        }

                    }
                    getProductData(
                        categoryID,
                        categorytypeID,
                        brandID,
                        subcatID,
                        styleID,
                        materialID,
                        patternID,
                        standardmeasurementID,
                        colorID,
                        "click"
                    )


                }
            }
            "By Brand" -> {
                if (!productListData!!.brand.isNullOrEmpty()) {
                    for (i in 0 until productListData!!.brand?.size!!) {
                        if (selectedName.equals(
                                productListData!!.brand?.get(i)?.brandName!!,
                                false
                            )
                        ) {
                            brandID = productListData!!.brand?.get(i)?.brandID!!
                            brandName = productListData!!.brand?.get(i)?.brandName!!
                            tv_ProductBrand?.text = brandName
                            break
                        }
                    }
                    getProductData(
                        categoryID,
                        categorytypeID,
                        brandID,
                        subcatID,
                        styleID,
                        materialID,
                        patternID,
                        standardmeasurementID,
                        colorID,
                        "click"
                    )

                }

            }
            "By Pattern" -> {
                if (!productListData!!.pattern.isNullOrEmpty()) {
                    for (i in 0 until productListData!!.pattern?.size!!) {
                        if (selectedName.equals(
                                productListData!!.pattern?.get(i)?.patternName!!,
                                false
                            )
                        ) {

                            patternID = productListData!!.pattern?.get(i)?.patternID!!
                            patternName = productListData!!.pattern?.get(i)?.patternName!!
                            tv_ProductPatten?.text = patternName
                            break
                        }
                    }
                    getProductData(
                        categoryID,
                        categorytypeID,
                        brandID,
                        subcatID,
                        styleID,
                        materialID,
                        patternID,
                        standardmeasurementID,
                        colorID,
                        "click"
                    )

                }
            }
            "By Style" -> {
                if (!productListData!!.style.isNullOrEmpty()) {
                    for (i in 0 until productListData!!.style?.size!!) {
                        if (selectedName.equals(productListData!!.style?.get(i)?.styleName!!, false)) {

                            styleID = productListData!!.style?.get(i)?.styleID!!
                            styleName = productListData!!.style?.get(i)?.styleName!!
                            tv_ProductStyle?.text = styleName
                            break
                        }
                    }
                    getProductData(
                        categoryID,
                        categorytypeID,
                        brandID,
                        subcatID,
                        styleID,
                        materialID,
                        patternID,
                        standardmeasurementID,
                        colorID,
                        "click"
                    )

                }
            }
        }


    }


    private fun getAddProduct() {

        if (productListData!!.isYourFavourite.equals("No", false)) {
            productListData!!.isYourFavourite = "Yes"
            setAddFavourite(productListData!!.productID, "AddFav")
        } else {
            productListData!!.isYourFavourite = "No"
            setAddFavourite(productListData!!.productID, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, type: String) {
        var data: RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@ProductDetailsFragment).get(
            FavuriteProductListViewModel::class.java
        )
        addFavDoctorModel.apiFunction(
            activity!!,
            false,
            data.userID!!,
            data.languageID!!,
            type,
            productID!!
        )
            .observe(this@ProductDetailsFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                productListData!!.isYourFavourite = "Yes"
                                //(activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                productListData!!.isYourFavourite = "No"

                                // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            if (productListData!!.isYourFavourite.equals("Yes", false)) {
                                toolbar_back.menuHeartlike.setImageResource(R.drawable.heart_red)
                            } else {
                                toolbar_back.menuHeartlike.setImageResource(R.drawable.heart_grey)

                            }
                        } else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    private fun getAddUserLikedProduct(pos: Int) {

        if (userProductListData!![pos]!!.isYourFavourite.equals("No", false)) {
            userProductListData!![pos]!!.isYourFavourite = "Yes"
            setAddUserLikedFavourite(userProductListData!![pos]!!.productID, pos, "AddFav")
        } else {
            userProductListData!![pos]!!.isYourFavourite = "No"
            setAddUserLikedFavourite(userProductListData!![pos]!!.productID, pos, "RemoveFav")
        }

    }

    private fun setAddUserLikedFavourite(productID: String?, pos: Int, type: String) {
        var data: RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@ProductDetailsFragment).get(
            FavuriteProductListViewModel::class.java
        )
        addFavDoctorModel.apiFunction(
            activity!!,
            false,
            data.userID!!,
            data.languageID!!,
            type,
            productID!!
        )
            .observe(this@ProductDetailsFragment,
                androidx.lifecycle.Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("AddFav", false)) {
                                userProductListData!![pos]!!.isYourFavourite = "Yes"
                                //(activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                /* if (where.equals("MyDoctor", false)) {
                                     searchDoctordata!![pos]!!.isFavorite = "No"
                                     searchDoctordata!!.removeAt(pos)
                                     searchDoctorAdapter!!.notifyItemRemoved(pos)
                                     searchDoctorAdapter!!.notifyItemChanged(pos, searchDoctordata!!.size)

                                     (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavdoctor))
                                 } else {*/
                                userProductListData!![pos]!!.isYourFavourite = "No"

                                // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))

                            }
                            mProductDetailsCustomerLikedAdapter?.notifyDataSetChanged()
                        } else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }


    private fun addItemToCart() {

    }

}
