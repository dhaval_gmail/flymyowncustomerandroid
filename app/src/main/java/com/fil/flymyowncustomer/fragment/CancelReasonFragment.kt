package com.fil.flymyowncustomer.fragment


import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle

import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.farmerprice.adapter.SortByAdapter

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.iterfaces.RecyclerViewItemClickListner
import com.fil.flymyowncustomer.pojo.SortingPojo
import kotlinx.android.synthetic.main.fragment_sorting.view.*
import java.util.ArrayList
import androidx.constraintlayout.widget.ConstraintAttribute.setAttributes

import android.util.TypedValue
import android.util.DisplayMetrics
import android.R.attr.y
import android.R.attr.x
import android.annotation.SuppressLint
import android.graphics.Point
import android.view.*
import androidx.constraintlayout.widget.ConstraintAttribute.setAttributes

import android.view.Window.FEATURE_NO_TITLE
import android.widget.RelativeLayout
import com.google.android.material.resources.MaterialResources.getDimensionPixelSize
import android.view.Display
import android.view.WindowManager
import android.widget.Toast
import com.fil.farmerprice.adapter.CancelReasonAdapter

/**
 * A simple [Fragment] subclass.
 *
 */
class CancelReasonFragment : DialogFragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mActivity = AppCompatActivity()
    var v:View?= null
    var mSelection :Int = -1
    var OnSelectionItemClickListener : OnItemClickListener?=null;
    var cancelReasonAdapter: CancelReasonAdapter?= null;

    var list:ArrayList<String>?=null
    var from:String=""
    var pos1=-1
    var strtypeofOperation=""
    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mSelection = arguments?.getInt("mSelection", 0)!!
            list = (arguments?.getSerializable("list") as ArrayList<String>?)!!
            from=arguments?.getString("from")!!
        }

        val style = DialogFragment.STYLE_NO_FRAME
        val theme = R.style.DialogTheme
        setStyle(style, theme)

    }

    /*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //val dialog = super.onCreateDialog(savedInstanceState)
       super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(context!!)

        setMargins( dialog, 20, 0, 20, 40 );

        val contentView = View.inflate(context, R.layout.fragment_sorting, null)
        dialog.setContentView(contentView)
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

         v = inflater.inflate(R.layout.fragment_sorting, container, false)
        dialog!!.setCancelable(true)
        dialog!!.setCanceledOnTouchOutside(true)

        setMargins( dialog!!, 20, 0, 20, 40 );


        linearLayoutManager = LinearLayoutManager(mActivity)
        when(from)
        {
            "Sort"->{
                v?.product_sort_dialog_textview?.text="Sort By"
                v?.btnSave?.visibility=View.GONE

            }
            "Filter"->{
                v?.product_sort_dialog_textview?.text="Filter By"
                v?.btnSave?.visibility=View.GONE

            }
            "cancel_order"->{
                v?.product_sort_dialog_textview?.text="Cancel order"
                v?.btnSave?.visibility=View.VISIBLE
            }
        }

        v?.product_sort_dialog_recyclerview?.layoutManager = linearLayoutManager
        v?.product_sort_dialog_recyclerview?.hasFixedSize()
        cancelReasonAdapter = CancelReasonAdapter(mActivity, list,object :
            RecyclerViewItemClickListner {
            override fun onItemClick(position: Int, typeofOperation: String) {
                if(cancelReasonAdapter!!.mSelection > -1){
                    pos1=position
                    strtypeofOperation=typeofOperation
                    when(from)
                    {
                        "Sort"->{
                            OnSelectionItemClickListener!!.onSellerSelectionClick(position,strtypeofOperation)
                            mSelection = position
                            dismiss()
                        }
                        "Filter"->{
                            OnSelectionItemClickListener!!.onSellerSelectionClick(position,strtypeofOperation)
                            mSelection = position
                            dismiss()
                        }

                    }

                }
            }

        })
        v?.product_sort_dialog_recyclerview?.adapter = cancelReasonAdapter
        cancelReasonAdapter?.mSelection = mSelection
        cancelReasonAdapter?.notifyDataSetChanged()
        v?.product_sort_dialog_close_imageview?.setOnClickListener {
            dismiss()
        }

        v?.btnSave?.setOnClickListener {
            if(strtypeofOperation.isNullOrEmpty())
            {
                Toast.makeText(mActivity,"Please select cancel reason",Toast.LENGTH_LONG).show()
            }
            else
            {
                if(OnSelectionItemClickListener != null)
                {
                    OnSelectionItemClickListener!!.onSellerSelectionClick(pos1,strtypeofOperation)
                    mSelection = pos1
                    dismiss()
                }
            }

        }

        return v
    }



    private fun setDialogPosition() {
        val window = dialog!!.window
        dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setGravity(Gravity.BOTTOM)

        val params = window.attributes
        params.y = dpToPx(60)
        window.attributes = params
    }

    private fun dpToPx(dp: Int): Int {
        val metrics = activity!!.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), metrics).toInt()
    }

    /*override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.fragment_sorting, null)
        dialog.setContentView(contentView)
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false)
        if (arguments != null) {
            mSelection = arguments?.getInt("mSelection", 0)!!
        }

        var arraySortingList: ArrayList<SortingPojo?>? = null
        arraySortingList = ArrayList<SortingPojo?>()
        var data1 = SortingPojo()
        data1.isSelection = false
        data1.sortingName = "Popular"
        arraySortingList.add(data1)
        var data2 = SortingPojo()
        data2.isSelection = false
        data2.sortingName = "Price - Low to high"
        arraySortingList.add(data2)
        var data3 = SortingPojo()
        data3.isSelection = false
        data3.sortingName = "Price - high to Low"
        arraySortingList.add(data3)
        var data4 = SortingPojo()
        data4.isSelection = false
        data4.sortingName = "Newest Arrivals"
        arraySortingList.add(data4)

        linearLayoutManager = LinearLayoutManager(mActivity)

        dialog.product_sort_dialog_recyclerview.layoutManager = linearLayoutManager
        dialog.product_sort_dialog_recyclerview.hasFixedSize()
        mSortByAdapter = SortByAdapter(mActivity, arraySortingList!!,object :
            RecyclerViewItemClickListner {
            override fun onItemClick(position: Int, typeofOperation: String) {
                if(mSortByAdapter!!.mSelection > -1){
                    if(OnSelectionItemClickListener != null){
                        OnSelectionItemClickListener!!.onSellerSelectionClick(position,typeofOperation)
                        mSelection = position
                        dismiss()
                    }

                }
            }

        })
        dialog.product_sort_dialog_recyclerview.adapter = mSortByAdapter
        mSortByAdapter?.mSelection = mSelection
        mSortByAdapter?.notifyDataSetChanged()
        dialog.product_sort_dialog_close_imageview.setOnClickListener {
            dismiss()
        }
    }*/

    fun setListener(listener: OnItemClickListener) {
        this.OnSelectionItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onSellerSelectionClick(position: Int,typeofOperation: String)
    }

    fun setMargins(dialog: Dialog, marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int): Dialog {
        val window = dialog.window
            ?: // dialog window is not available, cannot apply margins
            return dialog
        val context = dialog.context

        // set dialog to fullscreen
        val root = RelativeLayout(context)
        root.layoutParams =
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        // set background to get rid of additional margins
//        window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // apply left and top margin directly
        window.setGravity(Gravity.BOTTOM)
        val attributes = window.attributes
        attributes.x = marginLeft
        attributes.y = marginBottom
        window.attributes = attributes

        // set right and bottom margin implicitly by calculating width and height of dialog
        val displaySize = getDisplayDimensions(context)
        val width = displaySize.x - marginLeft - marginRight
        val height = displaySize.y - marginTop - marginBottom
        window.setLayout(width, height)
        window.setBackgroundDrawable(resources.getDrawable(R.drawable.background_inset))

        return dialog
    }

    fun getDisplayDimensions(context: Context): Point {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay

        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val screenWidth = metrics.widthPixels
        var screenHeight = metrics.heightPixels

        // find out if status bar has already been subtracted from screenHeight
        display.getRealMetrics(metrics)
        val physicalHeight = metrics.heightPixels
        val statusBarHeight = getStatusBarHeight(context)
        val navigationBarHeight = getNavigationBarHeight(context)
        val heightDelta = physicalHeight - screenHeight
        if (heightDelta == 0 || heightDelta == navigationBarHeight) {
            screenHeight -= statusBarHeight
        }

        return Point(screenWidth, screenHeight)
    }

    fun getStatusBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
    }

    fun getNavigationBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
    }

}
