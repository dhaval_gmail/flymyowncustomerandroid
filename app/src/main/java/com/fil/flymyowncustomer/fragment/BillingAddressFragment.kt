package com.fil.flymyowncustomer.fragment


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.BuildConfig

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.RegisterAddAddressActivity
import com.fil.flymyowncustomer.adapter.BillingAddressAdapter
import com.fil.flymyowncustomer.model.AddAddressModel
import com.fil.flymyowncustomer.pojo.BillingAddressData
import com.fil.flymyowncustomer.pojo.BillingAddressPojo
import com.fil.flymyowncustomer.pojo.FaqListPojo
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.DividerItemDecoration
import com.fil.flymyowncustomer.util.LocationProvider
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_billing_address.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class BillingAddressFragment : Fragment() {
    var v: View? = null
    var mActivity: Activity? = null
    var billingAddressAdapter: BillingAddressAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var sessionManager:SessionManager?=null
    var billingAddressData:ArrayList<BillingAddressData>?=null
    private val REQUEST_CODE_Location_PERMISSIONS = 6
    private  var locationProvider: LocationProvider?=null
    var currentapiVersion = Build.VERSION.SDK_INT
    var statename=""
    var relativeprogressBar : RelativeLayout? = null
    var noDatafoundRelativelayout : RelativeLayout? = null
    var nointernetMainRelativelayout : RelativeLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_billing_address, container, false)

        }
        return v
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.billing_address)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        relativeprogressBar = view?.findViewById(R.id.relativeprogressBar) as RelativeLayout
        noDatafoundRelativelayout = view?.findViewById(R.id.noDatafoundRelativelayout) as RelativeLayout
        nointernetMainRelativelayout = view?.findViewById(R.id.nointernetMainRelativelayout) as RelativeLayout

        btnAddAddressSave.setOnClickListener {
            Intent(mActivity!!,RegisterAddAddressActivity::class.java).apply {
                putExtra("from","addAddress")
                startActivityForResult(this, 122)
                (activity as MainActivity).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }

        }
        sessionManager= SessionManager(mActivity!!)
        noDatafoundRelativelayout?.visibility=View.GONE
        relativeprogressBar?.visibility=View.GONE
        nointernetMainRelativelayout?.visibility=View.GONE
        setAddress()

        tvUseMyCurrentLocation.setOnClickListener {
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                permissionLocation()
            }else {

                getCurrentLocation()
            }
        }



    }

    private fun setDefaultAddress(addressID: String, position: Int,addressType:String) {
        MyUtils.showProgressDialog(mActivity!!)

        var userDate = sessionManager!!.get_Authenticate_User()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userDate.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("addressID",addressID)
            jsonObject.put("addressType",addressType)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        val addAddressModel = ViewModelProviders.of(this@BillingAddressFragment).get(
            AddAddressModel::class.java
        )
        addAddressModel.address(
            mActivity!!,
            false,
            jsonArray.toString(),
            "setDefaultAddress"
        )
            .observe(this@BillingAddressFragment,
                androidx.lifecycle.Observer<List<BillingAddressPojo>> { addAddresspojo ->
                    if (addAddresspojo != null && addAddresspojo.isNotEmpty())
                    {
                        MyUtils.closeProgress()
                        if (addAddresspojo[0].status.equals("true", true)) {
                            val userData = sessionManager!!.get_Authenticate_User()

                            for (i in 0 until userData.billing!!.size)
                            {
                                if (addressID.equals(userData.billing!![i].addressID,false))
                                    userData.billing!![i].addressIsDefault = "Yes"
                                else
                                    userData.billing!![i].addressIsDefault = "No"
                            }
                            for (i in 0 until billingAddressData!!.size)
                            {

                                if (addressID.equals(
                                        billingAddressData!![i].addressID,
                                        false
                                    )
                                ) {
                                    billingAddressData!![i].addressIsDefault = "Yes"
                                } else {
                                    billingAddressData!![i].addressIsDefault = "No"
                                }
                            }
                           applyNotify(billingAddressData)

                            storeSessionManager(userData)

                        } else {
                            MyUtils.closeProgress()
                            (activity as MainActivity).showSnackBar(addAddresspojo[0].message!!)
                        }
                    } else {
                        MyUtils.closeProgress()
                        (activity as MainActivity).errorMethod()
                    }
                })

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
        }
    }


    private fun deleteAddress(addressID:String,position:Int) {
        MyUtils.showProgressDialog(mActivity!!)

        var userDate = sessionManager!!.get_Authenticate_User()
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userDate.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("addressID",addressID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        val addAddressModel = ViewModelProviders.of(this@BillingAddressFragment).get(
            AddAddressModel::class.java
        )
        addAddressModel.address(
            mActivity!!,
            false,
            jsonArray.toString(),
            "deleteAddress"
        )
            .observe(this@BillingAddressFragment,
                androidx.lifecycle.Observer<List<BillingAddressPojo>> { addAddresspojo ->
                    if (addAddresspojo != null && addAddresspojo.isNotEmpty())
                    {
                        MyUtils.closeProgress()
                        if (addAddresspojo[0].status.equals("true", true)) {

                            try {
                                val userData = sessionManager!!.get_Authenticate_User()
                                userData.billing!!.removeAt(position)
                                billingAddressData!!.removeAt(position)
                                billingAddressAdapter!!.notifyItemRemoved(position)
                                billingAddressAdapter!!.notifyItemChanged(position,billingAddressData!!.size)
                                storeSessionManager(userData)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                            MyUtils.closeProgress()
                            (activity as MainActivity). showSnackBar(addAddresspojo[0].message!!)

                        }

                    } else {
                          MyUtils.closeProgress()
                         (activity as MainActivity).errorMethod()
                    }
                })
    }


    private fun storeSessionManager(driverdata: RegisterNewPojo.Datum) {


        val gson = Gson()
        val json = gson.toJson(driverdata)
        sessionManager?.create_login_session(
            json,
            driverdata.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!
        )
    }


    public fun setAddress() {
        if (sessionManager!!.get_Authenticate_User().billing.isNullOrEmpty()) {
            noDatafoundRelativelayout?.visibility = View.VISIBLE
            nodatafoundtextview.text = "Please Add Address"
        } else {

            noDatafoundRelativelayout?.visibility = View.GONE
            billingAddressData = ArrayList()
            billingAddressData = sessionManager!!.get_Authenticate_User().billing
             applyNotify(billingAddressData)

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //   super.onActivityResult(requestCode, resultCode, data)
        when(requestCode)
        {
            122->if(data!=null)
            {
                billingAddressData= data.getSerializableExtra("address") as ArrayList<BillingAddressData>?
                applyNotify(billingAddressData)

            }
           LocationProvider.CONNECTION_FAILURE_RESOLUTION_REQUEST -> {
                getCurrentLocation()
            }
        }




    }

    public  fun applyNotify(billingAddressData: ArrayList<BillingAddressData>?)
    {
        noDatafoundRelativelayout?.visibility = View.GONE
        /*if(billingAddressData?.size!! >= 2)
        {
            btnAddAddressSave.visibility=View.GONE
        }
        else
        {
            btnAddAddressSave.visibility=View.VISIBLE

        }*/
        linearLayoutManager = LinearLayoutManager(mActivity!!)
        billingAddressAdapter = BillingAddressAdapter(
            mActivity!!,billingAddressData,
            object : BillingAddressAdapter.OnItemClick {
                override fun onClicklisneter(pos: Int, name: String) {
                    when(name)
                    {
                        "edit"->
                        {
                            Intent(mActivity!!,RegisterAddAddressActivity::class.java).apply {
                                putExtra("billingAddressData",
                                    this@BillingAddressFragment.billingAddressData!![pos])
                                putExtra("from","updateAddress")
                                startActivityForResult(this, 122)
                                (activity as MainActivity).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                            }
                        }
                        "delete"->{
                            (activity as MainActivity).showMessageOKCancel(mActivity!!, "Are you sure you want to delete address?",
                                DialogInterface.OnClickListener { dialogInterface, i ->
                                    dialogInterface.dismiss()
                                    deleteAddress(this@BillingAddressFragment.billingAddressData!![pos].addressID!!,pos)
                                })
                        }
                        "SetDefault"->{
                            (activity as MainActivity).showMessageOKCancel(mActivity!!, "Are you sure you want to set default address?",
                                DialogInterface.OnClickListener { dialogInterface, i ->
                                    dialogInterface.dismiss()
                                    setDefaultAddress(
                                        this@BillingAddressFragment.billingAddressData!![pos].addressID!!,pos,
                                        this@BillingAddressFragment.billingAddressData!![pos].addressType!!)
                                })
                        }
                    }
                }
            }
        )
        notificationRecyclerview?.layoutManager = linearLayoutManager
        notificationRecyclerview?.addItemDecoration(DividerItemDecoration(mActivity!!, null))
        notificationRecyclerview?.adapter = billingAddressAdapter

    }




    private fun permissionLocation() {

        if (!addPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            val message = getString(R.string.grant_access_location)

            MyUtils.showMessageOKCancel1(mActivity!!, message, resources.getString(R.string.use_location_service),
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    requestPermissions(
                        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                        REQUEST_CODE_Location_PERMISSIONS
                    )
                })

        }else {
            getCurrentLocation()
        }

    }

    private fun addPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(mActivity!!,permission) !== android.content.pm.PackageManager.PERMISSION_GRANTED) {
                return false
            }

        }

        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_Location_PERMISSIONS -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        getCurrentLocation()
                    } else {
                        Snackbar.make(
                            rvBillingAddress,
                            R.string.read_location_permissions_senied,
                            Snackbar.LENGTH_LONG
                        ).setAction(
                            "Retry"
                        ) {
                            val i = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                            )
                            startActivity(i)
                        }.show()

                        (activity as MainActivity).showSnackBar(
                            getString(R.string.read_location_permissions_senied)

                        )

                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


    fun getCurrentLocation() {
        MyUtils.showProgressDialog(mActivity!!)
        locationProvider = LocationProvider(
          mActivity!!,
            LocationProvider.LOW_POWER,
            object : LocationProvider.CurrentLocationCallback {
                override fun handleNewLocation(location: Location) {
                    Log.d("currentLocation", location.toString())
                    locationProvider?.disconnect()
                    MyUtils.currentLattitude=location.latitude
                    MyUtils.currentLongitude=location.longitude
                    CurrentCityName(location.latitude,location.longitude)
                }

            })

        locationProvider!!.connect()
    }

    private fun CurrentCityName(lattitude: Double, longitude: Double) {


        val geocoder: Geocoder
        var addresses: List<Address>? = null
        geocoder = Geocoder(mActivity!!, Locale.getDefault())

        try {

            addresses = geocoder.getFromLocation(
                lattitude,
                longitude,
                1
            )

            if (addresses != null) {
                MyUtils.closeProgress()

                var billingAddressData1=BillingAddressData(addresses[0].getAddressLine(0),"","","","","","","","","",addresses[0].latitude.toString(),addresses[0].locality.toString(),addresses[0].longitude.toString(),addresses[0].postalCode.toString(),"","","","",addresses[0].adminArea.toString())

                Intent(mActivity!!,RegisterAddAddressActivity::class.java).apply {
                    putExtra("billingAddressData",billingAddressData1)
                    putExtra("from","addAddress")
                    startActivityForResult(this, 122)
                    (activity as MainActivity).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                }

            }

        } catch (e: IOException) {
            MyUtils.closeProgress()
            e.printStackTrace()
        }

    }




}
