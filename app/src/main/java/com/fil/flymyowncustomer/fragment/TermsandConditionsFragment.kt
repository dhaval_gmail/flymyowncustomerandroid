package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.net.ParseException
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.model.GetCmspageModel
import com.fil.flymyowncustomer.pojo.GetCmspagePojo
import com.fil.flymyowncustomer.pojo.RegisterPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_termsand_conditions.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.webview.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class TermsandConditionsFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null



    var fragmentType: String?=null
    var WhichCMSPage: String?=""
    val mTerms = "Terms"
    val mPrivacyPolicy = "PrivacyPolicy"
    private val TAG= TermsandConditionsFragment::class.java.name
//    lateinit var sessionManager: SessionManager
    var userData : RegisterPojo.Data? = null
    var userId : String?="0"
    var languageId : String?="0"

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_termsand_conditions, container, false)
        }
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

          (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility=View.VISIBLE
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text=mActivity!!.resources.getString(R.string.termsandcondition)
        menuNotification.visibility=View.GONE
        toolbar_Imageview!!.visibility = View.GONE

//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE
//         (mActivity as MainActivity).handleSelection()
          /*(mActivity as MainActivity).selectBottomNavigationOption(0)*/
        (mActivity as MainActivity).setDrawerSwipe(true)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        fragmentType = arguments?.getString("Type", "")

        relativeprogressBar?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        webViewMainRelativeLayout?.visibility = View.VISIBLE
        llMainTermconfitions?.visibility = View.GONE

        webData()

        when(fragmentType){

            mTerms->{
                toolbar_title.text=(mActivity!!.resources.getString(R.string.termsandcondition))
                WhichCMSPage = "000001"
                commonMethod()
            }
            mPrivacyPolicy->{
//                (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.accept_Privacy_Poilicy))
                WhichCMSPage = "000003"
                commonMethod()
            }

        }

        btnRetry?.setOnClickListener {
            commonMethod()
        }
    }

    private fun webData(){

        val settings = webView.settings
        // Enable java script in web view
        settings.javaScriptEnabled = true
        // Enable zooming in web view
        settings.setSupportZoom(false)
        settings.builtInZoomControls = false
        settings.displayZoomControls = false
        settings.setDefaultFontSize(40);
        // Zoom web view text
//        settings.textZoom = 125
        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true
        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false

        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        webView.fitsSystemWindows = true
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY

        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        // Set web view client
        webView.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
            }
        }
        //  Case 2 .. Create your own html page...


    }

    private fun commonMethod(){

        if (MyUtils.internetConnectionCheck(mActivity!!) == true) {
            getCMSPages()

//            var mHtmlText: String =
//                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
//
//            webView.loadDataWithBaseURL(null, mHtmlText, "text/HTML", "UTF-8", null)
        } else {

            relativeprogressBar?.visibility = View.GONE
            llMainTermconfitions?.visibility = View.GONE

            nointernetImageview.visibility = View.VISIBLE
            nointernetMainRelativelayout?.visibility = View.VISIBLE
            noDatafoundRelativelayout?.visibility = View.GONE
            webViewMainRelativeLayout?.visibility = View.GONE
            nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
            nointernettextview1.text = (this.getString(R.string.internetmsg1))
            nointernettextview.text = (getString(R.string.error_common_network))
        }

    }

    private fun getCMSPages() {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
// cmspage/get-cms
//            [{
//                "cmspageConstantCode": "000001",
//                "cmspageFor": "Customer",
//                "apiType": "Android",
//                "apiVersion": "1.0"
//            }]
            jsonObject.put("cmspageConstantCode", WhichCMSPage)
            jsonObject.put("cmspageFor", "Customer")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.d(TAG,"Terms api call := "+jsonArray.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        relativeprogressBar?.visibility = View.VISIBLE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        llMainTermconfitions?.visibility = View.GONE

        var getCmspageModel = ViewModelProviders.of(this@TermsandConditionsFragment).get(GetCmspageModel::class.java)
        getCmspageModel.getCMSPage(mActivity!!, false,jsonArray.toString()).observe(this@TermsandConditionsFragment,
            Observer<List<GetCmspagePojo>> { cmspagePoJos ->
                if (cmspagePoJos != null) {

                    if (cmspagePoJos[0].status.equals("true",true)) {
                        relativeprogressBar?.visibility = View.GONE
                        llMainTermconfitions?.visibility = View.VISIBLE
                        webViewMainRelativeLayout?.visibility = View.VISIBLE

                        webView.loadDataWithBaseURL(null, cmspagePoJos!![0]!!.data!![0]!!.cmspageContents!!,"text/HTML", "UTF-8", null)
                    } else {
                        nodatafound()
                    }
                } else {
                    nodatafound()
                }
            })

    }

    private fun nodatafound() {

            try {
                relativeprogressBar?.visibility = View.GONE
                llMainTermconfitions?.visibility = View.GONE
                webViewMainRelativeLayout?.visibility = View.GONE
                nointernetMainRelativelayout?.visibility = View.VISIBLE
                if (MyUtils.isInternetAvailable(mActivity!!)) {
                    nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                    nointernettextview.text = (getString(R.string.error_something))
                    nointernettextview1.text = (this.getString(R.string.somethigwrong1))
                } else {
                    nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                    nointernettextview1.text = (this.getString(R.string.internetmsg1))
                    nointernettextview.text = (getString(R.string.error_common_network))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }







    }


}
