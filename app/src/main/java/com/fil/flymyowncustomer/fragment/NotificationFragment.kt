package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.model.NotificationlistModel
import com.fil.flymyowncustomer.model.DeleteNotificationModel
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.NotificationAdapter
import com.fil.flymyowncustomer.adapter.RecyclerItemTouchHelper
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.DividerItemDecoration
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.fil.flymyowncustomer.pojo.NotificationListPojo1
import com.fil.flymyowncustomer.util.PrefDb
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.util.*


class NotificationFragment : Fragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = NotificationFragment::class.java.name

    private lateinit var notificationAdapter: NotificationAdapter
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    private var notificationListData: ArrayList<NotificationListPojo1.Datum?>? = null
    var sessionManager: SessionManager? = null
    lateinit var notificationlistModel: NotificationlistModel
    var relativeprogressBar: RelativeLayout? = null
    var noDatafoundRelativelayout: RelativeLayout? = null
    var nointernetMainRelativelayout: RelativeLayout? = null
    var fragment_notification_linearlayout: LinearLayout? = null

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is NotificationAdapter.NotificationViewHolder && mActivity != null) {

            val builder = MaterialAlertDialogBuilder(mActivity)

            builder.setMessage(resources.getString(R.string.notification_remove_dialog))
//            builder.setTitle("")
            builder.setCancelable(false)
            builder.setPositiveButton("Yes") { dialogInterface, i ->

                if (MyUtils.isInternetAvailable(mActivity!!)) {
                    deleteNotification(notificationListData!![position]?.notificationID!!)
                } else {
                    MyUtils.showSnackbarkotlin(
                        mActivity!!,
                        fragment_notification_linearlayout!!,
                        mActivity!!.resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                    )
                }


//                notificationListData?.removeAt(position)
//                notificationAdapter.notifyDataSetChanged()
            }

            builder.setNegativeButton(
                "No"
            ) { dialog, which -> notificationAdapter.notifyItemChanged(position) }
            val alert = builder.create()
            alert.setCanceledOnTouchOutside(false)
            alert.setCancelable(false)
//            alert.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alert.show()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_notification, container, false)
        }
        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

        menu.findItem(R.id.menu_search).isVisible = false
        menu.findItem(R.id.menu_notification).isVisible = false
        super.onPrepareOptionsMenu(menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)
        (activity as MainActivity).bottom_navigation?.visibility = View.GONE
        (activity as MainActivity).menuNotification?.isVisible = false
        tvToolbarTitel.text = getString(R.string.notifications)
        (activity as MainActivity).setDrawerSwipe(false)

        sessionManager = SessionManager(mActivity!!)

        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        relativeprogressBar = view?.findViewById(R.id.relativeprogressBar) as RelativeLayout
        noDatafoundRelativelayout =
            view?.findViewById(R.id.noDatafoundRelativelayout) as RelativeLayout
        nointernetMainRelativelayout =
            view?.findViewById(R.id.nointernetMainRelativelayout) as RelativeLayout
        fragment_notification_linearlayout =
            view?.findViewById(R.id.fragment_notification_linearlayout) as LinearLayout

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(notificationRecyclerview)
        notificationlistModel =
            ViewModelProviders.of(this@NotificationFragment).get(NotificationlistModel::class.java)

        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        var data: NotificationListPojo1.Datum? = null
        /*for (i in 0 until 8) {
            data = NotificationListPojo1.Datum()
            data?.notificationTitle = "Title" + (i+1)
            data?.notificationMessageText =
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
            data?.notificationSendDate = "2019-05-12"
            data?.notificationSendTime = "14:05:11"
            notificationListData.add(data)
        }*/

        try {
            PrefDb(mActivity!!).putInt("unReadNotification", 0)
            val intent2 = Intent("Push")
            intent2.putExtra("SuccessfullyPlanSubScribe", "SubScribePlanData")
            LocalBroadcastManager.getInstance(mActivity!!).sendBroadcast(intent2)
//            val intentBroadcast = Intent("Push")
//            LocalBroadcastManager.getInstance(mActivity!!).sendBroadcast(intentBroadcast)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        bindData()

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getNotificationList()
                    }
                }
            }
        })

        btnRetry!!.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
            getNotificationList()
        }

    }

    private fun bindData() {

        if (notificationListData == null) {
            notificationListData = ArrayList<NotificationListPojo1.Datum?>()
            notificationAdapter = NotificationAdapter(
                activity as MainActivity,
                notificationListData!!,
                object : NotificationAdapter.OnItemClick {

                    override fun onClicklisneter(pos: Int, name: String) {
                        if (name.equals("WalletRecharge", true)) {
                            (activity as MainActivity).SelectionNavigation(name)
                        } else if (name.equals("ReferAndEarn", true)) {
                            (activity as MainActivity).SelectionNavigation(name)
                        } else if (name.equals("Order", true)) {
                            (activity as MainActivity).SelectionNavigation(name)
                        }
                    }

                })

            linearLayoutManager = LinearLayoutManager(mActivity)
            notificationRecyclerview?.layoutManager = linearLayoutManager
            notificationRecyclerview?.setNestedScrollingEnabled(false)
            notificationRecyclerview?.setHasFixedSize(true)
            notificationRecyclerview?.addItemDecoration(DividerItemDecoration(mActivity!!, null))
            notificationRecyclerview?.adapter = notificationAdapter

            getNotificationList()
        }
    }

    private fun getNotificationList() {
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar?.visibility = View.VISIBLE
            notificationListData?.clear()
            notificationAdapter.notifyDataSetChanged()
        } else {
            relativeprogressBar?.visibility = View.GONE
            notificationRecyclerview?.visibility = (View.VISIBLE)
            notificationListData?.add(null)
            notificationAdapter.notifyItemInserted(notificationListData!!.size - 1)
        }
        /*[{
              "loginuserID": "7",
              "logindealerID": "0",
              "page": "0",
              "pagesize": "10",
              "apiType": "Android",
              "apiVersion": "1.0"
            }]*/

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager?.get_Authenticate_User()?.userID)
//            jsonObject.put("loginuserID", "7")
            jsonObject.put("logindealerID", "0")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        notificationlistModel.getNotificationList(
            activity as MainActivity,
            false,
            jsonArray.toString()
        )
            .observe(this@NotificationFragment,
                Observer<List<NotificationListPojo1>>
                { notificationlistPojo ->
                    if (notificationlistPojo != null && !notificationlistPojo.isNullOrEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout?.visibility = View.GONE
                        nointernetMainRelativelayout?.visibility = View.GONE
                        relativeprogressBar?.visibility = View.GONE

                        if (pageNo > 0) {
                            notificationListData?.removeAt(notificationListData!!.size - 1)
                            notificationAdapter.notifyItemRemoved(notificationListData!!.size)
                        }

                        if (notificationlistPojo[0].status.equals("true")) {

                            if (pageNo == 0)
                                notificationListData?.clear()

                            if (notificationlistPojo[0]?.data!! == null) {
                                isLastpage = true
                            } else if (notificationlistPojo[0]?.data!!.size < 10) {
                                isLastpage = true
                            }

                            notificationListData?.addAll(notificationlistPojo[0].data!!)
                            notificationAdapter.notifyDataSetChanged()
                            pageNo = pageNo + 1
//                            if (notificationlistPojo[0].data!!.size < 10) {
//                                isLastpage = true
//                            }

                        } else {
                            if (notificationListData!!.size == 0) {
                                noDatafoundRelativelayout?.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE

                            } else {
                                noDatafoundRelativelayout?.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        if (activity != null) {
                            relativeprogressBar?.visibility = View.GONE

                            try {
                                nointernetMainRelativelayout?.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.something_went_wrong))
                                    nointernettextview.text =
                                        (activity!!.getString(R.string.error_something))
                                    nointernettextview1.text =
                                        (this.getString(R.string.somethigwrong1))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.no_internet_connection))
                                    nointernettextview1.text =
                                        (this.getString(R.string.internetmsg1))


                                    nointernettextview.text =
                                        (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                    }
                })
    }

    fun deleteNotification(notificationId: String) {

        var jsonArray = JSONArray()
        var jsonObject = JSONObject()
        /* [{
              "loginuserID": "7",
              "logindealerID": "0",
              "notificationID": "245",
              "apiType": "Android",
              "apiVersion": "1.0"
            }]
 */
        try {
            jsonObject.put("loginuserID", sessionManager?.get_Authenticate_User()!!.userID)
//            jsonObject.put("loginuserID", "7")
            jsonObject.put("logindealerID", "0")
            jsonObject.put("notificationID", notificationId)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var deleteNotificationModel =
            ViewModelProviders.of(this@NotificationFragment)
                .get(DeleteNotificationModel::class.java)
        deleteNotificationModel.getDeleteNotification(mActivity!!, true, jsonArray.toString())
            .observe(this@NotificationFragment, object : Observer<List<CommonPojo>> {
                override fun onChanged(commonResPojo: List<CommonPojo>?) {

                    if (commonResPojo != null && commonResPojo.isNotEmpty()) {

                        if (commonResPojo[0]!!.status.equals("true", true)) {

                            if (notificationListData!!.size > 0) {

                                for (i in 0 until notificationListData!!.size) {

                                    if (notificationId.equals(notificationListData!![i]?.notificationID)) {
                                        notificationListData!!.removeAt(i)
                                        break
                                    }
                                }
                            }

//                            MyUtils.showSnackbarkotlin(
//                                mActivity!!,
//                                fragment_notification_linearlayout!!,
//                                commonResPojo[0].message!!)

                            notificationAdapter?.notifyDataSetChanged()

                        } else {
                            MyUtils.showSnackbarkotlin(
                                mActivity!!,
                                fragment_notification_linearlayout!!,
                                commonResPojo[0].message!!
                            )
                        }
                    } else {
                        // errorMethod()
                        MyUtils.showSnackbarkotlin(
                            mActivity!!,
                            fragment_notification_linearlayout!!,
                            "Server Error"
                        )
                    }

                }

            })

    }


}
