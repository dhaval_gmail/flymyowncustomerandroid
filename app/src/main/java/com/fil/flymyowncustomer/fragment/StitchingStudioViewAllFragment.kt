package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.LifestyleStudioListingAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*

/**
 * A simple [Fragment] subclass.
 *
 */
class StitchingStudioViewAllFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = StitchingStudioViewAllFragment::class.java.name

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0

    var mLifestyleStudioListingAdapter : LifestyleStudioListingAdapter?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_stitching_studio_view_all, container, false)
        }
        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar_back!!)

        tvToolbarTitel.text = getString(R.string.stitching_store)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        toolbar_back.menuNotification.visibility=View.VISIBLE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        toolbar_back.menuNotification.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(), NotificationFragment::class.java.name, true)

        }


        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        linearLayoutManager = LinearLayoutManager(mActivity)
        notificationRecyclerview?.layoutManager = linearLayoutManager
        bindData()

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10) {

                        isLoading = true
//                        getNotificationList()
                    }
                }
            }
        })

        btnRetry?.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
//            getNotificationList()
        }


    }

    private fun bindData() {

//        if (notificationListData != null) {

     /*   mLifestyleStudioListingAdapter = LifestyleStudioListingAdapter(
            activity as MainActivity,
            object : LifestyleStudioListingAdapter.onItemClickk {
                override fun onClicklisneter(pos: Int) {
                    (activity as MainActivity).navigateTo(ProductListingFragment(), ProductListingFragment::class.java.name, true)
//                    (mActivity as MainActivity).navigateTo(ProductDetailsFragment(),ProductDetailsFragment::class.java.name,true)
                }

            },
           // dealerListData
        )*/


        notificationRecyclerview?.setNestedScrollingEnabled(false)
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview?.adapter = mLifestyleStudioListingAdapter

//            getNotificationList()
//        }
    }


}
