package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.util.PrefDb
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_my_wallet.*
import kotlinx.android.synthetic.main.nav_toolbar.*
//import kotlinx.android.synthetic.main.toolbar_back.*

/**
 * A simple [Fragment] subclass.
 *
 */
class MyWalletFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    public var menuNotification: AppCompatImageView?= null

    private val mPushCounterBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {

            var pushCount = 0

            pushCount = PrefDb(mActivity!!).getInt("unReadNotification")!!
            if(pushCount > 0){
                setNotificationIcon(1)

            }else if(pushCount == 0){
                setNotificationIcon(1)
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_my_wallet, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar!!)
        menuNotification = v?.findViewById<AppCompatImageView>(R.id.menuNotification)
        menuNotification?.visibility = View.GONE
        toolbar_title.text = getString(R.string.my_wallet)
        toolbar_Imageview.visibility=View.GONE
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)
        (activity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (activity as MainActivity).bottom_navigation!!.visibility = View.GONE
        (activity as MainActivity).setDrawerSwipe(true)
        LocalBroadcastManager.getInstance(mActivity!!).registerReceiver(mPushCounterBroadcastReceiver, IntentFilter("Push"))
        setNotificationIcon(0)
        menuNotification?.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(),NotificationFragment::class.java.name,true)
        }
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        llFlymyownMoneyLayout?.setOnClickListener {
            tvReferandEarn?.performClick()
        }

        FlymyownMoneylayout?.setOnClickListener {
            tvReferandEarn?.performClick()
        }

        Img_arrowFlyMyOwnMoney?.setOnClickListener {
            tvReferandEarn?.performClick()
        }

        tvFlymyownMoney?.setOnClickListener {
            tvReferandEarn?.performClick()
        }

        tvReferandEarn?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(MyWalletFlyMyOwnMoneyFragment(), MyWalletFlyMyOwnMoneyFragment::class.java!!.getName(), true)
    }

        llRechargeAmountLayout?.setOnClickListener {
            Img_RechargeAmount?.performClick()
        }

        tvRechargeAmount?.setOnClickListener {
            Img_RechargeAmount?.performClick()
        }

        Img_RechargeAmount?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(MyWalletRechargeAmountFragment(), MyWalletRechargeAmountFragment::class.java!!.getName(), true)
        }
    }

    fun setNotificationIcon(type : Int){

        if(type == 0){
            menuNotification?.setImageResource(R.drawable.notification_icon)
        }else {
            menuNotification?.setImageResource(R.drawable.notification_with_red_dot)
        }
    }


}
