package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.FAQAdapter
import com.fil.flymyowncustomer.model.FaqListModel
import com.fil.flymyowncustomer.pojo.FaqListPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.DividerItemDecoration
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_termsand_conditions.*
import kotlinx.android.synthetic.main.nav_toolbar.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.webview.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class FAQFragment : Fragment() {

    var mActivity: AppCompatActivity? = null
    private var v: View? = null
    var mFAQAdapter: FAQAdapter? = null
    var rootFAQFragmentLayout: RelativeLayout? = null

    internal var mFaqListPojo: ArrayList<FaqListPojo.Datum?>? = null

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var mFaqListModel: FaqListModel? = null
    var relativeprogressBar : RelativeLayout? = null
    var noDatafoundRelativelayout : RelativeLayout? = null
    var nointernetMainRelativelayout : RelativeLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_faq, container, false)
        }
        return v

    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setToolBar(toolbar!!)
        toolbar!!.visibility=View.VISIBLE
        toolbar_title!!.visibility = View.VISIBLE
        toolbar_title.text=mActivity!!.resources.getString(R.string.faq)
        menuNotification.visibility=View.GONE
        toolbar_Imageview!!.visibility = View.GONE

        relativeprogressBar = view?.findViewById(R.id.relativeprogressBar) as RelativeLayout
        noDatafoundRelativelayout = view?.findViewById(R.id.noDatafoundRelativelayout) as RelativeLayout
        nointernetMainRelativelayout = view?.findViewById(R.id.nointernetMainRelativelayout) as RelativeLayout

//        (mActivity as MainActivity).setTitle1(mActivity!!.resources.getString(R.string.faq))
        (mActivity as MainActivity).drawertoggle!!.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        (mActivity as MainActivity).bottom_navigation!!.visibility = View.GONE
//        (mActivity as MainActivity).handleSelection()
        /*
          (mActivity as MainActivity).selectBottomNavigationOption(0)*/
        (mActivity as MainActivity).setDrawerSwipe(true)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        linearLayoutManager = LinearLayoutManager(mActivity!!)
        notificationRecyclerview?.layoutManager = linearLayoutManager
        mFaqListModel = ViewModelProviders.of(this@FAQFragment).get(FaqListModel::class.java)

        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        /*mFAQAdapter = FAQAdapter(mActivity!!, object : FAQAdapter.onItemClickk {

            override fun onClicklisneter(pos: Int) {

            }

        })

        notificationRecyclerview?.setNestedScrollingEnabled(false)
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview.addItemDecoration(DividerItemDecoration(mActivity!!, null))
        notificationRecyclerview?.adapter = mFAQAdapter*/

        if(mFaqListPojo == null){
            mFaqListPojo = ArrayList<FaqListPojo.Datum?>()
            mFAQAdapter = FAQAdapter(mActivity!!, mFaqListPojo!!,object : FAQAdapter.onItemClickk {

                override fun onClicklisneter(pos: Int) {

                }

            })

            notificationRecyclerview?.setNestedScrollingEnabled(false)
            notificationRecyclerview?.setHasFixedSize(true)
            notificationRecyclerview.addItemDecoration(DividerItemDecoration(mActivity!!, null))
            notificationRecyclerview?.adapter = mFAQAdapter
            getFaqList()
        }

        btnRetry?.setOnClickListener {
            pageNo = 0
            getFaqList()
        }

    }

    private fun getFaqList() {
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar?.visibility = View.VISIBLE
            mFaqListPojo?.clear()
            mFAQAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar?.visibility = View.GONE
            notificationRecyclerview?.visibility = (View.VISIBLE)
            mFaqListPojo?.add(null)
            mFAQAdapter?.notifyItemInserted(mFaqListPojo!!.size - 1)
        }
        /*[{
          "faqFor": "Customer",
          "apiType": "Android",
          "apiVersion": "1.0"
        }]*/

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("faqFor", "Customer")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        mFaqListModel?.faqListApi(mActivity!!, false, jsonArray.toString())
            ?.observe(this@FAQFragment,
                Observer<List<FaqListPojo>> { faqListPojo ->
                    if (faqListPojo != null && faqListPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        notificationRecyclerview?.visibility = View.VISIBLE
                        noDatafoundRelativelayout?.visibility = View.GONE
                        nointernetMainRelativelayout?.visibility = View.GONE
                        relativeprogressBar?.visibility = View.GONE

                        if (pageNo > 0) {
                            mFaqListPojo?.removeAt(mFaqListPojo!!.size - 1)
                            mFAQAdapter?.notifyItemRemoved(mFaqListPojo!!.size)
                        }

                        /*if (faqListPojo[0]?.data.isNullOrEmpty()) {
                            isLastpage = true
                        } else if (faqListPojo[0]?.data!!.size < 10) {
                            isLastpage = true
                        }*/

                        if (faqListPojo[0].status.equals("true")) {

                            if (pageNo == 0) {
                                mFaqListPojo?.clear()
                            }

                            mFaqListPojo?.addAll(faqListPojo[0].data!!)
                            mFAQAdapter?.notifyDataSetChanged()
//                            pageNo = pageNo + 1

                        } else {

                        }
                        if (mFaqListPojo!!.size == 0) {
                            noDatafoundRelativelayout?.visibility = View.VISIBLE

                            notificationRecyclerview?.visibility = View.GONE

                        } else {
                            noDatafoundRelativelayout?.visibility = View.GONE
                            notificationRecyclerview?.visibility = View.VISIBLE
                        }


                    } else {

                        if (mActivity != null) {
                            relativeprogressBar?.visibility = View.GONE
                            notificationRecyclerview?.visibility = View.GONE
                            nodatafound()

                        }

                    }
                })
    }
    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout?.visibility=View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))


                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }




    }


}
