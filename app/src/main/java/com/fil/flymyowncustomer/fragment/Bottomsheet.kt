package com.fil.flymyowncustomer.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.adapter.BottomSheetListAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.layout_bottomsheet.view.*

class Bottomsheet() : BottomSheetDialogFragment(), View.OnClickListener {

    private val TAG = Bottomsheet::class.java.name
    private var mActivity = AppCompatActivity()
    var mBottomSettingListAdapter: BottomSheetListAdapter? = null
    var OnSelectionItemClickListener: OnItemClickListener? = null;
    lateinit var itemList : List<String>
    var mSelection: Int = 0;
    var typesOf = ""

    val keyTitle = "TITLE"
    val keyData = "DATA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        var contentView = View.inflate(mActivity, R.layout.layout_bottomsheet, null)
        dialog.setContentView(contentView)
//        (contentView.parent as View).setBackgroundColor(mActivity!!.resources.getColor(android.R.color.transparent))
//        itemList = getAllItemList()
        if (arguments != null) {
            typesOf = arguments?.getString(keyTitle)!!
            mSelection = arguments?.getInt(keyData)!!
            contentView.bottomsheet_title_tv?.text = "" + typesOf
        }

        contentView.bottomsheet_ImgCloseIcon!!.setOnClickListener(this)
        contentView.bottomsheet_recyclerview!!.layoutManager = LinearLayoutManager(mActivity)
        contentView.bottomsheet_recyclerview!!.setHasFixedSize(true)
        mBottomSettingListAdapter =
            BottomSheetListAdapter(mActivity, itemList!!, object : BottomSheetListAdapter.OnItemClickListener {
                override fun onItemClick(position: Int, typeofOperation: String) {

                }
            })
        mBottomSettingListAdapter!!.mSelection = mSelection
        contentView.bottomsheet_recyclerview!!.adapter = mBottomSettingListAdapter
        mBottomSettingListAdapter!!.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.bottomsheet_ImgCloseIcon -> {
                if (OnSelectionItemClickListener != null) {
                    dismiss()
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    fun setListener(listener: OnItemClickListener) {
        this.OnSelectionItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onSellerSelectionClick(position: Int, typeofOperation: String)
    }
}