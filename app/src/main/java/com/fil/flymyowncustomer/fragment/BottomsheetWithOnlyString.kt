package com.fil.flymyowncustomer.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.adapter.BottomSheetCountryAdapter
import com.fil.flymyowncustomer.adapter.BottomSheetStringAdapter
import com.fil.flymyowncustomer.adapter.BottomSheetStringWithCheckBoxAdapter
import com.fil.flymyowncustomer.pojo.CountryListPojo
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.profile_settings_bottomsheet.view.*
import java.util.*
import kotlin.collections.ArrayList




class BottomsheetWithOnlyString : BottomSheetDialogFragment(), View.OnClickListener {
    var contentView: View? = null
    private val TAG = BottomsheetWithOnlyString::class.java.name
    private var mActivity = AppCompatActivity()
    var mBottomSettingListAdapter: BottomSheetStringWithCheckBoxAdapter? = null
    var mBottomSettingStrignListAdapter: BottomSheetStringAdapter? = null
    var mBottomCountryAdapter: BottomSheetCountryAdapter? = null
    var OnSelectionItemClickListener: OnItemClickListener? = null
    lateinit var itemList: ArrayList<String>
    lateinit var countryList: ArrayList<CountryListClass>
    var mSelection: Int = -1
    var typesOf = ""
    val keyTitle = "TITLE"
    val keyBundleProductObj = "PRODUCTOBJECT"
    var from = ""
    val keyFrom = "FROM"
    val keyStringData = "STRINGDATA"
    val keyFromOnlyString = "FROMSTRING"
    val keyFromOnlyStringWitchCheckBox = "FROMSTRINGCHECKBOX"
    val keyFromRegisterCountery = "FROMREGISTERCountry"
    var arrayString = ArrayList<String>()
    var arrayCountry : CountryListPojo? = null
    var currentCountrycode = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        contentView = View.inflate(mActivity, R.layout.profile_settings_bottomsheet, null)
        dialog.setContentView(contentView)
//        (contentView.parent as View).setBackgroundColor(mActivity!!.resources.getColor(android.R.color.transparent))
//        itemList = getAllItemList()
        if (arguments != null) {
            typesOf = arguments?.getString(keyTitle)!!
            arrayString = arguments?.getStringArrayList(keyStringData)!!
            from = arguments?.getString(keyFrom)!!
            contentView?.bottomsheet_title_tv?.text = "" + typesOf

            if (arguments?.getSerializable("CountryList") != null){
                arrayCountry = arguments?.getSerializable("CountryList") as CountryListPojo
            }
        }

        contentView?.bottomsheet_ImgCloseIcon!!.setOnClickListener(this)
        contentView?.bottomsheet_recyclerview!!.layoutManager = LinearLayoutManager(mActivity)
        contentView?.bottomsheet_recyclerview!!.setHasFixedSize(true)

        setAdapter()

        /*mBottomSettingListAdapter =
            BottomSheetStringAdapter(mActivity, itemList!!, object : BottomSheetStringAdapter.OnItemClickListener {
                override fun onItemClick(position: Int) {

                    if (mBottomSettingListAdapter!!.mSelection > -1) {
                        if (OnSelectionItemClickListener != null) {
                            mBottomSettingListAdapter!!.notifyDataSetChanged()
                            OnSelectionItemClickListener!!.onSellerSelectionClick(position, "")
//                        dismiss()
                        }
                    }

                }
            })
        mBottomSettingListAdapter!!.mSelection = mSelection
        contentView.bottomsheet_recyclerview!!.adapter = mBottomSettingListAdapter
        mBottomSettingListAdapter!!.notifyDataSetChanged()*/
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            currentCountrycode = mActivity.resources.configuration.locales.get(0).displayCountry
        } else {
            currentCountrycode = mActivity.resources.configuration.locale.displayCountry
        }*/
    }

    private fun setAdapter() {

        when (from) {

            keyFromOnlyStringWitchCheckBox -> {
                mBottomSettingListAdapter = BottomSheetStringWithCheckBoxAdapter(
                    mActivity,
                    arrayString,
                    object : BottomSheetStringWithCheckBoxAdapter.OnItemClickListener {
                        override fun onItemClick(position: Int, selectedString: String) {
                            if (mBottomSettingListAdapter!!.mSelection > -1) {
                                if (OnSelectionItemClickListener != null) {
                                    mBottomSettingListAdapter!!.notifyDataSetChanged()
                                    OnSelectionItemClickListener!!.onSellerSelectionClick(position, selectedString, "", "")
                                    dismiss()
                                }
                            }

                        }
                    })
                mBottomSettingListAdapter!!.mSelection = mSelection
                contentView?.bottomsheet_recyclerview!!.adapter = mBottomSettingListAdapter
                mBottomSettingListAdapter!!.notifyDataSetChanged()
            }

            keyFromOnlyString -> {
                mBottomSettingStrignListAdapter = BottomSheetStringAdapter(
                    mActivity,
                    arrayString,
                    object : BottomSheetStringAdapter.OnItemClickListener {
                        override fun onItemClick(position: Int, selectedString: String) {
                            if (mBottomSettingStrignListAdapter!!.mSelection > -1) {
                                if (OnSelectionItemClickListener != null) {
                                    mBottomSettingStrignListAdapter!!.notifyDataSetChanged()
                                    OnSelectionItemClickListener!!.onSellerSelectionClick(position, selectedString, "", "")
                                    dismiss()
                                }
                            }
                        }
                    })
                mBottomSettingStrignListAdapter!!.mSelection = mSelection
                contentView?.bottomsheet_recyclerview!!.adapter = mBottomSettingStrignListAdapter
                mBottomSettingStrignListAdapter!!.notifyDataSetChanged()
            }

            keyFromRegisterCountery -> {
                if (arrayCountry != null){

                    for (i in 0 until arrayCountry?.data?.size!!)
                    {
                        if(arrayCountry?.data?.get(i)?.countryDialCode.equals("+91",false)){
                            Collections.swap(arrayCountry?.data, i, 0)
                            break
                        }
                    }
                    mBottomCountryAdapter = BottomSheetCountryAdapter(
                        mActivity,
                        arrayCountry?.data as ArrayList<CountryListPojo.Data?>,
                        object : BottomSheetCountryAdapter.OnItemClickListener {
                            override fun onItemClick(position: Int, selectedCountryCode: String, flagImage : String, cId : String) {
//                        if(mBottomSettingListAdapter!!.mSelection > -1){
                                if (OnSelectionItemClickListener != null) {
                                    mBottomCountryAdapter!!.notifyDataSetChanged()
                                    OnSelectionItemClickListener!!.onSellerSelectionClick(position, selectedCountryCode, flagImage, cId)
                                    dismiss()
                                }
//                        }
                            }
                        })
//                mBottomCountryAdapterr!!.mSelection = mSelection
                    contentView?.bottomsheet_recyclerview!!.adapter = mBottomCountryAdapter
                    mBottomCountryAdapter?.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.bottomsheet_ImgCloseIcon -> dismiss()
            R.id.btnSignup -> dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    fun setListener(listener: OnItemClickListener) {
        this.OnSelectionItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onSellerSelectionClick(position: Int, typeofOperation: String, selectedFlag : String, cId : String)
    }

    data class CountryListClass(val image: Int, val countrycode: String)

}