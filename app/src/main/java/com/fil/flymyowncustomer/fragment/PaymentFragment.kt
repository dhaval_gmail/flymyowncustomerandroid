package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.R2K2.driver.model.PaymentModel

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.ConfirmationActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.activity.PaymentWebViewActivity
import com.fil.flymyowncustomer.adapter.Paymentadapter
import com.fil.flymyowncustomer.model.PlaceOrderModel
import com.fil.flymyowncustomer.pojo.PaymentPojo
import com.fil.flymyowncustomer.pojo.Paymentmethod
import com.fil.flymyowncustomer.pojo.PlaceOrderPojo
import com.fil.flymyowncustomer.util.*
import kotlinx.android.synthetic.main.custom_reclyerview.notificationRecyclerview
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.fragment.app.FragmentManager
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.google.gson.Gson
import java.util.stream.Collectors


/**
 * A simple [Fragment] subclass.
 */
class PaymentFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var paymentadapter: Paymentadapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
     var rowListItem: List<String> = ArrayList()
     var paymentMethodData:ArrayList<Paymentmethod>?=null
    var placeOrderData:String=""
    var from:String=""
    var amount:Double=0.0
    var cartCalculate: CartCalculate? = null
    var sessionManager:SessionManager?=null
    var userWalletRechargeAmount : Double?= 0.0
    var nettotal : Double?= 0.0
    var nettotalTemp : Double?= 0.0
    var userData : RegisterNewPojo.Datum? = null
    var OrderWalletRechargeAmount = 0.0
    var isFlymyOwnWalletPayment = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_payment, container, false)
        }
        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context
        }
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        sessionManager= SessionManager(mActivity!!)

        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)

        if (sessionManager != null){
            if (sessionManager!!.isLoggedIn()){
                userData = sessionManager?.get_Authenticate_User()
                if(userData != null){
                    if(!userData?.userWalletRechargeAmount.isNullOrEmpty()){

                        if(userData?.userWalletRechargeAmount.equals("0.0")){
                            userWalletRechargeAmount = 0.0
                        }else {
                            userWalletRechargeAmount = java.lang.Double.parseDouble(userData?.userWalletRechargeAmount!!)
                        }
                    }
                }else {
                    userWalletRechargeAmount = 0.0
                }
            }else {
                userWalletRechargeAmount = 0.0
            }
        }
        Log.e("System out"," =  userWalletRechargeAmount  Value:= " +userWalletRechargeAmount)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        if(arguments!=null)
        {
            from=arguments!!.getString("from")!!
            when(from)
            {
                "MyCart"->{
                    placeOrderData=arguments!!.getString("placeOrderData")!!
                    amount=arguments!!.getDouble("amount")
                    tvToolbarTitel.text = getString(R.string.payment)
                    tvTotalPriceItems.text="Amount to Pay"
                    tvPayusingtitle.text="Pay Using"
                }
                "add_amount"->{
                    amount=arguments!!.getDouble("amount")
                    tvToolbarTitel.text = getString(R.string.add_cash)
                    tvTotalPriceItems.text="Amount to add wallet"
                    tvPayusingtitle.text="Add Wallet Using"


                }
            }

        }
        cartCalculate = CartCalculate

        tvViewOrderSummary.text=resources.getString(R.string.rs)+" "+MyUtils.formatPricePerCountery(amount.toDouble())


        Log.e("System out","nettotal!! Amoutn = "+ cartCalculate?.getOrderTotal())

        if(PrefDb(mActivity!!).getMasterData()!=null)
        {
            if(!PrefDb(mActivity!!).getMasterData()!!.paymentmethod.isNullOrEmpty())
            {
                paymentMethodData=ArrayList()
                paymentMethodData?.clear()


                    paymentMethodData?.addAll(PrefDb(mActivity!!).getMasterData()!!.paymentmethod)

                    /*var it = PrefDb(mActivity!!).getMasterData()!!.paymentmethod!!.iterator()
                   while (it.hasNext()) {

                       var  name = it?.next()

                       when(name.paymentmethodKeyword)
                       {
                           "COD"->{

                               it.remove()
                           }
                           "FlyMyOwn Wallet"->{
                               it.remove()

                           }
                       }*/
                    // Do something
            }
        }

        linearLayoutManager = LinearLayoutManager(mActivity!!)
        paymentadapter = Paymentadapter(mActivity!!,object :Paymentadapter.OnItemClick{
            override fun onClicklisneter(pos: Int, name: String) {
                (activity as MainActivity).showMessageOKCancel(mActivity!!, "Are you sure you want to Make payment through ${paymentMethodData!![pos].paymentmethodKeyword}?",
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                        if(paymentMethodData!![pos].paymentmethodKeyword.equals("COD",false))
                        {
                            placeOrder(paymentMethodData!![pos].paymentmethodKeyword)

                        }else if(paymentMethodData!![pos].paymentmethodKeyword.equals("FlyMyOwn Wallet",false))
                        {

                            nettotal = cartCalculate?.getOrderTotal()
                            nettotalTemp = cartCalculate?.getOrderTotal()

                            if(userWalletRechargeAmount!! > 0.0){

                                if(nettotal!! > userWalletRechargeAmount!!){

                                    nettotal = (nettotal!!  - userWalletRechargeAmount!!)

                                    OrderWalletRechargeAmount = Math.abs(nettotalTemp!! - nettotal!!)
//                                    Log.e("System out","nettotal!! > OrderWalletRechargeAmount!! = "+ OrderWalletRechargeAmount)
                                    userWalletRechargeAmount = 0.0
                                }else if(nettotal!! < userWalletRechargeAmount!!) {
//                                    Log.e("System out"," =  nettotal!! < userWalletRechargeAmount!!" )
                                    userWalletRechargeAmount = Math.abs(userWalletRechargeAmount!! - nettotal!! )
                                    OrderWalletRechargeAmount = nettotal!!
                                    nettotal = 0.0
//                                    Log.e("System out","nettotal!! > OrderWalletRechargeAmount!! = "+ OrderWalletRechargeAmount)
                                }else if(nettotal!! == userWalletRechargeAmount!!) {
                                    userWalletRechargeAmount = Math.abs(userWalletRechargeAmount!! - nettotal!!)
                                    OrderWalletRechargeAmount = Math.abs(nettotal!!)
                                    nettotal = 0.0
//                                    Log.e("System out","nettotal!! == userWalletRechargeAmount!! = "+ userWalletRechargeAmount)
                                }
//                                Log.e("System out","final netTotal Value : = "+ nettotal)

                                if(nettotal!! > 0.0){
                                    isFlymyOwnWalletPayment = true
                                    getPaymentApi("" + nettotal, getPositionOfPaymentMethod(paymentMethodData!!)!!)

                                }else {
                                    isFlymyOwnWalletPayment = true
                                    placeOrder(paymentMethodData!![pos].paymentmethodKeyword)
                                }

                            }else if(userWalletRechargeAmount!! == 0.0){
                                (activity as MainActivity).showSnackBar("you don't have enough money in your wallet to processed order")
                            }

                        }
                        else
                        {
                            getPaymentApi("" + amount, paymentMethodData!![pos].paymentmethodKeyword)

                        }

                        //placeOrder(paymentMethodData!![pos].paymentmethodKeyword)
                    })
            }

        },paymentMethodData!!,from)
        notificationRecyclerview?.layoutManager = linearLayoutManager
        notificationRecyclerview?.adapter = paymentadapter

    }

    fun getPositionOfPaymentMethod(arrayList: ArrayList<Paymentmethod>):String?{
        var paymentmethodKeyword = ""
        if(arrayList.size > 0){
            for (i in arrayList.indices){

                println(arrayList[i].paymentmethodKeyword)
                if(arrayList[i].paymentmethodKeyword.equals("Credit card")){
                    paymentmethodKeyword = arrayList[i]?.paymentmethodKeyword
                    break
                }
            }

        }
        return paymentmethodKeyword
    }

    private fun placeOrder(paymentmethodKeyword: String)
    {
        MyUtils.showProgressDialog(mActivity!!)
        var json_object = JSONObject(placeOrderData)
        if(isFlymyOwnWalletPayment == true){
            json_object.put(
                "orderNetAmount",
                String.format("%.2f", cartCalculate?.getOrderTotal())
            )

        }
        json_object.put("orderWalletRechargeAmount", OrderWalletRechargeAmount.toString())
        json_object.put("orderPaymentMode",paymentmethodKeyword)

        val jsonMainArray = JSONArray()
        jsonMainArray.put(json_object)
        Log.e("jsonArray",jsonMainArray.toString().replace("""\""","""""" ))

        val placeOrderModel = ViewModelProviders.of(this@PaymentFragment).get(PlaceOrderModel::class.java)
        placeOrderModel.getPlaceOrderM(mActivity!!, true, jsonMainArray.toString(),"placeOrder").observe(this@PaymentFragment,
            androidx.lifecycle.Observer<List<PlaceOrderPojo>> { verifyCartItem: List<PlaceOrderPojo> ->
                MyUtils.closeProgress()
                if (verifyCartItem[0].status.equals("true", true))
                {

                    cartCalculate?.clearCartItems()
                    (activity as MainActivity).setCartBadgeCount()
                    userData?.userWalletRechargeAmount = ""+userWalletRechargeAmount.toString()
                    val gson = Gson()
                    val json: String = gson.toJson(userData)
                    Log.e("System out","json == "+json)
                    sessionManager!!.create_login_session(
                        json,
                        userData!!.userEmail!!,
                        "",
                        true,
                        sessionManager!!.isEmailLogin()
                    )

                    Intent(mActivity!!, ConfirmationActivity::class.java)
                        .apply {
                            putExtra("orderNo", verifyCartItem[0].data[0].orderNo)
                            startActivityForResult(this, 304)
                        }
                    mActivity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


                }
                else
                {
                    (activity as MainActivity).showSnackBar(verifyCartItem[0].message)
                }
            })
    }

    private fun getPaymentApi(transactionTXNAMOUNT: String, transactionPAYMENTMODE: String) {

//        if (netAmoutResult.toDouble() <= 0) {
//            createTrip("0", "Cash")
//        } else {
        MyUtils.showProgressDialog(mActivity!!)

        val paymentModel = ViewModelProviders.of(this).get(PaymentModel::class.java)
        paymentModel.getPayment(
            mActivity!!,
            transactionTXNAMOUNT,
            transactionPAYMENTMODE,
            sessionManager?.get_Authenticate_User()?.userID!!
        ).observe(this, Observer<List<PaymentPojo>> { paymentPojo ->
            if (paymentPojo != null && paymentPojo.isNotEmpty()) {
                if (paymentPojo[0].status.equals("true", true)) {
                    MyUtils.closeProgress()

                    var intent = Intent(mActivity!!, PaymentWebViewActivity::class.java)
                    intent.putExtra("transactionID", paymentPojo[0].data[0].transactionID)
                    intent.putExtra("url", paymentPojo[0].data[0].url)
                    intent.putExtra("from", from)
                    intent.putExtra("amount", "" + amount)
                    intent.putExtra("transactionPAYMENTMODE", transactionPAYMENTMODE)
                    startActivityForResult(intent, 401)
                    (activity as MainActivity).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                } else {
                    MyUtils.closeProgress()
                    MyUtils.showSnackbar(mActivity!!, paymentPojo[0].message, rootMainPayment)

                }

            } else {
                MyUtils.closeProgress()

                try {
                    if (!MyUtils.isInternetAvailable(mActivity!!)) {
                        MyUtils.showSnackbar(
                           mActivity!!,
                            resources.getString(R.string.error_common_network),
                            rootMainPayment
                        )


                    } else {

                        MyUtils.showSnackbar(
                           mActivity!!,
                            resources.getString(R.string.error_crash_error_message),
                            rootMainPayment
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 401 && resultCode == Activity.RESULT_OK) {
            if(data!=null)
            {
                var  type=""
                var  failMessage=""

                if(data.hasExtra("type"))
                    {
                        type=data.getStringExtra("type")

                    }
                when(type)
                {
                    "successPayment"->{
                        if(data.hasExtra("transactionPAYMENTMODE"))
                        {
                            placeOrder(data.getStringExtra("transactionPAYMENTMODE"))

                        }
                    }
                    "failPayment"->{
                        if(data.hasExtra("failMessage"))
                        {
                            failMessage=data.getStringExtra("failMessage")
                        }
                        MyUtils.showSnackbar(mActivity!!,failMessage,rootMainPayment)
                    }
                    "add_amount"->
                       {

                           (activity as MainActivity).navigateTo(MyWalletRechargeAmountFragment(),MyWalletRechargeAmountFragment::class.java.name,true)
                       }

                }



            }
        }else  if (requestCode == 304) {
            if (data != null && data.hasExtra("navigation")) {
                if (data.getStringExtra("navigation").equals("Order",false))
                {

                    (activity as MainActivity). selectBottomNavigationOption1()
                   // (activity as MainActivity).navigateTo(MyOrderFragment(), MyOrderFragment::class.java.name, true)
                }
                else
                {
                    (activity as MainActivity).navigateTo(MainFragment(), MainFragment::class.java.name, true)
                }
            } else
            {
                (activity as MainActivity).navigateTo(MainFragment(), MainFragment::class.java.name, true)

            }
        }

    }

    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.N)
    fun <T> getListFromIterator(iterator: Iterator<T>): List<T> {

        // Create an empty list
        val list = ArrayList<T>()

        // Add each element of iterator to the List
        iterator.forEachRemaining { list.add(it) }

        // Return the List
        return list
    }


}

/* private fun getAllItemList(): List<String> {

     val allItems = ArrayList<String>()
     allItems.add("Card on Delivery")
     allItems.add("Paytm")
     allItems.add("Credit Card")
     allItems.add("Debit Card")
     allItems.add("Netbanking")

     return allItems
 }*/

