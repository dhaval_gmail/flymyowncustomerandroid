package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.ProductFullViewPagerAdapter
import com.fil.flymyowncustomer.adapter.StitchingServiceFullViewPagerAdapter
import com.fil.flymyowncustomer.pojo.ProductListPojo.Data.Productimage
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_product_fullview.*
import kotlinx.android.synthetic.main.fragment_profile_photo.ivClose
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class ProductImageFullScreenFragment : Fragment() {


    private var isLastpage = false
    private var userId = ""
    private var action = ""
    var mActivity: AppCompatActivity? = null
    private var v: View? = null
    var productimages: ArrayList<Productimage?>?=null
    var stitchingimages: List<StichingServicePojo.StichingServicData.StitchingserviceImage?>?=null

    var productFullViewPagerAdapter : ProductFullViewPagerAdapter?= null
    var stitchingServiceFullViewPagerAdapter : StitchingServiceFullViewPagerAdapter?= null

    var from:String?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {

            v = inflater.inflate(R.layout.fragment_product_fullview, container, false)
        }
        return v

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        MyUtils.changeStatusBarColor(mActivity!!, R.color.black)

        if(arguments!=null)
        {
            from=arguments?.getString("from")
           when(from)
           {
               "StitchingDetails"->{
                   stitchingimages= arguments?.getSerializable("productImageData") as ArrayList<StichingServicePojo.StichingServicData.StitchingserviceImage?>?
                   stitchingAdapter(stitchingimages)
               }
               "ProductDetails"->{
                   productimages= arguments?.getSerializable("productImageData") as ArrayList<Productimage?>?
                   OfferAdapter(productimages)

               }
           }
        }
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE

        ivClose.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

    }
    fun OfferAdapter(productimages: List<Productimage?>?) {
        productFullViewPagerAdapter = ProductFullViewPagerAdapter(mActivity as MainActivity,productimages)
        viewPager_fullview_fragment.clipToPadding = false
        viewPager_fullview_fragment?.adapter = productFullViewPagerAdapter

    }
    fun stitchingAdapter(productimages: List<StichingServicePojo.StichingServicData.StitchingserviceImage?>?) {
        stitchingServiceFullViewPagerAdapter = StitchingServiceFullViewPagerAdapter(mActivity as MainActivity,productimages!!)
        viewPager_fullview_fragment.clipToPadding = false
        viewPager_fullview_fragment?.adapter = stitchingServiceFullViewPagerAdapter

    }




}// Required empty public constructor
