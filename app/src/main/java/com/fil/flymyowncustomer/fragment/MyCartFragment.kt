package com.fil.flymyowncustomer.fragment


import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.model.VerifyCartItemsModel
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.ApplyCouponsActivity
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.MyCartAdapter
import com.fil.flymyowncustomer.model.CouponListModel
import com.fil.flymyowncustomer.pojo.ApplyCouponNewPojoData
import com.fil.flymyowncustomer.pojo.LocalCart
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.VeriFyCartPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_my_cart.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 *
 */
class MyCartFragment : Fragment() {

    var adapterCart: MyCartAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var v: View? = null
    var mActivity: AppCompatActivity? = null
    var minteger = 0
    var list: ArrayList<Int>? = null
    var cartCalculate: CartCalculate? = null
    var cartArrayList = ArrayList<LocalCart>()
    var deleteCount = 0

    val buttonPlus = "BUTTONPLUS"
    val buttonMinus = "BUTTONMINUS"
    val buttonDeliveryType = "BUTTONDELIVERYTYPE"
    val buttonDeliveryDate = "BUTTONDELIVERYDATE"
    val buttonDelete = "BUTTONDELETE"
    var sessionManager:SessionManager?=null
    var dealerID:String=""
    var netAmount = 0.0
    var couponAmount = 0.0
    var flyMoneyAmount = 0.0
    var gstAmount = 0.0
    var deliveryAmount = 0.0
    var objApplyCouponCode : ApplyCouponNewPojoData?= null
    var useraData : RegisterNewPojo.Datum? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_my_cart, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppCompatActivity) {
            mActivity = context
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (mActivity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.my_cart)
        tvRemoveitemCart.visibility = View.GONE
        (mActivity as MainActivity).bottom_navigation?.visibility = View.GONE
        (mActivity as MainActivity).setDrawerSwipe(false)
        sessionManager= SessionManager(mActivity!!)
        cartCalculate = CartCalculate

        if (sessionManager != null && sessionManager!!.isLoggedIn()){
            useraData = sessionManager!!.get_Authenticate_User()
        }
        toolbar_back.setNavigationOnClickListener {
            (mActivity as MainActivity).onBackPressed()
        }

        linearLayoutManager = LinearLayoutManager(mActivity)
        notificationRecyclerview.layoutManager = linearLayoutManager

        noDatafoundRelativelayout.visibility = View.GONE
        relativeprogressBar.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        list = ArrayList()

        if (cartCalculate?.getCartItems(mActivity!!)!!.size > 0)
        {
            cartArrayList.addAll(cartCalculate?.getCartItems(mActivity!!)!!)
            bindDataToAdapter()
            setCartprice()
            verifyCart(false)
        }
        else
        {
            checkForEmptyLayout()
        }

        ll_view_invoice.setOnClickListener {
            fmshowInvoice.visibility = View.VISIBLE
            ll_view_invoice.visibility = View.GONE
            //ll_sub_main.setBackgroundColor(mActivity?.resources?.getColor(R.color.dialog_transperent)!!)

        }
        ll_hide_invoice.setOnClickListener {
            fmshowInvoice.visibility = View.GONE
            ll_view_invoice.visibility = View.VISIBLE
            // ll_sub_main.setBackgroundColor(mActivity?.resources?.getColor(R.color.white)!!)
        }

        tvApplyCouponCode.setOnClickListener {

            Intent(mActivity!!, ApplyCouponsActivity::class.java).apply {
                 if(!cartArrayList.isNullOrEmpty())
                 {

                     putExtra("dealerID",cartArrayList[0].dealerID)
                     startActivityForResult(this, 101)

                 }
                (mActivity as MainActivity).overridePendingTransition(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left
                )
            }
        }

        btnCheckout.setOnClickListener {

            verifyCart(true)
        }

        tvRemoveitemCart.setOnClickListener {
            fmshowInvoice.visibility = View.GONE
            ll_view_invoice.visibility = View.VISIBLE
            MyUtils.showMessageOKCancel(
                mActivity!!,
                "Do you really want remove selected items?",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        if (cartCalculate?.clearCartItems(cartArrayList)!!){
                            adapterCart?.notifyDataSetChanged()
                            cartArrayList.clear()
                            cartArrayList.addAll(cartCalculate?.getCartItems(mActivity!!)!!)
                            deleteCount = 0
                            setRemoveText()
                            setCartprice()
                            checkForEmptyLayout()
                        }
                        bindDataToAdapter()
                    }
                })
        }

        btnRetry?.setOnClickListener {
            (mActivity as MainActivity).navigateTo(
                MainFragment(),
                MainFragment::class.java.name,
                true
            )
        }

        btnContinueShopping.setOnClickListener {

            (mActivity as MainActivity).navigateTo(
                MainFragment(),
                MainFragment::class.java.name,
                true
            )
        }
    }
    private fun chekOuTAfterVerifyCart() {

        val cartArray = cartCalculate?.getCartItems(mActivity!!)
        var flag = true
        if (!cartArray.isNullOrEmpty()) {
            for (i in 0 until cartArray!!.size) {
                if (cartArray[i].addToCartExceptedDate.isNullOrEmpty()) {
                    MyUtils.showSnackbarkotlin(
                        mActivity!!,
                        ll_main,
                        "Please select expected delivery date of " + cartArray[i].productName + "."
                    )
                    flag = false
                    break
                }

            }
        }

        if (!flag) {

        } else {
            val paymentFragment=PaymentFragment()
            Bundle().apply {
                putString("placeOrderData",placeOrder())
                putDouble("amount",cartCalculate?.getOrderTotal()!!)
                putString("from","MyCart" )
                paymentFragment.arguments=this

            }
            (mActivity as MainActivity).navigateTo(
                paymentFragment,
                paymentFragment::class.java.name,
                true
            )
        }
    }

    private fun setCartprice() {
        if (!cartArrayList.isNullOrEmpty() && !cartArrayList[0]?.dealerCompanyName.isNullOrEmpty()) {

            tvStoreName.text = cartArrayList[0]?.dealerCompanyName
        }
        if (!cartArrayList.isNullOrEmpty() && !cartArrayList[0]?.dealerID.isNullOrEmpty()) {

           dealerID = cartArrayList[0].dealerID
        }

        var netAmount = 0.0
        var couponAmount = 0.0
        var flyMoneyAmount = 0.0
        var gstAmount = 0.0
        var deliveryAmount = 0.0

        cartCalculate?.cartCalculation()

        netAmount = cartCalculate?.getOrderSubTotal()!!

        ll_IGST?.visibility = View.GONE
        ll_SGST?.visibility = View.GONE
        ll_CGST?.visibility = View.GONE

        if (cartCalculate?.getOrderIGST()!! > 0) {
            ll_CGST?.visibility = View.GONE
            ll_SGST?.visibility = View.GONE
            ll_IGST?.visibility = View.GONE

            gstAmount = gstAmount + cartCalculate?.getOrderIGST()!!

            tvIGSTAmountValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                    MyUtils.formatPricePerCountery(gstAmount)
        }

        tvNetAmountValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                MyUtils.formatPricePerCountery(cartCalculate?.getOrderSubTotal()!!+gstAmount)

        if (cartCalculate?.getOrderSGST()!! > 0 && cartCalculate?.getOrderCGST()!! > 0) {
            ll_CGST?.visibility = View.VISIBLE
            ll_SGST?.visibility = View.VISIBLE
            ll_IGST?.visibility = View.GONE

            gstAmount += cartCalculate?.getOrderCGST()!! + cartCalculate?.getOrderSGST()!!

            tvCGSTAmountValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                    MyUtils.formatPricePerCountery(cartCalculate?.getOrderCGST()!!)
            tvSGSTAmountValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                    MyUtils.formatPricePerCountery(cartCalculate?.getOrderSGST()!!)
        }

        couponAmount = cartCalculate?.getAppliedCoponValue()!!
        Log.e("System out","Print coupont Amount : "+couponAmount)
         if(couponAmount==0.0)
         {
             ll_discount.visibility=View.GONE
             tvCouponCode.visibility = View.GONE
//             tvApplyCouponCode.text = "Coupon code applied successfully:"
//             tvCouponCode.text = objApplyCouponCode?.appliedCouponCode
         }else
         {
             ll_discount.visibility=View.VISIBLE

             tvCouponDiscountValue?.text = "- " + mActivity!!.resources.getString(R.string.rs) +
                     MyUtils.formatPricePerCountery(couponAmount)
             tvCouponCode.visibility = View.VISIBLE
             tvApplyCouponCode.text = "Coupon code applied successfully:"
             tvCouponCode.text = objApplyCouponCode?.appliedCouponCode
         }


        tvFlyMyOwnMoneyValue?.text =
            mActivity!!.resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(0.0)
        flyMoneyAmount = 0.0

        tvDeliveryChargesValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                MyUtils.formatPricePerCountery(cartCalculate?.getShippingCharge()!!)
        deliveryAmount = cartCalculate?.getShippingCharge()!!


        var amountToPay = 0.0
        amountToPay = netAmount + deliveryAmount + gstAmount - couponAmount - flyMoneyAmount

        tv_amount_to_pay_price.text =
            mActivity!!.resources.getString(R.string.rs) + " " + MyUtils.formatPricePerCountery(
                    amountToPay
                    ).toString()
        tvTotalAmountValue?.text = mActivity!!.resources.getString(R.string.rs) + " " +
                MyUtils.formatPricePerCountery(amountToPay)
    }

    private fun setLayout(emptyOrNot: Boolean) {
        if (emptyOrNot) {
            notificationRecyclerview?.visibility = View.GONE
            ll_store_layout?.visibility = View.GONE
            noDatafoundRelativelayout?.visibility = View.GONE
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout?.visibility = View.VISIBLE
            nointernetImageview?.visibility = View.VISIBLE
            nointernetImageview?.setImageResource(R.drawable.ic_shopping_cart_black_24dp)
            nointernettextview1?.text = "No items in cart."
            nointernettextview?.visibility = View.GONE
            btnRetry?.text = "Shop Now"
            fmshowInvoice?.visibility = View.GONE
            fmmyCartBottom?.visibility = View.GONE
            tvRemoveitemCart?.visibility = View.GONE
//            setCartprice()
        } else {
            notificationRecyclerview?.visibility = View.VISIBLE
            ll_store_layout?.visibility = View.VISIBLE
            noDatafoundRelativelayout?.visibility = View.GONE
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout?.visibility = View.GONE
            nointernetImageview?.visibility = View.VISIBLE
            nointernetImageview?.setImageResource(R.drawable.ic_shopping_cart_black_24dp)
            nointernettextview1?.text = "No items in cart."
            nointernettextview?.visibility = View.GONE
            btnRetry?.text = "Shop Now"
            fmshowInvoice?.visibility = View.GONE
            fmmyCartBottom?.visibility = View.VISIBLE
            if (deleteCount > 0) tvRemoveitemCart?.visibility =
                View.VISIBLE else tvRemoveitemCart?.visibility = View.GONE
//            setCartprice()
        }
    }

    fun checkForEmptyLayout() {

//        Handler().postDelayed({
        val iCount = 1
        val cartArray = cartArrayList.size
        (activity as MainActivity).setCartBadgeCount()
//        val cartSize = cartCalculate?.getCaSize()

//
        if (cartArray!! > 0) {
            setLayout(false)
        } else {
            setLayout(true)
        }
//        },100)
    }

    private fun bindDataToAdapter() {
        cartCalculate?.setCartArrayList(cartCalculate?.getCartItems(mActivity!!)!!)
        adapterCart = MyCartAdapter(
            mActivity!!,
            cartCalculate?.getCartArrayList()!!,
            object : MyCartAdapter.OnItemClick {
                override fun newOnItemClicklistner(
                    buttonName: String,
                    pos: Int,
                    productObj: LocalCart,
                    cartId : String
                ) {

                    when (buttonName) {
                        buttonPlus -> {
                            fmshowInvoice.visibility = View.GONE
                            ll_view_invoice.visibility = View.VISIBLE
                            if (!productObj?.productId.isNullOrEmpty()) {
                                if (!cartCalculate?.AddToCart(
                                        productObj?.productData!![0],
                                        true,
                                        false,
                                        cartId
                                    )!!
                                ) {
                                    if (!productObj?.issueMsg.isNullOrEmpty()) {
                                        MyUtils.showSnackbarkotlin(
                                            mActivity!!,
                                            ll_main,
                                            productObj?.issueMsg
                                        )
                                    }
                                } else {
                                    adapterCart?.notifyDataSetChanged()
                                }
//                                adapterCart?.notifyDataSetChanged()
                            } else {
                                if (cartCalculate?.AddToCartService(
                                        productObj?.stichingServiceData!![0],
                                        true,
                                        false,
                                        cartId
                                    )!!
                                ) {
                                    adapterCart?.notifyDataSetChanged()
                                } else {
                                    if (!productObj?.issueMsg.isNullOrEmpty()) {
                                        MyUtils.showSnackbarkotlin(
                                            mActivity!!,
                                            ll_main,
                                            productObj?.issueMsg
                                        )
                                    }
                                }
                                adapterCart?.notifyDataSetChanged()
                            }
                            bindDataToAdapter()
                        }

                        buttonMinus -> {
                            fmshowInvoice.visibility = View.GONE
                            ll_view_invoice.visibility = View.VISIBLE
                            if (cartCalculate?.getCartQuantity(productObj!!)!! == 1) {
                                MyUtils.showMessageOKCancel(
                                    mActivity!!,
                                    "Are you sure to remove " + productObj?.productName + " from cart?",
                                    object : DialogInterface.OnClickListener {
                                        override fun onClick(dialog: DialogInterface?, which: Int) {
                                            if (!productObj?.productId.isNullOrEmpty()) {
                                                if (cartCalculate?.AddToCart(
                                                        productObj?.productData!![0],
                                                        false,
                                                        true,
                                                        cartId
                                                    )!!
                                                ) {
                                                    cartArrayList.removeAt(pos)
                                                    checkForEmptyLayout()

                                                } else {
                                                    if (!productObj?.issueMsg.isNullOrEmpty()) {
                                                        MyUtils.showSnackbarkotlin(
                                                            mActivity!!,
                                                            ll_main,
                                                            productObj?.issueMsg!!
                                                        )
                                                    }
                                                }
                                            } else {
                                                if (cartCalculate?.AddToCartService(
                                                        productObj?.stichingServiceData!![0],
                                                        false,
                                                        true,
                                                        cartId
                                                    )!!
                                                ) {
                                                    cartArrayList.removeAt(pos)
                                                    checkForEmptyLayout()

                                                } else {
                                                    if (!productObj?.issueMsg.isNullOrEmpty()) {
                                                        MyUtils.showSnackbarkotlin(
                                                            mActivity!!,
                                                            ll_main,
                                                            productObj?.issueMsg!!
                                                        )
                                                    }
                                                }
                                            }
                                            adapterCart?.notifyDataSetChanged()
                                            setCartprice()
//                                            verifyCart()
                                        }
                                    })
                            } else {
                                if (!productObj?.productId.isNullOrEmpty()) {
                                    if (cartCalculate?.AddToCart(
                                            productObj?.productData!![0],
                                            false,
                                            false,
                                            cartId
                                        )!!
                                    ) {
                                    } else {
                                        if (!productObj?.issueMsg.isNullOrEmpty()) {
                                            MyUtils.showSnackbarkotlin(
                                                mActivity!!,
                                                ll_main,
                                                productObj?.issueMsg!!
                                            )
                                        }
                                    }
                                } else {
                                    if (cartCalculate?.AddToCartService(
                                            productObj?.stichingServiceData!![0],
                                            false,
                                            false,
                                            cartId
                                        )!!
                                    ) {
                                        adapterCart?.notifyDataSetChanged()
                                    } else {
                                        if (!productObj?.issueMsg.isNullOrEmpty()) {
                                            MyUtils.showSnackbarkotlin(
                                                mActivity!!,
                                                ll_main,
                                                productObj?.issueMsg!!
                                            )
                                        }
                                    }
                                }
                            }
                            bindDataToAdapter()
                        }

                        buttonDeliveryDate -> {
                            fmshowInvoice.visibility = View.GONE
                            ll_view_invoice.visibility = View.VISIBLE
                            SelectStartDate(productObj)
                        }

                        buttonDeliveryType -> {
                            fmshowInvoice.visibility = View.GONE
                            ll_view_invoice.visibility = View.VISIBLE
                            showDialog(productObj)
                        }

                        buttonDelete -> {
                            fmshowInvoice.visibility = View.GONE
                            ll_view_invoice.visibility = View.VISIBLE
                            if (productObj?.isDelete) {
                                productObj?.isDelete = false
                                deleteCount -= 1
                                cartCalculate?.setIsDeleteOrNot(pos, false)
                                cartArrayList[pos].isDelete = false
                            } else {
                                deleteCount += 1
                                productObj?.isDelete = true
                                cartCalculate?.setIsDeleteOrNot(pos, true)
                                cartArrayList[pos].isDelete = true
                            }
                            setRemoveText()
                            checkForEmptyLayout()
                        }
                    }
//                    cartCalculate?.setCartArrayList(cartCalculate?.getCartItems(mActivity!!)!!)
                    adapterCart?.notifyDataSetChanged()
                    setCartprice()
                    checkForEmptyLayout()
//                    verifyCart()
                }
            }
        )
        notificationRecyclerview.setHasFixedSize(true)
        notificationRecyclerview.adapter = adapterCart
        adapterCart?.notifyDataSetChanged()
    }

    private fun setRemoveText() {
        if (deleteCount > 0) {
            tvRemoveitemCart.visibility = View.VISIBLE
            tvRemoveitemCart.text = "Remove($deleteCount)"
        } else {
            tvRemoveitemCart.visibility = View.GONE
        }
    }

    fun SelectStartDate(objProduct: LocalCart) {
        val c = Calendar.getInstance()
//        c.add(Calendar.DAY_OF_MONTH, 1)

        val today = c.getTime()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(
            mActivity!!, R.style.my_dialog_theme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                objProduct.addToCartExceptedDate = "$dayOfMonth-" + (monthOfYear + 1) + "-$year"
                cartCalculate?.onlySaveCart(objProduct)
                adapterCart?.notifyDataSetChanged()
            }, mYear, mMonth, mDay
        )
        dpd!!.show()
//        dpd!!.datePicker.minDate = c.timeInMillis - 1000
        dpd!!.datePicker.minDate = c.timeInMillis
    }

    private fun showDialog(objProduct: LocalCart) {
        var deliveryType = ""
        var deliveryFreeService = ""
        var deliveryServiceCharges = ""

        var isHome = false
        var isService = false

        if (!objProduct.productData.isNullOrEmpty()) {
            for (i in 0 until objProduct.productData!!.size) {
                if (!objProduct.productData!![i].dealerServiceType.isNullOrEmpty()) {
                    deliveryType = objProduct.productData!![i].dealerServiceType!!
                }

                if (!objProduct.productData!![i].dealerFreeServiceIsAvailable.isNullOrEmpty()) {
                    deliveryFreeService = objProduct.productData!![i].dealerFreeServiceIsAvailable!!
                }

                if (!objProduct.productData!![i].dealerDeliveryCharges.isNullOrEmpty()) {
                    deliveryServiceCharges = objProduct.productData!![i].dealerDeliveryCharges!!
                }

                if (deliveryType.equals("Home Service", true)) {
                    isHome = true
                    isService = false
//            selectedDeliveryTypeName = "Home Service"
                } else if (deliveryType.equals("Self Service", true)) {
                    isService = true
                    isHome = false
//            selectedDeliveryTypeName = "Self Service"
                } else if (deliveryType.equals("Both", true)) {
                    isHome = true
                    isService = true
                }
            }
        } else {
            for (i in 0 until objProduct.stichingServiceData!!.size) {
                if (!objProduct.stichingServiceData!![i].dealerServiceType.isNullOrEmpty()) {
                    deliveryType = objProduct.stichingServiceData!![i].dealerServiceType!!
                }

                if (!objProduct.stichingServiceData!![i].dealerFreeServiceIsAvailable.isNullOrEmpty()) {
                    deliveryFreeService =
                        objProduct.stichingServiceData!![i].dealerFreeServiceIsAvailable!!
                }

                if (!objProduct.stichingServiceData!![i].dealerDeliveryCharges.isNullOrEmpty()) {
                    deliveryServiceCharges =
                        objProduct.stichingServiceData!![i].dealerDeliveryCharges!!
                }

                if (deliveryType.equals("Home Service", true)) {
                    isHome = true
                    isService = false
//            selectedDeliveryTypeName = "Home Service"
                } else if (deliveryType.equals("Self Service", true)) {
                    isService = true
                    isHome = false
//            selectedDeliveryTypeName = "Self Service"
                } else if (deliveryType.equals("Both", true)) {
                    isHome = true
                    isService = true
                }
            }
        }

        val dialogs = MaterialAlertDialogBuilder(mActivity!!)

        //   dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)

        val dialogView = mActivity!!.layoutInflater.inflate(R.layout.popup_delivery, null)

        val ll_home_delivery = dialogView.findViewById<LinearLayout>(R.id.ll_home_delivery)
        val ll_self_pickup = dialogView.findViewById<LinearLayout>(R.id.ll_self_pickup)
        val img_home_delivery = dialogView.findViewById<ImageView>(R.id.img_home_delivery)
        val img_self_pickup = dialogView.findViewById<ImageView>(R.id.img_self_pickup)
        val tv_Cancel = dialogView.findViewById<AppCompatTextView>(R.id.tv_Cancel)
        val tv_Done = dialogView.findViewById<AppCompatTextView>(R.id.tv_Done)
        val tv_homedelivery = dialogView.findViewById<AppCompatTextView>(R.id.tv_homedelivery)
        val tv_self_pickup = dialogView.findViewById<AppCompatTextView>(R.id.tv_self_pickup)
        if (!objProduct.productData.isNullOrEmpty()) {
            tv_homedelivery.text="Home Delivery"
            tv_self_pickup.text="Self Pickup"

        }else if (!objProduct.stichingServiceData.isNullOrEmpty()) {
            tv_homedelivery.text="Home Service"
            tv_self_pickup.text="Self Service"

        }

        if (isHome && !isService) {
            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.GONE
            ll_home_delivery.isClickable = false
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_checked)
        } else if (!isHome && isService) {
            ll_home_delivery.visibility = View.GONE
            ll_self_pickup.visibility = View.VISIBLE
            ll_self_pickup.isClickable = false
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_checked)
        } else if (isHome && isService) {

            if (objProduct.addToCartDeliveryName.equals("Home Service", true)) {
                img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_checked)
                img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            } else {
                img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_unchecked)
                img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_checked)
            }

            ll_home_delivery.visibility = View.VISIBLE
            ll_self_pickup.visibility = View.VISIBLE
        }

        dialogs.setView(dialogView)
        val alertDialog = dialogs.create()
        alertDialog.show()
        tv_Cancel.setOnClickListener {
            alertDialog.dismiss()
        }
        tv_Done.setOnClickListener {
            alertDialog.dismiss()
            cartCalculate?.onlySaveCart(objProduct)
            adapterCart?.notifyDataSetChanged()
        }

        ll_home_delivery.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_checked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            objProduct?.addToCartDeliveryName = "Home Service"
            if (deliveryFreeService.equals("Yes", true)) {
                objProduct?.addToCartDeliveryCharges = 0.0
            } else {
                if (!objProduct.productData.isNullOrEmpty()) {
                    for (i in 0 until objProduct.productData!!.size) {
                        if (!objProduct.productData!![i].dealerServiceType.isNullOrEmpty()) {
                            objProduct?.addToCartDeliveryCharges =
                                java.lang.Double.valueOf(objProduct.productData!![i]?.dealerDeliveryCharges!!)
                        }
                    }
                } else {
                    for (i in 0 until objProduct.stichingServiceData!!.size) {
                        if (!objProduct.stichingServiceData!![i].dealerServiceType.isNullOrEmpty()) {
                            objProduct?.addToCartDeliveryCharges =
                                java.lang.Double.valueOf(objProduct.stichingServiceData!![i]?.dealerDeliveryCharges!!)
                        }
                    }
                }
            }
        }

        ll_self_pickup.setOnClickListener {
            img_home_delivery.setImageResource(R.drawable.radio_button_additional_details_unchecked)
            img_self_pickup.setImageResource(R.drawable.radio_button_additional_details_checked)
            objProduct?.addToCartDeliveryName = "Self Service"
            objProduct?.addToCartDeliveryCharges = 0.0
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //   super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            101 -> if (data != null) {
                objApplyCouponCode = data.getSerializableExtra("code")!! as ApplyCouponNewPojoData
                cartCalculate?.applyCouponOn(objApplyCouponCode!!)
                /*tvCouponCode.visibility = View.VISIBLE
                tvApplyCouponCode.text = "Coupon code applied successfully:"
                tvCouponCode.text = objApplyCouponCode?.appliedCouponCode*/
                adapterCart?.notifyDataSetChanged()
                setCartprice()
//                applyCouponCode(data.getStringExtra("code"))
            }
        }
    }

    private fun applyCouponCode(stringExtra: String?) {
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("dealerID", dealerID)
            jsonObject.put("offerlogVoucherCode",stringExtra)
            jsonObject.put("orderGrossAmt", netAmount)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var couponListModel =
            ViewModelProviders.of(this@MyCartFragment)
                .get(CouponListModel::class.java)
        couponListModel.getCouponList(mActivity!!, false, jsonArray.toString(),"favList")
            .observe(this@MyCartFragment,
                androidx.lifecycle.Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {


                        if (masterPojo[0].status.equals("true", false)) {
                            tvCouponCode.visibility = View.VISIBLE
                            tvApplyCouponCode.text = "Coupon code applied successfully:"
                            tvCouponCode.text = stringExtra
                        } else {
                            (activity as MainActivity).showSnackBar(masterPojo[0].message)
                        }

                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    fun verifyCart(buttonOrElse : Boolean){

        val verifyCartArray = cartCalculate?.getCartArrayList()

        cartCalculate?.CartCalculate(mActivity!!)
        val jsonMainObject = JSONObject()
        val jsonMainArray = JSONArray()
        val jsonProductArray = JSONArray()
        val jsonServiceArray = JSONArray()
        var userAddress = ""
        var totalDeliveryCharges = 0

        try {
            if (!useraData?.billing.isNullOrEmpty()){

                jsonMainObject.put("loginuserID", useraData?.userID)

                for (i in 0 until useraData?.billing!!.size){
                    if (useraData?.billing!![i].addressIsDefault.equals("Yes", true)){
                        userAddress = useraData?.billing!![i]?.addressAddressLine1!! + ", " +
                                useraData?.billing!![i]?.addressLandmark + ", " +
                                useraData?.billing!![i]?.addressLocality + ", " +
                                useraData?.billing!![i]?.addressAddressLine2 + ", " +
                                useraData?.billing!![i]?.stateName + ", " +
                                useraData?.billing!![i]?.addressPincode+ ", " +
                                useraData?.billing!![i]?.countryName
                        jsonMainObject.put("addressPincode", useraData?.billing!![i]?.addressPincode!!)
                        jsonMainObject.put("orderDeliveryLatitude", useraData?.billing!![i]?.addressLatitude!!)
                        jsonMainObject.put("orderDeliveryLongitude", useraData?.billing!![i]?.addressLongitude!!)
                        jsonMainObject.put("addressID", useraData?.billing!![i]?.addressID!!)
                    }
                }
                jsonMainObject.put("orderBillingAddress", userAddress)
                jsonMainObject.put("orderDeliveryAddress", userAddress)

                if (!verifyCartArray.isNullOrEmpty()){
                    jsonMainObject.put("dealerID", verifyCartArray[0].dealerID)
                    jsonMainObject.put("cartId", verifyCartArray[0].cartID.toString())
                    for(i in 0 until verifyCartArray.size){
                        val jsonProductObject = JSONObject()

                        if (verifyCartArray[i].productData.isNullOrEmpty()){
                            jsonProductObject.put("orderType", "Stitching")
                            // For service
                            jsonProductObject.put("stitchingserviceID", verifyCartArray[i].stitchingId)
                            jsonProductObject.put("orderdetailsstitchingPrice", String.format("%.2f", verifyCartArray[i].productDiscountedPrice))
                            jsonProductObject.put("orderdetailsstitchingQty", verifyCartArray[i].cartQuantity.toString())
                            jsonProductObject.put("orderdetailsstitchingCSGT", String.format("%.2f", verifyCartArray[i].CGST))
                            jsonProductObject.put("orderdetailsstitchingSGST", String.format("%.2f", verifyCartArray[i].SGST))
                            jsonProductObject.put("orderdetailsstitchingIGST", String.format("%.2f", verifyCartArray[i].IGST))
                            jsonProductObject.put("orderdetailsstitchingPercentageCSGT", String.format("%.2f", verifyCartArray[i].CGSTPercentage))
                            jsonProductObject.put("orderdetailsstitchingPercentageSGST", String.format("%.2f", verifyCartArray[i].SGSTPercentage))
                            jsonProductObject.put("orderdetailsstitchingPercentageIGST", String.format("%.2f", verifyCartArray[i].IGSTPercentage))
                            jsonProductObject.put("orderdetailsstitchingDeliveryType", verifyCartArray[i].addToCartDeliveryName)
                            jsonProductObject.put("orderdetailsstitchingDeliveryCharges", String.format("%.2f", verifyCartArray[i].addToCartDeliveryCharges))

                            if (verifyCartArray[i].addToCartExceptedDate.isNullOrEmpty()){
                                jsonProductObject.put("orderdetailsExpectedDeliveryDate", "")
                            }else{
                                jsonProductObject.put("orderdetailsstitchingExpectedDeliveryDate", MyUtils.formatDate(verifyCartArray[i].addToCartExceptedDate, "dd-MM-yyyy", "yyyy-MM-dd"))
                            }

                            if (!verifyCartArray[i].stichingServiceData.isNullOrEmpty()){
                                for(j in 0 until verifyCartArray[i].stichingServiceData!!.size){
                                    jsonProductObject.put("categorytypeID",verifyCartArray[i].stichingServiceData!![j].categorytypeID)
                                    jsonProductObject.put("categoryID",verifyCartArray[i].stichingServiceData!![j].categoryID)
                                    jsonProductObject.put("subcatID",verifyCartArray[i].stichingServiceData!![j].subcatID)
                                }
                            }
                            jsonMainObject.put("orderDiscountCode", cartArrayList[i].appliedCouponCode)
                            jsonMainObject.put("orderDiscountCodeCreatedBy", cartArrayList[i].appliedCouponCodeCreatedBy)
                            jsonServiceArray.put(jsonProductObject)
                        }else{
                            jsonMainObject.put("orderType", "Buy Fabric")
                            // for product
                            jsonProductObject.put("productID", verifyCartArray[i].productId)
                            jsonProductObject.put("productGST", verifyCartArray[i].categoryGST)
                            jsonProductObject.put("orderdetailsPrice", verifyCartArray[i].productDiscountedPrice)
                            jsonProductObject.put("orderdetailsQty", verifyCartArray[i].cartQuantity.toString())
                            jsonProductObject.put("orderdetailsCSGT", String.format("%.2f", verifyCartArray[i].CGST))
                            jsonProductObject.put("orderdetailsSGST", String.format("%.2f", verifyCartArray[i].SGST))
                            jsonProductObject.put("orderdetailsIGST", String.format("%.2f", verifyCartArray[i].IGST))
                            jsonProductObject.put("orderdetailsPercentageCSGT", String.format("%.2f", verifyCartArray[i].CGSTPercentage))
                            jsonProductObject.put("orderdetailsPercentageSGST", String.format("%.2f", verifyCartArray[i].SGSTPercentage))
                            jsonProductObject.put("orderdetailsPercentageIGST", String.format("%.2f", verifyCartArray[i].IGSTPercentage))
                            jsonProductObject.put("orderdetailsDeliveryType", verifyCartArray[i].addToCartDeliveryName)
                            jsonProductObject.put("orderdetailsDeliveryCharges", String.format("%.2f", verifyCartArray[i].addToCartDeliveryCharges))
                            if (verifyCartArray[i].addToCartExceptedDate.isNullOrEmpty()){
                                jsonProductObject.put("orderdetailsExpectedDeliveryDate", "")
                            }else{
                                jsonProductObject.put("orderdetailsExpectedDeliveryDate", MyUtils.formatDate(verifyCartArray[i].addToCartExceptedDate, "dd-MM-yyyy", "yyyy-MM-dd"))
                            }


                            if (!verifyCartArray[i].productData.isNullOrEmpty()){
                                for(j in 0 until verifyCartArray[i].productData!!.size){
                                    jsonProductObject.put("categorytypeID",verifyCartArray[i].productData!![j].categorytypeID)
                                    jsonProductObject.put("categoryID",verifyCartArray[i].productData!![j].categoryID)
                                    jsonProductObject.put("subcatID",verifyCartArray[i].productData!![j].subcatID)

                                    if (!verifyCartArray[i].productData!![j].productprice.isNullOrEmpty()){
                                        for(k in 0 until  verifyCartArray[i].productData!![j].productprice!!.size){
                                            if (verifyCartArray[i].productData!![j].productprice!![k].standardmeasurementID.equals(verifyCartArray[i].addToCartSizeId, true)){
                                                jsonProductObject.put("productpriceID",verifyCartArray[i].productData!![j].productprice!![k].productpriceID)
                                            }
                                        }
                                    }

                                }
                            }
                            jsonProductObject.put("brandID",verifyCartArray[i].addToCartBrandId)
                            jsonProductObject.put("materialID",verifyCartArray[i].addToCartMaterialId)
                            jsonProductObject.put("colorID",verifyCartArray[i].addToCartColorId)
                            jsonProductObject.put("patternID",verifyCartArray[i].addToCartPatternId)
                            jsonProductObject.put("styleID",verifyCartArray[i].addToCartStyleId)
                            jsonProductObject.put("standardmeasurementID",verifyCartArray[i].addToCartSizeId)
                            jsonMainObject.put("orderDiscountCode", cartArrayList[i].appliedCouponCode)
                            jsonMainObject.put("orderDiscountCodeCreatedBy", cartArrayList[i].appliedCouponCodeCreatedBy)

                            jsonProductArray.put(jsonProductObject)
                        }
                    }
                }
                jsonMainObject.put("orderdetails", jsonProductArray)
                jsonMainObject.put("stitching", jsonServiceArray)
                jsonMainObject.put("orderGrossAmt", String.format("%.2f", cartCalculate?.getOrderSubTotal()))
                jsonMainObject.put("orderCGST", String.format("%.2f", cartCalculate?.getOrderCGST()))
                jsonMainObject.put("orderSGST", String.format("%.2f", cartCalculate?.getOrderSGST()))
                jsonMainObject.put("orderIGST", String.format("%.2f", cartCalculate?.getOrderIGST()))
                jsonMainObject.put("orderDeliveryCharges", String.format("%.2f", cartCalculate?.getShippingCharge()))
                jsonMainObject.put("orderWalletRechargeAmount", "0.0")
                jsonMainObject.put("orderWalletFlyMyOwnAmount", "0.0")

//                var ordeTotal = cartCalculate?.getOrderTotal()

                //TODO To confirm this value with vaikung gandhi

                jsonMainObject.put("orderNetAmount", String.format("%.2f", cartCalculate?.getOrderTotal()))
                jsonMainObject.put("orderDiscount", String.format("%.2f", cartCalculate?.getAppliedCoponValue()))
                jsonMainObject.put("orderPaymentMode", "")
                jsonMainObject.put("apiVersion", RestClient.apiVersion)
                jsonMainObject.put("apiType", RestClient.apiType)
                jsonMainArray.put(jsonMainObject)

                val productVerify = ViewModelProviders.of(mActivity!!).get(VerifyCartItemsModel::class.java)
                productVerify.getVerifyCartItems(mActivity!!, true, jsonMainArray.toString()).observe(mActivity!!,
                    androidx.lifecycle.Observer<List<VeriFyCartPojo>> { verifyCartItem: List<VeriFyCartPojo> ->

                        if (!verifyCartItem.isNullOrEmpty()){
                            if (verifyCartItem[0].status.equals("true", true)) {
//                            if (verifyCartItem[0].product!!.isNotEmpty()) {
//                                cartCalculate!!.verifyItemSetIssue(verifyCartItem[0])
//                            } else {
//                                cartCalculate!!.verifyItemSetNoIssue()
//                            }
//
//                            cartCalculate!!.cartCalculation()
//                            recyclerViewCaartItemAdapter!!.notifyDataSetChanged()

                                if (buttonOrElse) Handler().postDelayed({chekOuTAfterVerifyCart()}, 500)

                            }
                        }else{
                            if (MyUtils.isInternetAvailable(mActivity!!)){
                                MyUtils.showSnackbarkotlin(mActivity!!, ll_main!!, mActivity!!.resources.getString(R.string.error_crash_error_message))
                            }else{
                                MyUtils.showSnackbarkotlin(mActivity!!, ll_main!!, mActivity!!.resources.getString(R.string.error_common_network))
                            }
                        }
                    })
            }else{
                MyUtils.showMessageOKCancel(
                    mActivity!!,
                    "Please add your delivery address.",
                    object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            (mActivity as MainActivity).navigateTo(
                                BillingAddressFragment(),
                                BillingAddressFragment::class.java.name,
                                true
                            )
                        }
                    })
            }
        }catch (e : java.lang.Exception){
            e.printStackTrace()
        }
    }

    private fun placeOrder():String
    {

        var placeOrder:String=""
        val verifyCartArray = cartCalculate?.getCartArrayList()

        cartCalculate?.CartCalculate(mActivity!!)
        val jsonMainObject = JSONObject()
        val jsonMainArray = JSONArray()
        val jsonProductArray = JSONArray()
        val jsonServiceArray = JSONArray()
        var userAddress = ""
        var totalDeliveryCharges = 0

        try {
            if (!useraData?.billing.isNullOrEmpty()) {
                for (i in 0 until useraData?.billing!!.size) {
                    if (useraData?.billing!![i]?.addressIsDefault.equals("Yes", true)) {
                        userAddress = useraData?.billing!![i].addressAddressLine1!! + ", " +
                                useraData?.billing!![i]?.addressLandmark + ", " +
                                useraData?.billing!![i]?.addressLocality + ", " +
                                useraData?.billing!![i]?.addressAddressLine2 + ", " +
                                useraData?.billing!![i]?.stateName + ", " +
                                useraData?.billing!![i]?.addressPincode + ", " +
                                useraData?.billing!![i]?.countryName
                        jsonMainObject.put(
                            "addressPincode",
                            useraData?.billing!![i]?.addressPincode
                        )
                        jsonMainObject.put(
                            "orderDeliveryLatitude",
                            useraData?.billing!![i]?.addressLatitude
                        )
                        jsonMainObject.put(
                            "orderDeliveryLongitude",
                            useraData?.billing!![i]?.addressLongitude
                        )
                        jsonMainObject.put("addressID", useraData?.billing!![i]?.addressID)
                    }
                }
                jsonMainObject.put("orderBillingAddress", userAddress)
                jsonMainObject.put("orderDeliveryAddress", userAddress)

                if (!verifyCartArray.isNullOrEmpty()) {
                    jsonMainObject.put("dealerID", verifyCartArray[0].dealerID)
                    for (i in 0 until verifyCartArray.size) {
                        val jsonProductObject = JSONObject()

                        if (verifyCartArray[i].productData.isNullOrEmpty()) {
                            jsonMainObject.put("orderType", "Stitching")
                            // For service
                            jsonProductObject.put(
                                "stitchingserviceID",
                                verifyCartArray[i].stitchingId
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingPrice",
                                String.format("%.2f", verifyCartArray[i].productDiscountedPrice)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingQty",
                                verifyCartArray[i].cartQuantity.toString()
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingCSGT",
                                String.format("%.2f", verifyCartArray[i].CGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingSGST",
                                String.format("%.2f", verifyCartArray[i].SGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingIGST",
                                String.format("%.2f", verifyCartArray[i].IGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingPercentageCSGT",
                                String.format("%.2f", verifyCartArray[i].CGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingPercentageSGST",
                                String.format("%.2f", verifyCartArray[i].SGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingPercentageIGST",
                                String.format("%.2f", verifyCartArray[i].IGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingDeliveryType",
                                verifyCartArray[i].addToCartDeliveryName
                            )
                            jsonProductObject.put(
                                "orderdetailsstitchingDeliveryCharges",
                                String.format("%.2f", verifyCartArray[i].addToCartDeliveryCharges)
                            )

                            if (verifyCartArray[i].addToCartExceptedDate.isNullOrEmpty()) {
                                jsonProductObject.put("orderdetailsExpectedDeliveryDate", "")
                            } else {
                                jsonProductObject.put(
                                    "orderdetailsstitchingExpectedDeliveryDate",
                                    MyUtils.formatDate(
                                        verifyCartArray[i].addToCartExceptedDate,
                                        "dd-MM-yyyy",
                                        "yyyy-MM-dd"
                                    )
                                )
                            }

                            if (!verifyCartArray[i].stichingServiceData.isNullOrEmpty()) {
                                for (j in 0 until verifyCartArray[i].stichingServiceData!!.size) {
                                    jsonProductObject.put(
                                        "categorytypeID",
                                        verifyCartArray[i].stichingServiceData!![j].categorytypeID
                                    )
                                    jsonProductObject.put(
                                        "categoryID",
                                        verifyCartArray[i].stichingServiceData!![j].categoryID
                                    )
                                    jsonProductObject.put(
                                        "subcatID",
                                        verifyCartArray[i].stichingServiceData!![j].subcatID
                                    )
                                }
                            }
                            jsonMainObject.put("orderDiscountCode", cartArrayList[i].appliedCouponCode)
                            jsonMainObject.put("orderDiscountCodeCreatedBy", cartArrayList[i].appliedCouponCodeCreatedBy)
                            jsonServiceArray.put(jsonProductObject)
                        }
                        else {
                            jsonMainObject.put("orderType", "Buy Fabric")
                            // for product
                            jsonProductObject.put("productID", verifyCartArray[i].productId)
                            jsonProductObject.put("productGST", verifyCartArray[i].categoryGST)
                            jsonProductObject.put(
                                "orderdetailsPrice",
                                verifyCartArray[i].productDiscountedPrice
                            )
                            jsonProductObject.put(
                                "orderdetailsQty",
                                verifyCartArray[i].cartQuantity.toString()
                            )
                            jsonProductObject.put(
                                "orderdetailsCSGT",
                                String.format("%.2f", verifyCartArray[i].CGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsSGST",
                                String.format("%.2f", verifyCartArray[i].SGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsIGST",
                                String.format("%.2f", verifyCartArray[i].IGST)
                            )
                            jsonProductObject.put(
                                "orderdetailsPercentageCSGT",
                                String.format("%.2f", verifyCartArray[i].CGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsPercentageSGST",
                                String.format("%.2f", verifyCartArray[i].SGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsPercentageIGST",
                                String.format("%.2f", verifyCartArray[i].IGSTPercentage)
                            )
                            jsonProductObject.put(
                                "orderdetailsDeliveryType",
                                verifyCartArray[i].addToCartDeliveryName
                            )
                            jsonProductObject.put(
                                "orderdetailsDeliveryCharges",
                                String.format("%.2f", verifyCartArray[i].addToCartDeliveryCharges)
                            )
                            if (verifyCartArray[i].addToCartExceptedDate.isNullOrEmpty()) {
                                jsonProductObject.put("orderdetailsExpectedDeliveryDate", "")
                            } else {
                                jsonProductObject.put(
                                    "orderdetailsExpectedDeliveryDate",
                                    MyUtils.formatDate(
                                        verifyCartArray[i].addToCartExceptedDate,
                                        "dd-MM-yyyy",
                                        "yyyy-MM-dd"
                                    )
                                )
                            }


                            if (!verifyCartArray[i].productData.isNullOrEmpty()) {
                                for (j in 0 until verifyCartArray[i].productData!!.size) {
                                    jsonProductObject.put(
                                        "categorytypeID",
                                        verifyCartArray[i].productData!![j]?.categorytypeID
                                    )
                                    jsonProductObject.put(
                                        "categoryID",
                                        verifyCartArray[i].productData!![j]?.categoryID
                                    )
                                    jsonProductObject.put(
                                        "subcatID",
                                        verifyCartArray[i].productData!![j]?.subcatID
                                    )

                                    if (!verifyCartArray[i].productData!![j]?.productprice.isNullOrEmpty()) {
                                        for (k in 0 until verifyCartArray[i].productData!![j].productprice!!.size) {
                                            if (verifyCartArray[i].productData!![j]?.productprice!![k]?.standardmeasurementID.equals(
                                                    verifyCartArray[i].addToCartSizeId,
                                                    true
                                                )
                                            ) {
                                                jsonProductObject.put(
                                                    "productpriceID",
                                                    verifyCartArray[i].productData!![j]?.productprice!![k]?.productpriceID
                                                )
                                            }
                                        }
                                    }

                                }
                            }
                            jsonProductObject.put("brandID", verifyCartArray[i]?.addToCartBrandId)
                            jsonProductObject.put(
                                "materialID",
                                verifyCartArray[i].addToCartMaterialId
                            )
                            jsonProductObject.put("colorID", verifyCartArray[i]?.addToCartColorId)
                            jsonProductObject.put(
                                "patternID",
                                verifyCartArray[i].addToCartPatternId
                            )
                            jsonProductObject.put("styleID", verifyCartArray[i]?.addToCartStyleId)
                            jsonProductObject.put(
                                "standardmeasurementID",
                                verifyCartArray[i].addToCartSizeId
                            )
                            jsonMainObject.put("orderDiscountCode", cartArrayList[i].appliedCouponCode)
                            jsonMainObject.put("orderDiscountCodeCreatedBy", cartArrayList[i].appliedCouponCodeCreatedBy)
                            jsonProductArray.put(jsonProductObject)
                        }
                    }
                }

                jsonMainObject.put("loginuserID", useraData?.userID)
                jsonMainObject.put("orderdetails", jsonProductArray)
                jsonMainObject.put("stitching", jsonServiceArray)
                jsonMainObject.put(
                    "orderGrossAmt",
                    String.format("%.2f", cartCalculate?.getOrderSubTotal())
                )
                jsonMainObject.put(
                    "orderCGST",
                    String.format("%.2f", cartCalculate?.getOrderCGST())
                )
                jsonMainObject.put(
                    "orderSGST",
                    String.format("%.2f", cartCalculate?.getOrderSGST())
                )
                jsonMainObject.put(
                    "orderIGST",
                    String.format("%.2f", cartCalculate?.getOrderIGST())
                )
                jsonMainObject.put(
                    "orderDeliveryCharges",
                    String.format("%.2f", cartCalculate?.getShippingCharge())
                )
                jsonMainObject.put("orderWalletRechargeAmount", "0.0")
                jsonMainObject.put("orderWalletFlyMyOwnAmount", "0.0")

//                var ordeTotal = cartCalculate?.getOrderTotal()

                //TODO To confirm this value with vaikung gandhi

                jsonMainObject.put(
                    "orderNetAmount",
                    String.format("%.2f", cartCalculate?.getOrderTotal())
                )

                jsonMainObject.put("orderDiscount", String.format("%.2f", cartCalculate?.getAppliedCoponValue()))
                jsonMainObject.put("orderPaymentMode", "")
                jsonMainObject.put("apiVersion", RestClient.apiVersion)
                jsonMainObject.put("apiType", RestClient.apiType)
                jsonMainArray.put(jsonMainObject)
                placeOrder = jsonMainObject.toString()

            }

        }catch (e:java.lang.Exception)
        {}
        return placeOrder
    }


}
