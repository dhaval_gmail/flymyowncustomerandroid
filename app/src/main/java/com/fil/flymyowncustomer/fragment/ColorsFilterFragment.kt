package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R

import com.fil.flymyowncustomer.adapter.FilterAdapter
import com.fil.flymyowncustomer.pojo.*
import kotlinx.android.synthetic.main.fragment_color_filter.*
import kotlinx.android.synthetic.main.fragment_color_filter.notificationRecyclerview
import kotlinx.android.synthetic.main.toolbar_back.*


/**
 * A simple [Fragment] subclass.
 */
class ColorsFilterFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null
    var filterAdapter: FilterAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var masterData: ArrayList<MasterPojo?>?=null
    var tabposition =-1
    var listSubItem: ArrayList<FilterSubItem?>? = null

    var colorsIDs:String=""
    var materialsIDs:String=""
    var patternsIDs:String=""
    var stylesIDs:String=""
    var brandsIDs:String=""
    var onSelectedDataPass: OnSelectedDataPass? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_color_filter, container, false)

        }
        return v
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as Activity
            onSelectedDataPass = activity as OnSelectedDataPass

        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar_back.visibility=View.GONE

        if(arguments!=null)
         {
             masterData= arguments!!.getSerializable("masterList") as ArrayList<MasterPojo?>?
             tabposition= arguments!!.getInt("position")

             colorsIDs=arguments!!.getString("colorsIDs")!!
             materialsIDs=arguments!!.getString("materialsIDs")!!
             patternsIDs=arguments!!.getString("patternsIDs")!!
             stylesIDs=arguments!!.getString("stylesIDs")!!
             brandsIDs=arguments!!.getString("brandsIDs")!!

         }
        listSubItem= ArrayList()


        when(tabposition)
        {
            1->{
                 tv_offer_name.text="By Colors"
                 setColorsData(masterData?.get(0)?.colors!!)
            }
            2->{
                tv_offer_name.text="By Material"
                setMaterialData(masterData?.get(0)?.material!!)

            }
            3->{
                tv_offer_name.text="By Pattern"
                setPatternData(masterData?.get(0)?.pattern!!)


            }
            4->{
                tv_offer_name.text="By Style"
                setStyleData(masterData?.get(0)?.style!!)

            }
            5->{
                tv_offer_name.text="By Brand"
                setBrandData(masterData?.get(0)?.brand!!)

            }
        }
        setAdapter()



    }

    private fun setBrandData(data: List<Brand?>) {
        listSubItem?.clear()
        var ids: List<String>? = null
        if (!brandsIDs.isNullOrEmpty()) {
            ids = brandsIDs.split(",")
        }
        data.forEach {


            var isSelected = false

            if (!ids.isNullOrEmpty()) {
                for (i in 0 until ids.size) {
                    if (ids[i].trim().equals(it?.brandID.toString().trim())) {
                        isSelected = true
                        break
                    }
                }
            }
            listSubItem?.add(FilterSubItem(it?.brandName.toString().capitalize(),it?.brandID.toString(), isSelected))

        }


        filterAdapter?.notifyDataSetChanged()
    }


    private fun setStyleData(data: List<Style?>) {
        listSubItem?.clear()
        var ids: List<String>? = null
        if (!stylesIDs.isNullOrEmpty()) {
            ids = stylesIDs.split(",")
        }
        data.forEach {


            var isSelected = false

            if (!ids.isNullOrEmpty()) {
                for (i in 0 until ids.size) {
                    if (ids[i].trim().equals(it?.styleID.toString().trim())) {
                        isSelected = true
                        break
                    }
                }
            }
            listSubItem?.add(FilterSubItem(it?.styleName.toString().capitalize(),it?.styleID.toString(), isSelected))
        }


        filterAdapter?.notifyDataSetChanged()
    }

    private fun setPatternData(data: List<Pattern?>) {
        listSubItem?.clear()
        var ids: List<String>? = null
        if (!patternsIDs.isNullOrEmpty()) {
            ids = patternsIDs.split(",")
        }
        data.forEach {


            var isSelected = false

            if (!ids.isNullOrEmpty()) {
                for (i in 0 until ids.size) {
                    if (ids[i].trim().equals(it?.patternID.toString().trim())) {
                        isSelected = true
                        break
                    }
                }
            }
            listSubItem?.add(FilterSubItem(it?.patternName.toString().capitalize(),it?.patternID.toString(), isSelected))
        }


        filterAdapter?.notifyDataSetChanged()
    }

    private fun setMaterialData(data: List<Material?>) {
        listSubItem?.clear()
        var ids: List<String>? = null
        if (!materialsIDs.isNullOrEmpty()) {
            ids = materialsIDs.split(",")
        }
        data.forEach {


            var isSelected = false

            if (!ids.isNullOrEmpty()) {
                for (i in 0 until ids.size) {
                    if (ids[i].trim().equals(it?.materialID.toString().trim())) {
                        isSelected = true
                        break
                    }
                }
            }
            listSubItem?.add(FilterSubItem(it?.materialName.toString().capitalize(),it?.materialID.toString(), isSelected))
        }


        filterAdapter?.notifyDataSetChanged()
    }

    private fun setColorsData(data: List<Color?>) {
        listSubItem?.clear()
        var ids: List<String>? = null
        if (!colorsIDs.isNullOrEmpty()) {
            ids = colorsIDs.split(",")
        }
        data.forEach {


            var isSelected = false

            if (!ids.isNullOrEmpty()) {
                for (i in 0 until ids.size) {
                    if (ids[i].trim().equals(it?.colorID.toString().trim())) {
                        isSelected = true
                        break
                    }
                }
            }
            listSubItem?.add(FilterSubItem(it?.colorName.toString().capitalize(),it?.colorID.toString(), isSelected))
        }


        filterAdapter?.notifyDataSetChanged()
    }

    private fun setAdapter() {
        linearLayoutManager = LinearLayoutManager(mActivity)
        filterAdapter = FilterAdapter(
            activity!!,
            object : FilterAdapter.OnItemClick {
                override fun onClicklisneter(pos: Int, name: String) {
                    var selectedIds=""
                    selectedIds=listSubItem?.filter {
                        it?.isSelected!!
                    }!!.joinToString {
                        it!!.id
                    }
                    when (tabposition) {
                        1 -> colorsIDs=selectedIds
                        2 -> materialsIDs=selectedIds
                        3 -> patternsIDs=selectedIds
                        4 -> stylesIDs=selectedIds
                        5 -> brandsIDs=selectedIds
                    }
                    onSelectedDataPass!!.onSelectedDataPass(colorsIDs,materialsIDs,patternsIDs,stylesIDs,brandsIDs)



                }
            },listSubItem,tabposition
        )
        notificationRecyclerview.layoutManager = linearLayoutManager
        notificationRecyclerview.adapter = filterAdapter


    }


    interface OnSelectedDataPass{

        fun onSelectedDataPass(
            colors: String,
            materialsIDs: String,
            patternsIDs: String,
            stylesIDs: String,
            brandsIDs: String

        )

    }






}
