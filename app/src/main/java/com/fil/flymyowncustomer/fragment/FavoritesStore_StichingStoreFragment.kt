package com.fil.flymyowncustomer.fragment


import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.LifestyleStudioListingAdapter
import com.fil.flymyowncustomer.model.DealerFavouriteViewModel
import com.fil.flymyowncustomer.model.DelearListModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.pojo.Stitchingstudio
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class FavoritesStore_StichingStoreFragment : Fragment() {

    private var v: View? = null
    var mActivity: AppCompatActivity? = null
    val TAG = FavoritesStore_StichingStoreFragment::class.java.name

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0

    var mLifestyleStudioListingAdapter : LifestyleStudioListingAdapter?= null
    var dealerListData: ArrayList<Stitchingstudio?>?=null
    var sessionManager:SessionManager?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_favorites_store__stiching_store, container, false)
        }
        return v

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_notification).isVisible = true

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager= SessionManager(mActivity!!)
        (activity as MainActivity).setToolBar(toolbar_back!!)

        tvToolbarTitel.text = getString(R.string.account_favoritestore_stitching_studio)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        toolbar_back.menuNotification.visibility=View.GONE
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }


        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE

        linearLayoutManager = LinearLayoutManager(mActivity)
        notificationRecyclerview?.layoutManager = linearLayoutManager
        bindData()

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10) {

                        isLoading = true
                        getFavouriteListList()
                    }
                }
            }
        })

        btnRetry?.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
           getFavouriteListList()
        }


    }

    private fun bindData() {

      if (dealerListData == null) {
          dealerListData=ArrayList()
          mLifestyleStudioListingAdapter = LifestyleStudioListingAdapter(
           mActivity!!,
            object : LifestyleStudioListingAdapter.onItemClickk {
                override fun onClicklisneter(pos: Int,form:String) {
                    when(form)
                    {
                        "click" -> {
                            var lifestyleStoredetailsFragment= LifestyleStoredetailsFragment()
                            Bundle().apply {
                                putSerializable("from",dealerListData!![pos]!!.dealerType)
                                putSerializable("dealerListData",dealerListData!![pos]!!)
                                lifestyleStoredetailsFragment.arguments=this

                            }
                            (activity as MainActivity).navigateTo(lifestyleStoredetailsFragment, LifestyleStoredetailsFragment::class.java.name, true)

                        }
                        "fav" -> {
                            getAddProduct(pos)
                        }
                    }

                }

            },dealerListData!!)


        notificationRecyclerview?.setNestedScrollingEnabled(false)
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview?.adapter = mLifestyleStudioListingAdapter

         getFavouriteListList()
       }
    }



    private fun getFavouriteListList() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            dealerListData!!.clear()
            mLifestyleStudioListingAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            dealerListData!!.add(null)
            mLifestyleStudioListingAdapter?.notifyItemInserted(dealerListData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var productListModel =
            ViewModelProviders.of(this@FavoritesStore_StichingStoreFragment)
                .get(DelearListModel::class.java)
        productListModel.getDealer(mActivity!!, false, jsonArray.toString(),"favList")
            .observe(this@FavoritesStore_StichingStoreFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        notificationRecyclerview.visibility = (View.VISIBLE)

                        if (pageNo > 0) {
                            dealerListData!!.removeAt(dealerListData!!.size - 1)
                            mLifestyleStudioListingAdapter?.notifyItemRemoved(dealerListData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                dealerListData!!.clear()

                            dealerListData!!.addAll(masterPojo[0].data!!)
                            mLifestyleStudioListingAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (dealerListData.isNullOrEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE

                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })

    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE

            if (MyUtils.isInternetAvailable(mActivity!!)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun getAddProduct(pos: Int) {

        if (dealerListData!![pos]!!.isFavourite.equals("No", false)) {
            dealerListData!![pos]!!.isFavourite = "Yes"
            setAddFavourite(dealerListData!![pos]!!.dealerID, pos, "AddFav")
        } else {
            dealerListData!![pos]!!.isFavourite = "No"
            setAddFavourite(dealerListData!![pos]!!.dealerID, pos, "RemoveFav")
        }

    }

    private fun setAddFavourite(productID: String?, pos: Int, type: String) {
        var data:  RegisterNewPojo.Datum = sessionManager!!.get_Authenticate_User()

        var addFavDoctorModel = ViewModelProviders.of(this@FavoritesStore_StichingStoreFragment).get(
            DealerFavouriteViewModel::class.java)
        addFavDoctorModel.apiFunction(activity!!, false, data.userID!!, data.languageID!!, type, productID!!)
            .observe(this@FavoritesStore_StichingStoreFragment,
                Observer { commonresponsePojo ->

                    if (commonresponsePojo != null && commonresponsePojo.isNotEmpty()) {
                        if (commonresponsePojo[0].status.equals("true", true)) {
                            //(activity as MainActivity).showSnackBar(commonresponsePojo[0].message)
                            if (type.equals("add", false)) {
                                dealerListData!![pos]!!.isFavourite = "Yes"
                                //(activity as MainActivity).showSnackBar(activity!!.getString(R.string.successfavproduct))

                            } else if (type.equals("RemoveFav", false)) {
                                dealerListData!![pos]!!.isFavourite = "No"
                                dealerListData!!.removeAt(pos)
                                mLifestyleStudioListingAdapter !!.notifyItemRemoved(pos)
                                mLifestyleStudioListingAdapter!!.notifyItemChanged(pos, dealerListData!!.size)
                               // (activity as MainActivity).showSnackBar(activity!!.getString(R.string.removefavproduct))
                            }
                            mLifestyleStudioListingAdapter!!.notifyDataSetChanged()
                        }else {
                            (activity as MainActivity).showSnackBar(commonresponsePojo[0].message!!)
                        }
                    } else {
                        (activity as MainActivity).errorMethod()
                    }
                })
    }


}
