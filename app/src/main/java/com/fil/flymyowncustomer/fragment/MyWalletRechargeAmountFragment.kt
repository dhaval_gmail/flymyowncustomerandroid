package com.fil.flymyowncustomer.fragment


import android.app.Activity
import android.os.Bundle

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.adapter.TransactionHistoryAdapter
import com.fil.flymyowncustomer.model.RechargeWalletHistoryModel
import com.fil.flymyowncustomer.pojo.WallethistoryData
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.fragment_my_wallet_recharge_amount.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 *
 */
class MyWalletRechargeAmountFragment : Fragment() {

    var v: View? = null
    var mActivity: Activity? = null

    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    var pageNo = 0
    var mTransactionHistoryAdapter : TransactionHistoryAdapter?=null
    var wallethistoryData:ArrayList<WallethistoryData>?=null
    var sessionManager:SessionManager?=null
    var type=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(v==null)
        {
            v= inflater.inflate(R.layout.fragment_my_wallet_recharge_amount, container, false)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setToolBar(toolbar_back!!)
        tvToolbarTitel.text = getString(R.string.my_wallet)
        (activity as MainActivity).bottom_navigation?.visibility=View.GONE
        (activity as MainActivity).setDrawerSwipe(false)
        sessionManager= SessionManager(mActivity!!)
        toolbar_back.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        if(arguments!=null)
        {
            type=arguments!!.getString("type","")
        }

        linearLayoutManager = LinearLayoutManager(mActivity)
        notificationRecyclerview?.layoutManager = linearLayoutManager

        bindData()

        btnRechargeAmountMyWallet.setOnClickListener {
            (activity as MainActivity).navigateTo(AddAmountFragment(),AddAmountFragment::class.java.name,true)
        }

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.childCount
                totalItemCount = linearLayoutManager.itemCount
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10) {

                        isLoading = true
                        rechargeWalletHistory()
                    }
                }
            }
        })



    }

    private fun bindData() {

        if (wallethistoryData == null) {
            wallethistoryData=ArrayList()
             mTransactionHistoryAdapter = TransactionHistoryAdapter(
            activity as MainActivity,
            object : TransactionHistoryAdapter.onItemClickk {
                override fun onClicklisneter(pos: Int) {

                }

            }, wallethistoryData!!)
        notificationRecyclerview?.isNestedScrollingEnabled = false
        notificationRecyclerview?.setHasFixedSize(true)
        notificationRecyclerview?.adapter = mTransactionHistoryAdapter
            rechargeWalletHistory()
       }
    }


    private  fun rechargeWalletHistory()
    {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            wallethistoryData!!.clear()
            mTransactionHistoryAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            wallethistoryData!!.add(null!!)
            mTransactionHistoryAdapter?.notifyItemInserted(wallethistoryData!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("wallettransactonType","Recharge")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var myWallethistory =
            ViewModelProviders.of(this@MyWalletRechargeAmountFragment).get(
                RechargeWalletHistoryModel::class.java)
        myWallethistory.rechargeWalletHistory(mActivity!!, false, jsonArray.toString())
            .observe(this@MyWalletRechargeAmountFragment,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        if (pageNo > 0) {
                            wallethistoryData!!.removeAt(wallethistoryData!!.size - 1)
                            mTransactionHistoryAdapter?.notifyItemRemoved(wallethistoryData!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            ll_transactionID.visibility=View.VISIBLE

                            tvRechargeAmountBalance.text= resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(java.lang.Double.valueOf(masterPojo[0].userWalletRechargeAmount))

                            if (pageNo == 0)
                                wallethistoryData!!.clear()

                            wallethistoryData!!.addAll(masterPojo[0].data!!)
                            mTransactionHistoryAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                            userData?.userWalletRechargeAmount = ""+masterPojo[0].userWalletRechargeAmount.toString()
                            userData?.userWalletReferAndEarnAmount = masterPojo[0].userWalletReferAndEarnAmount.toString()
                            val gson = Gson()
                            val json: String = gson.toJson(userData)
                            Log.e("System out","json == "+json)
                            sessionManager!!.create_login_session(
                                json,
                                userData!!.userEmail!!,
                                "",
                                true,
                                sessionManager!!.isEmailLogin()
                            )

                        } else {
                            tvRechargeAmountBalance.text= resources.getString(R.string.rs) + MyUtils.formatPricePerCountery(0.00)
                            ll_transactionID.visibility=View.GONE

                            if (wallethistoryData!!.isEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE


                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })

    }

    private fun nodatafound() {

        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility=View.VISIBLE
            ll_transactionID.visibility=View.GONE
            if (MyUtils.isInternetAvailable(mActivity!!))
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            }
            else
            {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }





}
