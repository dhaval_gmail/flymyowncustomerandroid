package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class ApplyCouponList(
    @SerializedName("date")
    val date: List<Date>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class Date(
    @SerializedName("offerDiscountType")
    val offerDiscountType: String,
    @SerializedName("offerDiscountValue")
    val offerDiscountValue: String,
    @SerializedName("offerMaximumDiscountPossible")
    val offerMaximumDiscountPossible: String
)