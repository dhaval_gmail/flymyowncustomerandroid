package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class ProductReviewList(
    @SerializedName("data")
    val `data`: List<ProductReviewListData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class ProductReviewListData(
    @SerializedName("reviewDateTime")
    val reviewDateTime: String,
    @SerializedName("reviewID")
    val reviewID: String,
    @SerializedName("reviewRatting")
    val reviewRatting: String,
    @SerializedName("reviewReview")
    val reviewReview: String,
    @SerializedName("userFullName")
    val userFullName: String,
    @SerializedName("userProfilePicture")
    val userProfilePicture: String
)