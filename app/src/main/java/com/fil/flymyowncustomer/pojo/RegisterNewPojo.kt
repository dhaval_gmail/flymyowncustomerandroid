package com.fil.flymyowncustomer.pojo

import java.io.Serializable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterNewPojo : Serializable {

    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("action")
    @Expose
    var action: String? = null

    class Datum : Serializable {

        @SerializedName("countryFlagImage")
        @Expose
        var countryFlagImage: String? = null
        @SerializedName("userID")
        @Expose
        var userID: String? = null
        @SerializedName("userFullName")
        @Expose
        var userFullName: String? = null
        @SerializedName("userEmail")
        @Expose
        var userEmail: String? = null
        @SerializedName("userFacebookID")
        @Expose
        var userFacebookID: String? = null
        @SerializedName("userGoogleID")
        @Expose
        var userGoogleID: String? = null
        @SerializedName("userCountryCode")
        @Expose
        var userCountryCode: String? = null
        @SerializedName("userMobile")
        @Expose
        var userMobile: String? = null
        @SerializedName("userPassword")
        @Expose
        var userPassword: String? = null
        @SerializedName("userProfilePicture")
        @Expose
        var userProfilePicture: String? = null
        @SerializedName("languageID")
        @Expose
        var languageID: String? = null
        @SerializedName("userDeviceType")
        @Expose
        var userDeviceType: String? = null
        @SerializedName("userDeviceID")
        @Expose
        var userDeviceID: String? = null
        @SerializedName("userVerified")
        @Expose
        var userVerified: String? = null
        @SerializedName("userOrderEmailNotification")
        @Expose
        var userOrderEmailNotification: String? = null
        @SerializedName("userOrderPushNotification")
        @Expose
        var userOrderPushNotification: String? = null
        @SerializedName("userOfferEmailNotification")
        @Expose
        var userOfferEmailNotification: String? = null
        @SerializedName("userOfferPushNotification")
        @Expose
        var userOfferPushNotification: String? = null
        @SerializedName("userStatus")
        @Expose
        var userStatus: String? = null
        @SerializedName("userOTP")
        @Expose
        var userOTP: String? = null
        @SerializedName("userLatitude")
        @Expose
        var userLatitude: String? = null
        @SerializedName("userLongitude")
        @Expose
        var userLongitude: String? = null
        @SerializedName("userType")
        @Expose
        var userType: String? = null
        @SerializedName("userCreatedDate")
        @Expose
        var userCreatedDate: String? = null
        @SerializedName("userWalletRechargeAmount")
        @Expose
        var userWalletRechargeAmount: String? = null
        @SerializedName("userWalletReferAndEarnAmount")
        @Expose
        var userWalletReferAndEarnAmount: String? = null
        @SerializedName("userReferCode")
        @Expose
        var userReferCode: String? = null
        @SerializedName("userSingupReferCode")
        @Expose
        var userSingupReferCode: String? = null
        @SerializedName("delivery")
        @Expose
        var delivery: List<BillingAddressData>? = null
        @SerializedName("billing")
        @Expose
        var billing: ArrayList<BillingAddressData>? = null
        @SerializedName("settings")
        @Expose
        var settings: List<Setting>? = null

        class Setting : Serializable {

            @SerializedName("settingsSupportEmail")
            @Expose
            var settingsSupportEmail: String? = null
            @SerializedName("settingsSupportMobile")
            @Expose
            var settingsSupportMobile: String? = null
            @SerializedName("settingsPaymentUrl")
            @Expose
            var settingsPaymentUrl: String? = null
            @SerializedName("settingsPaymentSuccessUrl")
            @Expose
            var settingsPaymentSuccessUrl: String? = null
            @SerializedName("settingsPaymentErrorUrl")
            @Expose
            var settingsPaymentErrorUrl: String? = null
            @SerializedName("settingsGSTOnFabric")
            @Expose
            var settingsGSTOnFabric: String? = null
            @SerializedName("settingsGSTOnStitchingservice")
            @Expose
            var settingsGSTOnStitchingservice: String? = null
            @SerializedName("settingsGSTOnReadymadeGarments")
            @Expose
            var settingsGSTOnReadymadeGarments: String? = null
            @SerializedName("settingsGSTOnOtherService")
            @Expose
            var settingsGSTOnOtherService: String? = null
            @SerializedName("settingsMoneyToBeUsedFromWalletForPayment")
            @Expose
            var settingsMoneyToBeUsedFromWalletForPayment: String? = null
            @SerializedName("settingsMinimumOrderValue")
            @Expose
            var settingsMinimumOrderValue: String? = null

            @SerializedName("settingsMoneyEarnedByCustomerThroughReferAndEarn")
            @Expose
            var settingsMoneyEarnedByCustomerThroughReferAndEarn: String? = null

            @SerializedName("settingsReferAndEarnStatus")
            @Expose
            var settingsReferAndEarnStatus: String? = null

//            "settingsMoneyEarnedByCustomerThroughReferAndEarn": "10.00",
//            "settingsReferAndEarnStatus": "Active"

        }

    }
}
