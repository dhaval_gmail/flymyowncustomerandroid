package com.fil.flymyowncustomer.pojo

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class userSocialLoginPojo : Serializable {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("action")
    @Expose
    var action: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
}
