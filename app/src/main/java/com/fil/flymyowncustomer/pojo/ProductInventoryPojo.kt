package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class ProductInventoryPojo(
    @SerializedName("data")
    var `data`: List<ProductInventoryData?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
)

data class  ProductInventoryData(
    @SerializedName("inventoryStock")
    var inventoryStock: String?
)