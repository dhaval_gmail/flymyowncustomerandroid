package com.fil.flymyowncustomer.pojo


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class getChangePassword {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
}

