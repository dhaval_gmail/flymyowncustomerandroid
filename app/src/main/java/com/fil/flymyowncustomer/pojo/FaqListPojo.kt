package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class FaqListPojo : Serializable {

    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

     class Datum : Serializable {

        @SerializedName("faqID")
        @Expose
        var faqID: String? = null
        @SerializedName("faqTitle")
        @Expose
        var faqQuestion: String? = null
        @SerializedName("faqAnswer")
        @Expose
        var faqAnswer: String? = null
        var isExpand:Boolean=false
    }
}
