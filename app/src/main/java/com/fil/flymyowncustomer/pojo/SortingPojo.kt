package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SortingPojo : Serializable {

    @SerializedName("sortingName")
    @Expose
    var sortingName: String? = null

    var isSelection = false
}