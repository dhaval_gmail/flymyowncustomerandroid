package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class CouponListPojo(
    @SerializedName("data")
    val `data`: List<CouponListData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class CouponListData(
    @SerializedName("dealerID")
    val dealerID: String,
    @SerializedName("offerCodeCount")
    val offerCodeCount: String,
    @SerializedName("offerCodeType")
    val offerCodeType: String,
    @SerializedName("offerCreatedDate")
    val offerCreatedDate: String,
    @SerializedName("offerDescription")
    val offerDescription: String,
    @SerializedName("offerDiscountType")
    val offerDiscountType: String,
    @SerializedName("offerDiscountValue")
    val offerDiscountValue: String,
    @SerializedName("offerEndDate")
    val offerEndDate: String,
    @SerializedName("offerID")
    val offerID: String,
    @SerializedName("offerImage")
    val offerImage: String,
    @SerializedName("offerMaximumDiscountPossible")
    val offerMaximumDiscountPossible: String,
    @SerializedName("offerMinimumOrderAmount")
    val offerMinimumOrderAmount: String,
    @SerializedName("offerName")
    val offerName: String,
    @SerializedName("offerStartDate")
    val offerStartDate: String,
    @SerializedName("offerStatus")
    val offerStatus: String,
    @SerializedName("offerUsageLimit")
    val offerUsageLimit: String,
    @SerializedName("offerUsageType")
    val offerUsageType: String,
    @SerializedName("offerlogVoucherCode")
    val offerlogVoucherCode: String,
    @SerializedName("offerCreatedBy")
    val offerCreatedBy: String
)