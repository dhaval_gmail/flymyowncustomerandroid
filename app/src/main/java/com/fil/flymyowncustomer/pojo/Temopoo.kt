package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Temopoo {

    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    inner class Datum {

        @SerializedName("notificationID")
        @Expose
        var notificationID: String? = null
        @SerializedName("notificationType")
        @Expose
        var notificationType: String? = null
        @SerializedName("notificationTitle")
        @Expose
        var notificationTitle: String? = null
        @SerializedName("notificationReferenceKey")
        @Expose
        var notificationReferenceKey: String? = null
        @SerializedName("notificationMessageText")
        @Expose
        var notificationMessageText: String? = null
        @SerializedName("notificationReadStatus")
        @Expose
        var notificationReadStatus: String? = null
        @SerializedName("notificationSendDate")
        @Expose
        var notificationSendDate: String? = null
        @SerializedName("notificationSendTime")
        @Expose
        var notificationSendTime: String? = null

    }
}
