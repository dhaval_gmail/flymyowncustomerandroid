package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RegisterPojo : Serializable{

    @SerializedName("data")
    @Expose
    var data: List<Data>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    class Data : Serializable{

        @SerializedName("userID")
        @Expose
        var userID: String? = null
        @SerializedName("userFullName")
        @Expose
        var userFullName: String? = null
        @SerializedName("userEmail")
        @Expose
        var userEmail: String? = null
        @SerializedName("userFacebookID")
        @Expose
        var userFacebookID: String? = null
        @SerializedName("userGoogleID")
        @Expose
        var userGoogleID: String? = null
        @SerializedName("userCountryCode")
        @Expose
        var userCountryCode: String? = null
        @SerializedName("userMobile")
        @Expose
        var userMobile: String? = null
        @SerializedName("userProfilePicture")
        @Expose
        var userProfilePicture: String? = null
        @SerializedName("languageID")
        @Expose
        var languageID: String? = null
        @SerializedName("languageName")
        @Expose
        var languageName: String? = null
        @SerializedName("userDeviceType")
        @Expose
        var userDeviceType: String? = null
        @SerializedName("userDeviceID")
        @Expose
        var userDeviceID: String? = null
        @SerializedName("userType")
        @Expose
        var userType: String? = null
        @SerializedName("userVerified")
        @Expose
        var userVerified: String? = null
        @SerializedName("userEmailNotification")
        @Expose
        var userEmailNotification: String? = null
        @SerializedName("userPushNotification")
        @Expose
        var userPushNotification: String? = null
        @SerializedName("userOTP")
        @Expose
        var userOTP: String? = null
        @SerializedName("userCreatedDate")
        @Expose
        var userCreatedDate: String? = null
        @SerializedName("userReferCode")
        @Expose
        var userReferCode: String? = null
        @SerializedName("userSingupReferCode")
        @Expose
        var userSingupReferCode: String? = null
        @SerializedName("userPoint")
        @Expose
        var userPoint: String? = null
        @SerializedName("userPassword")
        @Expose
        var userPassword: String? = null

        @SerializedName("IsYouPurchasedSubscriptionPlan")
        @Expose
        var IsYouPurchasedSubscriptionPlan: String? = null

        @SerializedName("userNotificationMyOrders")
        @Expose
        var userNotificationMyOrders: String? = null

        @SerializedName("userNotificationTrainingCourses")
        @Expose
        var userNotificationTrainingCourses: String? = null

        @SerializedName("userNotificationMaintenanceServiceRequest")
        @Expose
        var userNotificationMaintenanceServiceRequest: String? = null

        @SerializedName("userNotificationRequirementRequest")
        @Expose
        var userNotificationRequirementRequest: String? = null

        @SerializedName("userNotificationDemoKitRequest")
        @Expose
        var userNotificationDemoKitRequest: String? = null

        @SerializedName("userNotificationOffersPromotions")
        @Expose
        var userNotificationOffersPromotions: String? = null

        @SerializedName("userNotificationMyReviews")
        @Expose
        var userNotificationMyReviews: String? = null

        @SerializedName("userNotificationMyWishlist")
        @Expose
        var userNotificationMyWishlist: String? = null

        /*@SerializedName("Delivery")
        @Expose
        var delivery: List<AddressPojo.Data>? = null*/
        @SerializedName("settings")
        @Expose
        var settings: List<Setting>? = null

        /*@SerializedName("userresume")
        @Expose
        var userresume: List<ResumeDataPojo>? = null

        @SerializedName("currentactiveforreveal")
        @Expose
        var currentactiveforreveal: List<GetUserBuySubscriptionPlanDatum>? = null

        @SerializedName("currentactiveformaintenance")
        @Expose
        var currentactiveformaintenance: List<GetUserBuySubscriptionPlanDatum>? = null

        @SerializedName("userlastfilter")
        @Expose
        var userlastfilter: ArrayList<UserlastfilterPojo>? = null

        @SerializedName("deliverycriteria")
        @Expose
        var deliverycriteria: ArrayList<DeliverycriteriaPojo>? = null*/

        @SerializedName("IsYouSharedYourExperience")
        @Expose
        var IsYouSharedYourExperience: String? = null

        @SerializedName("IsYouPostMaintenanceRequest")
        @Expose
        var IsYouPostMaintenanceRequest: String? = null

        class Setting : Serializable{

            @SerializedName("settingsOrderReturnTimeMinutes")
            @Expose
            var settingsOrderReturnTimeMinutes: String? = null
            @SerializedName("settingsMinFilterPrice")
            @Expose
            var settingsMinFilterPrice: String? = null
            @SerializedName("settingsMaxFilterPrice")
            @Expose
            var settingsMaxFilterPrice: String? = null
            @SerializedName("settingsReferralPoint")
            @Expose
            var settingsReferralPoint: String? = null
            @SerializedName("settingsReferralMsgText")
            @Expose
            var settingsReferralMsgText: String? = null

        }

    }
}