package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class GetUserSettingsPojo : Serializable {

    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    inner class Datum {

        @SerializedName("settingsID")
        @Expose
        var settingsID: String? = null
        @SerializedName("settingsEmailFrom")
        @Expose
        var settingsEmailFrom: String? = null
        @SerializedName("settingsEmailTo")
        @Expose
        var settingsEmailTo: String? = null
        @SerializedName("settingsEmailGateway")
        @Expose
        var settingsEmailGateway: String? = null
        @SerializedName("settingEmailID")
        @Expose
        var settingEmailID: String? = null
        @SerializedName("settingsEmailPass")
        @Expose
        var settingsEmailPass: String? = null
        @SerializedName("settingsSSL")
        @Expose
        var settingsSSL: String? = null
        @SerializedName("settingsTLS")
        @Expose
        var settingsTLS: String? = null
        @SerializedName("settingEmailPort")
        @Expose
        var settingEmailPort: String? = null
        @SerializedName("settingSenderName")
        @Expose
        var settingSenderName: String? = null
        @SerializedName("settingPGMode")
        @Expose
        var settingPGMode: String? = null
        @SerializedName("settingsPGSandboxUrl")
        @Expose
        var settingsPGSandboxUrl: String? = null
        @SerializedName("settingPGSandboxCustomerKey")
        @Expose
        var settingPGSandboxCustomerKey: String? = null
        @SerializedName("settingsPGSandboxCustomerAuth")
        @Expose
        var settingsPGSandboxCustomerAuth: String? = null
        @SerializedName("settingsPGLiveUrl")
        @Expose
        var settingsPGLiveUrl: String? = null
        @SerializedName("settingPGLiveCustomerKey")
        @Expose
        var settingPGLiveCustomerKey: String? = null
        @SerializedName("settingPGLiveCustomerAuth")
        @Expose
        var settingPGLiveCustomerAuth: String? = null
        @SerializedName("settingsUserResetPinLinkExpHr")
        @Expose
        var settingsUserResetPinLinkExpHr: String? = null
        @SerializedName("settingsLogDeleteDays")
        @Expose
        var settingsLogDeleteDays: String? = null
        @SerializedName("settingsDeliveryCharges")
        @Expose
        var settingsDeliveryCharges: String? = null
        @SerializedName("settingsFreeDeliveryAbove")
        @Expose
        var settingsFreeDeliveryAbove: String? = null
        @SerializedName("settingsTollFree")
        @Expose
        var settingsTollFree: String? = null
        @SerializedName("settingsOrderReturnTimeMinutes")
        @Expose
        var settingsOrderReturnTimeMinutes: String? = null
        @SerializedName("settingsMasterOtp")
        @Expose
        var settingsMasterOtp: String? = null
        @SerializedName("settingsCompanyGST")
        @Expose
        var settingsCompanyGST: String? = null
        @SerializedName("settingAddress")
        @Expose
        var settingAddress: String? = null

    }

}
