package com.fil.flymyowncustomer.pojo

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class BillingAddressPojo(
    @SerializedName("data")
    var `data`: List<BillingAddressData>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
):Serializable

data class BillingAddressData(
    @SerializedName("addressAddressLine1")
    var addressAddressLine1: String?,
    @SerializedName("addressAddressLine2")
    var addressAddressLine2: String?,
    @SerializedName("addressCreatedDate")
    var addressCreatedDate: String?,
    @SerializedName("addressGST")
    var addressGST: String?="",
    @SerializedName("addressGSTCompany")
    var addressGSTCompany: String?="",
    @SerializedName("addressGSTState")
    var addressGSTState:String="",
    @SerializedName("addressGSTStateID")
    var addressGSTStateID: String?="",
    @SerializedName("addressID")
    var addressID: String?,
    @SerializedName("addressIsDefault")
    var addressIsDefault: String?,
    @SerializedName("addressLandmark")
    var addressLandmark: String?,
    @SerializedName("addressLatitude")
    var addressLatitude: String?,
    @SerializedName("addressLocality")
    var addressLocality: String?,
    @SerializedName("addressLongitude")
    var addressLongitude: String?,
    @SerializedName("addressPincode")
    var addressPincode: String?,
    @SerializedName("addressType")
    var addressType: String?,
    @SerializedName("countryID")
    var countryID: String?,
    @SerializedName("countryName")
    var countryName: String?,
    @SerializedName("stateID")
    var stateID: String?,
    @SerializedName("stateName")
    var stateName: String?
): Serializable