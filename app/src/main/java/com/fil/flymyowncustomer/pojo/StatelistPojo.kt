package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class StatelistPojo(
    @SerializedName("data")
    var `data`: List<StatelisData?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
):Serializable

data class StatelisData(
    @SerializedName("countryID")
    var countryID: String?,
    @SerializedName("stateID")
    var stateID: String?,
    @SerializedName("stateName")
    var stateName: String?
):Serializable