package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class OrderDetailsPojo(
    @SerializedName("data")
    val `data`: List<OrderDetailsData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable

data class OrderDetailsData(
    @SerializedName("dealerID")
    val dealerID: String,
    @SerializedName("dealerinfo")
    val dealerinfo: List<Dealerinfo>,
    @SerializedName("orderBillingAddress")
    val orderBillingAddress: String,
    @SerializedName("orderCGST")
    val orderCGST: String,
    @SerializedName("orderDate")
    val orderDate: String,
    @SerializedName("orderDealerCGST")
    val orderDealerCGST: String,
    @SerializedName("orderDealerGrossAmt")
    val orderDealerGrossAmt: String,
    @SerializedName("orderDealerIGST")
    val orderDealerIGST: String,
    @SerializedName("orderDealerNetAmount")
    val orderDealerNetAmount: String,
    @SerializedName("orderDealerSGST")
    val orderDealerSGST: String,
    @SerializedName("orderDeliveryAddress")
    val orderDeliveryAddress: String,
    @SerializedName("orderDeliveryCharges")
    val orderDeliveryCharges: String,
    @SerializedName("orderDeliveryLatitude")
    val orderDeliveryLatitude: String,
    @SerializedName("orderDeliveryLongitude")
    val orderDeliveryLongitude: String,
    @SerializedName("orderDiscount")
    val orderDiscount: String,
    @SerializedName("orderDiscountCode")
    val orderDiscountCode: String,
    @SerializedName("orderDiscountCodeCreatedBy")
    val orderDiscountCodeCreatedBy: String,
    @SerializedName("orderGrossAmt")
    val orderGrossAmt: String,
    @SerializedName("orderID")
    val orderID: String,
    @SerializedName("orderIGST")
    val orderIGST: String,
    @SerializedName("orderNetAmount")
    val orderNetAmount: String,
    @SerializedName("orderNo")
    val orderNo: String,
    @SerializedName("orderPaymentMode")
    val orderPaymentMode: String,
    @SerializedName("orderPaymentStatus")
    val orderPaymentStatus: String,
    @SerializedName("orderSGST")
    val orderSGST: String,
    @SerializedName("orderStatusRemark")
    val orderStatusRemark: String,
    @SerializedName("orderType")
    val orderType: String,
    @SerializedName("orderWalletFlyMyOwnAmount")
    val orderWalletFlyMyOwnAmount: String,
    @SerializedName("orderWalletRechargeAmount")
    val orderWalletRechargeAmount: String,
    @SerializedName("orderdetails")
    val orderdetails: List<Orderdetail>,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusName")
    val statusName: String,
    @SerializedName("stitching")
    val stitching: List<Stitching>,
    @SerializedName("userID")
    val userID: String,
    @SerializedName("userinfo")
    val userinfo: List<Userinfo>
):Serializable

data class Dealerinfo(
    @SerializedName("addressAddressLine1")
    val addressAddressLine1: String,
    @SerializedName("addressAddressLine2")
    val addressAddressLine2: String,
    @SerializedName("addressCreatedDate")
    val addressCreatedDate: String,
    @SerializedName("addressGST")
    val addressGST: String,
    @SerializedName("addressGSTCompany")
    val addressGSTCompany: String,
    @SerializedName("addressGSTState")
    val addressGSTState: String,
    @SerializedName("addressGSTStateID")
    val addressGSTStateID: String,
    @SerializedName("addressID")
    val addressID: String,
    @SerializedName("addressIsDefault")
    val addressIsDefault: String,
    @SerializedName("addressLandmark")
    val addressLandmark: String,
    @SerializedName("addressLatitude")
    val addressLatitude: String,
    @SerializedName("addressLocality")
    val addressLocality: String,
    @SerializedName("addressLongitude")
    val addressLongitude: String,
    @SerializedName("addressPAN")
    val addressPAN: String,
    @SerializedName("addressPincode")
    val addressPincode: String,
    @SerializedName("AdharCardImages")
    val adharCardImages: List<AdharCardImage>,
    @SerializedName("BankStatementImages")
    val bankStatementImages: List<BankStatementImage>,
    @SerializedName("BannerImages")
    val bannerImages: List<BannerImage>,
    @SerializedName("billing")
    val billing: List<Any>,
    @SerializedName("countryFlagImage")
    val countryFlagImage: String,
    @SerializedName("countryID")
    val countryID: String,
    @SerializedName("countryName")
    val countryName: String,
    @SerializedName("dealerAdharCardNumber")
    val dealerAdharCardNumber: String,
    @SerializedName("dealerAlternateContact")
    val dealerAlternateContact: String,
    @SerializedName("dealerBankAccountNumber")
    val dealerBankAccountNumber: String,
    @SerializedName("dealerCompanyName")
    val dealerCompanyName: String,
    @SerializedName("dealerCountryCode")
    val dealerCountryCode: String,
    @SerializedName("dealerCreatedDate")
    val dealerCreatedDate: String,
    @SerializedName("dealerDeliveryCharges")
    val dealerDeliveryCharges: String,
    @SerializedName("dealerDeviceID")
    val dealerDeviceID: String,
    @SerializedName("dealerDeviceType")
    val dealerDeviceType: String,
    @SerializedName("dealerEmail")
    val dealerEmail: String,
    @SerializedName("dealerFacebookID")
    val dealerFacebookID: String,
    @SerializedName("dealerFreeServiceIsAvailable")
    val dealerFreeServiceIsAvailable: String,
    @SerializedName("dealerFullName")
    val dealerFullName: String,
    @SerializedName("dealerGST")
    val dealerGST: String,
    @SerializedName("dealerGoogleID")
    val dealerGoogleID: String,
    @SerializedName("dealerID")
    val dealerID: String,
    @SerializedName("dealerIsPopular")
    val dealerIsPopular: String,
    @SerializedName("dealerLatitude")
    val dealerLatitude: String,
    @SerializedName("dealerLongitude")
    val dealerLongitude: String,
    @SerializedName("dealerMobile")
    val dealerMobile: String,
    @SerializedName("dealerOTP")
    val dealerOTP: String,
    @SerializedName("dealerOfferRelatedEmail")
    val dealerOfferRelatedEmail: String,
    @SerializedName("dealerOfferRelatedPush")
    val dealerOfferRelatedPush: String,
    @SerializedName("dealerOrderRelatedEmail")
    val dealerOrderRelatedEmail: String,
    @SerializedName("dealerOrderRelatedPush")
    val dealerOrderRelatedPush: String,
    @SerializedName("dealerPAN")
    val dealerPAN: String,
    @SerializedName("dealerPassword")
    val dealerPassword: String,
    @SerializedName("dealerProfilePicture")
    val dealerProfilePicture: String,
    @SerializedName("dealerRatting")
    val dealerRatting: String,
    @SerializedName("dealerReviewRattingCount")
    val dealerReviewRattingCount: String,
    @SerializedName("dealerServiceType")
    val dealerServiceType: String,
    @SerializedName("dealerStatus")
    val dealerStatus: String,
    @SerializedName("dealerType")
    val dealerType: String,
    @SerializedName("dealerVerified")
    val dealerVerified: String,
    @SerializedName("dealeropeninghours")
    val dealeropeninghours: List<Dealeropeninghour>,
    @SerializedName("delivery")
    val delivery: List<Any>,
    @SerializedName("languageID")
    val languageID: String,
    @SerializedName("PANCardImages")
    val pANCardImages: List<PANCardImage>,
    @SerializedName("profile")
    val profile: List<Profile>,
    @SerializedName("settings")
    val settings: List<Setting>,
    @SerializedName("stateID")
    val stateID: String,
    @SerializedName("stateName")
    val stateName: String
):Serializable

data class AdharCardImage(
    @SerializedName("dealerimageID")
    val dealerimageID: String,
    @SerializedName("dealerimageName")
    val dealerimageName: String,
    @SerializedName("dealerimageStatus")
    val dealerimageStatus: String,
    @SerializedName("dealerimageType")
    val dealerimageType: String
):Serializable

data class BankStatementImage(
    @SerializedName("dealerimageID")
    val dealerimageID: String,
    @SerializedName("dealerimageName")
    val dealerimageName: String,
    @SerializedName("dealerimageStatus")
    val dealerimageStatus: String,
    @SerializedName("dealerimageType")
    val dealerimageType: String
):Serializable

data class BannerImage(
    @SerializedName("dealerimageID")
    val dealerimageID: String,
    @SerializedName("dealerimageName")
    val dealerimageName: String,
    @SerializedName("dealerimageStatus")
    val dealerimageStatus: String,
    @SerializedName("dealerimageType")
    val dealerimageType: String
):Serializable

data class Dealeropeninghour(
    @SerializedName("dealeropeninghourCreatedDate")
    val dealeropeninghourCreatedDate: String,
    @SerializedName("dealeropeninghourDay")
    val dealeropeninghourDay: String,
    @SerializedName("dealeropeninghourFrom")
    val dealeropeninghourFrom: String,
    @SerializedName("dealeropeninghourHoliday")
    val dealeropeninghourHoliday: String,
    @SerializedName("dealeropeninghourID")
    val dealeropeninghourID: String,
    @SerializedName("dealeropeninghourTo")
    val dealeropeninghourTo: String
):Serializable

data class PANCardImage(
    @SerializedName("dealerimageID")
    val dealerimageID: String,
    @SerializedName("dealerimageName")
    val dealerimageName: String,
    @SerializedName("dealerimageStatus")
    val dealerimageStatus: String,
    @SerializedName("dealerimageType")
    val dealerimageType: String
):Serializable

data class Profile(
    @SerializedName("addressAddressLine1")
    val addressAddressLine1: String,
    @SerializedName("addressAddressLine2")
    val addressAddressLine2: String,
    @SerializedName("addressCreatedDate")
    val addressCreatedDate: String,
    @SerializedName("addressGST")
    val addressGST: String,
    @SerializedName("addressGSTCompany")
    val addressGSTCompany: String,
    @SerializedName("addressGSTState")
    val addressGSTState: String,
    @SerializedName("addressGSTStateID")
    val addressGSTStateID: String,
    @SerializedName("addressID")
    val addressID: String,
    @SerializedName("addressIsDefault")
    val addressIsDefault: String,
    @SerializedName("addressLandmark")
    val addressLandmark: String,
    @SerializedName("addressLatitude")
    val addressLatitude: String,
    @SerializedName("addressLocality")
    val addressLocality: String,
    @SerializedName("addressLongitude")
    val addressLongitude: String,
    @SerializedName("addressPincode")
    val addressPincode: String,
    @SerializedName("addressType")
    val addressType: String,
    @SerializedName("countryID")
    val countryID: String,
    @SerializedName("countryName")
    val countryName: String,
    @SerializedName("stateID")
    val stateID: String,
    @SerializedName("stateName")
    val stateName: String
):Serializable

data class Setting(
    @SerializedName("settingsGSTOnFabric")
    val settingsGSTOnFabric: String,
    @SerializedName("settingsGSTOnOtherService")
    val settingsGSTOnOtherService: String,
    @SerializedName("settingsGSTOnReadymadeGarments")
    val settingsGSTOnReadymadeGarments: String,
    @SerializedName("settingsGSTOnStitchingservice")
    val settingsGSTOnStitchingservice: String,
    @SerializedName("settingsMinimumOrderValue")
    val settingsMinimumOrderValue: String,
    @SerializedName("settingsMoneyToBeUsedFromWalletForPayment")
    val settingsMoneyToBeUsedFromWalletForPayment: String,
    @SerializedName("settingsPaymentErrorUrl")
    val settingsPaymentErrorUrl: String,
    @SerializedName("settingsPaymentSuccessUrl")
    val settingsPaymentSuccessUrl: String,
    @SerializedName("settingsPaymentUrl")
    val settingsPaymentUrl: String,
    @SerializedName("settingsSupportEmail")
    val settingsSupportEmail: String,
    @SerializedName("settingsSupportMobile")
    val settingsSupportMobile: String
):Serializable

data class Orderdetail(
    @SerializedName("brandID")
    val brandID: String,
    @SerializedName("brandName")
    val brandName: String,
    @SerializedName("categoryID")
    val categoryID: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("categorytypeID")
    val categorytypeID: String,
    @SerializedName("categorytypeName")
    val categorytypeName: String,
    @SerializedName("colorID")
    val colorID: String,
    @SerializedName("colorImage")
    val colorImage: String,
    @SerializedName("colorName")
    val colorName: String,
    @SerializedName("materialID")
    val materialID: String,
    @SerializedName("materialName")
    val materialName: String,
    @SerializedName("orderdetailsCSGT")
    val orderdetailsCSGT: String,
    @SerializedName("orderdetailsDeliveryCharges")
    val orderdetailsDeliveryCharges: String,
    @SerializedName("orderdetailsDeliveryType")
    val orderdetailsDeliveryType: String,
    @SerializedName("orderdetailsEstimatedDeliveryDate")
    val orderdetailsEstimatedDeliveryDate: String,
    @SerializedName("orderdetailsExpectedDeliveryDate")
    val orderdetailsExpectedDeliveryDate: String,
    @SerializedName("orderdetailsID")
    val orderdetailsID: String,
    @SerializedName("orderdetailsIGST")
    val orderdetailsIGST: String,
    @SerializedName("orderdetailsPercentageCSGT")
    val orderdetailsPercentageCSGT: String,
    @SerializedName("orderdetailsPercentageIGST")
    val orderdetailsPercentageIGST: String,
    @SerializedName("orderdetailsPercentageSGST")
    val orderdetailsPercentageSGST: String,
    @SerializedName("orderdetailsPrice")
    val orderdetailsPrice: String,
    @SerializedName("orderdetailsQty")
    val orderdetailsQty: String,
    @SerializedName("orderdetailsSGST")
    val orderdetailsSGST: String,
    @SerializedName("orderdetailsStatusDate")
    val orderdetailsStatusDate: String,
    @SerializedName("orderdetailsStatusRemark")
    val orderdetailsStatusRemark: String,
    @SerializedName("patternID")
    val patternID: String,
    @SerializedName("patternName")
    val patternName: String,
    @SerializedName("productDescription")
    val productDescription: String,
    @SerializedName("productID")
    val productID: String,
    @SerializedName("productImage")
    val productImage: List<ProductImage>,
    @SerializedName("productName")
    val productName: String,
    @SerializedName("productType")
    val productType: String,
    @SerializedName("standardmeasurementID")
    val standardmeasurementID: String,
    @SerializedName("standardmeasurementName")
    val standardmeasurementName: String,
    @SerializedName("standardmeasurementSize")
    val standardmeasurementSize: String,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusName")
    val statusName: String,
    @SerializedName("styleID")
    val styleID: String,
    @SerializedName("styleName")
    val styleName: String,
    @SerializedName("subcatID")
    val subcatID: String,
    @SerializedName("subcatName")
    val subcatName: String,
    @SerializedName("IsYouRated")
    val isYouRated: String,
    @SerializedName("reasonID")
    var reasonID: String,
    @SerializedName("reasonInfo")
    var reasonInfo: String

):Serializable

data class ProductImage(
    @SerializedName("prodimgID")
    val prodimgID: String,
    @SerializedName("prodimgName")
    val prodimgName: String
):Serializable

data class Userinfo(
    @SerializedName("billing")
    val billing: List<Any>,
    @SerializedName("countryFlagImage")
    val countryFlagImage: String,
    @SerializedName("delivery")
    val delivery: List<Any>,
    @SerializedName("languageID")
    val languageID: String,
    @SerializedName("settings")
    val settings: List<SettingX>,
    @SerializedName("userCountryCode")
    val userCountryCode: String,
    @SerializedName("userCreatedDate")
    val userCreatedDate: String,
    @SerializedName("userDeviceID")
    val userDeviceID: String,
    @SerializedName("userDeviceType")
    val userDeviceType: String,
    @SerializedName("userEmail")
    val userEmail: String,
    @SerializedName("userFacebookID")
    val userFacebookID: String,
    @SerializedName("userFullName")
    val userFullName: String,
    @SerializedName("userGoogleID")
    val userGoogleID: String,
    @SerializedName("userID")
    val userID: String,
    @SerializedName("userLatitude")
    val userLatitude: Any,
    @SerializedName("userLongitude")
    val userLongitude: Any,
    @SerializedName("userMobile")
    val userMobile: String,
    @SerializedName("userOTP")
    val userOTP: String,
    @SerializedName("userOfferEmailNotification")
    val userOfferEmailNotification: String,
    @SerializedName("userOfferPushNotification")
    val userOfferPushNotification: String,
    @SerializedName("userOrderEmailNotification")
    val userOrderEmailNotification: String,
    @SerializedName("userOrderPushNotification")
    val userOrderPushNotification: String,
    @SerializedName("userPassword")
    val userPassword: String,
    @SerializedName("userProfilePicture")
    val userProfilePicture: String,
    @SerializedName("userReferCode")
    val userReferCode: String,
    @SerializedName("userSingupReferCode")
    val userSingupReferCode: String,
    @SerializedName("userStatus")
    val userStatus: String,
    @SerializedName("userType")
    val userType: String,
    @SerializedName("userVerified")
    val userVerified: String,
    @SerializedName("userWalletRechargeAmount")
    val userWalletRechargeAmount: String,
    @SerializedName("userWalletReferAndEarnAmount")
    val userWalletReferAndEarnAmount: String
):Serializable

data class SettingX(
    @SerializedName("settingsGSTOnFabric")
    val settingsGSTOnFabric: String,
    @SerializedName("settingsGSTOnOtherService")
    val settingsGSTOnOtherService: String,
    @SerializedName("settingsGSTOnReadymadeGarments")
    val settingsGSTOnReadymadeGarments: String,
    @SerializedName("settingsGSTOnStitchingservice")
    val settingsGSTOnStitchingservice: String,
    @SerializedName("settingsMinimumOrderValue")
    val settingsMinimumOrderValue: String,
    @SerializedName("settingsMoneyToBeUsedFromWalletForPayment")
    val settingsMoneyToBeUsedFromWalletForPayment: String,
    @SerializedName("settingsPaymentErrorUrl")
    val settingsPaymentErrorUrl: String,
    @SerializedName("settingsPaymentSuccessUrl")
    val settingsPaymentSuccessUrl: String,
    @SerializedName("settingsPaymentUrl")
    val settingsPaymentUrl: String,
    @SerializedName("settingsSupportEmail")
    val settingsSupportEmail: String,
    @SerializedName("settingsSupportMobile")
    val settingsSupportMobile: String
):Serializable

data class Stitching(
    @SerializedName("categoryID")
    val categoryID: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("categorytypeID")
    val categorytypeID: String,
    @SerializedName("categorytypeName")
    val categorytypeName: String,
    @SerializedName("orderID")
    val orderID: String,
    @SerializedName("orderdetailsstitchingCSGT")
    val orderdetailsstitchingCSGT: String,
    @SerializedName("orderdetailsstitchingDealerIGST")
    val orderdetailsstitchingDealerIGST: String,
    @SerializedName("orderdetailsstitchingDealerPrice")
    val orderdetailsstitchingDealerPrice: String,
    @SerializedName("orderdetailsstitchingDealerSGST")
    val orderdetailsstitchingDealerSGST: String,
    @SerializedName("orderdetailsstitchingDeliveryCharges")
    val orderdetailsstitchingDeliveryCharges: String,
    @SerializedName("orderdetailsstitchingDeliveryType")
    val orderdetailsstitchingDeliveryType: String,
    @SerializedName("orderdetailsstitchingEstimatedDeliveryDate")
    val orderdetailsstitchingEstimatedDeliveryDate: String,
    @SerializedName("orderdetailsstitchingExpectedDeliveryDate")
    val orderdetailsstitchingExpectedDeliveryDate: String,
    @SerializedName("orderdetailsstitchingID")
    val orderdetailsstitchingID: String,
    @SerializedName("orderdetailsstitchingIGST")
    val orderdetailsstitchingIGST: String,
    @SerializedName("orderdetailsstitchingIsRequestedForReturnReplace")
    val orderdetailsstitchingIsRequestedForReturnReplace: String,
    @SerializedName("orderdetailsstitchingPercentageCSGT")
    val orderdetailsstitchingPercentageCSGT: String,
    @SerializedName("orderdetailsstitchingPercentageIGST")
    val orderdetailsstitchingPercentageIGST: String,
    @SerializedName("orderdetailsstitchingPercentageSGST")
    val orderdetailsstitchingPercentageSGST: String,
    @SerializedName("orderdetailsstitchingPrice")
    val orderdetailsstitchingPrice: String,
    @SerializedName("orderdetailsstitchingQty")
    val orderdetailsstitchingQty: String,
    @SerializedName("orderdetailsstitchingSGST")
    val orderdetailsstitchingSGST: String,
    @SerializedName("orderdetailsstitchingStatusDate")
    val orderdetailsstitchingStatusDate: String,
    @SerializedName("orderdetailsstitchingStatusRemark")
    val orderdetailsstitchingStatusRemark: String,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusName")
    val statusName: String,
    @SerializedName("statusNameDisplay")
    val statusNameDisplay: String,
    @SerializedName("stitchingserviceID")
    val stitchingserviceID: String,
    @SerializedName("stitchingserviceImages")
    val stitchingserviceImages: List<StitchingserviceImage>,
    @SerializedName("stitchingserviceName")
    val stitchingserviceName: String,
    @SerializedName("subcatID")
    val subcatID: String,
    @SerializedName("subcatName")
    val subcatName: String ,
    @SerializedName("IsYouRated")
    val IsYouRated: String
):Serializable

data class StitchingserviceImage(
    @SerializedName("stitchingserviceimagesID")
    val stitchingserviceimagesID: String,
    @SerializedName("stitchingserviceimagesName")
    val stitchingserviceimagesName: String
):Serializable



