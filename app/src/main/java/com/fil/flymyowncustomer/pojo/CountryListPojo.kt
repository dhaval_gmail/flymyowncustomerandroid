package com.fil.flymyowncustomer.pojo

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CountryListPojo : Serializable{

    @SerializedName("data")
    @Expose
    val data: List<Data>? = null
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("message")
    @Expose
    val message: String? = null

    class Data : Serializable {

        @SerializedName("countryID")
        @Expose
        var countryID: String? = null
        @SerializedName("countryName")
        @Expose
        var countryName: String? = null
        @SerializedName("countryDialCode")
        @Expose
        var countryDialCode: String? = null
        @SerializedName("countryFlagImage")
        @Expose
        var countryFlagImage: String? = null

    }

}