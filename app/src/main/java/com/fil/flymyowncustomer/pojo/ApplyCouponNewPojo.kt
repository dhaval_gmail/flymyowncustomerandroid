package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ApplyCouponNewPojo(
    @SerializedName("date")
    val data: List<ApplyCouponNewPojoData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) : Serializable

class ApplyCouponNewPojoData(
    @SerializedName("offerDiscountType")
    var offerDiscountType: String?,
    @SerializedName("offerDiscountValue")
    var offerDiscountValue: String?,
    @SerializedName("offerMaximumDiscountPossible")
    var offerMaxDiscountAmount: String?,
    var appliedCouponCode: String?,
    var couponCodeCreatedBy: String?
) : Serializable