package com.fil.flymyowncustomer.pojo

class LocalCart{

    var dealerID = ""
    var cartID = 0
    var productId = ""
    var stitchingId = ""
    var dealerCompanyName = ""
    var productName = ""

    var addToCartDeliveryName = ""
    var addToCartDeliveryCharges = 0.0
    var addToCartExceptedDate = ""
    var cartQuantity = 0
    var cartStock = 0.0
    var orderdetailsPrice = 0.0
    var productPriceId = 0.0



    var addToCartSizeId = ""
    var addToCartColorId = ""
    var addToCartMaterialId = ""
    var addToCartStyleId = ""
    var addToCartBrandId = ""
    var addToCartPatternId = ""
    var addToCartCategoryTypeId = ""
    var addToCartCategoryId = ""
    var addToCartsubcatId = ""

    var productDiscountedPrice = 0.0
    var orderTotal = 0.0
    var categoryGST = 0.0

    var IGSTPercentage = 0.0
    var IGST = 0.0

    var SGSTPercentage = 0.0
    var SGST = 0.0

    var CGSTPercentage = 0.0
    var CGST = 0.0

    var issue = ""
    var issueMsg = ""

    var isDelete  = false
    var isIGST = false

    var appliedDiscount = 0.0
    var appliedCouponCode = ""
    var appliedCouponCodeCreatedBy = ""

    var productData : ArrayList<ProductListPojo.Data>? = null
    var stichingServiceData : ArrayList<StichingServicePojo.StichingServicData>? = null
}