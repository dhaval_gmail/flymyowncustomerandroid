package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class PaymentPojo(
    @SerializedName("data")
    val `data`: List<PaymentData> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
)

data class PaymentData(
    @SerializedName("transactionID")
    val transactionID: String = "",
    @SerializedName("url")
    val url: String = ""
)