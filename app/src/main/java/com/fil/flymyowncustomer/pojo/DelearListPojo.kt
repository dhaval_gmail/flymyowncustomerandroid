package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class DelearListPojo(
    @SerializedName("data")
    var `data`: List<Stitchingstudio?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
)

