package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class UserHomePojo(
    @SerializedName("banner")
    var banner: List<Banner?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("productviews")
    var productviews: List<ProductListPojo.Data?>?,
    @SerializedName("status")
    var status: String?,
    @SerializedName("stitchingstore")
    var stitchingstore: List<Stitchingstudio?>?,
    @SerializedName("stitchingstudio")
    var stitchingstudio: List<Stitchingstudio?>?
):Serializable


data class Banner(
    @SerializedName("bannerID")
    var bannerID: String?,
    @SerializedName("bannerImage")
    var bannerImage: String?,
    @SerializedName("bannerName")
    var bannerName: String?
):Serializable

data class Stitchingstudio(
    @SerializedName("addressAddressLine1")
    var addressAddressLine1: String?,
    @SerializedName("addressAddressLine2")
    var addressAddressLine2: String?,
    @SerializedName("addressCreatedDate")
    var addressCreatedDate: String?,
    @SerializedName("addressGST")
    var addressGST: String?,
    @SerializedName("addressGSTCompany")
    var addressGSTCompany: String?,
    @SerializedName("addressGSTState")
    var addressGSTState: String?,
    @SerializedName("addressGSTStateID")
    var addressGSTStateID: String?,
    @SerializedName("addressID")
    var addressID: String?,
    @SerializedName("addressIsDefault")
    var addressIsDefault: String?,
    @SerializedName("addressLandmark")
    var addressLandmark: String?,
    @SerializedName("addressLatitude")
    var addressLatitude: String?,
    @SerializedName("addressLocality")
    var addressLocality: String?,
    @SerializedName("addressLongitude")
    var addressLongitude: String?,
    @SerializedName("addressPAN")
    var addressPAN: String?,
    @SerializedName("addressPincode")
    var addressPincode: String?,
    @SerializedName("BannerImages")
    var bannerImages: List<BannerImage?>?,
    @SerializedName("countryFlagImage")
    var countryFlagImage: String?,
    @SerializedName("countryID")
    var countryID: String?,
    @SerializedName("countryName")
    var countryName: String?,
    @SerializedName("dealerCompanyName")
    var dealerCompanyName: String?,
    @SerializedName("dealerCountryCode")
    var dealerCountryCode: String?,
    @SerializedName("dealerCreatedDate")
    var dealerCreatedDate: String?,
    @SerializedName("dealerDeliveryCharges")
    var dealerDeliveryCharges: String?,
    @SerializedName("dealerDeviceID")
    var dealerDeviceID: String?,
    @SerializedName("dealerDeviceType")
    var dealerDeviceType: String?,
    @SerializedName("dealerEmail")
    var dealerEmail: String?,
    @SerializedName("dealerFreeServiceIsAvailable")
    var dealerFreeServiceIsAvailable: String?,
    @SerializedName("dealerFullName")
    var dealerFullName: String?,
    @SerializedName("dealerID")
    var dealerID: String?,
    @SerializedName("dealerIsPopular")
    var dealerIsPopular: String?,
    @SerializedName("dealerMobile")
    var dealerMobile: String?,
    @SerializedName("dealerProfilePicture")
    var dealerProfilePicture: String?,
    @SerializedName("dealerRatting")
    var dealerRatting: String?,
    @SerializedName("dealerReviewRattingCount")
    var dealerReviewRattingCount: String?,
    @SerializedName("dealerServiceType")
    var dealerServiceType: String?,
    @SerializedName("dealerType")
    var dealerType: String?,
    @SerializedName("distance")
    var distance: String?,
    @SerializedName("IsFavourite")
    var isFavourite: String?,
    @SerializedName("IsWorking")
    var isWorking: String?,
    @SerializedName("offerDiscountValue")
    var offerDiscountValue: String?,
    @SerializedName("stateID")
    var stateID: String?,
    @SerializedName("dealerratting")
    @Expose
    var dealerratting: List<Dealerratting>? = null,
    @SerializedName("stateName")
    var stateName: String?
):Serializable

data class Stitchingstore(
    @SerializedName("addressAddressLine1")
    var addressAddressLine1: String?,
    @SerializedName("addressAddressLine2")
    var addressAddressLine2: String?,
    @SerializedName("addressCreatedDate")
    var addressCreatedDate: String?,
    @SerializedName("addressGST")
    var addressGST: String?,
    @SerializedName("addressGSTCompany")
    var addressGSTCompany: String?,
    @SerializedName("addressGSTState")
    var addressGSTState: String?,
    @SerializedName("addressGSTStateID")
    var addressGSTStateID: String?,
    @SerializedName("addressID")
    var addressID: String?,
    @SerializedName("addressIsDefault")
    var addressIsDefault: String?,
    @SerializedName("addressLandmark")
    var addressLandmark: String?,
    @SerializedName("addressLatitude")
    var addressLatitude: String?,
    @SerializedName("addressLocality")
    var addressLocality: String?,
    @SerializedName("addressLongitude")
    var addressLongitude: String?,
    @SerializedName("addressPAN")
    var addressPAN: String?,
    @SerializedName("addressPincode")
    var addressPincode: String?,
    @SerializedName("BannerImages")
    var bannerImages: List<BannerImage?>?,
    @SerializedName("countryFlagImage")
    var countryFlagImage: String?,
    @SerializedName("countryID")
    var countryID: String?,
    @SerializedName("countryName")
    var countryName: String?,
    @SerializedName("dealerCompanyName")
    var dealerCompanyName: String?,
    @SerializedName("dealerCountryCode")
    var dealerCountryCode: String?,
    @SerializedName("dealerCreatedDate")
    var dealerCreatedDate: String?,
    @SerializedName("dealerDeliveryCharges")
    var dealerDeliveryCharges: String?,
    @SerializedName("dealerDeviceID")
    var dealerDeviceID: String?,
    @SerializedName("dealerDeviceType")
    var dealerDeviceType: String?,
    @SerializedName("dealerEmail")
    var dealerEmail: String?,
    @SerializedName("dealerFreeServiceIsAvailable")
    var dealerFreeServiceIsAvailable: String?,
    @SerializedName("dealerFullName")
    var dealerFullName: String?,
    @SerializedName("dealerID")
    var dealerID: String?,
    @SerializedName("dealerIsPopular")
    var dealerIsPopular: String?,
    @SerializedName("dealerMobile")
    var dealerMobile: String?,
    @SerializedName("dealerProfilePicture")
    var dealerProfilePicture: String?,
    @SerializedName("dealerRatting")
    var dealerRatting: String?,
    @SerializedName("dealerReviewRattingCount")
    var dealerReviewRattingCount: String?,
    @SerializedName("dealerServiceType")
    var dealerServiceType: String?,
    @SerializedName("dealerType")
    var dealerType: String?,
    @SerializedName("distance")
    var distance: String?,
    @SerializedName("IsFavourite")
    var isFavourite: String?,
    @SerializedName("IsWorking")
    var isWorking: String?,
    @SerializedName("offerDiscountValue")
    var offerDiscountValue: String?,
    @SerializedName("stateID")
    var stateID: String?,
    @SerializedName("dealerratting")
    @Expose
    var dealerratting: List<Dealerratting>? = null,
    @SerializedName("stateName")
    var stateName: String?
):Serializable

data class Dealerratting (

    @SerializedName("Excellent")
    @Expose
    var Excellent: String? = null,
    @SerializedName("Good")
    @Expose
    var Good: String? = null,
    @SerializedName("Average")
    @Expose
    var Average: String? = null,
    @SerializedName("BelowAverage")
    @Expose
    var BelowAverage: String? = null,
    @SerializedName("Poor")
    @Expose
    var Poor: String? = null
): Serializable

