package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class MyOrderPojo(
    @SerializedName("data")
    val `data`: List<MyOrderData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable


data class MyOrderData(
    @SerializedName("orderDate")
    val orderDate: String,
    @SerializedName("orderDealerNetAmount")
    val orderDealerNetAmount: String,
    @SerializedName("orderID")
    val orderID: String,
    @SerializedName("orderNetAmount")
    val orderNetAmount: String,
    @SerializedName("orderNo")
    val orderNo: String,
    @SerializedName("orderStatusRemark")
    val orderStatusRemark: String,
    @SerializedName("orderType")
    val orderType: String,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusName")
    val statusName: String
) : Serializable