package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FiluploadResponse {

    @SerializedName("data")
    @Expose
    var data: List<Data>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    class Data {

        @SerializedName("filename")
        @Expose
        var filename: String? = null
        @SerializedName("uploadstatus")
        @Expose
        var uploadstatus: String? = null

    }
}
