package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class StichingServicePojo : Serializable {

    @SerializedName("data")
    var data: List<StichingServicData?>? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("stitchingservicelistcount")
    var stitchingservicelistcount: String? = null

    class StichingServicData : Serializable {


        var addToCartProductId = ""
        var addToCartSizeId = ""
        var addToCartColorId = ""
        var addToCartMaterialId = ""
        var addToCartStyleId = ""
        var addToCartBrandId = ""
        var addToCartPatternId = ""
        var addToCartDeliveryName = ""
        var addToCartDeliveryCharges = 0.0
        var addToCartDeliveryId = ""
        var addToCartExceptedDate = ""
        var cartQuantity = 0
        var cartStock = 0.0

        var productDiscountedPrice = 0.0
        var orderTotal = 0.0
        var categoryGST = 0.0

        var IGSTPercentage = 0.0
        var IGST = 0.0

        var SGSTPercentage = 0.0
        var SGST = 0.0

        var CGSTPercentage = 0.0
        var CGST = 0.0

        var issue = ""
        var issueMsg = ""

        var isDelete  = false
        var isIGST = false
        var cartId = 0


        @SerializedName("categoryID")
        var categoryID: String?? = null
        @SerializedName("categoryName")
        var categoryName: String?? = null
        @SerializedName("categorytypeID")
        var categorytypeID: String?? = null
        @SerializedName("categorytypeName")
        var categorytypeName: String?? = null
        @SerializedName("dealerID")
        var dealerID: String?? = null
        @SerializedName("IsYourFavourite")
        var isYourFavourite: String?? = null
        @SerializedName("stitchingserviceActualPrice")
        var stitchingserviceActualPrice: String?? = null
        @SerializedName("stitchingserviceCustomerCrossPrice")
        var stitchingserviceCustomerCrossPrice: String?? = null
        @SerializedName("stitchingserviceCustomerPayablePrice")
        var stitchingserviceCustomerPayablePrice: String?? = null
        @SerializedName("stitchingserviceDescription")
        var stitchingserviceDescription: String?? = null
        @SerializedName("stitchingserviceDiscount")
        var stitchingserviceDiscount: String?? = null
        @SerializedName("stitchingserviceDiscountType")
        var stitchingserviceDiscountType: String?? = null
        @SerializedName("stitchingserviceID")
        var stitchingserviceID: String?? = null
        @SerializedName("stitchingserviceImages")
        var stitchingserviceImages: List<StitchingserviceImage?>? = null
        @SerializedName("stitchingserviceIsPopular")
        var stitchingserviceIsPopular: String? = null
        @SerializedName("stitchingserviceName")
        var stitchingserviceName: String? = null
        @SerializedName("stitchingservicePrice")
        var stitchingservicePrice: String? = null
        @SerializedName("stitchingserviceRatting")
        var stitchingserviceRatting: String? = null
        @SerializedName("stitchingserviceReviewRattingCount")
        var stitchingserviceReviewRattingCount: String? = null
        @SerializedName("subcatID")
        var subcatID: String? = null
        @SerializedName("subcatName")
        var subcatName: String? = null

        @SerializedName("dealerServiceType")
        var dealerServiceType: String? = null

        @SerializedName("dealerFreeServiceIsAvailable")
        var dealerFreeServiceIsAvailable: String? = null

        @SerializedName("dealerDeliveryCharges")
        var dealerDeliveryCharges: String? = null

        @SerializedName("dealerCompanyName")
        var dealerCompanyName: String? = null

        @SerializedName("dealerGST")
        var dealerGST: String? = null

        @SerializedName("dealerFullName")
        var dealerFullName: String? = null

        @SerializedName("stitchingratting")
        @Expose
        var stitchingratting: List<Stitchingratting>? = null

        class StitchingserviceImage :  Serializable{
            @SerializedName("stitchingserviceimagesID")
            var stitchingserviceimagesID: String? = null
            @SerializedName("stitchingserviceimagesName")
            var stitchingserviceimagesName: String? = null
        }

        class Stitchingratting : Serializable{

            @SerializedName("Excellent")
            @Expose
            var Excellent: String? = null
            @SerializedName("Good")
            @Expose
            var Good: String? = null
            @SerializedName("Average")
            @Expose
            var Average: String? = null
            @SerializedName("BelowAverage")
            @Expose
            var BelowAverage: String? = null
            @SerializedName("Poor")
            @Expose
            var Poor: String? = null

        }
    }
}