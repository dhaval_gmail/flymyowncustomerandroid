package com.fil.flymyowncustomer.pojo

data class FilterSubItem(
    var title:String="",
    var id:String="",
    var isSelected:Boolean=false
)
