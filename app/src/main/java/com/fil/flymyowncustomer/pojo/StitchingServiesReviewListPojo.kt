package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName


data class StitchingServiesReviewListPojo(
    @SerializedName("data")
    val `data`: List<StitchingServiesData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class StitchingServiesData(
    @SerializedName("stitchingservicereviewDateTime")
    val stitchingservicereviewDateTime: String,
    @SerializedName("stitchingservicereviewID")
    val stitchingservicereviewID: String,
    @SerializedName("stitchingservicereviewRatting")
    val stitchingservicereviewRatting: String,
    @SerializedName("stitchingservicereviewReview")
    val stitchingservicereviewReview: String,
    @SerializedName("userFullName")
    val userFullName: String,
    @SerializedName("userProfilePicture")
    val userProfilePicture: String
)