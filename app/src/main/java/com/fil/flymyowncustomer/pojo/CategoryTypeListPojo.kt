package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CategoryTypeListPojo(
    @SerializedName("data")
    var `data`: ArrayList<CategoryTypeData?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("status")
    var status: String?
):Serializable

data class CategoryTypeData(
    @SerializedName("category")
    var  category: List<Category?>?,
    @SerializedName("categorytypeID")
    var categorytypeID: String?,
    @SerializedName("categorytypeImage")
    var categorytypeImage: String?,
    @SerializedName("categorytypeName")
    var categorytypeName: String?
):Serializable

data class Category(
    @SerializedName("categoryID")
    var categoryID: String?,
    @SerializedName("categoryImage")
    var categoryImage: String?,
    @SerializedName("categoryName")
    var categoryName: String?,
    @SerializedName("subcategory")
    var subcategory: List<Subcategory?>?,
    var isExpand:Boolean=false
):Serializable

data class Subcategory(
    @SerializedName("subcatID")
    var subcatID: String?,
    @SerializedName("subcatName")
    var subcatName: String?
):Serializable