package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class PlaceOrderPojo(
    @SerializedName("data")
    val `data`: List<PlaceOrderData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable

data class PlaceOrderData(
    @SerializedName("orderNo")
    val orderNo: String
):Serializable