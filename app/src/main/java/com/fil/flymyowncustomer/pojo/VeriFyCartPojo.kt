package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VeriFyCartPojo : java.io.Serializable {

    @SerializedName("message")
    val message: String? = ""
    @SerializedName("orderdetails")
    val product: List<Product>? = null
    @SerializedName("stitching")
    val service: List<Product>? = null
    @SerializedName("status")
    val status: String? = ""

    class Product : Serializable {
        @SerializedName("itemissue")
        val itemissue: String? = ""
        @SerializedName("orderdetailsPrice")
        val orderdetailsPrice: String? = ""
        @SerializedName("productGST")
        val productGST: String? = ""
        @SerializedName("productID")
        val productID: String? = ""
        @SerializedName("productStock")
        val productStock: String? = ""
    }

    class Service : Serializable {
        @SerializedName("itemissue")
        val itemissue: String? = ""
        @SerializedName("orderdetailsPrice")
        val orderdetailsPrice: String? = ""
        @SerializedName("productGST")
        val productGST: String? = ""
        @SerializedName("productID")
        val productID: String? = ""
        @SerializedName("productStock")
        val productStock: String? = ""
    }
}