package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class MasterPojo(
    @SerializedName("brand")
    val brand: List<Brand>,
    @SerializedName("colors")
    val colors: List<Color>,
    @SerializedName("material")
    val material: List<Material>,
    @SerializedName("message")
    val message: String,
    @SerializedName("pattern")
    val pattern: List<Pattern>,
    @SerializedName("paymentmethod")
    val paymentmethod: ArrayList<Paymentmethod>,
    @SerializedName("reason")
    val reason: List<Reason>,
    @SerializedName("status")
    val status: String,
    @SerializedName("style")
    val style: List<Style>
):Serializable

data class Brand(
    @SerializedName("brandID")
    val brandID: String,
    @SerializedName("brandImage")
    val brandImage: String,
    @SerializedName("brandName")
    val brandName: String
):Serializable

data class Color(
    @SerializedName("colorID")
    val colorID: String,
    @SerializedName("colorImage")
    val colorImage: String,
    @SerializedName("colorName")
    val colorName: String
):Serializable

data class Material(
    @SerializedName("materialID")
    val materialID: String,
    @SerializedName("materialName")
    val materialName: String
):Serializable

data class Pattern(
    @SerializedName("patternID")
    val patternID: String,
    @SerializedName("patternName")
    val patternName: String
):Serializable

data class Paymentmethod(
    @SerializedName("paymentmethodID")
    val paymentmethodID: String,
    @SerializedName("paymentmethodImage")
    val paymentmethodImage: String,
    @SerializedName("paymentmethodKeyword")
    val paymentmethodKeyword: String,
    @SerializedName("paymentmethodText")
    val paymentmethodText: String
):Serializable

data class Reason(
    @SerializedName("reasonID")
    val reasonID: String,
    @SerializedName("reasonInfo")
    val reasonInfo: String
):Serializable

data class Style(
    @SerializedName("styleID")
    val styleID: String,
    @SerializedName("styleName")
    val styleName: String
):Serializable