package com.fil.flymyowncustomer.pojo

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserForgotPasswordPojo : Serializable {
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    class Datum : Serializable {

        /*[
        {
            "data": [
            {
                "userID": "64",
                "userCountryCode": "91",
                "userMobile": "9979517447"
            }
            ],
            "status": "true",
            "message": "We have sent otp on your mobile."
        }
        ]*/
        @SerializedName("userID")
        @Expose
        var userID: String? = null

        @SerializedName("userMobile")
        @Expose
        var userMobile: String? = ""

        @SerializedName("userCountryCode")
        @Expose
        var userCountryCode: String? = ""

        @SerializedName("dealerEmail")
        @Expose
        var dealerEmail: String? = ""

    }
}
