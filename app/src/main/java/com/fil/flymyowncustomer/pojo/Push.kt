package com.fil.flymyowncustomer.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Push : Serializable {
    companion object {
        val action: String = "Push"
    }

    @SerializedName("RefKey")
    @Expose
    var refKey: String? = null
    @SerializedName("priority")
    @Expose
    var priority: String? = null
    @SerializedName("msgKey")
    @Expose
    var msgKey: String? = null
    @SerializedName("msg")
    @Expose
    var msg: String? = null
    @SerializedName("body")
    @Expose
    var body: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("msgType")
    @Expose
    var msgType: String? = null

}
