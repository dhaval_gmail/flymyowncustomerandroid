package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductListPojo : Serializable{

    @SerializedName("data")
    @Expose
    var data: ArrayList<Data>? = null
    @SerializedName("productlistcount")
    @Expose
    var productlistcount: String? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null


    class Data : Serializable{


        var addToCartProductId = ""
        var addToCartSizeId = ""
        var addToCartColorId = ""
        var addToCartMaterialId = ""
        var addToCartStyleId = ""
        var addToCartBrandId = ""
        var addToCartPatternId = ""
        var addToCartDeliveryName = ""
        var addToCartDeliveryCharges = 0.0
        var addToCartDeliveryId = ""
        var addToCartExceptedDate = ""
        var cartQuantity = 0
        var cartStock = 0.0

        var productDiscountedPrice = 0.0
        var orderTotal = 0.0
        var categoryGST = 0.0
        var productPriceId = 0.0

        var IGSTPercentage = 0.0
        var IGST = 0.0

        var SGSTPercentage = 0.0
        var SGST = 0.0

        var CGSTPercentage = 0.0
        var CGST = 0.0

        var issue = ""
        var issueMsg = ""

        var isDelete  = false
        var isIGST = false

        var cartId = 0

        @SerializedName("IsYourFavourite")
        @Expose
        var isYourFavourite: String? = null

        @SerializedName("dealerCompanyName")
        @Expose
        var dealerCompanyName: String? = null

        @SerializedName("dealerGST")
        @Expose
        var dealerGST: String? = null

        @SerializedName("productID")
        @Expose
        var productID: String? = null
        @SerializedName("dealerID")
        @Expose
        var dealerID: String? = null
        @SerializedName("categorytypeID")
        @Expose
        var categorytypeID: String? = null
        @SerializedName("categoryID")
        @Expose
        var categoryID: String? = null
        @SerializedName("subcatID")
        @Expose
        var subcatID: String? = null
        @SerializedName("productUOM")
        @Expose
        var productUOM: String? = null
        @SerializedName("productName")
        @Expose
        var productName: String? = null
        @SerializedName("productDescription")
        @Expose
        var productDescription: String? = null
        @SerializedName("brandIDs")
        @Expose
        var brandIDs: String? = null
        @SerializedName("materialIDs")
        @Expose
        var materialIDs: String? = null
        @SerializedName("colorIDs")
        @Expose
        var colorIDs: String? = null
        @SerializedName("patternIDs")
        @Expose
        var patternIDs: String? = null
        @SerializedName("styleIDs")
        @Expose
        var styleIDs: String? = null
        @SerializedName("productStock")
        @Expose
        var productStock: String? = null
        @SerializedName("productReviewRattingCount")
        @Expose
        var productReviewRattingCount: String? = null
        @SerializedName("productRatting")
        @Expose
        var productRatting: String? = null
        @SerializedName("productStatus")
        @Expose
        var productStatus: String? = null
        @SerializedName("productCreatedDate")
        @Expose
        var productCreatedDate: String? = null
        @SerializedName("productSoldQty")
        @Expose
        var productSoldQty: String? = null
        @SerializedName("productType")
        @Expose
        var productType: String? = null
        @SerializedName("productIsPopular")
        @Expose
        var productIsPopular: String? = null
        @SerializedName("dealerFullName")
        @Expose
        var dealerFullName: String? = null
        @SerializedName("dealerServiceType")
        @Expose
        var dealerServiceType: String? = null
        @SerializedName("dealerFreeServiceIsAvailable")
        @Expose
        var dealerFreeServiceIsAvailable: String? = null
        @SerializedName("dealerDeliveryCharges")
        @Expose
        var dealerDeliveryCharges: String? = null
        @SerializedName("categorytypeName")
        @Expose
        var categorytypeName: String? = null
        @SerializedName("categoryAdminMarginType")
        @Expose
        var categoryAdminMarginType: String? = null
        @SerializedName("categoryAdminMargin")
        @Expose
        var categoryAdminMargin: String? = null
        @SerializedName("categoryName")
        @Expose
        var categoryName: String? = null
        @SerializedName("subcatName")
        @Expose
        var subcatName: String? = null
        @SerializedName("productimages")
        @Expose
        var productimages: ArrayList<Productimage>? = null
        @SerializedName("productprice")
        @Expose
        var productprice: List<Productprice>? = null
        @SerializedName("colors")
        @Expose
        var colors: List<Color>? = null
        @SerializedName("pattern")
        @Expose
        var pattern: List<Pattern>? = null
        @SerializedName("brand")
        @Expose
        var brand: List<Brand>? = null
        @SerializedName("style")
        @Expose
        var style: List<Style>? = null
        @SerializedName("productratting")
        @Expose
        var productratting: List<Productratting>? = null
        @SerializedName("material")
        @Expose
        var material: List<Material>? = null


        class Brand : Serializable{

            @SerializedName("brandID")
            @Expose
            var brandID: String? = null
            @SerializedName("productbrandStock")
            @Expose
            var productbrandStock: String? = null
            @SerializedName("brandName")
            @Expose
            var brandName: String? = null
            @SerializedName("brandImage")
            @Expose
            var brandImage: String? = null

        }

        class Color : Serializable{

            @SerializedName("colorID")
            @Expose
            var colorID: String? = null
            @SerializedName("productcolorStock")
            @Expose
            var productcolorStock: String? = null
            @SerializedName("colorName")
            @Expose
            var colorName: String? = null
            @SerializedName("colorImage")
            @Expose
            var colorImage: String? = null

        }

        class Material : Serializable{

            @SerializedName("materialID")
            @Expose
            var materialID: String? = null
            @SerializedName("prodmaterialStock")
            @Expose
            var prodmaterialStock: String? = null
            @SerializedName("materialName")
            @Expose
            var materialName: String? = null

        }

        class Pattern : Serializable{

            @SerializedName("patternID")
            @Expose
            var patternID: String? = null
            @SerializedName("productpatternStock")
            @Expose
            var productpatternStock: String? = null
            @SerializedName("patternName")
            @Expose
            var patternName: String? = null

        }
        class Productratting : Serializable{

            @SerializedName("Excellent")
            @Expose
            var Excellent: String? = null
            @SerializedName("Good")
            @Expose
            var Good: String? = null
            @SerializedName("Average")
            @Expose
            var Average: String? = null
            @SerializedName("BelowAverage")
            @Expose
            var BelowAverage: String? = null
            @SerializedName("Poor")
            @Expose
            var Poor: String? = null

        }

        class Productimage : Serializable{

            @SerializedName("prodimgID")
            @Expose
            var prodimgID: String? = null
            @SerializedName("prodimgName")
            @Expose
            var prodimgName: String? = null

        }

        class Productprice : Serializable{

            @SerializedName("productpriceID")
            @Expose
            var productpriceID: String? = null
            @SerializedName("productID")
            @Expose
            var productID: String? = null
            @SerializedName("standardmeasurementID")
            @Expose
            var standardmeasurementID: String? = null
            @SerializedName("productpricePrice")
            @Expose
            var productpricePrice: String? = null
            @SerializedName("productpriceDiscount")
            @Expose
            var productpriceDiscount: String? = null
            @SerializedName("productpriceDiscountType")
            @Expose
            var productpriceDiscountType: String? = null
            @SerializedName("productpriceActualPrice")
            @Expose
            var productpriceActualPrice: String? = null
            @SerializedName("productpriceCustomerPayablePrice")
            @Expose
            var productpriceCustomerPayablePrice: String? = null
            @SerializedName("productpriceCustomerCrossPrice")
            @Expose
            var productpriceCustomerCrossPrice: String? = null
            @SerializedName("standardmeasurementName")
            @Expose
            var standardmeasurementName: String? = null
            @SerializedName("standardmeasurementSize")
            @Expose
            var standardmeasurementSize: String? = null

        }

        class Style : Serializable{

            @SerializedName("styleID")
            @Expose
            var styleID: String? = null
            @SerializedName("productstyleStock")
            @Expose
            var productstyleStock: String? = null
            @SerializedName("styleName")
            @Expose
            var styleName: String? = null

        }

    }


}
/*
data class ProductListPojo(
    @SerializedName("data")
    var `data`: ArrayList<ProductListData?>?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("productlistcount")
    var productlistcount: String?,
    @SerializedName("status")
    var status: String?
):Serializable

data class ProductListData(
    @SerializedName("brand")
    var brand: List<Brand?>?,
    @SerializedName("brandIDs")
    var brandIDs: String?,
    @SerializedName("categoryAdminMargin")
    var categoryAdminMargin: String?,
    @SerializedName("categoryAdminMarginType")
    var categoryAdminMarginType: String?,
    @SerializedName("categoryID")
    var categoryID: String?,
    @SerializedName("categoryName")
    var categoryName: String?,
    @SerializedName("categorytypeID")
    var categorytypeID: String?,
    @SerializedName("categorytypeName")
    var categorytypeName: String?,
    @SerializedName("colorIDs")
    var colorIDs: String?,
    @SerializedName("colors")
    var colors: List<Color?>?,
    @SerializedName("dealerDeliveryCharges")
    var dealerDeliveryCharges: String?,
    @SerializedName("dealerFreeServiceIsAvailable")
    var dealerFreeServiceIsAvailable: String?,
    @SerializedName("dealerFullName")
    var dealerFullName: String?,
    @SerializedName("dealerID")
    var dealerID: String?,
    @SerializedName("dealerServiceType")
    var dealerServiceType: String?,
    @SerializedName("IsYourFavourite")
    var isYourFavourite: String?,
    @SerializedName("material")
    var material: List<Material?>?,
    @SerializedName("materialIDs")
    var materialIDs: String?,
    @SerializedName("pattern")
    var pattern: List<Pattern?>?,
    @SerializedName("patternIDs")
    var patternIDs: String?,
    @SerializedName("productCreatedDate")
    var productCreatedDate: String?,
    @SerializedName("productDescription")
    var productDescription: String?,
    @SerializedName("productID")
    var productID: String?,
    @SerializedName("productIsPopular")
    var productIsPopular: String?,
    @SerializedName("productName")
    var productName: String?,
    @SerializedName("productRatting")
    var productRatting: String?,
    @SerializedName("productReviewRattingCount")
    var productReviewRattingCount: String?,
    @SerializedName("productSoldQty")
    var productSoldQty: String?,
    @SerializedName("productStatus")
    var productStatus: String?,
    @SerializedName("productStock")
    var productStock: String?,
    @SerializedName("productType")
    var productType: String?,
    @SerializedName("productUOM")
    var productUOM: String?,
    @SerializedName("productimages")
    var productimages: List<Productimage?>?,
    @SerializedName("productprice")
    var productprice: List<Productprice?>?,
    @SerializedName("style")
    var style: List<Style?>?,
    @SerializedName("styleIDs")
    var styleIDs: String?,
    @SerializedName("subcatID")
    var subcatID: String?,
    @SerializedName("subcatName")
    var subcatName: String?
):Serializable

data class Productimage(
    @SerializedName("prodimgID")
    var prodimgID: String?,
    @SerializedName("prodimgName")
    var prodimgName: String?
):Serializable

data class Productprice(
    @SerializedName("productID")
    var productID: String?,
    @SerializedName("productpriceActualPrice")
    var productpriceActualPrice: String?,
    @SerializedName("productpriceCustomerCrossPrice")
    var productpriceCustomerCrossPrice: String?,
    @SerializedName("productpriceCustomerPayablePrice")
    var productpriceCustomerPayablePrice: String?,
    @SerializedName("productpriceDiscount")
    var productpriceDiscount: String?,
    @SerializedName("productpriceDiscountType")
    var productpriceDiscountType: String?,
    @SerializedName("productpriceID")
    var productpriceID: String?,
    @SerializedName("productpricePrice")
    var productpricePrice: String?,
    @SerializedName("standardmeasurementID")
    var standardmeasurementID: String?,
    @SerializedName("standardmeasurementName")
    var standardmeasurementName: String?,
    @SerializedName("standardmeasurementSize")
    var standardmeasurementSize: String?
):Serializable
*/
