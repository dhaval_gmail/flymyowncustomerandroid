package com.fil.flymyowncustomer.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Wallethistory(
    @SerializedName("data")
    val `data`: List<WallethistoryData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("userWalletRechargeAmount")
    val userWalletRechargeAmount: String,
    @SerializedName("userWalletReferAndEarnAmount")
    val userWalletReferAndEarnAmount: String
):Serializable

data class WallethistoryData(
    @SerializedName("userID")
    val userID: String,
    @SerializedName("wallettransactonActivity")
    val wallettransactonActivity: String,
    @SerializedName("wallettransactonAmount")
    val wallettransactonAmount: String,
    @SerializedName("wallettransactonAmountExpiredDate")
    val wallettransactonAmountExpiredDate: Any,
    @SerializedName("wallettransactonAvailableAmount")
    val wallettransactonAvailableAmount: String,
    @SerializedName("wallettransactonDate")
    val wallettransactonDate: String,
    @SerializedName("wallettransactonDesc")
    val wallettransactonDesc: String,
    @SerializedName("wallettransactonID")
    val wallettransactonID: String,
    @SerializedName("wallettransactonMode")
    val wallettransactonMode: String,
    @SerializedName("wallettransactonReferenceID")
    val wallettransactonReferenceID: String,
    @SerializedName("wallettransactonStatus")
    val wallettransactonStatus: String,
    @SerializedName("wallettransactonType")
    val wallettransactonType: String
):Serializable