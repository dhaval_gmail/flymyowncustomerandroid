package com.fil.flymyowncustomer.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.fragment.ColorsFilterFragment
import com.fil.flymyowncustomer.fragment.PriceFilterFragment
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.fragment_my_order.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import java.io.Serializable
import java.util.ArrayList

class FilterActivity : AppCompatActivity(),ColorsFilterFragment.OnSelectedDataPass,PriceFilterFragment.OnMRangeValuePass
{

    override fun onMRangeValuePass(maxValue: String, minValue: String) {
        minValue1=minValue
        maxValue1=maxValue
    }
    override fun onSelectedDataPass(
        colors: String,
        materialsIDs: String,
        patternsIDs: String,
        stylesIDs: String,
        brandsIDs: String

    )
    {
        colorsIDs1=colors
        materialsIDs1=materialsIDs
        patternsIDs1=patternsIDs
        stylesIDs1=stylesIDs
        brandsIDs1=brandsIDs

    }

    var adapter: ViewPagerAdapter? = null
    var tabposition = 0
    var masterData: ArrayList<MasterPojo?>?=null

    var colorsIDs1:String=""
    var materialsIDs1:String=""
    var patternsIDs1:String=""
    var stylesIDs1:String=""
    var brandsIDs1:String=""

    var maxValue1:String=""
    var minValue1:String=""
    var from:String=""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        toolbar_back.background=resources.getDrawable(R.color.colorPrimary)
        toolbar_back.navigationIcon = null
        toolbar_back.tvToolbarTitel.text="Filter By"
        toolbar_back.tvToolbarTitel.setTextColor(resources.getColor(R.color.white))
         if(intent!=null)
         {
             if(intent.hasExtra("masterList")) {
                 masterData= intent.getSerializableExtra("masterList") as ArrayList<MasterPojo?>?

             }
             if(intent.hasExtra("colorsIDs2")) {
                 colorsIDs1 = intent.getStringExtra("colorsIDs2")
             }
             if(intent.hasExtra("materialsIDs2")) {
                 materialsIDs1 = intent.getStringExtra("materialsIDs2")
             }
             if(intent.hasExtra("patternsIDs2")) {
                 patternsIDs1 = intent.getStringExtra("patternsIDs2")
             }
             if(intent.hasExtra("stylesIDs2")) {
                 stylesIDs1 = intent.getStringExtra("stylesIDs2")
             }
             if(intent.hasExtra("brandsIDs2")) {
                 brandsIDs1 = intent.getStringExtra("brandsIDs2")
             }
             if(intent.hasExtra("maxValue2")) {
                 maxValue1 = intent.getStringExtra("maxValue2")
             }
             if(intent.hasExtra("maxValue2"))
             {
                 minValue1=intent.getStringExtra("minValue2")

             }
             if(intent.hasExtra("from")) {
                 from = intent.getStringExtra("from")
             }

         }
        setupViewPager(filterViewPager!!)

        filterTablayout!!.setupWithViewPager(filterViewPager)
        filterViewPager.offscreenPageLimit = 0

        filter_cancel.setOnClickListener {
            colorsIDs1=""
            materialsIDs1=""
            patternsIDs1=""
            stylesIDs1=""
            brandsIDs1=""
            maxValue1=""
            minValue1=""
            Intent().apply {
                putExtra("colorsIDs1",colorsIDs1)
                putExtra("materialsIDs1",materialsIDs1)
                putExtra("patternsIDs1",patternsIDs1)
                putExtra("stylesIDs1",stylesIDs1)
                putExtra("brandsIDs1",brandsIDs1)
                putExtra("maxValue1",maxValue1)
                putExtra("minValue1",minValue1)
                setResult(105,this)

            }
            Handler().postDelayed({

                onBackPressed()
            }, 1000)

        }
        filter_reset.setOnClickListener {
            colorsIDs1=""
            materialsIDs1=""
            patternsIDs1=""
            stylesIDs1=""
            brandsIDs1=""
            maxValue1=""
            minValue1=""
           setupViewPager(filterViewPager)
        }

        filterApply.setOnClickListener {
            Intent().apply {
                putExtra("colorsIDs1",colorsIDs1)
                putExtra("materialsIDs1",materialsIDs1)
                putExtra("patternsIDs1",patternsIDs1)
                putExtra("stylesIDs1",stylesIDs1)
                putExtra("brandsIDs1",brandsIDs1)
                putExtra("maxValue1",maxValue1)
                putExtra("minValue1",minValue1)
                setResult(105,this)

            }
            Handler().postDelayed({

                onBackPressed()
            }, 1000)

        }

    }

    private fun setupViewPager(viewPager: ViewPager) {
      //  if (adapter == null) {
            adapter = ViewPagerAdapter(supportFragmentManager)
            if (from.equals("StitchingService", false)) {
                adapter?.addFragment(PriceFilterFragment(), "Price")
                filterTablayout.visibility = View.GONE
            } else {
                filterTablayout.visibility = View.VISIBLE

                adapter?.addFragment(PriceFilterFragment(), "Price")
                adapter?.addFragment(ColorsFilterFragment(), "Colors")
                adapter?.addFragment(ColorsFilterFragment(), "Material")
                adapter?.addFragment(ColorsFilterFragment(), "Pattern")
                adapter?.addFragment(ColorsFilterFragment(), "style")
                adapter?.addFragment(ColorsFilterFragment(), "Brand")
            }


            viewPager.adapter = adapter
            viewPager.currentItem = tabposition
      //  }
    }

    inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
//            val bundle = Bundle()
//            bundle.putInt("position", position)
//            val fragment = mFragmentList[position]
//            fragment.arguments = bundle


            val bundle = Bundle()
            bundle.putInt("position", position)
            bundle.putString("colorsIDs",colorsIDs1)
            bundle.putString("materialsIDs",materialsIDs1)
            bundle.putString("patternsIDs",patternsIDs1)
            bundle.putString("stylesIDs",stylesIDs1)
            bundle.putString("brandsIDs",brandsIDs1)
            bundle.putString("maxValue",maxValue1)
            bundle.putString("minValue",minValue1)

            if(!masterData.isNullOrEmpty())
            {
                bundle.putSerializable("masterList",masterData as Serializable)

            }

             /*when(position)
             {
                 1->{
                     bundle.putString("type", "Colors")

                     if(!masterData!![0]!!.colors.isNullOrEmpty())
                     {
                         bundle.putSerializable("Colors",masterData!![0]!!.colors as Serializable)

                     }

                 }
                 2->{
                     bundle.putString("type", "Material")
                     if(!masterData.isNullOrEmpty())
                     {
                         bundle.putSerializable("masterList",masterData as Serializable)

                     }
                     if(!masterData!![0]!!.material.isNullOrEmpty())
                     {
                         bundle.putSerializable("Material",masterData!![0]!!.material as Serializable)

                     }
                     bundle.putString("type", "Colors")
                 }
                 3->{
                     bundle.putString("type", "Pattern")
                     if(!masterData.isNullOrEmpty())
                     {
                         bundle.putSerializable("masterList",masterData as Serializable)

                     }
                     if(!masterData!![0]!!.pattern.isNullOrEmpty())
                     {
                         bundle.putSerializable("Pattern",masterData!![0]!!.pattern as Serializable)

                     }
                 }
                 4->{
                     bundle.putString("type", "style")
                     if(!masterData.isNullOrEmpty())
                     {
                         bundle.putSerializable("masterList",masterData as Serializable)

                     }
                     if(!masterData!![0]!!.style.isNullOrEmpty())
                     {
                         bundle.putSerializable("style",masterData!![0]!!.style as Serializable)

                     }
                 }
                 5->{
                     bundle.putString("type", "Brand")
                     if(!masterData.isNullOrEmpty())
                     {
                         bundle.putSerializable("masterList",masterData as Serializable)

                     }
                    if(!masterData!![0]!!.brand.isNullOrEmpty())
                    {
                        bundle.putSerializable("Brand",masterData!![0]!!.brand as Serializable)

                    }
                 }
             }*/
            val fragment = mFragmentList[position]
            fragment.arguments = bundle

            return fragment
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun saveState(): Parcelable? {
            return null
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }

    }


    override fun onBackPressed() {
        MyUtils.finishActivity(this@FilterActivity,true)
    }

}
