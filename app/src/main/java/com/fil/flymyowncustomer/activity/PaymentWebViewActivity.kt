package com.fil.flymyowncustomer.activity

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.R2K2.driver.model.AddMoneyModel
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.payment_webview_activity.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class PaymentWebViewActivity : AppCompatActivity() {
    lateinit var sBundle: Bundle
    private var TAG = PaymentWebViewActivity::class.java.name
    val webViewClient = WebClientClass()
    var totalAmount: String = ""
    var from = ""
    var amount = ""
    var transactionID: String = ""
    var url: String = ""
    var transactionPAYMENTMODE: String = ""
     var sessionManager: SessionManager?=null

    /* companion object {
         fun startActivity(sContext: Context, sBundle: Bundle) {
             val sIntent = Intent(sContext, PaymentActivity::class.java)
             sIntent.putExtra("paytm_bundle", sBundle)
             sContext.startActivity(sIntent)
         }

         fun startActivity(sContext: Context) {
             val sIntent = Intent(sContext, PaymentActivity::class.java)
             sContext.startActivity(sIntent)
         }
     }
 */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_webview_activity)

        sessionManager = SessionManager(this)

        if (intent != null) {
            if (intent.hasExtra("transactionID")) {
                transactionID = intent.getStringExtra("transactionID")
            }
            if (intent.hasExtra("url")) {
                url = intent.getStringExtra("url")
            }
            if (intent.hasExtra("transactionPAYMENTMODE")) {
                transactionPAYMENTMODE = intent.getStringExtra("transactionPAYMENTMODE")
            }

            if (intent.hasExtra("from")) {
                from = intent.extras?.getString("from", "")!!
                amount = intent.extras?.getString("amount", "0.00")!!

            }
        }

        webViewPayment.settings.javaScriptEnabled = true
        webViewPayment.addJavascriptInterface(WebAppInterface(this@PaymentWebViewActivity), "Android")

        webViewPayment.isVerticalScrollBarEnabled = false
        webViewPayment.isHorizontalScrollBarEnabled = false
        if (Build.VERSION.SDK_INT >= 21) {
            webViewPayment.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        webViewPayment.settings.pluginState = WebSettings.PluginState.ON
        webViewPayment.webViewClient = WebViewClient()
        webViewPayment.loadUrl(url)
    }


    inner class WebAppInterface(private val mContext: Context) {

        @JavascriptInterface
        fun successPayment(orderId: String) {

            if (from.equals("add_amount", false)) {
                addMoneyApiCall(transactionID)
            } else {
                var intent = Intent()
                intent.putExtra("type", "successPayment")
                intent.putExtra("transactionID", "transactionID")
                intent.putExtra("transactionPAYMENTMODE", transactionPAYMENTMODE)
                setResult(Activity.RESULT_OK, intent)
                MyUtils.finishActivity(this@PaymentWebViewActivity, true)

            }

        }

        @JavascriptInterface
        fun failPayment(message: String) {

            MyUtils.showMessageOK(this@PaymentWebViewActivity, message, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    var intent = Intent()
                    intent.putExtra("type", "failPayment")
                    intent.putExtra("failMessage", message)
                    setResult(Activity.RESULT_OK, intent)
                    MyUtils.finishActivity(this@PaymentWebViewActivity, true)
                }

            })
        }



    }


    inner class WebClientClass : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            Log.e("out","S")
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            Log.e("out","S")
            when {
                url.contains("success.php") -> {
                    Log.e("out","S")
                }
                url.contains("error.php") -> {
                    Log.e("out","1")

                }
            }
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            var message = "SSL Certificate error."


            when (error.primaryError) {
                SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                SslError.SSL_EXPIRED -> message = "The certificate has expired."
                SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
            }
            message += " Do you want to continue anyway?"

            val builder = MaterialAlertDialogBuilder(this@PaymentWebViewActivity)
            builder.setTitle("SSL Certificate Error")
            builder.setMessage(message)
            builder.setCancelable(false)
            builder.setPositiveButton("Yes") { dialog, which ->

                handler.proceed()
                dialog.dismiss()
            }

            builder.setNegativeButton("No") { dialog, which ->
                handler.cancel()
                dialog.dismiss()
                var intent = Intent()
                intent.putExtra("type", "failPayment")
                intent.putExtra("failMessage", message)
                setResult(Activity.RESULT_OK, intent)
                MyUtils.finishActivity(this@PaymentWebViewActivity, true)

            }
            val alert = builder.create()
            alert.setCanceledOnTouchOutside(false)
            alert.show()

        }
    }


    override fun onBackPressed() {
        MyUtils.showMessageOK(
            this@PaymentWebViewActivity,
            "Are you sure you want to exit?",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    var intent = Intent()
                    intent.putExtra("type", "failPayment")
                    intent.putExtra("failMessage", "Payment unsuccessful")
                    setResult(Activity.RESULT_OK, intent)
                    MyUtils.finishActivity(this@PaymentWebViewActivity, true)
                }

            })
    }

   private fun addMoneyApiCall(transactionID: String) {

       runOnUiThread {
           MyUtils.showProgressDialog(this@PaymentWebViewActivity)

           val jsonArray = JSONArray()
           val jsonObject = JSONObject()
           try {
               jsonObject.put("loginuserID", sessionManager?.get_Authenticate_User()?.userID)
               jsonObject.put("transactionID", transactionID)
               jsonObject.put("languageID", "1")
               jsonObject.put("apiType", RestClient.apiType)
               jsonObject.put("apiVersion", RestClient.apiVersion)

           } catch (e: JSONException) {
               e.printStackTrace()
           }
           jsonArray.put(jsonObject)


           val addMoney = ViewModelProviders.of(this).get(AddMoneyModel::class.java)
           addMoney.addMoney(this, jsonArray.toString())
               .observe(this,
                   Observer<List<CommonPojo>> { addMoneyList ->


                       if (addMoneyList != null && addMoneyList.size > 0) {
                           MyUtils.closeProgress()
                           if (addMoneyList[0].status.equals("true", true)) {
                               var intent = Intent()
                               intent.putExtra("type", "add_amount")
                               intent.putExtra("transactionID", "transactionID")
                               intent.putExtra("transactionPAYMENTMODE", transactionPAYMENTMODE)
                               setResult(Activity.RESULT_OK, intent)
                               MyUtils.finishActivity(this@PaymentWebViewActivity, true)
                           } else {
                               MyUtils.showSnackbar(
                                   this,
                                   addMoneyList[0].message!!,
                                   linear_payment
                               )
                           }

                       } else {

                           MyUtils.closeProgress()
                           try {
                               if (!MyUtils.isInternetAvailable(this)) {
                                   MyUtils.showSnackbar(
                                       this,
                                       resources.getString(R.string.error_crash_error_message),
                                       linear_payment
                                   )

                               } else {

                                   MyUtils.showSnackbar(
                                       this,
                                       resources.getString(R.string.error_common_network),
                                       linear_payment
                                   )
                               }
                           } catch (e: Exception) {
                               e.printStackTrace()
                           }
                       }
                   })
       }

   }


}