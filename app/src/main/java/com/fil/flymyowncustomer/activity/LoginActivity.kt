package com.fil.flymyowncustomer.activity

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.model.userSocialLoginModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.ModelLoginUser
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PrefDb
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException

import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.button.MaterialButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.header_layout.*
import org.json.JSONArray
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
// https://stackoverflow.com/questions/56108316/circular-reference-error-in-firebase-with-r8/56111506
// https://stackoverflow.com/questions/57630387/after-update-to-android-studio-3-5-getting-the-following-error-failure-build-f
// https://issuetracker.google.com/issues/132575649#comment8
// https://stackoverflow.com/questions/57625092/multiple-entries-with-same-key-firebaseinstanceid
// https://developers.google.com/identity/sign-in/android/start-integrating?authuser=2

// https://developers.google.com/identity/sign-in/android/start-integrating
//

//642932076341-gs3bd2e5qqac5r0un937e58o4itjt5mn.apps.googleusercontent.com

// https://www.androhub.com/take-a-screenshot-programmatically-in-android/

//  AIzaSyC6q_dBYhBWfBiqLMLt0RgmR7lBqJkngfE

// live sha1 google plus OAuth2 key
// SHA1 : FD:C5:6B:CD:54:91:D8:E9:9A:17:02:F1:F7:EE:0A:B1:D9:11:C0:DF
// key :  642932076341-cm3rn2el26r3h9s7pq3crpdf93u57ijq.apps.googleusercontent.com

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    var sessionManager : SessionManager? = null
    val keyUserId = "keyUserId"
    val keyUserMobile = "keyUserMobile"
    val keyUserMobileCountryCode = "keyUserMobileCountryCode"
    val keyUserEmail = "keyUserEmail"
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var callbackManager: CallbackManager
    var user_Name = ""
    var user_Email = ""
    var userGoogleID = ""

    var buttonFacebookLogin: LoginButton? = null
    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]
    companion object {
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        setContentView(R.layout.activity_login)
        buttonFacebookLogin = findViewById(R.id.buttonFacebookLoginScreen)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(this@LoginActivity.resources.getString(R.string.server_client_id))
            .requestEmail()
            .build()

//            .requestIdToken(getString(R.string.default_web_client_id))

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        tvHeaderText?.text = ""
        sessionManager = SessionManager(this@LoginActivity)
        imgCloseIcon?.visibility=View.VISIBLE

        imgCloseIcon?.setOnClickListener {
            onBackPressed()
        }
        //customTextView(tvSignup)
        tvForgotText?.setOnClickListener(this@LoginActivity)
//        btnLogin?.setOnClickListener(this@LoginActivity)
        ll_signup?.setOnClickListener(this@LoginActivity)

        btnLogin?.setOnClickListener {
            MyUtils.hideKeyboard(this@LoginActivity,btnLogin)
            if (checkValidation()){
                FirebaseInstanceId.getInstance().instanceId
                    .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                        override fun onSuccess(instanceIdResult: InstanceIdResult) {
                            val instanceId = instanceIdResult.id
                            var newtoken = instanceIdResult.token
                            Log.e("System out", "new token:= " + instanceIdResult.token)
                            if (MyUtils.isInternetAvailable(this@LoginActivity)){
                                loginAPI(newtoken)
                            }else{
                                MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                            }
                        }
                    })
            }
        }

        googleBtnLogin?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@LoginActivity, googleBtnLogin)
            signIn()
        }

        buttonFacebookLogin?.setReadPermissions("public_profile","email")
        buttonFacebookLogin?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d(TAG, "facebook:onSuccess:$loginResult")
//                handleFacebookAccessToken(loginResult.accessToken)
                LoginManager.getInstance().logOut()

                val request = GraphRequest.newMeRequest(loginResult.accessToken) { jsonObj, response ->

                    try {
                        //here is the data that you want
                        user_Email = jsonObj.get("email").toString()
                        val fbID = jsonObj.get("id").toString()
                        user_Name = jsonObj.get("name").toString()
                        userGoogleID = ""
//                        full_Name_edit_text?.setText(user_Name)
                        Log.e("System out","Print Fblogin Name :=  "+ user_Email)
//                        email_edit_text?.setText(user_Email)
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                                    val instanceId = instanceIdResult.id
                                    var newtoken = instanceIdResult.token
//                                Log.e("System out", "new token:= " + instanceIdResult.token)
                                    if (MyUtils.isInternetAvailable(this@LoginActivity)){
                                        userSocialLogin(newtoken,fbID,"")
                                    }else{
                                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                                    }
                                }
                            })


                        /*FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(object :
                            OnSuccessListener<InstanceIdResult> {

                            override fun onSuccess(instanceIdResult: InstanceIdResult?) {

                                val instanceId = instanceIdResult!!.id
                                val newToken = instanceIdResult.token


                                getFacebookLogin(fbEmail, fbID, fbMobile,fbName,newToken)

                            }

                        })*/

                        Log.d("FBLOGIN_JSON_RES", jsonObj.toString())

                        if (jsonObj.has("id")) {
//                            handleSignInResultFacebook(jsonObj)
                            Log.e("FBLOGIN_Success", jsonObj.toString())
                        } else {
                            Log.e("FBLOGIN_FAILD", jsonObj.toString())
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
//                        dismissDialogLogin()
                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "name,email,id,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                Log.d(TAG, "facebook:onCancel")
                // [START_EXCLUDE]
                Toast.makeText(baseContext, "facebook:onCancel.",
                    Toast.LENGTH_SHORT).show()
//                updateUI(null)
                // [END_EXCLUDE]
            }

            override fun onError(error: FacebookException) {
                Log.d(TAG, "facebook:onError", error)
                // [START_EXCLUDE]
                Toast.makeText(baseContext, "facebook:onError.",
                    Toast.LENGTH_SHORT).show()
//                updateUI(null)
                // [END_EXCLUDE]
            }
        })
        // [END initialize_fblogin]

        fbBtnLogin?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@LoginActivity, fbBtnLogin)

            buttonFacebookLogin?.performClick()
        }

    }

    override fun onBackPressed() {
//        super.onBackPressed()
        finish()
    }

    // [START signin]
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    // [START onactivityresult]
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                // [START_EXCLUDE]
                MyUtils.showSnackbarkotlin(
                    this@LoginActivity,
                    rootLoginLayout,
                    "Authentication Failed."
                )
//                updateUI(null)
                // [END_EXCLUDE]
            }
        }else {
            // Pass the activity result back to the Facebook SDK
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)
        userGoogleID = acct.id!!
        // [START_EXCLUDE silent]
        showProgressDialog()
        // [END_EXCLUDE]

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser

                    handleSignInResult(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    MyUtils.showSnackbarkotlin(
                        this@LoginActivity,
                        rootLoginLayout,
                        "Authentication Failed."
                    )
//                    Snackbar.make(main_layout, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
//                    handleSignInResult(null)
                }

                // [START_EXCLUDE]
                hideProgressDialog()
                // [END_EXCLUDE]
            }
    }
    // [END auth_with_google]

    private fun handleSignInResult(user: FirebaseUser?) {
        hideProgressDialog()
        Log.d(TAG, "handleSignInResult:" + user?.displayName)
//        if (result.isSuccess) {

        // Signed in successfully, show authenticated UI.
//            val acct = result.signInAccount
        Log.e("login deatils : ","GPluse :=  "+user?.uid)

        Log.e(TAG, "display name: " + user!!.displayName!!)

        user_Name = user.displayName!!
        user_Email = user.email!!
// logout
        signOut()
//        full_Name_edit_text?.setText(user_Name)
        Log.e("System out","Print displayName := "+user_Name)
//        email_edit_text?.setText(user_Email)

        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                    val instanceId = instanceIdResult.id
                    var newtoken = instanceIdResult.token
//                                Log.e("System out", "new token:= " + instanceIdResult.token)
                    if (MyUtils.isInternetAvailable(this@LoginActivity)){
                        userSocialLogin(newtoken,"",userGoogleID)
                    }else{
                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                    }
                }
            })



        /*if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
        }*/

//        } else {
        // Signed out, show unauthenticated UI.
        // updateUI(false)

//        }
    }

    private fun signOut() {
        // Firebase sign out
        auth.signOut()

        // Google sign out
        googleSignInClient.signOut().addOnCompleteListener(this) {
            //            updateUI(null)
        }
    }

    @VisibleForTesting
    val progressDialog by lazy {
        ProgressDialog(this)
    }

    fun showProgressDialog() {
        progressDialog.setMessage(getString(R.string.loading))
        progressDialog.isIndeterminate = true
        progressDialog.show()
    }

    fun hideProgressDialog() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.tvForgotText -> {
                MyUtils.hideKeyboardFrom(this@LoginActivity, tvForgotText!!)
//                MyUtils.startActivity(this@LoginActivity, ForgotPasswordActivity::class.java, false)
                MyUtils.startActivity(this@LoginActivity,ForgotPasswordActivity::class.java,false,true)
            }
            R.id.ll_signup -> {
                MyUtils.hideKeyboard1(this@LoginActivity)
//                val myIntent = Intent(this@LoginActivity, RegisterActivity::class.java)
//                startActivity(myIntent)
                val myIntent = Intent(this@LoginActivity, RegisterActivity::class.java)
                myIntent.putExtra("userFbUserId", "")
                myIntent.putExtra("userGPluseId", "")
                myIntent.putExtra("from", "Normal")
                myIntent.putExtra("user_Name", "")
                myIntent.putExtra(keyUserEmail, "")
                startActivity(myIntent)
//                MyUtils.startActivity(this@LoginActivity,RegisterActivity::class.java,false,true)

            }

            R.id.btnLogin -> {
                if (checkValidation()){
                    FirebaseInstanceId.getInstance().instanceId
                        .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                            override fun onSuccess(instanceIdResult: InstanceIdResult) {
                                val instanceId = instanceIdResult.id
                                var newtoken = instanceIdResult.token
//                                Log.e("System out", "new token:= " + instanceIdResult.token)
                                if (MyUtils.isInternetAvailable(this@LoginActivity)){
                                    loginAPI(newtoken)
                                }else{
                                    MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                                }
                            }
                        })
                }
            }


        }
    }

    private fun loginAPI(newtoken: String) {
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        btnLogin.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootLoginLayout, false)
        // http://betaapplication.com/flymyown/backend/web/index.php/v1/users/login
        /*[{
            "languageID": "1",
            "userDeviceType": "Android",
            "userDeviceID": "dsadasdasdsdsds",
            "userEmail": "",
            "userMobile": "9979517448",
            "userPassword": "ankit@12345",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/

        try {

            val loginType = TextUtils.isDigitsOnly(email_edit_text!!.text.toString().trim())

            if (loginType && (email_edit_text!!.text.toString().trim().length >= 10)) {
                jsonObject.put("userMobile", email_edit_text!!.text.toString())
                jsonObject.put("userEmail", "")
            } else {
                jsonObject.put("userMobile", "")
                jsonObject.put("userEmail", email_edit_text!!.text.toString())
            }

            jsonObject.put("userPassword", password_edit_text.text.toString())
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", ""+newtoken)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

            Log.e("System Out", "Login Request $jsonArray")
        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }

        val userLogin = ViewModelProviders.of(this@LoginActivity).get(ModelLoginUser::class.java)
        userLogin.userLogin(this@LoginActivity, false, jsonArray.toString()).observe(this@LoginActivity,
            object : Observer<List<RegisterNewPojo>> {
                override fun onChanged(response: List<RegisterNewPojo>?) {
                    if (!response.isNullOrEmpty()){

                        if (response.size > 0){
                            if (response[0].status.equals("true", true)){
                                btnLogin.endAnimation()

                                MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                                MyUtils.hideKeyboardFrom(this@LoginActivity, btnLogin!!)
                                if (!response[0].data.isNullOrEmpty()){
                                    if (response[0].data!!.size > 0){
                                        sessionManager?.clear_login_session()

                                        if (!response[0].data!![0].userVerified.isNullOrEmpty() && response[0].data!![0].userVerified.equals("No", true)){
                                            val sendUserId = response[0].data!![0].userID!!
                                            val myIntent = Intent(this@LoginActivity, VerificationOTPActivity::class.java)
                                            myIntent.putExtra(keyUserId, sendUserId)
                                            myIntent.putExtra(keyUserMobile, response[0].data!![0].userMobile)
                                            myIntent.putExtra(keyUserMobileCountryCode, response[0].data!![0].userCountryCode)
                                            myIntent.putExtra(keyUserEmail, response[0].data!![0].userEmail)
                                            startActivity(myIntent)

                                        }else{
                                            storeSessionManager(response[0]?.data!!)
                                            Handler().postDelayed({
                                                val myIntent = Intent(this@LoginActivity, MainActivity::class.java)
                                                startActivity(myIntent)
                                                finishAffinity()
                                            }, 1000)
                                        }
                                    }
                                }
                            }else{
                                //No data and no internet
                                btnLogin.endAnimation()
                                MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                                if (MyUtils.isInternetAvailable(this@LoginActivity)){
                                    if (!response[0].message.isNullOrEmpty()){
                                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, response[0].message!!)
                                    }else{
                                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.err_login))
                                    }
                                }else{
                                    MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                                }
                            }
                        }
                    }else{
                        btnLogin.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(this@LoginActivity)){
                            MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_crash_error_message))
                        }else{
                            MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                        }
                    }
                }
            })

    }

    private fun userSocialLogin(newtoken: String,fbid : String,GPlusId :String) {
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        btnLogin.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootLoginLayout, false)
        // http://betaapplication.com/flymyown/backend/web/index.php/v1/users/login
        /*[{
"loginuserID": "0",
"languageID": "1",
"userFacebookID": "1655220881248219",
"userGoogleID": "",
"userFullName": "Ankit Patel",
"userMobile": "",
"userEmail": "patelankit1516@gmail.com",
"userDeviceType": "Andorid",
"userDeviceID": "121212121211221212",
"userLatitude": "1212121212",
"userLongitude": "121212121",
"apiType": "Android",
"apiVersion": "1.0"
}]*/

        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("userFacebookID", fbid)
            jsonObject.put("userGoogleID", GPlusId)
            jsonObject.put("userFullName", ""+user_Name)
            jsonObject.put("userMobile", "")
            jsonObject.put("userEmail", ""+user_Email)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", ""+newtoken)
            jsonObject.put("userLatitude", ""+ PrefDb(this@LoginActivity).getString("LOCATIONLATE"))
            jsonObject.put("userLongitude", ""+ PrefDb(this@LoginActivity).getString("LOCATIONLONG"))
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System Out", "Login Request $jsonArray")
        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }

        val userLogin = ViewModelProviders.of(this@LoginActivity).get(userSocialLoginModel::class.java)
        userLogin.apiFunction(this@LoginActivity, false, jsonArray.toString()).observe(this@LoginActivity,
            object : Observer<List<RegisterNewPojo>> {
                override fun onChanged(response: List<RegisterNewPojo>?) {
                    if (!response.isNullOrEmpty()){
                        btnLogin.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                        if (response.size > 0){
                            if (response[0].status.equals("true", true)){
                                MyUtils.hideKeyboardFrom(this@LoginActivity, btnLogin!!)
                                if (!response[0].data.isNullOrEmpty()){
                                    if (response[0].data!!.size > 0){
//                                        sessionManager?.clear_login_session()

                                        if (!response[0].data!![0].userVerified.isNullOrEmpty() && response[0].data!![0].userVerified.equals("No", true)){
                                            val sendUserId = response[0].data!![0].userID!!
                                            val myIntent = Intent(this@LoginActivity, VerificationOTPActivity::class.java)
                                            myIntent.putExtra(keyUserId, sendUserId)
                                            myIntent.putExtra(keyUserMobile, response[0].data!![0].userMobile)
                                            myIntent.putExtra(keyUserMobileCountryCode, response[0].data!![0].userCountryCode)
                                            myIntent.putExtra(keyUserEmail, response[0].data!![0].userEmail)
                                            startActivity(myIntent)
                                        }else{
                                            storeSessionManager(response[0]?.data!!)
                                            Handler().postDelayed({
                                                val myIntent = Intent(this@LoginActivity, MainActivity::class.java)
                                                startActivity(myIntent)
                                            }, 1000)
                                        }
                                    }
                                }
                            }else if (response[0].status.equals("false", true)){

                                if(response[0].action.equals("register")){
                                    val myIntent = Intent(this@LoginActivity, RegisterActivity::class.java)
                                    myIntent.putExtra("userFbUserId", fbid)
                                    myIntent.putExtra("userGPluseId", GPlusId)
                                    myIntent.putExtra("user_Name", user_Name)
                                    myIntent.putExtra("from", "Social")
                                    myIntent.putExtra(keyUserEmail, user_Email)
                                    startActivity(myIntent)
                                }

                            }
                            else{
                                //No data and no internet
                                btnLogin.endAnimation()
                                MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                                if (MyUtils.isInternetAvailable(this@LoginActivity)){
                                    if (!response[0].message.isNullOrEmpty()){
                                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, response[0].message!!)
                                    }else{
                                        MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.err_login))
                                    }
                                }else{
                                    MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                                }
                            }
                        }
                    }else{
                        btnLogin.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootLoginLayout, true)
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(this@LoginActivity)){
                            MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_crash_error_message))
                        }else{
                            MyUtils.showSnackbarkotlin(this@LoginActivity, rootLoginLayout!!, resources.getString(R.string.error_common_network))
                        }
                    }
                }
            })

    }

    private fun checkValidation(): Boolean {

        var checkFlag = true
        val loginType = TextUtils.isDigitsOnly(email_edit_text.text.toString().trim())

        if (email_edit_text.text.toString().trim().isEmpty()) {
            MyUtils.showSnackbarkotlin(
                this@LoginActivity,
                rootLoginLayout!!,
                this@LoginActivity.resources.getString(R.string.err_empty_email_mobile)
            )
            checkFlag = false
        } else if (loginType && (email_edit_text.text.toString().trim().length < 10)) {
            MyUtils.showSnackbarkotlin(
                this@LoginActivity,
                rootLoginLayout!!,
                this@LoginActivity.resources.getString(R.string.err_enter_reg_mo)
            )
            checkFlag = false
        } else if (!loginType && !MyUtils.isEmailValid(email_edit_text.text.toString().trim())) {
            MyUtils.showSnackbarkotlin(
                this@LoginActivity,
                rootLoginLayout!!,
                this@LoginActivity.resources.getString(R.string.err_valid_email)
            )
            checkFlag = false
        } else if (password_edit_text!!.text.toString().trim().isEmpty()) {
            MyUtils.showSnackbarkotlin(
                this@LoginActivity,
                rootLoginLayout!!,
                this@LoginActivity.resources.getString(R.string.err_empty_pwd)
            )
            checkFlag = false
        } else if (password_edit_text!!.text!!.length < 8) {
            MyUtils.showSnackbarkotlin(
                this@LoginActivity,
                rootLoginLayout!!,
                this@LoginActivity.resources.getString(R.string.err_pws_lth_8)
            )
            checkFlag = false
        } else {
            //
        }
        return checkFlag
    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()
        val json = gson.toJson(driverdata[0]!!)
        sessionManager?.create_login_session(
            json,
            driverdata[0]!!.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!)
    }

    private fun customTextView(view: AppCompatTextView) {

        val spanTxt =
            SpannableStringBuilder("" + this@LoginActivity.resources.getString(R.string.don_t_have_and_account) + " ")
        val signup = this@LoginActivity.resources.getString(R.string.signup)

        spanTxt.append(signup)
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                val myIntent = Intent(this@LoginActivity, RegisterActivity::class.java)
                startActivity(myIntent)
            }
        }, spanTxt.length - signup.length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.green_1)),
            spanTxt.length - signup.length,
            spanTxt.length,
            0
        )
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    fun printHasyKeyFb(){
        // https://farmerprice.in/privacypolicy.html
        try {
            val info = packageManager.getPackageInfo("com.fil.flymyowncustomer", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.d("System out", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("System out", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("System out", "printHashKey()", e)
        }

    }

}