package com.fil.flymyowncustomer.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_confirmation.*

import kotlinx.android.synthetic.main.toolbar_back.*

import android.content.Intent
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.model.RechargeWalletHistoryModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.Gson
import com.google.gson.JsonParseException
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Double


class ConfirmationActivity : AppCompatActivity() {
    var isViewOrder = false
    var orderNo = ""
    var sessionManager: SessionManager? = null
    var userData: RegisterNewPojo.Datum? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        sessionManager = SessionManager(this@ConfirmationActivity!!)
        setSupportActionBar(toolbar_back!!)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        tvToolbarTitel.text = getString(R.string.confirmation)

        toolbar_back.setNavigationOnClickListener {
            onBackPressed()
        }

        rechargeWalletHistory()

        if (intent != null) {
            if (intent.hasExtra("orderNo")) {
                orderNo = intent.getStringExtra("orderNo")!!
                tv_coupon.text = orderNo

            }
        }

        btn_View_Order_Details.setOnClickListener {
            isViewOrder = true
            onBackPressed()
        }

    }

    override fun onBackPressed() {
//        super.onBackPressed()
        val intent = Intent()
        if (isViewOrder)
            intent.putExtra("navigation", "Order")
        else
            intent.putExtra("navigation", "Home")

        setResult(304, intent)
        MyUtils.finishActivity(this@ConfirmationActivity, true)
    }


    private fun rechargeWalletHistory() {

        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("wallettransactonType", "Recharge")
            jsonObject.put("page", 0)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var myWallethistory =
            ViewModelProviders.of(this@ConfirmationActivity).get(
                RechargeWalletHistoryModel::class.java
            )
        myWallethistory.rechargeWalletHistory(
            this@ConfirmationActivity,
            false,
            jsonArray.toString()
        )
            .observe(this@ConfirmationActivity,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {

                        if (masterPojo[0].status.equals("true", false)) {

                            userData?.userWalletRechargeAmount =
                                "" + masterPojo[0].userWalletRechargeAmount.toString()
                            userData?.userWalletReferAndEarnAmount =
                                masterPojo[0].userWalletReferAndEarnAmount.toString()
                            val gson = Gson()
                            val json: String = gson.toJson(userData)
                            Log.e("System out", "json == " + json)
                            sessionManager!!.create_login_session(
                                json,
                                userData!!.userEmail!!,
                                "",
                                true,
                                sessionManager!!.isEmailLogin()
                            )

                        } else {
//                            MyUtils.showSnackbarkotlin(
//                                this@ConfirmationActivity,
//                                rootConfirmationMainLayout!!,
//                                "" + masterPojo[0].message)

                        }

                    } else {

                        nodatafound()
                    }
                })

    }

    private fun nodatafound() {

        try {

            if (MyUtils.isInternetAvailable(this@ConfirmationActivity)) {
//                MyUtils.showSnackbarkotlin(
//                    this@ConfirmationActivity,
//                    rootConfirmationMainLayout!!,
//                    resources.getString(R.string.error_something)
//                )

            } else {
                /*MyUtils.showSnackbarkotlin(
                    this@ConfirmationActivity,
                    rootConfirmationMainLayout!!,
                    resources.getString(R.string.error_common_network)
                )*/

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
