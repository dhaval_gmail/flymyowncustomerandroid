package com.fil.flymyowncustomer.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.UserForgotPasswordPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.UserForgotPasswordmodel
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.header_layout.*
import org.json.JSONArray
import org.json.JSONObject


class ForgotPasswordActivity : AppCompatActivity() {
    var keyUserMobileCountryCode = "keyUserMobileCountryCode"
    var keyUserId = "keyUserId"
    var keyUserMobile = "keyUserMobile"
    var keyUserEmail = "keyUserEmail"
    var sendUserId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        tvHeaderText?.text = ""
        imgCloseIcon?.setOnClickListener {
            onBackPressed()
        }

        btnResetPassword?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@ForgotPasswordActivity, btnResetPassword)
            email_forgot_text_input.error = null // Clear the error

            if (checkValidation()) {
//                btnResetPassword?.startAnimation()
//                MyUtils.setViewAndChildrenEnabled(rootForgotLayout, false)
                if (MyUtils.isInternetAvailable(this@ForgotPasswordActivity)){
                    loginAPI()
//                    Handler().postDelayed({
//                        btnResetPassword?.endAnimation()
//                        MyUtils.setViewAndChildrenEnabled(rootForgotLayout, true)
//                    }, 2000)
                }else{
                    MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                }
            }
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        MyUtils.finishActivity(this@ForgotPasswordActivity, true)
    }

    private fun checkValidation(): Boolean {
        var checkFlag = true
        var loginType = TextUtils.isDigitsOnly(email_forgot_edit_text!!.text.toString().trim())

        if (email_forgot_edit_text?.text.toString().trim().isEmpty()) {
            MyUtils.showSnackbarkotlin(
                this@ForgotPasswordActivity,
                rootForgotLayout,
                this@ForgotPasswordActivity.resources.getString(R.string.forgot_err_msg)
            )
            checkFlag = false
        } else if (loginType && (email_forgot_edit_text?.text.toString().trim().length < 10)) {
            MyUtils.showSnackbarkotlin(
                this@ForgotPasswordActivity,
                rootForgotLayout,
                this@ForgotPasswordActivity.resources.getString(R.string.forgot_err_enter_reg_mo)
            );
            checkFlag = false
        } else if (!loginType && !MyUtils.isEmailValid(email_forgot_edit_text?.text.toString().trim())) {
            MyUtils.showSnackbarkotlin(
                this@ForgotPasswordActivity,
                rootForgotLayout,
                this@ForgotPasswordActivity.resources.getString(R.string.reg_err_enter_valid_emailid)
            )
            checkFlag = false
        }

        return checkFlag
    }

    fun loginAPI() {

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        btnResetPassword?.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootForgotLayout, false)
        // users/forgot-password
        /*[{
            "languageID": "1",
            "userEmail": "",
            "userMobile": "9979517447",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/

        try {

            val loginType = TextUtils.isDigitsOnly(email_forgot_edit_text?.text.toString().trim())

            if (loginType && (email_forgot_edit_text?.text.toString().trim().length >= 10)) {
                jsonObject.put("userMobile", email_forgot_edit_text?.text.toString())
                jsonObject.put("userEmail", "")
            } else {
                jsonObject.put("userMobile", "")
                jsonObject.put("userEmail", email_forgot_edit_text?.text.toString())
            }
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }
        Log.e("System Out", "Login Request $jsonArray")

        val userLogin = ViewModelProviders.of(this@ForgotPasswordActivity).get(UserForgotPasswordmodel::class.java)
        userLogin.getForgotPasswordApi(this@ForgotPasswordActivity, false, jsonArray.toString()).observe(this@ForgotPasswordActivity,
            object : Observer<List<UserForgotPasswordPojo>> {
                override fun onChanged(response: List<UserForgotPasswordPojo>?) {

                    if (!response.isNullOrEmpty()){
//                        btnResetPassword?.endAnimation()
//                        MyUtils.setViewAndChildrenEnabled(rootForgotLayout, true)
                        if (response[0].status.equals("true", true)){
                            btnResetPassword?.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(rootForgotLayout, true)
                            MyUtils.hideKeyboardFrom(this@ForgotPasswordActivity, btnResetPassword)
                            if (!response[0].message.isNullOrEmpty()){
                                MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, response[0].message!!)
                            }

                            if (!response[0].data.isNullOrEmpty()){
                                if (response[0].data!!.size > 0){
                                    sendUserId = response[0].data!![0].userID!!
                                    Handler().postDelayed({
                                        val myIntent = Intent(this@ForgotPasswordActivity, ForgotOTPVerificationActivity::class.java)
                                        myIntent.putExtra(keyUserId, sendUserId)
                                        myIntent.putExtra(keyUserMobile, email_forgot_edit_text?.text.toString())
                                        myIntent.putExtra(keyUserEmail, email_forgot_edit_text?.text.toString())
                                        myIntent.putExtra(keyUserMobileCountryCode, response[0].data!![0].userCountryCode)
                                        startActivity(myIntent)
                                    }, 2000)
                                }
                            }
                        }else{
                            btnResetPassword?.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(rootForgotLayout, true)
                            //No data and no internet
                            if (MyUtils.isInternetAvailable(this@ForgotPasswordActivity)){
                                MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, response[0].message!!)
                            }else{
                                MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                            }
                        }

                    }else{
                        btnResetPassword?.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootForgotLayout, true)
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(this@ForgotPasswordActivity)){
                            MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, resources.getString(R.string.error_crash_error_message))
                        }else{
                            MyUtils.showSnackbarkotlin(this@ForgotPasswordActivity, rootForgotLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                        }
                    }
                }
            })
    }
}
