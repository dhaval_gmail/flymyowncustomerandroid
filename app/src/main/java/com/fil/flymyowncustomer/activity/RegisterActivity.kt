package com.fil.flymyowncustomer.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.InputType
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.fragment.Bottomsheet
import com.fil.flymyowncustomer.fragment.BottomsheetWithOnlyString
import com.fil.flymyowncustomer.fragment.MediaChooseImageBottomsheet
import com.fil.flymyowncustomer.model.CheckDuplicationModel
import com.fil.flymyowncustomer.model.CountryListModel
import com.fil.flymyowncustomer.model.RegisterUserModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.CountryListPojo
import com.fil.flymyowncustomer.pojo.FiluploadResponse
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PrefDb
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.JsonParseException
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.header_layout.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    val keyStringData = "STRINGDATA"
    var signupLayoutMain: LinearLayoutCompat? = null
    var signupLayoutSub: NestedScrollView? = null
    var imv_dp: SimpleDraweeView? = null
    var imv_edit: AppCompatImageView? = null
    val keyTitle = "TITLE"
    val keyData = "DATA"
    var arrayCountry: CountryListPojo? = null
    var compressedImage: File? = null
    private val TAKE_PICTURE = 1
    private var pictureUri: Uri? = null
    private var picturePath: String? = null
    private val SELECT_PICTURE = 2
    private var timeForImageName: Long = 0
    private var imgName: String? = null
    private var actualImage: File? = null
    private var mediaChooseBottomSheet = MediaChooseImageBottomsheet()
    internal var pbDialog: Dialog? = null
    var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    var selectedImageName = ""
    var selectedStoreType = ""
    var selectedCountryCode = ""
    var selectedCountryId = ""
    var arrayString = ArrayList<String>()
    val keyFromOnlyStringWitchCheckBox = "FROMSTRINGCHECKBOX"

    val keyFrom = "FROM"
    val keyFromRegisterCountery = "FROMREGISTERCountry"
    var isFirstTime = false
    val keyUserId = "keyUserId"
    val keyUserMobile = "keyUserMobile"
    val keyUserMobileCountryCode = "keyUserMobileCountryCode"
    val keyUserEmail = "keyUserEmail"
    var userFbUserId = ""
    var userGPluseId = ""
    var userEmailId = ""
    var from = ""
    var user_Name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        signupLayoutMain = findViewById<LinearLayoutCompat>(R.id.signupLayoutMain)
        signupLayoutSub = findViewById<NestedScrollView>(R.id.signupLayoutSub)
        imv_dp = findViewById<SimpleDraweeView>(R.id.imv_dp)
        imv_edit = findViewById<AppCompatImageView>(R.id.imv_edit)
        tvHeaderText.setOnClickListener {
            onBackPressed()
        }
        customTextView(tv_signup_terms)

        if (intent != null) {
            from = intent.getStringExtra("from")!!
            userFbUserId = intent.getStringExtra("userFbUserId")!!
            userGPluseId = intent.getStringExtra("userGPluseId")!!
            userEmailId = intent.getStringExtra(keyUserEmail)!!
            user_Name = intent.getStringExtra("user_Name")!!

            if (from.equals("Social")) {
                signup_edit_type_pwd?.visibility = View.GONE
            } else {
                signup_edit_type_pwd?.visibility = View.VISIBLE
            }
            signup_edit_name?.setText("" + user_Name)
            signup_edit_email?.setText("" + userEmailId)

        }

        signup_edit_name.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        signup_edit_name?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub
            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (signup_edit_name?.text.toString().isNotEmpty()) {
                    try {
                        var x: Char
                        val t = IntArray(signup_edit_name?.text.toString().length)

                        for (i in 0 until signup_edit_name?.text.toString().length) {
                            x = signup_edit_name?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..91 || z in 97..122 || z == 32) {

                            } else {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val ss = signup_edit_name?.text.toString()
                                    .substring(0, signup_edit_name?.text.toString().length - 1)
                                signup_edit_name?.setText(ss)
                                signup_edit_name?.setSelection(signup_edit_name?.text.toString().length)
                            }
                        }
                    } catch (e: IndexOutOfBoundsException) {
                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        signup_edit_referral?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub
            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (signup_edit_referral?.text.toString().isNotEmpty()) {
                    try {
                        var x: Char
                        val t = IntArray(signup_edit_referral?.text.toString().length)

                        for (i in 0 until signup_edit_referral?.text.toString().length) {
                            x = signup_edit_referral?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..91 || z in 97..122 || z in 48..57) {

                            } else {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val ss = signup_edit_referral?.text.toString()
                                    .substring(0, signup_edit_referral?.text.toString().length - 1)
                                signup_edit_referral?.setText(ss)
                                signup_edit_referral?.setSelection(signup_edit_referral?.text.toString().length)
                            }
                        }
                    } catch (e: IndexOutOfBoundsException) {
                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        imgCloseIcon.setOnClickListener(this)
        btnSignup.setOnClickListener {
            MyUtils.hideKeyboard(this@RegisterActivity, btnSignup)
            if (checkValidation()) {
                if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                    checkDuplication()
                } else {
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.error_common_network)
                    )
                }
            }
        }
        signup_country_selection.setOnClickListener(this)
        tvHeaderText.text = ""

        imv_edit?.setOnClickListener {
            val currentapiVersion = Build.VERSION.SDK_INT
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                getWriteStoragePermissionOther()
            } else {
                mediaChooseBottomSheet.show(supportFragmentManager, "BottomSheet demoFragment")
            }
        }

        if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
            isFirstTime = true
            getCountryList()
        } else {
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.error_common_network)
            )
        }


    }

    fun checkValidation(): Boolean {
        var checkFlag = true

        /*if(actualImage == null){
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_empty_profile_pic)
            )
        }else*/
        if (signup_edit_name.text.toString().isNullOrEmpty() || signup_edit_name.text.toString().isNullOrBlank()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_empty_full_name)
            )
        } else if (signup_edit_name.text.toString().length < 3) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_lnth_full_name_3)
            )
        } else if (signup_tv_country_code.text.toString().isNullOrBlank() || signup_tv_country_code.text.toString().isNullOrEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_empty_country_code)
            )
        } else if (signup_edit_mobile.text.toString().isNullOrBlank() || signup_edit_mobile.text.toString().isNullOrEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_empty_mobile_number)
            )
        } else if (signup_edit_mobile.text.toString().length < 10) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_lnth_mobile_10)
            )
        } else if (signup_edit_email.text.toString().isNullOrBlank() || signup_edit_email.text.toString().isNullOrEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_empty_email)
            )
        } else if (!MyUtils.isEmailValid(signup_edit_email?.text.toString().trim())) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterActivity,
                signupLayoutMain!!,
                resources.getString(R.string.err_valid_email)
            )
        } else {
            if (from.equals("Social")) {
                if (!signup_checkbox_terms.isChecked) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.accept_terms)
                    )
                }
            } else {
                if (signup_edit_pwd.text.toString().isNullOrBlank() || signup_edit_pwd.text.toString().isNullOrEmpty()) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.err_pwd)
                    )
                } else if (signup_edit_pwd.text.toString().length < 8) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.err_lnth_pwd_6)
                    )
                } else if (!MyUtils.isValidPassword(signup_edit_pwd?.text.toString().trim())) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.validerror_password)
                    )
                }else if(!signup_edit_referral?.text.toString().trim().isNullOrEmpty() && signup_edit_referral?.text.toString().trim().length < 8) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.err_lnth_refferal_6)
                    )
                }

                else if (!signup_checkbox_terms.isChecked) {
                    checkFlag = false
                    MyUtils.showSnackbarkotlin(
                        this@RegisterActivity,
                        signupLayoutMain!!,
                        resources.getString(R.string.accept_terms)
                    )
                }
            }
        }


        return checkFlag
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgCloseIcon -> {
                onBackPressed()
            }

            R.id.btnSignup -> {
                if (checkValidation()) {
                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                        checkDuplication()
                    } else {
                        MyUtils.showSnackbarkotlin(
                            this@RegisterActivity,
                            signupLayoutMain!!,
                            resources.getString(R.string.error_common_network)
                        )
                    }
                }
            }

            R.id.signup_country_selection -> {

                if (arrayCountry != null) {
                    openBottomSheet("Select Country Code", keyFromRegisterCountery, arrayString, 1)
                } else {
                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                        getCountryList()
                    } else {
                        MyUtils.showSnackbarkotlin(
                            this@RegisterActivity,
                            signupLayoutMain!!,
                            resources.getString(R.string.error_common_network)
                        )
                    }
                }

            }
        }
    }

    private fun customTextView(view: AppCompatTextView) {

        val spanTxt =
            SpannableStringBuilder("" + this@RegisterActivity.resources.getString(R.string.terms) + " ")
        val signup = this@RegisterActivity.resources.getString(R.string.terms1)

        spanTxt.append("\n" + signup)
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                val myIntent = Intent(this@RegisterActivity, TermsandConditionsActivity::class.java)
                myIntent.putExtra("Type", "Terms")
                startActivity(myIntent)
            }
        }, spanTxt.length - signup.length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue_3)),
            spanTxt.length - signup.length,
            spanTxt.length,
            0
        )
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    fun openBottomSheet() {
        val sellerBottomsheet = Bottomsheet()
        val bundle = Bundle()
        bundle.putString(keyTitle, "Select Country")
//        bundle.putSerializable(keyData, )
        sellerBottomsheet.isCancelable = false
        sellerBottomsheet.arguments = bundle
        sellerBottomsheet.show(supportFragmentManager, "Select Country")
        sellerBottomsheet.setListener(object : Bottomsheet.OnItemClickListener {
            override fun onSellerSelectionClick(position: Int, typeofOperation: String) {


            }
        })
    }

    fun openBottomSheet(title: String, From: String, array: ArrayList<String>, from: Int) {
        val sellerBottomsheet = BottomsheetWithOnlyString()
        val bundle = Bundle()
        bundle.putString(keyTitle, title)
        bundle.putString(keyFrom, From)
        bundle.putStringArrayList(keyStringData, arrayString)
        if (From.equals(keyFromRegisterCountery, true)) {
            bundle.putSerializable("CountryList", arrayCountry)
        }
        sellerBottomsheet.isCancelable = false
        sellerBottomsheet.arguments = bundle
        sellerBottomsheet.show(supportFragmentManager, title)
        sellerBottomsheet.setListener(object : BottomsheetWithOnlyString.OnItemClickListener {
            override fun onSellerSelectionClick(
                position: Int,
                typeofOperation: String,
                selectedFlag: String,
                cId: String
            ) {
                when (From) {
                    keyFromOnlyStringWitchCheckBox -> {
//                        signup_selection_store.text = typeofOperation
                        selectedStoreType = typeofOperation
                    }

                    keyFromRegisterCountery -> {
                        signup_tv_country_code.text = typeofOperation
                        var imgUri = ""
                        if (!selectedFlag.isNullOrEmpty()) {
                            imgUri = selectedFlag
                        }
                        signup_recyclerview_contact_detail.setImageURI(Uri.parse(imgUri))
                        selectedCountryCode = typeofOperation
                        selectedCountryId = cId
                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        MyUtils.finishActivity(this@RegisterActivity, true)
    }

    fun getWriteStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(
                this@RegisterActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getReadStoragePermissionOther()
        } else {
            ActivityCompat.requestPermissions(
                this@RegisterActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getReadStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(
                this@RegisterActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getCameraPermissionOther()
        } else {
            ActivityCompat.requestPermissions(
                this@RegisterActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1
            )
        }
    }

    fun getCameraPermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(this@RegisterActivity, Manifest.permission.CAMERA)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            mediaChooseBottomSheet.show(
                this@RegisterActivity.supportFragmentManager,
                "BottomSheet demoFragment"
            )
        } else {
            ActivityCompat.requestPermissions(
                this@RegisterActivity,
                arrayOf(Manifest.permission.CAMERA),
                MyUtils.Per_REQUEST_CAMERA_1
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MyUtils.Per_REQUEST_WRITE_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getReadStoragePermissionOther()
            } else {
                getWriteStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_READ_EXTERNAL_STORAGE_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCameraPermissionOther()
            } else {
                getReadStoragePermissionOther()
            }
            MyUtils.Per_REQUEST_CAMERA_1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mediaChooseBottomSheet.show(
                    this@RegisterActivity.supportFragmentManager,
                    "BottomSheet demoFragment"
                )
            } else {
                getCameraPermissionOther()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            timeForImageName = System.currentTimeMillis()
            imgName = "img$timeForImageName.jpg"

            when (requestCode) {
                TAKE_PICTURE -> if (mediaChooseBottomSheet.selectedImage() != null) {
                    pictureUri = mediaChooseBottomSheet.selectedImage()
                    picturePath = pictureUri?.path
                    Log.d("compressedImage Name", picturePath.toString())
                    imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@RegisterActivity)
                    actualImage = File(picturePath)

                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                        compressedImage = customCompressImage(actualImage!!)
                        uploadImage(compressedImage!!)
//                        imv_dp?.setImageURI(Uri.fromFile(actualImage))

                    } else {
                        MyUtils.showSnackbarkotlin(
                            this@RegisterActivity,
                            signupLayoutMain!!,
                            this@RegisterActivity.resources.getString(R.string.error_common_network)
                        )
                    }
                }
                SELECT_PICTURE -> {
                    if(data !=null && data!!.data !=null)
                    {
                        pictureUri = data!!.data
                        picturePath = MyUtils.getPath5(pictureUri!!, this@RegisterActivity)
                        if (picturePath != null) {
                            if (picturePath?.contains("https:")!!) {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    "Please, select another profile pic."
                                )
                            } else {

                                imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@RegisterActivity)
                                actualImage = File(picturePath)
                                try {
                                    picturePath = MyUtils.getPath5(pictureUri!!, this@RegisterActivity)
                                    imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@RegisterActivity)
                                    actualImage = File(picturePath)
                                    compressedImage = customCompressImage(actualImage!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                if (MyUtils.isInternetAvailable(this@RegisterActivity)) {

                                    uploadImage(compressedImage!!)

                                } else {
                                    MyUtils.showSnackbarkotlin(
                                        this@RegisterActivity,
                                        signupLayoutMain!!,
                                        this@RegisterActivity.resources.getString(R.string.error_common_network)
                                    )
                                }
                            }
                        } else {
                            picturePath = MyUtils.getPathFromInputStreamUri(this@RegisterActivity!!, data!!.data)

                            imv_dp?.setImageURI(Uri.fromFile(File(picturePath)), this@RegisterActivity)
                            actualImage = File(picturePath)

                            if (MyUtils.isInternetAvailable(this@RegisterActivity)) {

                                uploadImage(actualImage!!)

                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    this@RegisterActivity.resources.getString(R.string.error_common_network)
                                )
                            }

                            /*MyUtils.showSnackbarkotlin(
                                this@RegisterActivity,
                                signupLayoutMain!!,
                                "Please, select another profile pic."
                            )*/
                        }
                    }
                }
            }
        }
    }

    fun customCompressImage(actualImage: File?): File? {

        timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())

        var compressedImage: File? = null
        try {
            compressedImage = Compressor(this@RegisterActivity)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    ).absolutePath
                )
                .compressToFile(actualImage, "IMG_" + timeStamp + ".jpg")
        } catch (e: IOException) {
            e.printStackTrace()
//            showError(e.message)
        }
        return compressedImage
    }

    fun uploadImage(file: File) {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val surveyImagesParts = arrayOfNulls<MultipartBody.Part>(1)

        for (i in 0 until 1) {
            val fileNew = File(file.absolutePath)
            val surveyBody = RequestBody.create(MediaType.parse("image/*"), fileNew)
            surveyImagesParts[i] =
                MultipartBody.Part.createFormData("MultipleFiles[]", fileNew.name, surveyBody)
        }

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("foldername", "users")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val call = RestClient.get()!!.uploadAttachment(
            surveyImagesParts, RequestBody.create(
                MediaType.parse("text/plain"), jsonArray.toString()
            )
        )

        Log.i("System out", "json " + jsonArray.toString())

        pbDialog = MyUtils.showProgressDialog(this@RegisterActivity)
        pbDialog!!.show()

        call.enqueue(object : Callback<List<FiluploadResponse>> {
            override fun onResponse(
                call: Call<List<FiluploadResponse>>,
                response: Response<List<FiluploadResponse>>
            ) {

                if (response.body() != null) {
                    if (pbDialog != null)
                        pbDialog!!.dismiss()
                    if (response.body()!![0].status.equals("true")) {

                        if (!response.body()!![0].data.isNullOrEmpty()) {
                            if (!response.body()!![0].data!![0].filename.isNullOrEmpty()) {
                                selectedImageName = response.body()!![0].data!![0].filename!!
//                                imv_dp?.setImageURI(Uri.parse(RestClient.image_base_url + "dealer/" + selectedImageName))
                            }
                        }
                    } else {
                        try {
                            if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    this@RegisterActivity.resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    this@RegisterActivity.resources.getString(R.string.error_common_network)
                                )
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } else {

                }
            }

            override fun onFailure(call: Call<List<FiluploadResponse>>, t: Throwable) {
                // Log error here since request failed
                Log.e("System out", t.message)
                if (pbDialog != null)
                    pbDialog!!.dismiss()
            }
        })

    }

    fun getCountryList() {

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
//        Log.e("System Out", "Login Request $jsonArray")

        val countryList =
            ViewModelProviders.of(this@RegisterActivity).get(CountryListModel::class.java)
        countryList.apiFunction(this@RegisterActivity, true, jsonArray.toString())
            .observe(this@RegisterActivity,
                object : Observer<List<CountryListPojo>> {
                    override fun onChanged(response: List<CountryListPojo>?) {
                        if (!response.isNullOrEmpty()) {
                            if (response.size > 0) {
                                if (response[0].status.equals("true", true)) {
                                    if (!response[0].data.isNullOrEmpty()) {
                                        if (response[0].data!!.size > 0) {

                                            arrayCountry = response[0]
                                            for (i in 0 until response[0].data?.size!!)
                                            {
                                                if(response[0].data?.get(i)?.countryDialCode.equals("+91",false)){
                                                    Collections.swap(response[0].data, i, 0)
                                                    break
                                                }
                                            }
                                            if (!response!![0].data!![0].countryFlagImage.isNullOrEmpty()) {
                                                signup_recyclerview_contact_detail.setImageURI(
                                                    Uri.parse(response!![0].data!![0].countryFlagImage),
                                                    this@RegisterActivity
                                                )
                                            }

                                            if (!response!![0].data!![0].countryDialCode.isNullOrEmpty()) {
                                                signup_tv_country_code.text =
                                                    response!![0].data!![0].countryDialCode
                                                selectedCountryCode =
                                                    response!![0].data!![0].countryDialCode!!
                                            }
                                            if (!response!![0].data!![0].countryDialCode.isNullOrEmpty()) {
                                                selectedCountryId =
                                                    response!![0].data!![0].countryID!!
                                            }

//                                        arrayCountry?.addAll(response!![0].data!!)
                                        }
                                    }
                                } else {

                                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
//                                    MyUtils.showSnackbarkotlin(this@ActivityAddNewAddress, newAddressLayoutMain, resources.getString(R.string.err_no_city))
                                    } else {
                                        MyUtils.showSnackbarkotlin(
                                            this@RegisterActivity,
                                            signupLayoutMain!!,
                                            resources.getString(R.string.error_common_network)
                                        )
                                    }
                                }
                            }
                        } else {
                            //No internet and somting went rong
                            if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_common_network)
                                )
                            }
                        }
                    }
                })
    }

    fun checkDuplication() {
        btnSignup.startAnimation()
        MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, false)
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
//            [{
//                "loginuserID": "0",
//                "userCountryCode": "+91",
//                "userMobile": "99795217447",
//                "userEmail": "",
//                "apiType": "Android",
//                "apiVersion": "1.0"
//            }]

            jsonObject.put("logindealerID", "0")
            jsonObject.put("userCountryCode", selectedCountryCode)
            jsonObject.put("userEmail", signup_edit_mobile?.text.toString().trim())
            jsonObject.put("userEmail", signup_edit_email?.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }
        Log.e("System Out", "Login Request $jsonArray")

        val countryList =
            ViewModelProviders.of(this@RegisterActivity).get(CheckDuplicationModel::class.java)
        countryList.apiFunction(this@RegisterActivity, false, jsonArray.toString())
            .observe(this@RegisterActivity,
                object : Observer<List<CommonPojo>> {
                    override fun onChanged(response: List<CommonPojo>?) {
                        if (!response.isNullOrEmpty()) {
                            if (response.size > 0) {
                                if (response[0].status.equals("true", true)) {
//                                setDataToObject()
                                    btnSignup.endAnimation()
                                    MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                        FirebaseInstanceId.getInstance().instanceId
                                            .addOnSuccessListener(object :
                                                OnSuccessListener<InstanceIdResult> {
                                                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                                                    var newtoken = instanceIdResult.token
                                                    Log.e(
                                                        "System out",
                                                        "new token:= " + instanceIdResult.token
                                                    )
                                                    userRegisterPojo(newtoken)
                                                }
                                            })

                                    } else {
                                        btnSignup.endAnimation()
                                        MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                                        MyUtils.showSnackbarkotlin(
                                            this@RegisterActivity,
                                            signupLayoutMain!!,
                                            resources.getString(R.string.error_common_network)
                                        )
                                    }

                                } else {
                                    btnSignup.endAnimation()
                                    MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                                    //No data and no internet
                                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                        if (!response[0].message.isNullOrEmpty()) {
                                            MyUtils.showSnackbarkotlin(
                                                this@RegisterActivity,
                                                signupLayoutMain!!,
                                                response[0].message!!
                                            )
                                        } else {
                                            MyUtils.showSnackbarkotlin(
                                                this@RegisterActivity,
                                                signupLayoutMain!!,
                                                resources.getString(R.string.err_duplication)
                                            )
                                        }
                                    } else {
                                        MyUtils.showSnackbarkotlin(
                                            this@RegisterActivity,
                                            signupLayoutMain!!,
                                            resources.getString(R.string.error_common_network)
                                        )
                                    }
                                }
                            }
                        } else {
                            btnSignup.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                            //No internet and somting went rong
                            if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_common_network)
                                )
                            }
                        }
                    }
                })
    }

    fun userRegisterPojo(newtoken: String?) {

        /*[{
            "loginuserID": "0",
            "languageID": "1",
            "userProfilePicture": "profilepic.png",
            "userFullName": "Ankit Patel",
            "userCountryCode": "+91",
            "userMobile": "9979517448",
            "userEmail": "patelankit@gmail.com",
            "userDeviceType": "Andorid",
            "userDeviceID": "121212121211221212",
            "userPassword": "ankit@12345",
            "userSingupReferCode": "",
            "userLatitude": "1212121212",
            "userLongitude": "121212121",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/
        btnSignup.startAnimation()
        MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, false)
        val jsonArray = JSONArray()
        var jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("userProfilePicture", selectedImageName)
            jsonObject.put("userFullName", signup_edit_name?.text.toString().trim())
            jsonObject.put("userCountryCode", selectedCountryCode.toString())
            jsonObject.put("userMobile", signup_edit_mobile?.text.toString().trim())
            jsonObject.put("userEmail", signup_edit_email?.text.toString().trim())
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", "" + newtoken)
            if (from.equals("Social")) {
                jsonObject.put("userPassword", "")
            } else {
                jsonObject.put("userPassword", signup_edit_pwd?.text.toString().trim())
            }

            jsonObject.put("userSingupReferCode", signup_edit_referral?.text.toString().trim())
            jsonObject.put(
                "userLatitude",
                "" + PrefDb(this@RegisterActivity).getString("LOCATIONLATE")
            )
            jsonObject.put(
                "userLongitude",
                "" + PrefDb(this@RegisterActivity).getString("LOCATIONLONG")
            )

            if (from.equals("Social")) {
                if (!userFbUserId.isNullOrEmpty()) {
                    jsonObject.put("userFacebookID", userFbUserId)
                } else {
                    jsonObject.put("userFacebookID", "")
                }

                if (!userGPluseId.isNullOrEmpty()) {
                    jsonObject.put("userGoogleID", userGPluseId)
                } else {
                    jsonObject.put("userGoogleID", "")
                }

            } else {
                jsonObject.put("userGoogleID", "")
                jsonObject.put("userFacebookID", "")
            }

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("Register request", "JSON is ${jsonArray.toString()}")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val registerUser =
            ViewModelProviders.of(this@RegisterActivity).get(RegisterUserModel::class.java)
        registerUser.apiFunction(this@RegisterActivity, false, jsonArray.toString())
            .observe(this@RegisterActivity,
                object : Observer<List<RegisterNewPojo>> {
                    override fun onChanged(response: List<RegisterNewPojo>?) {
                        if (!response.isNullOrEmpty()) {
                            if (response.size > 0) {
                                btnSignup.endAnimation()
                                MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                                if (response[0].status.equals("true", true)) {
                                    if (!response[0].data.isNullOrEmpty()) {
                                        if (response[0].data!!.size > 0) {

                                            actualImage?.delete()
                                            compressedImage?.delete()

                                            val sendUserId = response[0].data!![0].userID!!
                                            val myIntent =
                                                Intent(
                                                    this@RegisterActivity,
                                                    VerificationOTPActivity::class.java
                                                )
                                            myIntent.putExtra(keyUserId, sendUserId)
                                            myIntent.putExtra(
                                                keyUserMobile,
                                                response[0].data!![0].userMobile
                                            )
                                            myIntent.putExtra(
                                                keyUserMobileCountryCode,
                                                response[0].data!![0].userCountryCode
                                            )
                                            myIntent.putExtra(
                                                keyUserEmail,
                                                response[0].data!![0].userEmail
                                            )
                                            startActivity(myIntent)

                                        }
                                    }
                                } else {
                                    btnSignup.endAnimation()
                                    MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                                    //No data and no internet
                                    if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                        if (!response[0].message.isNullOrEmpty()) {
                                            MyUtils.showSnackbarkotlin(
                                                this@RegisterActivity,
                                                signupLayoutMain!!,
                                                response[0].message!!
                                            )
                                        } else {
                                            MyUtils.showSnackbarkotlin(
                                                this@RegisterActivity,
                                                signupLayoutMain!!,
                                                resources.getString(R.string.err_regstration)
                                            )
                                        }
                                    } else {
                                        MyUtils.showSnackbarkotlin(
                                            this@RegisterActivity,
                                            signupLayoutMain!!,
                                            resources.getString(R.string.error_common_network)
                                        )
                                    }
                                }
                            }
                        } else {
                            btnSignup.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(signupLayoutMain!!, true)
                            //No internet and somting went rong
                            if (MyUtils.isInternetAvailable(this@RegisterActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@RegisterActivity,
                                    signupLayoutMain!!,
                                    resources.getString(R.string.error_common_network)
                                )
                            }
                        }
                    }
                })
    }
}
