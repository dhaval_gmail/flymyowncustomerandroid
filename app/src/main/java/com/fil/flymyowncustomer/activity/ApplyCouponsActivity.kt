package com.fil.flymyowncustomer.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.fil.flymyowncustomer.adapter.CouponsListAdapter
import com.fil.flymyowncustomer.model.ApplyCouponNewModel
import com.fil.flymyowncustomer.model.CouponListModel
import com.fil.flymyowncustomer.pojo.CouponListData
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_apply_coupons.*
import kotlinx.android.synthetic.main.custom_reclyerview.*
import kotlinx.android.synthetic.main.nodatafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*
import org.json.JSONArray
import org.json.JSONObject
import android.text.TextUtils
import com.fil.flymyowncustomer.R


class ApplyCouponsActivity : AppCompatActivity() {

    var couponsListAdapter: CouponsListAdapter? = null
    var couponListData: ArrayList<String?>? = ArrayList()
    private lateinit var linearLayoutManager: LinearLayoutManager
    var pageNo = 0
    var pageSize = 10
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var firstVisibleItemPosition: Int = 0
    var dealerID: String = ""
    private var isLoading = false
    private var isLastpage = false
    var y: Int = 0
    var sessionManager: SessionManager? = null
    var userdata: RegisterNewPojo.Datum? = null
    var myCouponListList: ArrayList<CouponListData?>? = null
    var datum: CouponListData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply_coupons)

        setSupportActionBar(toolbar_back!!)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        tvToolbarTitel.text = getString(R.string.apply_coupon_code)

        toolbar_back.menuNotification.visibility = View.GONE
        toolbar_back.setNavigationOnClickListener {
            onBackPressed()
        }


        relativeprogressBar?.visibility = View.GONE
        noDatafoundRelativelayout?.visibility = View.GONE
        nointernetMainRelativelayout?.visibility = View.GONE
        if (intent != null) {
            if (intent.hasExtra("dealerID")) {
                dealerID = intent.getStringExtra("dealerID")!!
            }
        }
        sessionManager = SessionManager(this@ApplyCouponsActivity)
        userdata = sessionManager?.get_Authenticate_User()

        linearLayoutManager = LinearLayoutManager(this@ApplyCouponsActivity)
        notificationRecyclerview?.layoutManager = linearLayoutManager

        notificationRecyclerview?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.childCount
                totalItemCount = linearLayoutManager.itemCount
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getMyCouponListList()
                    }
                }
            }
        })

        btnRetry?.setOnClickListener {
            notificationRecyclerview?.visibility = View.GONE
            getMyCouponListList()
        }

        if (myCouponListList == null) {
            myCouponListList = ArrayList()
            couponsListAdapter = CouponsListAdapter(
                this@ApplyCouponsActivity,
                object : CouponsListAdapter.OnItemClick {
                    override fun onClicklisneter(pos: Int, name: String) {
                        when (name) {
                            "ApplyCode" -> {
                                applyCoupn(myCouponListList!![pos]!!)
                            }
                        }
                    }
                },
                "ApplyCode",
                myCouponListList!!
            )

            notificationRecyclerview?.setNestedScrollingEnabled(false)
            notificationRecyclerview?.setHasFixedSize(true)
            notificationRecyclerview?.adapter = couponsListAdapter
            couponsListAdapter?.notifyDataSetChanged()
            getMyCouponListList()

        }

        btn_apply_code?.setOnClickListener {
            MyUtils.hideKeyboard(this@ApplyCouponsActivity, btn_apply_code)
            if (edt_apply_code?.text.toString().trim().isNullOrEmpty()) {
                MyUtils.showSnackbarkotlin(
                    this@ApplyCouponsActivity,
                    applyCouponMainLayout,
                    this@ApplyCouponsActivity.resources.getString(R.string.textfield_applycoupons_eror_pwd)
                )
            } else if (!edt_apply_code?.text.toString().trim().isNullOrEmpty() && edt_apply_code?.text.toString().trim().length < 3) {
                MyUtils.showSnackbarkotlin(
                    this@ApplyCouponsActivity,
                    applyCouponMainLayout,
                    this@ApplyCouponsActivity.resources.getString(R.string.applycoupons_error_code)
                )
            } else {

                if(myCouponListList!!.size > 0){
                    datum = GetCouponCodeData(edt_apply_code?.text.toString().trim(), myCouponListList!!)
                    if(datum != null){
                        applyCoupn(datum!!)
                    }else {
                        MyUtils.showSnackbarkotlin(
                            this@ApplyCouponsActivity,
                            applyCouponMainLayout,
                            "Please Enter Valid Coupon Code")
                    }
                }else {
                    MyUtils.showSnackbarkotlin(
                        this@ApplyCouponsActivity,
                        applyCouponMainLayout,
                        "Coupon code not available")

                }


            }
        }

    }

    fun GetCouponCodeData(code: String, arrayList: ArrayList<CouponListData?>): CouponListData? {

        var data: CouponListData? = null

        if (!TextUtils.isEmpty(code)) {

            for (i in 0 until arrayList!!.size) {
                if (code.equals(arrayList[i]?.offerlogVoucherCode?.trim(), ignoreCase = true)) {
                    data = arrayList[i]
                    break
                }
            }

        }

        return data
    }

    fun internalSearch(query: String) {
        couponsListAdapter?.filter?.filter(query)
    }

    private fun getMyCouponListList() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        if (pageNo == 0) {
            relativeprogressBar!!.visibility = View.VISIBLE
            myCouponListList!!.clear()
            couponsListAdapter?.notifyDataSetChanged()
        } else {
            relativeprogressBar!!.visibility = View.GONE
            notificationRecyclerview.visibility = (View.VISIBLE)
            myCouponListList!!.add(null)
            couponsListAdapter?.notifyItemInserted(myCouponListList!!.size - 1)
        }
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("dealerID", dealerID)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var couponListModel =
            ViewModelProviders.of(this@ApplyCouponsActivity)
                .get(CouponListModel::class.java)
        couponListModel.getCouponList(
            this@ApplyCouponsActivity,
            false,
            jsonArray.toString(),
            "favList"
        )
            .observe(this@ApplyCouponsActivity,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar!!.visibility = View.GONE
                        notificationRecyclerview.visibility = (View.VISIBLE)

                        if (pageNo > 0) {
                            myCouponListList!!.removeAt(myCouponListList!!.size - 1)
                            couponsListAdapter?.notifyItemRemoved(myCouponListList!!.size)
                        }
                        if (masterPojo[0].status.equals("true", false)) {
                            if (pageNo == 0)
                                myCouponListList!!.clear()

                            myCouponListList!!.addAll(masterPojo[0].data!!)
                            couponsListAdapter?.notifyDataSetChanged()
                            pageNo += 1
                            if (masterPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        } else {
                            if (myCouponListList.isNullOrEmpty()) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                notificationRecyclerview.visibility = View.GONE
                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                notificationRecyclerview.visibility = View.VISIBLE

                            }
                        }

                    } else {

                        nodatafound()
                    }
                })


    }

    private fun nodatafound() {
        try {
            relativeprogressBar?.visibility = View.GONE
            nointernetMainRelativelayout.visibility = View.VISIBLE

            if (MyUtils.isInternetAvailable(this@ApplyCouponsActivity)) {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.something_went_wrong))
                nointernettextview.text = (getString(R.string.error_something))
                nointernettextview1.text = (this.getString(R.string.somethigwrong1))
            } else {
                nointernetImageview.setImageDrawable(resources.getDrawable(R.drawable.no_internet_connection))
                nointernettextview1.text = (this.getString(R.string.internetmsg1))
                nointernettextview.text = (getString(R.string.error_common_network))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onBackPressed() {
        MyUtils.finishActivity(this@ApplyCouponsActivity, true)
    }


    fun applyCoupn(offerlogVoucherCode: CouponListData) {


        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        val cartcalculate = CartCalculate
        cartcalculate.CartCalculate(this@ApplyCouponsActivity)

        try {

            jsonObject.put("loginuserID", userdata?.userID)
            jsonObject.put("dealerID", offerlogVoucherCode?.dealerID)
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("offerlogVoucherCode", offerlogVoucherCode?.offerlogVoucherCode)
            jsonObject.put("orderGrossAmt", cartcalculate.getOrderSubTotal())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var couponListModel =
            ViewModelProviders.of(this@ApplyCouponsActivity)
                .get(ApplyCouponNewModel::class.java)
        couponListModel.apiFunction(this@ApplyCouponsActivity, true, jsonArray.toString())
            .observe(this@ApplyCouponsActivity,
                Observer { response ->
                    if (!response.isNullOrEmpty()) {
                        //REsponse from Server
                        if (response[0].status.equals("true", true)) {
                            // applied coupon
                            if (response[0].message.isNullOrEmpty()) {
                                //static message
                                MyUtils.showSnackbarkotlin(
                                    this@ApplyCouponsActivity,
                                    applyCouponMainLayout,
                                    resources.getString(R.string.success_applu_copon)
                                )
                            } else {
                                //server message
                                MyUtils.showSnackbarkotlin(
                                    this@ApplyCouponsActivity,
                                    applyCouponMainLayout,
                                    response[0].message
                                )
                            }

                            Handler().postDelayed({
                                val intent1 = Intent().apply {
                                    response[0].data[0].appliedCouponCode =
                                        offerlogVoucherCode?.offerlogVoucherCode
                                    response[0].data[0].couponCodeCreatedBy =
                                        offerlogVoucherCode?.offerCreatedBy
                                    putExtra("code", response[0].data[0])
                                }
//                                setResult(122, intent1)
                                setResult(101, intent1)
                                onBackPressed()
                            }, 2000)

                        } else {
                            // not applied coupon
                            if (response[0].message.isNullOrEmpty()) {
                                //static message
                                MyUtils.showSnackbarkotlin(
                                    this@ApplyCouponsActivity,
                                    applyCouponMainLayout,
                                    resources.getString(R.string.err_applu_copon1)
                                )
                            } else {
                                //server message
                                MyUtils.showSnackbarkotlin(
                                    this@ApplyCouponsActivity,
                                    applyCouponMainLayout,
                                    response[0].message
                                )
                            }


                        }
                    } else {
                        // no intternet somthing rong

                        if (MyUtils.isInternetAvailable(this@ApplyCouponsActivity)) {
                            MyUtils.showSnackbarkotlin(
                                this@ApplyCouponsActivity,
                                applyCouponMainLayout!!,
                                resources.getString(R.string.error_crash_error_message)
                            )
                        } else {
                            MyUtils.showSnackbarkotlin(
                                this@ApplyCouponsActivity,
                                applyCouponMainLayout!!,
                                resources.getString(R.string.no_internet_connection)
                            )
                        }

                    }
                })
    }


}
