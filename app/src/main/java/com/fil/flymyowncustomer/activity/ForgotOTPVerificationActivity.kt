package com.fil.flymyowncustomer.activity

import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.ModelResendOTP
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.model.ModelVerifyOTP
import com.fil.flymyowncustomer.util.MySMSBroadcastReceiver
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_forgot_otpverification.*
import kotlinx.android.synthetic.main.header_layout.*
import org.json.JSONArray
import org.json.JSONObject

class ForgotOTPVerificationActivity : AppCompatActivity(), View.OnClickListener, MySMSBroadcastReceiver.OTPReceiveListener {

    var userId = ""
    var userMobile = ""
    var userEmail = ""
    var userCountryCode = ""
    var keyUserId = "keyUserId"
    var keyUserMobile = "keyUserMobile"
    var keyUserMobileCountryCode = "keyUserMobileCountryCode"
    var keyUserEmail = "keyUserEmail"
    val smsBroadcast = MySMSBroadcastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_otpverification)
        tvHeaderText?.text = ""
        try {
            if (intent != null) {
                userId = intent.getStringExtra(keyUserId)!!
                userMobile = intent.getStringExtra(keyUserMobile)!!
                userEmail = intent.getStringExtra(keyUserEmail)!!
                userCountryCode = intent.getStringExtra(keyUserMobileCountryCode)!!
            }
        } catch (e: java.lang.Exception) {

        }

        startSMSListener()
        smsBroadcast.initOTPListener(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        applicationContext.registerReceiver(smsBroadcast, intentFilter)

        imgCloseIcon?.setOnClickListener(this@ForgotOTPVerificationActivity)
//        btnForgotOTPVerifiedSubmit?.setOnClickListener(this@ForgotOTPVerificationActivity)
        tvResendOTP?.setOnClickListener(this@ForgotOTPVerificationActivity)

        btnForgotOTPVerifiedSubmit?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@ForgotOTPVerificationActivity, btnForgotOTPVerifiedSubmit)

            if (exitTextVerification1.length() == 0 && exitTextVerification1.length() < 4) {

                MyUtils.showSnackbarkotlin(
                    this@ForgotOTPVerificationActivity,
                    rootForgotVerificationMainLayout,
                    this@ForgotOTPVerificationActivity.resources.getString(R.string.OTPV)
                )

            } else {
                if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                    FirebaseInstanceId.getInstance().instanceId
                        .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                            override fun onSuccess(instanceIdResult: InstanceIdResult) {
                                val instanceId = instanceIdResult.id
                                var newtoken = instanceIdResult.token
                                Log.e("System out", "new token:= " + instanceIdResult.token)
                                if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)){
                                    verifyOTP(newtoken)
                                }else{
                                    MyUtils.showSnackbarkotlin(this@ForgotOTPVerificationActivity, rootForgotVerificationMainLayout!!, resources.getString(R.string.error_common_network))
                                }
                            }
                        })
                } else {
                    MyUtils.showSnackbarkotlin(
                        this@ForgotOTPVerificationActivity,
                        rootForgotVerificationMainLayout,
                        resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                    )
                }

            }
        }
    }

    override fun onBackPressed() {
        // super.onBackPressed()
        MyUtils.hideKeyboard1(this@ForgotOTPVerificationActivity)
        MyUtils.showMessageYesNo(this@ForgotOTPVerificationActivity, "Are you sure you want to exit?", object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                val i = Intent(this@ForgotOTPVerificationActivity, LoginActivity::class.java)
                startActivity(i)
                finishAffinity()
            }
        })
    }

    private fun startSMSListener() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {
            Log.d("Task on success", "SMS Retriever starts")
        }

        task.addOnFailureListener {
            Log.d("Task on success", "SMS Retriever failed")
        }


    }

    override fun onOTPReceived(otp: String) {
        if (smsBroadcast != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsBroadcast)
        }

        if (otp != null) {
            exitTextVerification1!!.text = Editable.Factory.getInstance().newEditable(otp.toString())
//            editSecondPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[1].toString())
//            editthirdPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[2].toString())
//            editfourthPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[3].toString())
        }
    }

    override fun onOTPTimeOut() {
        Toast.makeText(this, "OTP receving time out", Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgCloseIcon -> {

                onBackPressed()
            }

            R.id.btnForgotOTPVerifiedSubmit -> {
                MyUtils.hideKeyboardFrom(this@ForgotOTPVerificationActivity, btnForgotOTPVerifiedSubmit)

                if (exitTextVerification1.length() == 0 && exitTextVerification1.length() < 4) {

                    MyUtils.showSnackbarkotlin(
                        this@ForgotOTPVerificationActivity,
                        rootForgotVerificationMainLayout,
                        this@ForgotOTPVerificationActivity.resources.getString(R.string.OTPV)
                    )

                } else {
                    if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                                    val instanceId = instanceIdResult.id
                                    var newtoken = instanceIdResult.token
                                    Log.e("System out", "new token:= " + instanceIdResult.token)
                                    if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)){
                                        verifyOTP(newtoken)
                                    }else{
                                        MyUtils.showSnackbarkotlin(this@ForgotOTPVerificationActivity, rootForgotVerificationMainLayout!!, resources.getString(R.string.error_common_network))
                                    }
                                }
                            })
                    } else {
                        MyUtils.showSnackbarkotlin(
                            this@ForgotOTPVerificationActivity,
                            rootForgotVerificationMainLayout,
                            resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                        )
                    }

                }
            }

            R.id.tvResendOTP -> {
                MyUtils.hideKeyboard(this@ForgotOTPVerificationActivity, tvResendOTP)
                exitTextVerification1.text!!.clear()

                if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                    resendOTP()
                } else {
                    MyUtils.showSnackbarkotlin(
                        this@ForgotOTPVerificationActivity,
                        rootForgotVerificationMainLayout,
                        resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                    )
                }

            }
        }
    }


    private fun verifyOTP(newToken: String?) {
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        btnForgotOTPVerifiedSubmit.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootForgotVerificationMainLayout, false)
/*[{
            "userOTP": "1595",
            "userDeviceType": "Android",
            "userDeviceID": "czxczxczx",
            "languageID": "1",
            "loginuserID": "1",
            "apiType": "Iphone",
            "apiVersion": "1.0"
        }]*/


        try {
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userId)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", newToken)
            jsonObject.put("userOTP", exitTextVerification1.text.toString())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System Out", "Verify OTP $jsonArray")
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        val verifyOTP = ViewModelProviders.of(this@ForgotOTPVerificationActivity).get(ModelVerifyOTP::class.java)
        verifyOTP.verifyOTP(this@ForgotOTPVerificationActivity, false, jsonArray.toString())
            .observe(this@ForgotOTPVerificationActivity,
                object : Observer<List<RegisterNewPojo>> {
                    override fun onChanged(response: List<RegisterNewPojo>?) {
                        if (!response.isNullOrEmpty()) {
                            if (response.size > 0) {

                                if (response[0].status.equals("true", true)) {
                                    btnForgotOTPVerifiedSubmit.endAnimation()
                                    MyUtils.setViewAndChildrenEnabled(rootForgotVerificationMainLayout, true)
                                    MyUtils.hideKeyboardFrom(
                                        this@ForgotOTPVerificationActivity,
                                        btnForgotOTPVerifiedSubmit
                                    )
                                    if (!response[0].data.isNullOrEmpty()) {
                                        if (response[0].data!!.size > 0) {
                                            val sendUserId = response[0].data!![0].userID
                                            Handler().postDelayed({
                                                val myIntent = Intent(
                                                    this@ForgotOTPVerificationActivity,
                                                    ResetPasswordActivity::class.java
                                                )
                                                myIntent.putExtra(keyUserId, sendUserId)
                                                startActivity(myIntent)

                                            }, 500)
                                        }
                                    } else {
                                    }
                                } else {
                                    btnForgotOTPVerifiedSubmit.endAnimation()
                                    MyUtils.setViewAndChildrenEnabled(rootForgotVerificationMainLayout, true)
                                    //No data and no internet
                                    if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                                        MyUtils.showSnackbarkotlin(
                                            this@ForgotOTPVerificationActivity,
                                            rootForgotVerificationMainLayout,
                                            response[0].message!!
                                        )
                                    } else {
                                        MyUtils.showSnackbarkotlin(
                                            this@ForgotOTPVerificationActivity,
                                            rootForgotVerificationMainLayout,
                                            resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                                        )
                                    }
                                }
                            }
                        } else {
                            btnForgotOTPVerifiedSubmit.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(rootForgotVerificationMainLayout, true)
                            //No internet and somting went rong
                            if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@ForgotOTPVerificationActivity,
                                    rootForgotVerificationMainLayout,
                                    resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@ForgotOTPVerificationActivity,
                                    rootForgotVerificationMainLayout,
                                    resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                                )
                            }
                        }
                    }
                })
    }

    private fun resendOTP() {

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()

/*[{
            "languageID": "1",
            "loginuserID": "4",
            "userCountryCode": "+91",
            "userMobile": "8888888880",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/
        try {
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userId)
            jsonObject.put("userCountryCode", userCountryCode)
            jsonObject.put("userMobile", userMobile)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        Log.e("System Out", "Verify OTP $jsonArray")

        val resendOTP = ViewModelProviders.of(this@ForgotOTPVerificationActivity).get(ModelResendOTP::class.java)
        resendOTP.resendOTP(this@ForgotOTPVerificationActivity, true, jsonArray.toString())
            .observe(this@ForgotOTPVerificationActivity,
                object : Observer<List<CommonPojo>> {
                    override fun onChanged(response: List<CommonPojo>?) {
                        if (!response.isNullOrEmpty()) {
                            if (response.size > 0) {

                                if (response[0].status.equals("true", true)) {
                                    MyUtils.hideKeyboardFrom(
                                        this@ForgotOTPVerificationActivity,
                                        btnForgotOTPVerifiedSubmit
                                    )
                                    MyUtils.showSnackbarkotlin(
                                        this@ForgotOTPVerificationActivity,
                                        rootForgotVerificationMainLayout,
                                        response[0].message!!
                                    )
                                } else {
                                    //No data and no internet
                                    if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                                        MyUtils.showSnackbarkotlin(
                                            this@ForgotOTPVerificationActivity,
                                            rootForgotVerificationMainLayout,
                                            response[0].message!!
                                        )
                                    } else {
                                        MyUtils.showSnackbarkotlin(
                                            this@ForgotOTPVerificationActivity,
                                            rootForgotVerificationMainLayout,
                                            resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                                        )
                                    }
                                }
                            }
                        } else {
                            //No internet and somting went rong
                            if (MyUtils.isInternetAvailable(this@ForgotOTPVerificationActivity)) {
                                MyUtils.showSnackbarkotlin(
                                    this@ForgotOTPVerificationActivity,
                                    rootForgotVerificationMainLayout,
                                    resources.getString(R.string.error_crash_error_message)
                                )
                            } else {
                                MyUtils.showSnackbarkotlin(
                                    this@ForgotOTPVerificationActivity,
                                    rootForgotVerificationMainLayout,
                                    resources.getString(R.string.error_common_netdon_t_have_and_accountwork)
                                )
                            }
                        }
                    }
                })
    }
}
