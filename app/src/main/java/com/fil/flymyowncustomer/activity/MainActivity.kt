package com.fil.flymyowncustomer.activity

import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.net.ParseException
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.FrameLayout
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.facebook.drawee.view.SimpleDraweeView
import com.fil.flymyowncustomer.util.CartCalculate
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.fragment.*
import com.fil.flymyowncustomer.iterfaces.NavigationHost
import com.fil.flymyowncustomer.model.UpdateDeviceTokenModel
import com.fil.flymyowncustomer.pojo.Push
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.PrefDb
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.item_navigation.view.*
import kotlinx.android.synthetic.main.nav_header.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity(), NavigationHost, View.OnClickListener {


    private var doubleBackToExitPressedOnce = false
    private var snackBarParent: View? = null
    var drawertoggle: ActionBarDrawerToggle? = null
    //var bottomnavigation: AHBottomNavigation? = null
    var bottom_navigation: BottomNavigationView? = null
    var listViewLinearlayout: LinearLayoutCompat? = null
    var list_Search: ListView? = null
    var menuSearch: MenuItem? = null
    var menuNotification: MenuItem? = null
    var toolbar: Toolbar? = null
    var toolbar_title: TextView? = null
    var toolbar_Imageview: AppCompatImageView? = null
    var cartLayout: View? = null
    var notificationLayout: View? = null
    private val TAG = MainActivity::class.java.name
    // var tvCartCount: AppCompatTextView? = null
    var tvCartCount1: AppCompatTextView? = null
    var tvCartBadgeSlide: AppCompatTextView? = null
    var txtNotificationCount: AppCompatTextView? = null
    var tvCartCountLayout: FrameLayout? = null
    var notificationBadge: View? = null
    var searchkeyword = ""
    lateinit var sessionManager: SessionManager
    var userData: RegisterNewPojo.Datum? = null
    //    var searchListViewAdapter: SearchProductListViewAdapter? = null
//    var globalSearchListsPojo: MutableList<GlobalSearchList.Datum> = ArrayList<GlobalSearchList.Datum>()
//    var globalSearch: GlobalSearch? = null
    var profileImageview: SimpleDraweeView? = null
    var tvUserName: AppCompatTextView? = null
    var tvUserEmail: AppCompatTextView? = null
    var cartCalculate: CartCalculate? = null
    var cartItemCount = 0
    var fromPush = false
    var push: Push? = null

    private val mPushCounterBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {

            var pushCount = 0

            pushCount = PrefDb(this@MainActivity).getInt("unReadNotification")!!
            if (pushCount > 0) {
                if (menuNotification != null) {

                    menuNotification?.setIcon(getDrawable(R.drawable.notification_with_red_dot))
                } else {
                    menuNotification?.setIcon(getDrawable(R.drawable.notification_icon))
                }
            } else if (pushCount == 0) {
                menuNotification?.setIcon(getDrawable(R.drawable.notification_icon))
            }


        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(mPushCounterBroadcastReceiver, IntentFilter("Push"))

        /* listViewLinearlayout = findViewById(R.id.listViewLinearlayout)
         list_Search = findViewById(R.id.list_Search)*/
        sessionManager = SessionManager(this@MainActivity)
        if (sessionManager.isLoggedIn()) {

            userData = sessionManager?.get_Authenticate_User()

        } else {

        }

        cartCalculate = CartCalculate
        cartCalculate?.CartCalculate(this@MainActivity)

        addViewSnackBar()

        /*val actionBar = supportActionBar
        actionBar!!.title = ""
        actionBar.elevation = 1.0F
        actionBar.setDisplayShowHomeEnabled(true)*/

        toolbar_title = findViewById<TextView>(R.id.toolbar_title)
        toolbar_Imageview = findViewById<AppCompatImageView>(R.id.toolbar_Imageview)
        bottom_navigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        supportActionBar?.setDisplayShowTitleEnabled(false)

        /*val layoutParams = bottom_navigation?.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationViewBehavior()*/

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        if (intent.hasExtra("Push")) {
            fromPush = true
            push = intent.getSerializableExtra("Push") as Push
        }

        if (intent != null && intent.hasExtra("Push")) {
            if (push?.type.equals("Order", true)) {
                val bundle = Bundle()
                bundle.apply {

                    /**
                     * 0 for pending
                     * 1 for completed
                     * 2 for cancelled
                     * 3 for return replace
                     **/

                    /*bundle.putString(
                        keyOrderId,
                        if (push?.refKey.isNullOrEmpty()) "-1" else push?.refKey
                    )
                    bundle.putInt(keyOrderType, -1)*/
                }

                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, MyOrderFragment(), MyOrderFragment::class.java.name)
                    .addToBackStack(null)
                    .commit()
            } else if (push?.type.equals("WalletRecharge", true)) {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, MyWalletFragment(), MyWalletFragment::class.java.name)
                    .addToBackStack(null)
                    .commit()
            } else {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, MainFragment(), MainFragment::class.java.name)
                    .addToBackStack(null)
                    .commit()

//                navigateTo(MainFragment(), MainFragment::class.java.name, true)
            }
        } else {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, MainFragment(), MainFragment::class.java.name)
                .addToBackStack(null)
                .commit()

        }


        /*try {
            getVersionInfo()
        } catch (e: Exception) {
            e.printStackTrace()
        }*/

//        val navigationViewHeaderView = navigation.getHeaderView(0)
        profileImageview = navigation.findViewById(R.id.svUserProfile)
        tvUserName = navigation.findViewById(R.id.tvUserName) as AppCompatTextView
        tvUserEmail = navigation.findViewById(R.id.tvUserEmail) as AppCompatTextView

        navigation.tvUserName.text = sessionManager.get_Authenticate_User().userFullName
        navigation.tvUserEmail.text = sessionManager.get_Authenticate_User().userEmail

        navigation.ll_profile_navigation.setOnClickListener {
            mDrawerLayout.closeDrawers()
            hideCategoryListingView()
            if (getCurrentFragment() !is MyAccountFragment) {
                navigateTo(MyAccountFragment(), MyAccountFragment::class.java.name, true)
                true
            }
        }

        navigation.navHome.setOnClickListener(this)
        navigation.navContactUs.setOnClickListener(this)
        navigation.navHome.setOnClickListener(this)
        navigation.navMyCart.setOnClickListener(this)
        navigation.navMyOrders.setOnClickListener(this)
        navigation.navReturn_Replace.setOnClickListener(this)
        navigation.navMyWallet.setOnClickListener(this)
        navigation.navNotifications.setOnClickListener(this)
        navigation.navReferAndEarn.setOnClickListener(this)
        navigation.navRateTheApp.setOnClickListener(this)
        navigation.navFAQ.setOnClickListener(this)
        navigation.navTermsConditions.setOnClickListener(this)
        navigation.navPrivacyPolicy.setOnClickListener(this)
        navigation.navContactUs.setOnClickListener(this)
        navigation.navLogout.setOnClickListener(this)

        try {
            setheaderData()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        bottom_navigation?.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home ->
                    navigation.navHome.performClick()
                R.id.menu_MyCart -> {
                    navigation.navMyCart.performClick()

                }
                R.id.menu_MyOrder ->
                    navigation.navMyOrders.performClick()
                R.id.menu_ReferAndEarn ->

                    navigation.navReferAndEarn.performClick()

                else -> {
                    false
                }
            }
        }
        setCartBadgeCount()
        //addBadgeCartBottomMenu()

        drawertoggle = setupDrawerToggle()
        mDrawerLayout.addDrawerListener(drawertoggle!!)
        drawertoggle?.syncState()
        drawertoggle?.setHomeAsUpIndicator(R.drawable.menu_hamburger_icon)
        drawertoggle?.isDrawerIndicatorEnabled = false
        drawertoggle?.toolbarNavigationClickListener = View.OnClickListener {
            var currentFragment = getCurrentFragment()
            if (currentFragment is ChangePasswordFragment || currentFragment is SettingsFragment || currentFragment is MyProfileFragment || currentFragment is FavoritesStore_StichingStoreFragment || currentFragment is FavoritesProductListingFragment
                || currentFragment is MyWalletRechargeAmountFragment || currentFragment is MyWalletFlyMyOwnMoneyFragment
                || currentFragment is ReturnReplaceDetailsFragment
            ) {

                val fm = fragmentManager
                if (fm.backStackEntryCount > 0) {
                    Log.i("MainActivity", "popping backstack")
                    fm.popBackStack()
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super")
                    onBackPressed()
                }
            } else {
                mDrawerLayout.openDrawer(navigation)
            }
        }

        listViewLinearlayout?.visibility = View.GONE

        list_Search?.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->

            }

        /*Handler().post {
            if (intent != null && intent.hasExtra("Push")) {
                navigateTo(NotificationFragment(), NotificationFragment::class.java.name, true)
            } else {
                navigateTo(MainFragment(), MainFragment::class.java.name, true)
                navigation.setCheckedItem(R.id.navHome)
            }
        }*/


        if (sessionManager.isLoggedIn() && FirebaseInstanceId.getInstance().token != null) {
            updateDeviceToken()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.header_menu, menu)
        menuNotification = menu?.findItem(R.id.menu_notification);


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_notification -> {
                navigateTo(
                    NotificationFragment(),
                    NotificationFragment::class.java!!.getName(),
                    true
                )

                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

/*
    fun addBadgeCartBottomMenu() {
        val item1 = AHBottomNavigationItem("Home", R.drawable.footer_icon_home_black, R.color.white)
        val item2 = AHBottomNavigationItem("My Cart", R.drawable.footer_icon_my_bookings_black, R.color.white)
        val item3 = AHBottomNavigationItem("My Orders", R.drawable.footer_icon_my_orders_black, R.color.white)
        val item4 = AHBottomNavigationItem("Refer and Earn", R.drawable.footer_icon_account_black, R.color.white)

        // Add items
        bottomnavigation?.addItem(item1)
        bottomnavigation?.addItem(item2)
        bottomnavigation?.addItem(item3)
        bottomnavigation?.addItem(item4)
        // Change colors
        bottomnavigation?.defaultBackgroundColor = Color.parseColor("#FFFFFF")
        bottomnavigation?.accentColor = this@MainActivity.resources.getColor(R.color.bottommenuselection_1)
        bottomnavigation?.inactiveColor = this@MainActivity.resources.getColor(R.color.bottommenudeselection_1)
//        bottomnavigation?.setNotificationBackgroundColor(Color.parseColor("#09b256"))
        bottomnavigation?.titleState = AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE
        bottomnavigation?.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW

        // Use colored navigation with circle reveal effect
//        bottomnavigation?.setColored(true);

        // Force to tint the drawable (useful for font with icon for example)

        bottomnavigation?.isForceTint = true
        bottomnavigation?.setNotification("", 1)

        // Set listeners
        bottomnavigation?.setOnTabSelectedListener(AHBottomNavigation.OnTabSelectedListener { position, wasSelected ->
            // Do something cool here...

            when (position) {
                0 ->
                    // Switch to page one
                    if (getCurrentFragment() !is MainFragment) {
                        val frag =
                            supportFragmentManager.findFragmentByTag(MainFragment::class.java.name)


                        if (frag != null && frag is MainFragment) {
                            navigateTo(frag, MainFragment::class.java.name, true)
//                            replaceFragment(frag, HomeFragment::class.java!!.getName(), true)

                        } else {
                            navigateTo(MainFragment(), MainFragment::class.java.name, true)
//                            replaceFragment(HomeFragment(), HomeFragment::class.java!!.getName(), true)
                        }
                    }
                1 ->
                    // Switch to page two
                    if (getCurrentFragment() !is MyCartFragment) {
                        navigateTo(MyCartFragment(), MyCartFragment::class.java.name, true)
                    }
                2 -> if (getCurrentFragment() !is MyOrderFragment) {

                    navigateTo(MyOrderFragment(), MyOrderFragment::class.java.name, true)

                }
                3 ->
                    // Switch to page three
                    if (getCurrentFragment() !is MyAccountFragment) {

                        navigateTo(MyAccountFragment(), MyAccountFragment::class.java.name, true)
                    }
            }
            true
        })

    }
*/

    private fun setheaderData() {

        if (userData != null) {
            tvUserName?.text = userData?.userFullName
            tvUserEmail?.text = userData?.userEmail
            profileImageview?.setImageURI(userData?.userProfilePicture, this@MainActivity)

        }
    }

    fun hideCategoryListingView() {

        if (listViewLinearlayout?.visibility == View.VISIBLE)
            listViewLinearlayout?.visibility = View.GONE

        listViewLinearlayout?.visibility = View.GONE
        list_Search?.visibility = View.GONE
        searchkeyword = ""

    }

    fun setDrawerSwipe(b: Boolean) {
        if (b) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }

    fun selectBottomNavigationOption() {

//        bottom_navigation?.selectedItemId = position
        bottom_navigation?.setSelectedItemId(R.id.menu_ReferAndEarn)
    }

    fun SelectionNavigation(str: String) {
        if (str.equals("WalletRecharge") || str.equals("ReferAndEarn")) {
            navigation.navMyWallet.performClick()
        } else if (str.equals("Order")) {
            navigation.navMyOrders.performClick()
        }

    }

    fun handleSelection() {
        if (navigation != null) {
            val fragment = getCurrentFragment()
            val size = navigation.menu.size()
            for (i in 0 until size) {
                navigation.menu.getItem(i).isChecked = false
            }

            if (fragment is MainFragment) {
                navigation.menu.findItem(R.id.navHome).isChecked = true
            } else if (fragment is MyCartFragment) {
                navigation.menu.findItem(R.id.navMyCart).isChecked = true
            } else if (fragment is MyOrderFragment) {
                navigation.menu.findItem(R.id.navMyOrders).isChecked = true
            } else if (fragment is ReturnReplaceFragment) {
                navigation.menu.findItem(R.id.navReturn_Replace).isChecked = true
            } else if (fragment is MyWalletFragment) {
                navigation.menu.findItem(R.id.navMyWallet).isChecked = true
            } else if (fragment is ReferralFragment) {
                navigation.menu.findItem(R.id.navReferAndEarn).isChecked = true
            } else if (fragment is NotificationFragment) {
                navigation.menu.findItem(R.id.navNotifications).isChecked = true
            } else if (fragment is FAQFragment) {
                navigation.menu.findItem(R.id.navFAQ).isChecked = true
            } else if (fragment is TermsandConditionsFragment) {
                navigation.menu.findItem(R.id.navTermsConditions).isChecked = true
            } else if (fragment is PrivacyPolicyFragment) {
                navigation.menu.findItem(R.id.navPrivacyPolicy).isChecked = true
            } else if (fragment is ContactUsFragment) {
                navigation.menu.findItem(R.id.navContactUs).isChecked = true
            } else if (fragment is ContactUsFragment) {
                navigation.menu.findItem(R.id.navContactUs).isChecked = true
            }

        }
    }

    private fun setupDrawerToggle(): ActionBarDrawerToggle {
        return object :
            ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close
            ) {

        }
    }

    override fun navigateTo(fragment: Fragment, tag: String, addToBackstack: Boolean) {

        val transaction = supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_out_right,
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            .replace(R.id.container, fragment, tag)

        if (addToBackstack) {
            transaction.addToBackStack(null)
        }

        transaction.commit()

    }

    override fun navigateTo(
        fragment: Fragment,
        bundle: Bundle,
        tag: String,
        addToBackstack: Boolean
    ) {

        fragment.arguments = bundle
        val transaction = supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_out_right,
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            .replace(R.id.container, fragment, tag)

        if (addToBackstack) {
            transaction.addToBackStack(null)
        }

        transaction.commit()

    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)
    }

    fun openDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START)
    }

    fun showSnackBar(message: String) {
        if ((snackBarParent != null) and !isFinishing)
            Snackbar.make(this.snackBarParent!!, message, Snackbar.LENGTH_LONG).show()

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawertoggle?.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawertoggle?.onConfigurationChanged(newConfig)
    }

    private fun addViewSnackBar() {
        snackBarParent = View(this)
        val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 200)
        layoutParams.gravity = Gravity.BOTTOM
        snackBarParent!!.layoutParams = layoutParams
        container.addView(snackBarParent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onActivityResult(requestCode, resultCode, data)


    }

    fun setCartBadgeCount() {
        val badgeDrawable = bottom_navigation?.getOrCreateBadge(R.id.menu_MyCart)
        cartItemCount = cartCalculate?.getCartItems(this@MainActivity)!!.size



        if (cartItemCount > 0) {
            /* tvCartCount?.visibility = View.VISIBLE
             tvCartCount?.text = */
            badgeDrawable?.number = cartItemCount
            badgeDrawable?.isVisible = true
            badgeDrawable?.backgroundColor = resources.getColor(R.color.colorPrimary)
            // setCartBadge(cartItemCount.toString())

        } else {
            // tvCartCount?.visibility = View.GONE
            badgeDrawable?.isVisible = false

        }
    }


    private fun setCartBadge(value: String) {

        // bottom_navigation?.removeAllViews()
        /* var bottomNavigationMenuView = bottom_navigation?.getChildAt(1) as BottomNavigationView

             var v = bottomNavigationMenuView?.getChildAt(2)
     //        var itemView = v as BottomNavigationView

             var badge = LayoutInflater.from(this@MainActivity)
                 .inflate(R.layout.cart_badge, bottomNavigationMenuView , true)

             var text = badge.findViewById<AppCompatTextView>(R.id.cart_badge)
             text.setText("2")

             bottomNavigationMenuView .addView(badge)*/
    }


    fun getVersionInfo() {
        var versionName: String = ""
        var versionCode: Int = -1
        try {
            var packageInfo = packageManager.getPackageInfo(packageName, 0)
            versionName = packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
//        Log.e("System Out", "Version Code: "+String.format("V. %s", versionName))
//        Log.d("System Out", "Version Code: "+String.format("V. %s", versionName))
    }

    private fun updateDeviceToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                    val newtoken = instanceIdResult.token

                    Log.d(TAG, "new token:= " + instanceIdResult.token)
                    updateDeviceTokenApi(newtoken, false)
                }
            })
    }

    private fun updateDeviceTokenApi(token: String, bolPass: Boolean) {

        var jsonArray = JSONArray()
        var jsonObject = JSONObject()
        /*[{
            "languageID": "1",
            "loginuserID": "1",
            "userDeviceType": "Android",
            "userDeviceID": "cxzczxczxzxcxccc",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/
        try {
            val data = sessionManager.get_Authenticate_User()
            jsonObject.put("languageID", data.languageID)
            jsonObject.put("loginuserID", data.userID)
            jsonObject.put("userDeviceID", token)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System out", "DeviceToken JSON Array:=  " + jsonArray.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val updateDeviceTokenModel =
            ViewModelProviders.of(this@MainActivity).get(UpdateDeviceTokenModel::class.java)

        updateDeviceTokenModel.getupdateDeviceTokenApi(
            this@MainActivity,
            bolPass,
            jsonArray.toString()
        )
            .observe(this@MainActivity, object : Observer<List<RegisterNewPojo>> {
                override fun onChanged(@Nullable updateDeviceTokens: List<RegisterNewPojo>?) {

                    if (updateDeviceTokens != null && updateDeviceTokens is ArrayList<*> && updateDeviceTokens.size > 0) {

                        if (updateDeviceTokens[0].status.equals("true", true)) {

                            if (token.length == 0) {
                                val sessionManager = SessionManager(this@MainActivity)
                                sessionManager.clear_login_session()
//                                PrefDb(this@MainActivity).cleanUserLoction()
                                var intent = Intent(this@MainActivity, LoginActivity::class.java)
                                startActivity(intent)
                                this@MainActivity.finishAffinity()
                            } else {
                                if (updateDeviceTokens[0].data!!.size > 0) {
                                    sessionManager.clear_login_session()
                                    storeSessionManager(updateDeviceTokens[0].data!!)
                                } else {
//                                    CartCalculateNew.clearCartItems()
                                    sessionManager.sessionExpire(
                                        this@MainActivity,
                                        DialogInterface.OnClickListener { dialogInterface, i ->
                                            sessionManager.clear_login_session()
                                            var intent =
                                                Intent(this@MainActivity, LoginActivity::class.java)
                                            startActivity(intent)
                                            this@MainActivity.finishAffinity()
                                        })
                                }
                            }
                        } else {
                            if (token.length > 0) {

                                sessionManager.sessionExpire(
                                    this@MainActivity,
                                    DialogInterface.OnClickListener { dialogInterface, i ->
                                        //                                        PrefDb(this@MainActivity).clearValue("ServiceNotProvide")
                                        //                                    PrefDb(this@MainActivity).userFirstTime(true)
                                        //                                        CartCalculateNew.clearCartItems()
                                        sessionManager.clear_login_session()
                                        //                                        PrefDb(this@MainActivity).cleanUserLoction()
                                        var intent =
                                            Intent(this@MainActivity, LoginActivity::class.java)
                                        startActivity(intent)
                                        this@MainActivity.finishAffinity()
                                    })
                            } else {
                                showSnackBar("" + getString(R.string.error_crash_error_message))

                            }

                        }

                    } else {
                        showSnackBar("" + getString(R.string.error_crash_error_message))

                    }
                }
            })
    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()
        val json = gson.toJson(driverdata[0])
        sessionManager.create_login_session(
            json,
            driverdata[0].userEmail!!,
            "",
            true,
            sessionManager.isEmailLogin()
        )
    }

    private fun showexit() {
        //Store Cart Data before exit app
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
            return
        }
        doubleBackToExitPressedOnce = true

        showSnackBar("To exit, press back again.")
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 3000)

    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers()
        } else {
            val manager = supportFragmentManager

            if (fromPush) {
                fromPush = false
                navigateTo(MainFragment(), MainFragment::class.java.name, true)
            } else if (manager.backStackEntryCount >= 1) {
                val f = supportFragmentManager.findFragmentById(R.id.container)
                if (manager.backStackEntryCount === 1) {
                    if (f != null && f is MainFragment) {
                        showexit()
                    } else {
                        navigateTo(MainFragment(), MainFragment::class.java.name, true)

                    }
                } else if (f != null && f is MainFragment) {
                    showexit()
                } else if (f != null && f is MyOrderFragment) {
                    bottom_navigation?.selectedItemId = R.id.menu_home
                } else
                    manager.popBackStack()
            } else {
                showexit()
            }
        }

    }

    fun setTitle1(stringResId: Int) {
        toolbar_title!!.text = this@MainActivity.resources.getString(stringResId)
    }

    fun setTitle1(string: String?) {
        toolbar_title!!.text = string
    }

    fun showMessageOKCancel(
        context: Context,
        message: String,
        okListener: DialogInterface.OnClickListener
    ): AlertDialog {
        val builder = MaterialAlertDialogBuilder(context)
        builder.setMessage(message)
        builder.setCancelable(false)
        builder.setPositiveButton(this@MainActivity.resources.getString(R.string.yes), okListener)

        builder.setNegativeButton(
            this@MainActivity.resources.getString(R.string.no)
        ) { dialog, which -> dialog.dismiss() }
        val alert = builder.create()
//        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show()

        return alert
    }


    fun setToolBar(toolbar: Toolbar?) {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)


    }

    fun updateNavigationBarState(actionId: Int) {
        bottom_navigation?.menu?.findItem(actionId)?.isChecked = true

        // showBadge()


    }

    /* fun updateNavigationBarState(actionId: Int) {
         bottom_navigation?.menu?.findItem(actionId)?.isChecked = true

         showBadge()

     }
     fun showBadge() {
         doAsync {
             var myItemList = db?.orderLog()?.getAll()!! as ArrayList<Orderdetail>
             uiThread {
                 if (myItemList.size == 0) {
                     bottom_navigation?.removeBadge(R.id.cart_Menu_Footer)
                     myCartCount.visibility = View.GONE
                 } else {
                     var badgeDrawable: BadgeDrawable? = bottom_navigation?.getOrCreateBadge(R.id.cart_Menu_Footer)
                     badgeDrawable?.number = myItemList.size
                     badgeDrawable?.backgroundColor = ContextCompat.getColor(this@MainActivity, R.color.colorPrimary)
                     myCartCount.visibility = View.VISIBLE
                     myCartCount.text = myItemList.size.toString()
                 }

             }
         }
     }*/

    override fun onClick(v: View?) {
        // close drawer when item is tapped
        mDrawerLayout.closeDrawers()
        val currentFargment = getCurrentFragment()
        when (v!!.id) {
            R.id.navHome -> {
                if (currentFargment !is MainFragment)
                    navigateTo(MainFragment(), MainFragment::class.java.name, true)
            }
            R.id.navMyCart -> {
                if (currentFargment !is MyCartFragment)
                    navigateTo(MyCartFragment(), MyCartFragment::class.java.name, true)
            }
            R.id.navMyOrders -> {
                if (currentFargment !is MyOrderFragment)
                    navigateTo(MyOrderFragment(), MyOrderFragment::class.java.name, true)
            }
            R.id.navReturn_Replace -> {
                if (currentFargment !is ReturnReplaceFragment)
                    navigateTo(
                        ReturnReplaceFragment(),
                        ReturnReplaceFragment::class.java.name,
                        true
                    )
            }

            R.id.navMyWallet -> {
                if (currentFargment !is MyWalletFragment)
                    navigateTo(MyWalletFragment(), MyWalletFragment::class.java.name, true)
            }

            R.id.navNotifications -> {
                if (currentFargment !is NotificationFragment)
                    navigateTo(NotificationFragment(), NotificationFragment::class.java.name, true)
            }

            R.id.navReferAndEarn -> {

                if (currentFargment !is ReferralFragment)
                    navigateTo(ReferralFragment(), ReferralFragment::class.java.name, true)

            }
            R.id.navRateTheApp -> {
                val appPackageName = packageName // getPackageName() from Context or Activity object
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }

            }
            R.id.navFAQ -> {
                if (currentFargment !is FAQFragment)
                    navigateTo(FAQFragment(), FAQFragment::class.java.name, true)
            }
            R.id.navTermsConditions -> {
                if (currentFargment !is TermsandConditionsFragment) {
                    var bundle = Bundle()
                    bundle.putString("Type", "Terms")
                    navigateTo(
                        TermsandConditionsFragment(),
                        bundle,
                        TermsandConditionsFragment::class.java.name,
                        true
                    )
                }


            }
            R.id.navPrivacyPolicy -> {
                if (currentFargment !is PrivacyPolicyFragment) {
                    var bundle = Bundle()
                    bundle.putString("Type", "PrivacyPolicy")
                    navigateTo(
                        PrivacyPolicyFragment(),
                        bundle,
                        PrivacyPolicyFragment::class.java.name,
                        true
                    )
                }
            }
            R.id.navContactUs -> {
                if (currentFargment !is ContactUsFragment){
                    var bundle = Bundle()
                    bundle.putString("Type", "ContactUS")
                    navigateTo(ContactUsFragment(),bundle ,ContactUsFragment::class.java.name, true)
                }
            }
            R.id.navLogout -> {
                showMessageOKCancel(this@MainActivity,
                    resources.getString(R.string.are_you_sure_logout),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                        /*val sessionManager = SessionManager(this@MainActivity)
//                            PrefDb(this@MainActivity).clearValue("ServiceNotProvide")
                        sessionManager.clear_login_session()

                    val myIntent = Intent(this@MainActivity, LoginActivity::class.java)
                    startActivity(myIntent)
                    finishAffinity()
                    (this@MainActivity as Activity).overridePendingTransition(
                        R.anim.slide_in_right,
                        R.anim.slide_out_left
                    )*/
                        updateDeviceTokenApi("", true)
                    })
            }

        }
        false

    }

    fun errorMethod() {
        try {
            if (MyUtils.isInternetAvailable(this@MainActivity)) {
                MyUtils.showSnackbarkotlin(
                    this@MainActivity,
                    signupLayoutMain!!,
                    resources.getString(R.string.error_crash_error_message)
                )
            } else {
                MyUtils.showSnackbarkotlin(
                    this@MainActivity,
                    signupLayoutMain!!,
                    resources.getString(R.string.error_common_network)
                )
            }
        } catch (e: Exception) {
        }
    }
    /*override fun onSelectedDataPass(from: String, selectedName: String) {
        var frag: Fragment?=null
           frag=supportFragmentManager.findFragmentByTag(ProductDetailsFragment::class.java.name)

        if (frag != null && frag is ProductDetailsFragment)
            frag.ProductByData(from,selectedName)


    }*/

    fun selectBottomNavigationOption1() {

//        bottom_navigation?.selectedItemId = position
        bottom_navigation?.selectedItemId = R.id.menu_MyOrder
    }
}
