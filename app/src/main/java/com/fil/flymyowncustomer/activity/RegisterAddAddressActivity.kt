package com.fil.flymyowncustomer.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.*
import android.util.DisplayMetrics
import android.util.Log
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.util.GeocodingLocation
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.fragment.BottomSheetListFragment
import com.fil.flymyowncustomer.model.AddAddressModel
import com.fil.flymyowncustomer.model.StateListModel
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register_add_address.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class RegisterAddAddressActivity : AppCompatActivity() , BottomSheetListFragment.SelectLanguage{
    override fun onLanguageSelect(value: String, from: String) {
        when (from) {
            "state" -> {
                adAdressEditState.text = value
                for (i in 0 until statelist!!.size) {
                    if (value.equals(statelist!![i]!!.stateName, false)) {
                        stateID = statelist!![i]!!.stateID!!
                        break
                    }


                }
            }
            "gststate" -> {
                adAdressEditStateOfGST.text = value
                for (i in 0 until statelist!!.size) {
                    if (value.equals(statelist!![i]!!.stateName, false)) {
                        gstStateId = statelist!![i]!!.stateID!!
                        break
                    }


                }
            }
        }

    }

    var sessionManager: SessionManager? = null
    var stateID = ""
    var gstStateId = ""
    var statelist: ArrayList<StatelisData?>? = null
    var billingAddressData:BillingAddressData?=null
    var from=""
    var addressLate = "0.0"
    var addressLong = "0.0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_add_address)
        sessionManager = SessionManager(this@RegisterAddAddressActivity)

        setSupportActionBar(toolbar_back)

        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        statelist = ArrayList()


        toolbar_back.setNavigationOnClickListener {
            onBackPressed()
        }
        if(intent!=null)
        {
            if(intent.hasExtra("billingAddressData"))
            {
                billingAddressData= intent.getSerializableExtra("billingAddressData") as BillingAddressData?
                setEditData()
            }
            if(intent.hasExtra("billingAddressDataCurrentLocation"))
            {
                billingAddressData= intent.getSerializableExtra("billingAddressDataCurrentLocation") as BillingAddressData?

                setCurrentData()
            }
            if(intent.hasExtra("from"))
            {
                from= intent.getStringExtra("from")

            }
        }
        if(from.equals("updateAddress",false))
        {
            tvToolbarTitel.text = resources.getString(R.string.edit_address)
        }
        else
        {
            tvToolbarTitel.text = resources.getString(R.string.add_address)
        }

        adAdressEditGstNumber?.setFilters(arrayOf(InputFilter.LengthFilter(15), InputFilter.AllCaps()))
        adAdressEditAddress?.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        adAdressEditAddress?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {


            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {


            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {


                if (adAdressEditAddress?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditAddress?.text.toString().length)

                        for (i in 0 until adAdressEditAddress?.text.toString().length) {
                            x = adAdressEditAddress?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z in 48..57 || z == 45 || z == 47 || z == 32) {

                            } else {
                                Toast.makeText(
                                    this@RegisterAddAddressActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                var ss = adAdressEditAddress?.text.toString()
                                    .substring(0, adAdressEditAddress?.text.toString().length - 1)
                                adAdressEditAddress?.setText(ss)
                                adAdressEditAddress?.setSelection(adAdressEditAddress?.text.toString().length)


                            }

                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {

                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        adAdressEditCity?.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        adAdressEditCity?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (adAdressEditCity?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditCity?.text.toString().length)
//                        searchKeyWord = adAdressEditCity.text.toString()
//                        getCityList()
                        for (i in 0 until adAdressEditCity?.text.toString().length) {
                            x = adAdressEditCity?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z == 32) {
//                        searchKeyWord = adAdressEditCity.text.toString()
//                        getCityList()
                            }
                            else {
                                Toast.makeText(this@RegisterAddAddressActivity, "" + "Special Character not allowed", Toast.LENGTH_SHORT).show()
                                val ss = adAdressEditCity?.text.toString().substring(0,adAdressEditCity?.text.toString().length - 1)
                                adAdressEditCity?.setText(ss)
                                adAdressEditCity?.setSelection(adAdressEditCity?.text.toString().length)


                            }

                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {

                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        adAdressEditLocality?.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        adAdressEditLocality?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (adAdressEditLocality?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditLocality?.text.toString().length)

                        for (i in 0 until adAdressEditLocality?.text.toString().length) {
                            x = adAdressEditLocality?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z == 32) {

                            } else {
                                Toast.makeText(
                                    this@RegisterAddAddressActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                var ss = adAdressEditLocality?.text.toString()
                                    .substring(0, adAdressEditLocality?.text.toString().length - 1)
                                adAdressEditLocality?.setText(ss)
                                adAdressEditLocality?.setSelection(adAdressEditLocality?.text.toString().length)


                            }

                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {

                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        adAdressEditLandmark?.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        adAdressEditLandmark?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (adAdressEditLandmark?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditLandmark?.text.toString().length)

                        for (i in 0 until adAdressEditLandmark?.text.toString().length) {
                            x = adAdressEditLandmark?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z == 32 || z in 48..57 || z == 45 || z == 47) {

                            } else {
                                Toast.makeText(
                                    this@RegisterAddAddressActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                var ss = adAdressEditLandmark?.text.toString()
                                    .substring(0, adAdressEditLandmark?.text.toString().length - 1)
                                adAdressEditLandmark?.setText(ss)
                                adAdressEditLandmark?.setSelection(adAdressEditLandmark?.text.toString().length)


                            }

                        }
                    } catch (e: IndexOutOfBoundsException) {

                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        adAdressEditGstCompanyName?.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        adAdressEditGstCompanyName?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            @SuppressLint("LongLogTag")
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (adAdressEditGstCompanyName?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditGstCompanyName?.text.toString().length)

                        for (i in 0 until adAdressEditGstCompanyName?.text.toString().length) {
                            x = adAdressEditGstCompanyName?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z == 32 || z in 48..57 ||  z == 46) {

                            } else {
                                Toast.makeText(
                                    this@RegisterAddAddressActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val ss = adAdressEditGstCompanyName?.text.toString()
                                    .substring(0, adAdressEditGstCompanyName?.text.toString().length - 1)
                                adAdressEditGstCompanyName?.setText(ss)
                                adAdressEditGstCompanyName?.setSelection(adAdressEditGstCompanyName?.text.toString().length)


                            }

                        }
                    } catch (e: IndexOutOfBoundsException) {

                        Log.d("System out", "IndexOutOfBoundsException : " + e.toString())
                    }
                }
            }
        })

        adAdressEditGstNumber?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (adAdressEditGstNumber?.text.toString().length > 0) {

                    try {

                        var x: Char
                        val t = IntArray(adAdressEditGstNumber?.text.toString().length)

                        for (i in 0 until adAdressEditGstNumber?.text.toString().length) {
                            x = adAdressEditGstNumber?.text.toString().toCharArray()[i]
                            val z = x.toInt()
                            t[i] = z

                            if (z in 65..90 || z in 97..122 || z == 32 || z in 48..57) {

                            } else {
                                Toast.makeText(
                                    this@RegisterAddAddressActivity,
                                    "" + "Special Character not allowed",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val ss = adAdressEditGstNumber?.text.toString()
                                    .substring(0, adAdressEditGstNumber?.text.toString().length - 1)
                                adAdressEditGstNumber.setText(ss)
                                adAdressEditGstNumber.setSelection(adAdressEditGstNumber?.text.toString().length)
                            }

                        }
                    } catch (e: IndexOutOfBoundsException) {
                        Log.d("System out", "IndexOutOfBoundsException" + e.toString())
                    }
                }
            }
        })



        adAdressEditState?.setOnClickListener {
            /* MyUtils.hideKeyboardFrom(this@RegisterAddAddressActivity, adAdressEditState)
             var arrayState = ArrayList<String>()

             arrayState.add("Andhra Pradesh")
             arrayState.add("Arunachal Pradesh")
             arrayState.add("Assam")
             arrayState.add("Bihar")
             arrayState.add("Chhattisgarh")
             arrayState.add("Goa")
             arrayState.add("Gujarat")
             arrayState.add("Haryana")

             showDropDownLayout(arrayState!!, adAdressEditState, 0)*/
             if(statelist.isNullOrEmpty())
             {
                 getStateList("state")
             }else
             {
                 openCityListBottomSheet(statelist,"state")
             }

        }

        adAdressEditStateOfGST?.setOnClickListener {
            /* MyUtils.hideKeyboardFrom(this@RegisterAddAddressActivity, adAdressEditStateOfGST)
             var arrayState = ArrayList<String>()

             arrayState.add("Andhra Pradesh")
             arrayState.add("Arunachal Pradesh")
             arrayState.add("Assam")
             arrayState.add("Bihar")
             arrayState.add("Chhattisgarh")
             arrayState.add("Goa")
             arrayState.add("Gujarat")
             arrayState.add("Haryana")

             showDropDownLayout(arrayState!!, adAdressEditStateOfGST, 1)*/
            if(statelist.isNullOrEmpty())
            {
                getStateList("gststate")
            }else
            {
                openCityListBottomSheet(statelist,"gststate")
            }
        }

        btnRegisterAddAddressSave?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@RegisterAddAddressActivity, btnRegisterAddAddressSave)
            if (checkValidation()) {
                getLateLong()
                addAddress(from)
            }
        }

    }

    private fun setCurrentData() {
        billingAddressData.apply {
            adAdressEditAddress.setText(this!!.addressAddressLine1!!)

        }
    }

    private fun setEditData() {

          billingAddressData.apply {
              adAdressEditAddress.setText(this!!.addressAddressLine1!!)
              adAdressEditState.text=stateName
              stateID= this.stateID
              gstStateId=addressGSTStateID!!
              adAdressEditCity.setText(addressAddressLine2)
              adAdressEditLocality.setText(addressLocality)
              adAdressEditLandmark.setText(addressLandmark)
              adAdressEditPin.setText(addressPincode)
              adAdressEditGstCompanyName.setText(addressGSTCompany)
              adAdressEditGstNumber.setText(addressGST)
              adAdressEditStateOfGST.text = addressGSTState
              addressLate=addressLatitude!!
              addressLong=addressLongitude!!
           }

    }

    private fun checkValidation(): Boolean {
        var flag = true

        if (adAdressEditAddress?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_address)
            )
        } else if (!adAdressEditAddress?.text.toString().isNullOrEmpty() && adAdressEditAddress?.text.toString().length < 15) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_address_min_char)
            )
        } else if (adAdressEditState?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_state)
            )
        } else if (adAdressEditCity?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_city)
            )
        } else if (adAdressEditLocality?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_locality)
            )
        } else if (!adAdressEditLocality?.text.toString().isNullOrEmpty() && adAdressEditLocality?.text.toString().length < 3) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_locality_min_char)
            )
        } else if (!adAdressEditLandmark?.text.toString().isNullOrEmpty() && adAdressEditLandmark?.text.toString().length < 3) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_landmark_min_char)
            )
        } else if (adAdressEditPin?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_pincode)
            )
        }
        else if (!adAdressEditPin?.text.toString().isNullOrEmpty() && adAdressEditPin?.text.toString().length < 6) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_add_pincode_min_chare)
            )
        }
        /*else if (adAdressEditGstCompanyName?.text.toString().isNullOrEmpty()) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_gst_company_name)
            )
        }
        */else if (!adAdressEditGstCompanyName?.text.toString().trim().isNullOrEmpty() && adAdressEditGstCompanyName?.text.toString().length < 3) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.err_gst_company_name_min_char)
            )
        } else if (!adAdressEditGstNumber?.text.toString().trim().isNullOrEmpty() && adAdressEditGstNumber?.text.toString().length < 15) {
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                this@RegisterAddAddressActivity.resources.getString(R.string.reg_err_gstnumber_validation)
            )
            flag = false
        } else if (!TextUtils.isEmpty(adAdressEditGstNumber?.text.toString().trim()) && MyUtils.validGSTIN(
                adAdressEditGstNumber?.text.toString().trim()
            ) == false
        ) {
            flag = false
            MyUtils.showSnackbarkotlin(
                this@RegisterAddAddressActivity,
                rootRegisterAddAddressMainLayout,
                resources.getString(R.string.reg_eror_correct_gst_num)
            )

        }else if(!adAdressEditGstCompanyName?.text.toString().trim().isNullOrEmpty() && !adAdressEditGstNumber?.text.toString().trim().isNullOrEmpty()){
            if (adAdressEditStateOfGST?.text.toString().isNullOrEmpty()) {
                flag = false
                MyUtils.showSnackbarkotlin(
                    this@RegisterAddAddressActivity,
                    rootRegisterAddAddressMainLayout,
                    resources.getString(R.string.err_gst_state_of_GST)
                )
            }
        }

        return flag
    }

    private fun showDropDownLayout(
        arrayDataList: ArrayList<String>,
        editView: AppCompatTextView, type: Int
    ) {

        val displayMetrics = DisplayMetrics()
        this@RegisterAddAddressActivity.windowManager.defaultDisplay.getMetrics(displayMetrics)

        /*PopupMenu(this@RegisterAddAddressActivity, editView, arrayDataList).showPopUp(
            object : PopupMenu.OnMenuSelectItemClickListener {
                override fun onItemClick(item: String) {
                    editView?.text = item

                    when(type){
                        0 -> {

                            *//*for (i in 0 until arrayStateList!!.size){
                                if (arrayStateList!![i].stateName.equals(item, true)){
                                    selectedStateId = arrayStateList!![i].stateID!!
                                }
                            }*//*
                        }
                        1 -> {

                        *//*for (i in 0 until arrayStateList!!.size){
                            if (arrayStateList!![i].stateName.equals(item, true)){
                                selectedStateId = arrayStateList!![i].stateID!!
                            }
                        }*//*
                    }
                    }
                }
            })*/
    }

    override fun onBackPressed() {
        MyUtils.finishActivity(this@RegisterAddAddressActivity, true)
    }

    private fun addAddress(from: String) {
        var userDate = sessionManager!!.get_Authenticate_User()
        btnRegisterAddAddressSave.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootRegisterAddAddressMainLayout, false)

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userDate.userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("addressAddressLine1", adAdressEditAddress.text.toString())
            jsonObject.put("stateName", adAdressEditState.text.toString().trim())

            if(from.equals("updateAddress",false))
            {
                jsonObject.put("addressID", billingAddressData!!.addressID)
                jsonObject.put("countryID", billingAddressData!!.countryID)
                jsonObject.put("stateID", billingAddressData!!.stateID)
                jsonObject.put("addressLatitude", billingAddressData!!.addressLatitude)
                jsonObject.put("addressLongitude", billingAddressData!!.addressLongitude)

            }
            else
            {
                jsonObject.put("stateID",stateID)
                jsonObject.put("addressLatitude", addressLate)
                jsonObject.put("addressLongitude", addressLong)
                jsonObject.put("countryID", "1")
            }
            jsonObject.put("addressAddressLine2", adAdressEditCity.text.toString().trim())
            jsonObject.put("addressLocality", adAdressEditLocality.text.toString().trim())
            jsonObject.put("addressPincode", adAdressEditPin.text.toString().trim())
            jsonObject.put("addressLandmark", adAdressEditLandmark.text.toString().trim())
            jsonObject.put("addressGSTCompany", adAdressEditGstCompanyName.text.toString().trim())
            jsonObject.put("addressGST", adAdressEditGstNumber.text.toString().trim())
            jsonObject.put("addressGSTStateID", gstStateId)
            jsonObject.put("addressGSTStateName", adAdressEditStateOfGST.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonObject.put("addressIsDefault", "Yes")
            jsonObject.put("addressType", "Billing")


        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        val addAddressModel = ViewModelProviders.of(this@RegisterAddAddressActivity).get(
            AddAddressModel::class.java
        )
        addAddressModel.address(
            this@RegisterAddAddressActivity,
            false,
            jsonArray.toString(),
            from
        )
            .observe(this@RegisterAddAddressActivity,
                androidx.lifecycle.Observer<List<BillingAddressPojo>> { addAddresspojo ->
                    if (addAddresspojo != null && addAddresspojo.isNotEmpty()) {
                        if (addAddresspojo[0].status.equals("true", true)) {
                            btnRegisterAddAddressSave.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(
                                rootRegisterAddAddressMainLayout,
                                true
                            )
                            try {
                                val userData = sessionManager!!.get_Authenticate_User()

                                if(from.equals("updateAddress",false)) {
                                    /* for(i in 0 until addAddresspojo[0].data!!.size)
                                     {
                                        if(billingAddressData!!.addressID!!.equals(addAddresspojo[0].data!![i].addressID,false))
                                        {
                                         addAddresspojo[0].data!!.removeAt(i)
                                        }
                                         break
                                     }*/
                                    for (i in 0 until userData.billing!!.size) {
                                        if (billingAddressData!!.addressID!!.equals(userData.billing?.get(i)?.addressID,false)) {
                                            userData?.billing!![i] = addAddresspojo[0].data?.get(0)!!
                                            break
                                        }
                                    }
                                }else
                                {

                                    userData.billing!!.addAll(addAddresspojo[0].data!!)
                                }
                                for (i in 0 until userData.billing!!.size) {
                                    if (addAddresspojo[0].data?.get(0)!!.addressID.equals(
                                            userData.billing!![i].addressID
                                        )
                                    )
                                        userData.billing!![i].addressIsDefault = "Yes"
                                    else
                                        userData.billing!![i].addressIsDefault = "No"
                                }

                                storeSessionManager(userData)

                                showSnackBar(addAddresspojo[0].message!!)
                                var intent= Intent().apply {
                                    putExtra("address",userData.billing)

                                }
                                setResult(122,intent)


                                Handler().postDelayed({

                                    onBackPressed()
                                }, 1000)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                            btnRegisterAddAddressSave.endAnimation()
                            MyUtils.setViewAndChildrenEnabled(
                                rootRegisterAddAddressMainLayout,
                                true
                            )
                            showSnackBar(addAddresspojo[0].message!!)

                        }

                    } else {
                        btnRegisterAddAddressSave.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootRegisterAddAddressMainLayout, true)
                        errorMethod()
                    }
                })
    }

    private fun errorMethod() {
        try {
            if (MyUtils.isInternetAvailable(this@RegisterAddAddressActivity)) {
                MyUtils.showSnackbarkotlin(
                    this@RegisterAddAddressActivity,
                    signupLayoutMain!!,
                    resources.getString(R.string.error_crash_error_message)
                )
            } else {
                MyUtils.showSnackbarkotlin(
                    this@RegisterAddAddressActivity,
                    signupLayoutMain!!,
                    resources.getString(R.string.error_common_network)
                )
            }
        } catch (e: Exception) {
        }
    }


    fun showSnackBar(message: String) {
        if ((rootRegisterAddAddressMainLayout != null) and !isFinishing)
            Snackbar.make(
                this.rootRegisterAddAddressMainLayout!!,
                message,
                Snackbar.LENGTH_LONG
            ).show()

    }


    private fun storeSessionManager(driverdata: RegisterNewPojo.Datum) {


        val gson = Gson()
        val json = gson.toJson(driverdata)
        sessionManager?.create_login_session(
            json,
            driverdata.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!
        )
    }

    fun getStateList(from:String) {
        MyUtils.showProgressDialog(this@RegisterAddAddressActivity)
        var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", userData.userID)
            jsonObject.put("countryID", "1")
            jsonObject.put("searchkeyword", "")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var stateListModel =
            ViewModelProviders.of(this@RegisterAddAddressActivity).get(StateListModel::class.java)
        stateListModel.getStateList(this@RegisterAddAddressActivity, false, jsonArray.toString())
            .observe(this@RegisterAddAddressActivity,
                Observer { countryListPojo ->
                    if (countryListPojo != null && countryListPojo.isNotEmpty()) {

                        if (countryListPojo[0].status.equals("true", false)) {
                            MyUtils.closeProgress()

                            statelist!!.clear()
                            statelist!!.addAll(countryListPojo[0].data!!)
                            openCityListBottomSheet(countryListPojo[0].data!!,from)
                        } else {
                            MyUtils.closeProgress()

                            showSnackBar(countryListPojo[0].message!!)
                        }

                    } else {
                        MyUtils.closeProgress()

                        errorMethod()
                    }
                })
    }

    private fun openCityListBottomSheet(
        data: List<StatelisData?>?,
        from: String
    ) {

        var languagelistDatum1 = ArrayList<String>()
        languagelistDatum1.clear()
        for (i in 0 until data!!.size) {
            languagelistDatum1.add(data[i]!!.stateName!!)
        }
        val bottomSheet = BottomSheetListFragment()
        val bundle = Bundle()
        bundle.putSerializable("data", languagelistDatum1)
        bundle.putString("from", from)
        bottomSheet.arguments = bundle
        bottomSheet.show(supportFragmentManager, "List")


    }


    private fun getLateLong() {
        val addressForLateLong = adAdressEditAddress.text.toString().trim() + ", " +
                adAdressEditLocality.text.toString().trim() + ", " +
                adAdressEditLandmark.text.toString().trim() + ", " +
                adAdressEditCity.text.toString().trim() + ", " +
                adAdressEditState.text.toString().trim() + ", " +
                adAdressEditPin.text.toString().trim()

        val locationAddress = GeocodingLocation
        locationAddress.getAddressFromLocation(
            addressForLateLong,
            getApplicationContext(), GeocoderHandler()
        )

    }

    private inner class GeocoderHandler : Handler() {
        override fun handleMessage(message: Message) {
            val locationAddress: String?
            when (message.what) {
                1 -> {
                    val bundle = message.getData()
                    locationAddress = bundle.getString("address")
                    if (!locationAddress.isNullOrEmpty()) {
                        if (locationAddress.contains(",")) {
                            val elephantList = locationAddress.split(",")
                            addressLate = elephantList[0]
                            addressLong = elephantList[1]


                        }
                    } else {
                        MyUtils.showSnackbarkotlin(
                            this@RegisterAddAddressActivity,
                            rootRegisterAddAddressMainLayout!!,
                            "Please add proper address"
                        )
                    }
                }
                else -> MyUtils.showSnackbarkotlin(
                    this@RegisterAddAddressActivity,
                    rootRegisterAddAddressMainLayout!!,
                    "Please add proper address"
                )
            }
        }
    }




}
