package com.fil.flymyowncustomer.activity


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.fil.flymyowncustomer.R

import com.fil.flymyowncustomer.adapter.ProductByAdapter
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.fragment_color_filter.*
import kotlinx.android.synthetic.main.fragment_color_filter.notificationRecyclerview
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.view.*


/**
 * A simple [Fragment] subclass.
 */
class ProductByActivity : AppCompatActivity() {

    var v: View? = null
    var mActivity: Activity? = null
    var productByAdapter: ProductByAdapter? = null
    private  var linearLayoutManager: LinearLayoutManager?=null
    var masterData: ArrayList<MasterPojo?>?=null
    var tabposition =-1
    var listSubItem: ArrayList<String?>? = null

    var colorsIDs:String=""
    var materialsIDs:String=""
    var patternsIDs:String=""
    var stylesIDs:String=""
    var brandsIDs:String=""
    var from:String=""
    var onSelectedDataPass: OnSelectedDataPass? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_color_filter)
        setSupportActionBar(toolbar_back!!)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        tvToolbarTitel.visibility=View.VISIBLE
        toolbar_back.menu_Cart.visibility=View.GONE
        toolbar_back.menuHeartlike.visibility=View.GONE
        toolbar_back.menuShare.visibility=View.GONE
        ll_main_filter.setBackgroundColor(resources.getColor(R.color.white))
        tv_offer_name.visibility=View.GONE
        toolbar_back.setNavigationOnClickListener {
          onBackPressed()
        }
        if(intent!=null)
        {

            listSubItem= intent!!.getSerializableExtra("List") as ArrayList<String?>?
            from=intent!!.getStringExtra("from")!!


        }

        tv_offer_name?.visibility = View.GONE

        tvToolbarTitel.text=from
        tv_offer_name.text=from
        tv_offer_name.setTextColor(this@ProductByActivity.resources.getColor(R.color.text_primary))
        setAdapter()
    }

    private fun setAdapter() {
        linearLayoutManager = LinearLayoutManager(mActivity)
        productByAdapter = ProductByAdapter(
            this@ProductByActivity,
            object : ProductByAdapter.OnItemClick {
                override fun onClicklisneter(pos: Int, name: String) {


                        Intent().apply {
                            putExtra("from",from)
                            putExtra("name",name)
                            setResult(107,this)

                        }
                        Handler().postDelayed({

                            onBackPressed()
                        }, 1000)


                }
            },listSubItem,tabposition
        )
        notificationRecyclerview.layoutManager = linearLayoutManager
        notificationRecyclerview.adapter = productByAdapter


    }

    override fun onBackPressed() {
        MyUtils.finishActivity(this@ProductByActivity,true)
    }

    interface OnSelectedDataPass{
        fun onSelectedDataPass(
            from: String,
            selectedName: String

        )
    }





}
