package com.fil.flymyowncustomer.activity

import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.ModelResendOTP
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SessionManager
import com.fil.flymyowncustomer.model.ModelVerifyOTP
import com.fil.flymyowncustomer.util.MySMSBroadcastReceiver
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_verification_otp.*
import kotlinx.android.synthetic.main.header_layout.*
import org.json.JSONArray
import org.json.JSONObject

class VerificationOTPActivity : AppCompatActivity(), View.OnClickListener, MySMSBroadcastReceiver.OTPReceiveListener {

    var userId = ""
    var userMobile = ""
    var userEmail = ""
    var keyUserId = "keyUserId"
    var keyUserMobile = "keyUserMobile"
    var keyUserEmail = "keyUserEmail"
    var sessionManager : SessionManager? = null
    val keyUserMobileCountryCode = "keyUserMobileCountryCode"
    var userCountryCode = ""
    val smsBroadcast = MySMSBroadcastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_otp)

        tvHeaderText?.text = ""
        sessionManager = SessionManager(this@VerificationOTPActivity)

        if (intent != null){
            userId = intent.getStringExtra(keyUserId)!!
            userMobile = intent.getStringExtra(keyUserMobile)!!
            userEmail = intent.getStringExtra(keyUserEmail)!!
            userCountryCode = intent.getStringExtra(keyUserMobileCountryCode)!!
        }

        startSMSListener()
        smsBroadcast.initOTPListener(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        applicationContext.registerReceiver(smsBroadcast, intentFilter)

        imgCloseIcon?.setOnClickListener(this@VerificationOTPActivity)
//        btnOTPVerifiedSubmit?.setOnClickListener(this@VerificationOTPActivity)
        tvResendOTP?.setOnClickListener(this@VerificationOTPActivity)

        btnOTPVerifiedSubmit?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@VerificationOTPActivity,btnOTPVerifiedSubmit)

            if (exitTextVerification1.length() == 0 && exitTextVerification1.length() < 4) {

                MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, this@VerificationOTPActivity.resources.getString(R.string.OTPV))

            }else {
                if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                    FirebaseInstanceId.getInstance().instanceId
                        .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                            override fun onSuccess(instanceIdResult: InstanceIdResult) {

                                var newtoken = instanceIdResult.token
                                Log.e("System out", "new token:= " + instanceIdResult.token)
                                if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                                    verifyOTP(newtoken)
                                }else{
                                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout!!, resources.getString(R.string.error_common_network))
                                }
                            }
                        })

                }else{
                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                }

            }
        }
    }

    override fun onBackPressed() {
        // super.onBackPressed()
        MyUtils.hideKeyboard1(this@VerificationOTPActivity)
//        MyUtils.finishActivity(this@VerificationOTPActivity,true)
        MyUtils.showMessageYesNo(this@VerificationOTPActivity, "Are you sure you want to exit?", object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                val i = Intent(this@VerificationOTPActivity, LoginActivity::class.java)
                startActivity(i)
                finishAffinity()
            }
        })
    }

    private fun startSMSListener() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {
            Log.d("Task on success", "SMS Retriever starts")
        }

        task.addOnFailureListener {
            Log.d("Task on success", "SMS Retriever failed")
        }


    }

    override fun onOTPReceived(otp: String) {
        if (smsBroadcast != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsBroadcast)
        }

        if (otp != null) {
            exitTextVerification1!!.text = Editable.Factory.getInstance().newEditable(otp.toString())
//            editSecondPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[1].toString())
//            editthirdPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[2].toString())
//            editfourthPin!!.text = Editable.Factory.getInstance().newEditable(otp.toCharArray()[3].toString())
        }
    }

    override fun onOTPTimeOut() {
        Toast.makeText(this, "OTP receving time out", Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgCloseIcon -> {
                onBackPressed()
            }

            R.id.btnOTPVerifiedSubmit -> {
                MyUtils.hideKeyboardFrom(this@VerificationOTPActivity,btnOTPVerifiedSubmit)

                if (exitTextVerification1.length() == 0 && exitTextVerification1.length() < 4) {

                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, this@VerificationOTPActivity.resources.getString(R.string.OTPV))

                }else {
                    if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnSuccessListener(object : OnSuccessListener<InstanceIdResult> {
                                override fun onSuccess(instanceIdResult: InstanceIdResult) {

                                    var newtoken = instanceIdResult.token
                                    Log.e("System out", "new token:= " + instanceIdResult.token)
                                    if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                                        verifyOTP(newtoken)
                                    }else{
                                        MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout!!, resources.getString(R.string.error_common_network))
                                    }
                                }
                            })

                    }else{
                        MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                    }

                }
            }

            R.id.tvResendOTP -> {
                MyUtils.hideKeyboard(this@VerificationOTPActivity, tvResendOTP)
                exitTextVerification1.text!!.clear()

                if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                    resendOTP()
                }else{
                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                }

            }
        }
    }

    private fun verifyOTP(newtoken : String?){
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        btnOTPVerifiedSubmit.startAnimation()
        MyUtils.setViewAndChildrenEnabled(rootVerificationOTPLayout, false)
        /*[{
            "userOTP": "1595",
            "userDeviceType": "Android",
            "userDeviceID": "czxczxczx",
            "languageID": "1",
            "loginuserID": "1",
            "apiType": "Iphone",
            "apiVersion": "1.0"
        }]*/

        try {
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userId)
            jsonObject.put("userOTP", exitTextVerification1.text.toString())
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", newtoken.toString())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System Out", "Verify OTP $jsonArray")
        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }

        val verifyOTP = ViewModelProviders.of(this@VerificationOTPActivity).get(ModelVerifyOTP::class.java)
        verifyOTP.verifyOTP(this@VerificationOTPActivity, false, jsonArray.toString()).observe(this@VerificationOTPActivity,
            object : Observer<List<RegisterNewPojo>> {
                override fun onChanged(response: List<RegisterNewPojo>?){
                    if (!response.isNullOrEmpty()){
                        if (response.size > 0){

                            if (response[0].status.equals("true", true)){
                                btnOTPVerifiedSubmit.endAnimation()
                                MyUtils.setViewAndChildrenEnabled(rootVerificationOTPLayout, true)
                                MyUtils.hideKeyboardFrom(this@VerificationOTPActivity, btnOTPVerifiedSubmit)
                                if (!response[0].data.isNullOrEmpty()){
                                    if (response[0].data!!.size > 0){
                                        sessionManager?.clear_login_session()
                                        storeSessionManager(response[0]?.data!!)

                                        Handler().postDelayed({
                                            val myIntent = Intent(this@VerificationOTPActivity, MainActivity::class.java)
                                            startActivity(myIntent)
                                            finishAffinity()
                                        }, 500)
                                    }
                                }else{
                                }
                            }else{
                                //No data and no internet
                                btnOTPVerifiedSubmit.endAnimation()
                                MyUtils.setViewAndChildrenEnabled(rootVerificationOTPLayout, true)
                                if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                                    if(!response[0].message!!.isNullOrEmpty()){
                                        MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, response[0].message!!)
                                    }

                                }else{
                                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                                }
                            }
                        }
                    }else{
                        btnOTPVerifiedSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootVerificationOTPLayout, true)
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                            MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_crash_error_message))
                        }else{
                            MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                        }
                    }
                }
            })
    }

    private fun resendOTP(){

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()

        /*[{
            "languageID": "1",
            "loginuserID": "4",
            "userCountryCode": "+91",
            "userMobile": "8888888880",
            "apiType": "Android",
            "apiVersion": "1.0"
        }]*/
        try {
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("loginuserID", userId)
            jsonObject.put("userCountryCode", userCountryCode)
            jsonObject.put("userMobile", userMobile)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)

        }catch (e : Exception){
            e.printStackTrace()
        }catch (e : JsonParseException){
            e.printStackTrace()
        }

        Log.e("System Out", "Verify OTP $jsonArray")

        val resendOTP = ViewModelProviders.of(this@VerificationOTPActivity).get(ModelResendOTP::class.java)
        resendOTP.resendOTP(this@VerificationOTPActivity, true, jsonArray.toString()).observe(this@VerificationOTPActivity,
            object : Observer<List<CommonPojo>> {
                override fun onChanged(response: List<CommonPojo>?){
                    if (!response.isNullOrEmpty()){
                        if (response.size > 0){

                            if (response[0].status.equals("true", true)){
                                MyUtils.hideKeyboardFrom(this@VerificationOTPActivity, btnOTPVerifiedSubmit)
                                MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, response[0].message!!)
                            }else{
                                //No data and no internet
                                if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, response[0].message!!)
                                }else{
                                    MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                                }
                            }
                        }
                    }else{
                        //No internet and somting went rong
                        if (MyUtils.isInternetAvailable(this@VerificationOTPActivity)){
                            MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_crash_error_message))
                        }else{
                            MyUtils.showSnackbarkotlin(this@VerificationOTPActivity, rootVerificationOTPLayout, resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                        }
                    }
                }
            })
    }

    private fun storeSessionManager(driverdata: List<RegisterNewPojo.Datum>) {

        val gson = Gson()
        val json = gson.toJson(driverdata[0]!!)
        sessionManager?.create_login_session(
            json,
            driverdata[0]!!.userEmail!!,
            "",
            true,
            sessionManager?.isEmailLogin()!!)
    }
}
