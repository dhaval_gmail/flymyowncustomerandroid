package com.fil.flymyowncustomer.activity

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.model.ResetPasswordModel
import com.fil.flymyowncustomer.model.UserChangePasswordModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.getChangePassword
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.header_layout.*
import org.json.JSONArray
import org.json.JSONObject

class ResetPasswordActivity : AppCompatActivity() {

    var userId = ""
    var userMobile = ""
    var userEmail = ""
    var keyUserId = "keyUserId"
    var keyUserMobile = "keyUserMobile"
    var keyUserEmail = "keyUserEmail"
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        if (intent != null) {
            userId = intent.getStringExtra(keyUserId)!!
        }
        tvHeaderText?.text = ""
        imgCloseIcon?.setOnClickListener {
            onBackPressed()
        }

        btnResetPasswordSubmit?.setOnClickListener {
            MyUtils.hideKeyboardFrom(this@ResetPasswordActivity, btnResetPasswordSubmit)
            if (checkValidation()) {
                if (MyUtils.isInternetAvailable(this@ResetPasswordActivity)) {
                       getUserChangePwdAPI()

                }else {
                    MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, this@ResetPasswordActivity.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))
                }

            }
        }


    }

    override fun onBackPressed() {
        // super.onBackPressed()
        MyUtils.hideKeyboard1(this@ResetPasswordActivity)
        MyUtils.showMessageYesNo(this@ResetPasswordActivity, "If you press yes then you will navigate to sigin in screen.", object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                val i = Intent(this@ResetPasswordActivity, LoginActivity::class.java)
                startActivity(i)
                finishAffinity()
            }
        })
    }

    private fun checkValidation(): Boolean {
        var checkFlag = true

        if (changePasswordEditTexteNewPassword?.text.toString().isEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.textfield_ch_new_eror_pwd)
            )
        } else if (changePasswordEditTexteNewPassword?.text.toString().length < 8) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.shr_error_password)
            )
        }else if(MyUtils.isValidPassword(changePasswordEditTexteNewPassword?.text.toString().trim()) == false){
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.validerror_password)
            )
            checkFlag = false
        }
        else if (changePasswordEditTexteReEnterPassword!!.text.toString().isEmpty()) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.textfield_ch_confirm_eror_pwd)
            )
        } else if (changePasswordEditTexteReEnterPassword!!.text.toString().length < 8) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.shr_error_password)
            )
        }else if(MyUtils.isValidPassword(changePasswordEditTexteNewPassword?.text.toString().trim()) == false){
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.validerror_password)
            )
            checkFlag = false
        }
        else if (!changePasswordEditTexteNewPassword?.text.toString().equals(changePasswordEditTexteReEnterPassword?.text.toString())) {
            checkFlag = false
            MyUtils.showSnackbarkotlin(
                this@ResetPasswordActivity,
                rootResetPasswordMainLayout,
                this@ResetPasswordActivity.resources.getString(R.string.shr_error_password_not_match)
            )
        }

        return checkFlag
    }

    private fun getUserChangePwdAPI() {
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            btnResetPasswordSubmit.startAnimation()
            MyUtils.setViewAndChildrenEnabled(rootResetPasswordMainLayout, false)
            /*[{
                "languageID": "1",
                "loginuserID": "1",
                "userPassword": "ankit@12345",
                "apiType": "Android",
                "apiVersion": "1.0"
            }]*/
            jsonObject.put("loginuserID", userId)
            jsonObject.put("languageID", RestClient.languageID)
            jsonObject.put("userPassword", changePasswordEditTexteNewPassword?.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System out", "ChangePassword api call := " + jsonArray.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var getUserChangePwdModel = ViewModelProviders.of(this@ResetPasswordActivity).get(ResetPasswordModel::class.java)
        getUserChangePwdModel.apiFunction(this@ResetPasswordActivity, false, jsonArray.toString())
            .observe(this@ResetPasswordActivity, Observer<List<CommonPojo>> { changePwdPojo ->

                if (changePwdPojo != null) {
                    if (changePwdPojo[0].status.equals("true", true)) {
                        btnResetPasswordSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootResetPasswordMainLayout, true)
                        if (!changePwdPojo[0].message.isNullOrEmpty()){
                            MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, changePwdPojo[0].message!!)
                        }else{
                            MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, resources.getString(R.string.change_password_success))
                        }
                        Handler().postDelayed({
                            //                                sessionManager.clear_login_session()
                            val myIntent = Intent(this@ResetPasswordActivity, LoginActivity::class.java)
                            this@ResetPasswordActivity.startActivity(myIntent)
                            this@ResetPasswordActivity.finishAffinity()
                        }, 1500)
                    } else {
                        btnResetPasswordSubmit.endAnimation()
                        MyUtils.setViewAndChildrenEnabled(rootResetPasswordMainLayout, true)
                        if (!changePwdPojo[0].message.isNullOrEmpty()){
                            MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, changePwdPojo[0].message!!)
                        }else {
                            MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, resources.getString(R.string.change_password_failed))
                        }
                    }
                } else {
                    btnResetPasswordSubmit.endAnimation()
                    MyUtils.setViewAndChildrenEnabled(rootResetPasswordMainLayout, true)
                    errorMethod()
                }
            })

    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(this@ResetPasswordActivity)) {
                MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, this@ResetPasswordActivity.resources.getString(R.string.error_common_netdon_t_have_and_accountwork))

            } else {
                MyUtils.showSnackbarkotlin(this@ResetPasswordActivity, rootResetPasswordMainLayout, this@ResetPasswordActivity.resources.getString(R.string.error_crash_error_message))

            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

    }
}
