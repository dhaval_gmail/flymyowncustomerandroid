package com.fil.flymyowncustomer.activity

import android.content.Intent
import android.content.pm.PackageManager

import android.location.Location
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.util.SamLocationRequestService
import com.fil.flymyowncustomer.util.SessionManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.os.Build
import android.util.Base64
import android.view.View
import com.crashlytics.android.Crashlytics.setString
import androidx.core.location.LocationManagerCompat.isLocationEnabled
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fil.flymyowncustomer.model.MasterListModel
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.pojo.Push
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.PrefDb
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.no_data_internet.*
import org.json.JSONArray
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : AppCompatActivity() {

    var sessionManager: SessionManager?= null
    var samLocationRequestService : SamLocationRequestService? = null
    private val REQUEST_CODE = 1000
    var masterData: ArrayList<MasterPojo?>? = null

    var fromPush = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        try {
            printHasyKeyFb()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        sessionManager = SessionManager(this@SplashActivity)
        try {
            getVersionInfo()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (intent.hasExtra("Push")) fromPush = true

        try {
            val currentapiVersion = Build.VERSION.SDK_INT
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                getLocationPermission()
            } else {
                if(sessionManager!!.isLoggedIn())
                {
                    /*Handler().postDelayed({
                        val myIntent = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(myIntent)
                        finish()
                    }, 1000)*/
                    onGetLatLong()
                }else
                {
                    onGetLatLong()

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        btnRetry.setOnClickListener {
            getMastersList()
        }


    }

    fun printHasyKeyFb(){
        // https://farmerprice.in/privacypolicy.html
        try {
            val info = packageManager.getPackageInfo("com.fil.flymyowncustomer", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.d("System out", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("System out", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("System out", "printHashKey()", e)
        }

    }

    fun onGetLatLong(){
        MyUtils.showProgress(this@SplashActivity)
        samLocationRequestService =
                SamLocationRequestService(this@SplashActivity,
                    SamLocationRequestService.SamLocationListener { location ->
                        MyUtils.closeProgress()
                        if (location!=null){
                            Log.e("System out","Print : "+location.latitude.toString()!!)
                            PrefDb(this@SplashActivity).putString("LOCATIONLATE", location.latitude.toString()!!)
                            PrefDb(this@SplashActivity).putString("LOCATIONLONG", location.longitude.toString()!!)

                            getMastersList()


                        }else{
                            /*if (MyUtils.isInternetAvailable(this@SplashActivity)){
                                                    MyUtils.showSnackbarkotlin(this@SplashActivity, constraintlayoutMain, resources.getString(R.string.error_common_network))
                                                    finish()
                                                }else{
                                                    MyUtils.showSnackbarkotlin(this@SplashActivity, constraintlayoutMain, resources.getString(R.string.error_crash_error_message))
                                                    finish()
                                                }*/
                        }
                    }, REQUEST_CODE)
    }

    fun getMastersList() {
        ll_no_data.visibility = View.VISIBLE
        tNodataTitle.text = "Please wait...."
        btnRetry.visibility = View.GONE

//        MyUtils.showProgressDialog(this@SplashActivity)
      //  var userData = sessionManager!!.get_Authenticate_User()

        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("logindealerID", "0")
            jsonObject.put("languageID", "")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
            jsonArray.put(jsonObject)
            Log.e("System out","Print json Api:= "+ jsonArray.toString())

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: JsonParseException) {
            e.printStackTrace()
        }

        var masterListModel =
            ViewModelProviders.of(this@SplashActivity).get(MasterListModel::class.java)
        masterListModel.getMasterList(this@SplashActivity, false, jsonArray.toString())
            .observe(this@SplashActivity,
                Observer { masterPojo ->
                    if (masterPojo != null && masterPojo.isNotEmpty()) {

                        if (masterPojo[0].status.equals("true", false)) {
//                            MyUtils.closeProgress()
                            ll_no_data.visibility = View.GONE
                            masterData = ArrayList()
                            masterData!!.clear()
                            masterData!!.addAll(masterPojo)

                            storeSessionManager(masterPojo)

                            if (sessionManager!!.isLoggedIn()) {
                                if (sessionManager!!.get_Authenticate_User() != null && sessionManager!!.get_Authenticate_User().userVerified.equals("No")
                                ) {
                                    sessionManager!!.clear_login_session()
                                    Handler().postDelayed({
                                        MyUtils.startActivity(this@SplashActivity, LoginActivity::class.java, true)
                                    }, 500)
                                } else {
                                    val myIntent = Intent(this@SplashActivity, MainActivity::class.java)
                                    if (fromPush){
                                        val push = intent.getSerializableExtra("Push") as Push
                                        myIntent.putExtra("Push", push)
                                    }
                                    Handler().postDelayed({
                                        startActivity(myIntent)
                                        finish()
                                    }, 1000)

                                    /*Handler().postDelayed({
                                        val myIntent = Intent(this@SplashActivity, MainActivity::class.java)
                                        startActivity(myIntent)
                                        finish()
                                    }, 1000)*/
                                }

                            }else {
                                Handler().postDelayed({
                                    val myIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                                    startActivity(myIntent)
                                    finish()
                                }, 1000)
                            }
                        } else {
//                            MyUtils.closeProgress()
                            ll_no_data.visibility = View.VISIBLE
                            tNodataTitle.text = "No Data Found"
                            btnRetry.visibility = View.GONE
                            if(!masterPojo[0].message.isNullOrEmpty()){

                                MyUtils.showSnackbarkotlin(
                                    this@SplashActivity,
                                    splashMainLayout,
                                    masterPojo[0].message!!
                                )
//                                (activity as MainActivity).showSnackBar(masterPojo[0].message!!)
                            }
                        }

                    } else {
                        ll_no_data.visibility = View.VISIBLE

                        try {
                            if (!MyUtils.isInternetAvailable(this@SplashActivity)) {
                                tNodataTitle.text = "No Internet"
                                btnRetry.visibility = View.VISIBLE
                            } else {
                                tNodataTitle.text = "Something wen't wrong"
                                btnRetry.visibility = View.VISIBLE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                })
    }

    private fun storeSessionManager(masterdata: List<MasterPojo>) {
        val gson = Gson()
        val json = gson.toJson(masterdata[0])
        PrefDb(this@SplashActivity!!).set_Mastersession(json)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            samLocationRequestService?.startLocationUpdates()
        }
    }

    fun getLocationPermission() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            if(sessionManager!!.isLoggedIn())
            {
                /*Handler().postDelayed({
                    val myIntent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(myIntent)
                    finish()
                }, 1000)*/
                onGetLatLong()
            }else
            {
                onGetLatLong()

            }
        } else {
            ActivityCompat.requestPermissions(
                this@SplashActivity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                MyUtils.Per_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
             MyUtils.Per_REQUEST_ACCESS_FINE_LOCATION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                 if(sessionManager!!.isLoggedIn())
                 {
                     /*Handler().postDelayed({
                         val myIntent = Intent(this@SplashActivity, MainActivity::class.java)
                         startActivity(myIntent)
                         finish()
                     }, 1000)*/
                     onGetLatLong()
                 }else
                 {
                     onGetLatLong()

                 }
            } else {
                getLocationPermission()
            }

        }
    }

    fun getVersionInfo() {
        var versionName: String = ""
        var versionCode: Int = -1
        try {
            var packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace();
        }
        Log.e("System Out", "Version Code: " + String.format("V. %s", versionName))
        Log.d("System Out", "Version Code: " + String.format("V. %s", versionName))
        tvVersionCodeName?.visibility= View.GONE
        tvVersionCodeName?.text = "" + String.format("V. %s", versionName).toString()
    }
}
