package com.fil.flymyowncustomer.retrofit




import com.fil.flymyowncustomer.pojo.StatelistPojo
import com.fil.flymyowncustomer.pojo.*
import com.fil.flymyowncustomer.pojo.FiluploadResponse
import com.fil.flymyowncustomer.pojo.UserForgotPasswordPojo

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


public interface RestApi {

    @FormUrlEncoded
    @POST("users/login")
    fun loginUser(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("cmspage/get-cms")
    fun getCmspageApi(@Field("json") json: String): retrofit2.Call<List<GetCmspagePojo>>

    @FormUrlEncoded
    @POST("settings/get-user-settings")
    fun getUserSettingsApi(@Field("json") json: String): retrofit2.Call<List<GetUserSettingsPojo>>

    @FormUrlEncoded
    @POST("faq/get-faq")
    fun getFaqListApi(@Field("json") json: String): retrofit2.Call<List<FaqListPojo>>

    @FormUrlEncoded
    @POST("users/user-update-profile-picture")
    fun UserupdateProfilePictureApi(@Field("json") jsonLogin: String): Call<List<RegisterPojo>>

    @Multipart
    @POST("users/file-upload")
    fun uploadAttachment(@Part filePart: MultipartBody.Part, @Part("FilePath") filePath: RequestBody, @Part("json") json: RequestBody): Call<List<FiluploadResponse>>

    @FormUrlEncoded
    @POST("users/user-update-profile")
    fun UserupdateProfileApi(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("users/change-password")
    fun UserChangePasswordApi(@Field("json") jsonLogin: String): Call<List<getChangePassword>>

    @FormUrlEncoded
    @POST("users/otp-verification")
    fun verifyOTP(@Field("json") json: String): retrofit2.Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("users/otp-resend")
    fun resendOTP(@Field("json") json: String): retrofit2.Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("users/forgot-password")
    fun forgotPassword(@Field("json") jsonLogin: String): Call<List<UserForgotPasswordPojo>>

    @FormUrlEncoded
    @POST("users/reset-password")
    fun resetPassword(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("users/update-settings")
    fun updateSetting(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("users/update-device-token")
    fun getUserUpdateDeviceTokenApi(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("users/check-duplication")
    fun checkDuplication(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

    @Multipart
    @POST("apilog/file-upload-multiple")
    fun uploadAttachment(@Part filePart : Array<MultipartBody.Part?>, /*@Part("FilePath") filePath: RequestBody,*/ @Part("json") json: RequestBody): Call<List<FiluploadResponse>>

    @FormUrlEncoded
    @POST("users/user-registration")
    fun userRegistration(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("country/get-country-list")
    fun getCountryList(@Field("json") jsonLogin: String): Call<List<CountryListPojo>>

    @FormUrlEncoded
    @POST("state/get-state-list")
    fun getStatelist(@Field("json") jsonLogin: String): Call<List<StatelistPojo>>

    @FormUrlEncoded
    @POST("users/update-profile")
    fun updateDalerProfile(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>


    @FormUrlEncoded
    @POST("useraddress/add-address")
    fun addAddress(@Field("json") json: String):Call<List<BillingAddressPojo>>

    @FormUrlEncoded
    @POST("useraddress/update-address")
    fun updateAddress(@Field("json") json: String):Call<List<BillingAddressPojo>>

    @FormUrlEncoded
    @POST("useraddress/set-default-address")
    fun setDefaultAddress(@Field("json") json: String):Call<List<BillingAddressPojo>>

    @FormUrlEncoded
    @POST("useraddress/delete-address")
    fun deleteAddress(@Field("json") json: String):Call<List<BillingAddressPojo>>

    @FormUrlEncoded
    @POST("categorytype/get-master")
    fun getMasters(@Field("json") json: String):Call<List<MasterPojo>>

    @FormUrlEncoded
    @POST("users/user-home")
    fun getUserHome(@Field("json") json: String):Call<List<UserHomePojo>>

    @FormUrlEncoded
    @POST("dealer/get-dealer-list")
    fun getDealerList(@Field("json") json: String):Call<List<DelearListPojo>>
 

    @FormUrlEncoded
    @POST("categorytype/get-category-type-list")
    fun getCategoryTypeList(@Field("json") json: String):Call<List<CategoryTypeListPojo>>


    @FormUrlEncoded
    @POST("product/get-product-list")
    fun getProductList(@Field("json") json: String):Call<List<ProductListPojo>>

    @FormUrlEncoded
    @POST("productviews/user-add-product-views")
    fun getAddProductViews(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouriteproduct/user-add-favourite-product")
    fun getAddProductFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouriteproduct/user-remove-favourite-product")
    fun geremoveProductFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouriteproduct/user-favourite-product-list")
    fun getProductFavList(@Field("json") json: String):Call<List<ProductListPojo>>

    @FormUrlEncoded
    @POST("stitchingservice/get-stitching-service-list")
    fun getProductStichingService(@Field("json") json: String):Call<List<StichingServicePojo>>


    @FormUrlEncoded
    @POST("userfavouritedealer/user-add-favourite-dealer")
    fun getAddDealerFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouritedealer/user-remove-favourite-dealer")
    fun geremoveDealerFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouritedealer/user-favourite-dealer-list")
    fun getDealerFavList(@Field("json") json: String):Call<List<DelearListPojo>>

     @FormUrlEncoded
    @POST("userfavouritestitchingservice/user-add-favourite-stitchingservice")
    fun getAddStitchingserviceFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouritestitchingservice/user-remove-favourite-stitchingservice")
    fun geremoveStitchingserviceFav(@Field("json") json: String):Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("userfavouritestitchingservice/user-favourite-stitchingservice-list")
    fun getStitchingserviceFavList(@Field("json") json: String):Call<List<StichingServicePojo>>

    @FormUrlEncoded
    @POST("productinventory/get-product-stock")
    fun getProductInventoryApi(@Field("json") json: String):Call<List<ProductInventoryPojo>>

    @FormUrlEncoded
    @POST("orders/my-orders")
    fun getMyOrderApi(@Field("json") json: String):Call<List<MyOrderPojo>>

    @FormUrlEncoded
    @POST("users/user-social-login")
    fun userSocialLoginApi(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("users/user-registration")
    fun userRegistrationApi(@Field("json") jsonLogin: String): Call<List<RegisterNewPojo>>

    @FormUrlEncoded
    @POST("offers/get-coupon-list")
    fun getCouponList(@Field("json") jsonLogin: String): Call<List<CouponListPojo>>

    @FormUrlEncoded
    @POST("offers/apply-coupon-code")
    fun getApplyCouponList(@Field("json") jsonLogin: String): Call<List<ApplyCouponList>>

    @FormUrlEncoded
    @POST("orders/verify-cart-item")
    fun verifyCart(@Field("json") jsonLogin: String): Call<List<VeriFyCartPojo>>
   
     @FormUrlEncoded
     @POST("orders/cancel-order")
     fun getCancelOrder(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

     @FormUrlEncoded
     @POST("orders/place-order")
     fun getPlaceOrder(@Field("json") jsonLogin: String): Call<List<PlaceOrderPojo>>

     @FormUrlEncoded
     @POST("orders/my-orders-details")
     fun getOrderDetails(@Field("json") jsonLogin: String): Call<List<OrderDetailsPojo>>

    @FormUrlEncoded
    @POST("offers/apply-coupon-code")
    fun applyCoupon(@Field("json") jsonLogin: String): Call<List<ApplyCouponNewPojo>>

    @FormUrlEncoded
    @POST("orders/return-replace-request")
    fun getReturnReplaceOrder(@Field("json") jsonLogin: String): Call<List<OrderDetailsPojo>>


    @FormUrlEncoded
    @POST("notification/get-notification-list")
    fun getNotificationListApi(@Field("json") jsonLogin: String): Call<List<NotificationListPojo1>>

    @FormUrlEncoded
    @POST("notification/delete-notification")
    fun deleteNotificationApi(@Field("json") jsonLogin: String): Call<List<CommonPojo>>


    @FormUrlEncoded
    @POST("productreviews/user-add-product-reviews")
    fun getAddProductReview(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("productreviews/get-product-reviews-list")
    fun getProductReviewList(@Field("json") jsonLogin: String): Call<List<ProductReviewList>>

    @FormUrlEncoded
    @POST("stitchingservicereviews/user-add-stitchingservice-reviews")
    fun getAddStitchingserviceRevie(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("stitchingservicereviews/get-stitchingservice-reviews-list")
    fun getStitchingserviceReviewList(@Field("json") jsonLogin: String): Call<List<StitchingServiesReviewListPojo>>

    @FormUrlEncoded
    @POST("transactionrazorpay/user-payment")
    fun paymentCard(@Field("json") jsonLogin: String): Call<List<PaymentPojo>>

    @FormUrlEncoded
    @POST("wallettransacton/recharge-wallet")
    fun getRechargeWallet(@Field("json") jsonLogin: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("wallettransacton/get-wallet-history")
    fun getRechargeHistory(@Field("json") jsonLogin: String): Call<List<Wallethistory>>

}