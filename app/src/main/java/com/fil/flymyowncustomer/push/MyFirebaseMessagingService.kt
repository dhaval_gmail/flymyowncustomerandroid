package com.fil.flymyowncustomer.push

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.app.Notification.BADGE_ICON_SMALL
import com.fil.flymyowncustomer.R
import com.fil.flymyowncustomer.activity.SplashActivity
import com.fil.flymyowncustomer.pojo.Push
import com.fil.flymyowncustomer.util.PrefDb
import com.fil.flymyowncustomer.util.SessionManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

public class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "MyFirebaseToken"
    private lateinit var notificationManager: NotificationManager
    private val ADMIN_CHANNEL_ID = "FarmerSeller"
    var sessionManager: SessionManager? = null
    private var push = Push()

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.i(TAG, token)
    }

    @SuppressLint("WrongThread")
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        Log.d("System out", "push message" + remoteMessage?.data)

        try {
            if (remoteMessage?.data != null) {
                val gson = Gson()
                val message = gson.toJson(remoteMessage.data)
                val push: Push?
                push = Gson().fromJson(message, Push::class.java)

                if (push != null) {
                    if (ForegroundCheckTask().execute(this).get()) {
                        Log.e("pushData", "" + push)
                        val intentBroadcast = Intent(Push.action)
                        intentBroadcast.putExtra("PushData", push)
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intentBroadcast)
                    }

                    sendNotification(push,this)



                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /*remoteMessage?.let { message ->
            Log.i(TAG, message.getData().get("message"))

            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            //Setting up Notification channels for android O and above
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                setupNotificationChannels()
            }
            val notificationId = Random().nextInt(60000)

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)  //a resource for your custom small icon
                .setContentTitle(message.data["title"]) //the "title" value you sent in your notification
                .setContentText(message.data["message"]) //ditto
                .setAutoCancel(true)  //dismisses the notification on click
                .setSound(defaultSoundUri)

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(notificationId *//* ID of notification *//*, notificationBuilder.build())

        }*/

    }

    @SuppressLint("WrongConstant")
    fun sendNotification(push: Push?, context: Context) {

        /*if (push != null && push.type.equals("ITR")) {
            val sessionManager = SessionManager(context)
            if (sessionManager.isLoggedIn()) {
                val data = sessionManager.get_Authenticate_User()
                data.userGSTITR=("Verified")


                sessionManager.create_login_session(
                    Gson().toJson(data),
                    data.userEmail!!,
                    "",
                    true,
                    sessionManager.isEmailLogin()
                )


            }
        }*/
        val msg = push!!.msg

        var pushCount = 0

        pushCount = PrefDb(context).getInt("unReadNotification")!!
        pushCount = pushCount + 1
        Log.e("System out", "Print pushCount:=  $pushCount")

        PrefDb(context).putInt("unReadNotification", pushCount)

        val intent: Intent

        intent = Intent(context, SplashActivity::class.java)
        intent.putExtra("Push", push)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val largeIcon = BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher)
        val notificationCompat = NotificationCompat.Builder(context, context.getString(R.string.app_name))
            .setSmallIcon(R.mipmap.ic_stat_directions_bike)
            .setLargeIcon(largeIcon)
            .setBadgeIconType(BADGE_ICON_SMALL)

            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
            .setContentText(msg)
            .setContentTitle(context.getString(R.string.app_name))
            .setGroup(context.getString(R.string.app_name))
            .setPriority(Notification.PRIORITY_MAX)


        val notification = notificationCompat.build()
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val id = context.getString(R.string.app_name)
            val name = context.getString(R.string.app_name)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            var channel: NotificationChannel? = null
            channel = NotificationChannel(id, name, importance)
            channel.description = msg
            channel.setShowBadge(true)
            notificationManager.createNotificationChannel(channel)
        }
        val requestID = System.currentTimeMillis().toInt()
        notificationManager.notify(requestID, notification)

        try {
            val intentBroadcast = Intent("Push")
            intentBroadcast.putExtra("pushdata", push)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast)
        } catch (e: Exception) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels() {
        val adminChannelName = getString(R.string.app_name)
        val adminChannelDescription = getString(R.string.app_name)

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        notificationManager.createNotificationChannel(adminChannel)
    }

    internal inner class ForegroundCheckTask : AsyncTask<Context, Void, Boolean>() {

        override fun doInBackground(vararg params: Context): Boolean? {
            val context = params[0].applicationContext
            return isAppOnForeground(context)
        }

        private fun isAppOnForeground(context: Context): Boolean {
            val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val appProcesses = activityManager.runningAppProcesses ?: return false
            val packageName = context.packageName
            for (appProcess in appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == packageName) {
                    return true
                }
            }
            return false
        }
    }
}
