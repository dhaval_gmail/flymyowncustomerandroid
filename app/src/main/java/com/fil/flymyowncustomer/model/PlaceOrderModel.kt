package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.PlaceOrderPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class PlaceOrderModel : ViewModel(){

    var languageresponse: LiveData<List<PlaceOrderPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getPlaceOrderM(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<PlaceOrderPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<PlaceOrderPojo>> {
        val data = MutableLiveData<List<PlaceOrderPojo>>()

        val call =RestClient.get()!!.getPlaceOrder(json)
        call.enqueue(object : RestCallback<List<PlaceOrderPojo>>(mContext) {
            override fun Success(response: Response<List<PlaceOrderPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}