package com.fil.flymyowncustomer.util

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import com.fil.flymyowncustomer.pojo.UserForgotPasswordPojo
import retrofit2.Call
import retrofit2.Response

class UserForgotPasswordmodel : ViewModel(){

    lateinit var getForgotPasswordresponse: LiveData<List<UserForgotPasswordPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun getForgotPasswordApi(context: Context, isShowing: Boolean,
                       json: String): LiveData<List<UserForgotPasswordPojo>> {
        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        getForgotPasswordresponse = getForgotPasswordFun()
        return getForgotPasswordresponse
    }
    private fun getForgotPasswordFun(): LiveData<List<UserForgotPasswordPojo>> {
        if (isShowing) {
            MyUtils.showProgressDialog(this!!.mContext!!)
        }
        val data = MutableLiveData<List<UserForgotPasswordPojo>>()
        val call = RestClient.get()!!.forgotPassword(json)
        call.enqueue(object : retrofit2.Callback<List<UserForgotPasswordPojo>> {
            override fun onResponse(call: Call<List<UserForgotPasswordPojo>>, response: Response<List<UserForgotPasswordPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = response.body()
            }
            override fun onFailure(call: Call<List<UserForgotPasswordPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }
        })
        return data
    }

}