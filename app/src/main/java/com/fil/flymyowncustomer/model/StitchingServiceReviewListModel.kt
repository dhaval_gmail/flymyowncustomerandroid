package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.StitchingServiesReviewListPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class StitchingServiceReviewListModel : ViewModel(){

    var languageresponse: LiveData<List<StitchingServiesReviewListPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getStitchingServiceReviewList(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<StitchingServiesReviewListPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getStitchingServiceReviewApi()

        return languageresponse!!
    }

    private fun getStitchingServiceReviewApi(): LiveData<List<StitchingServiesReviewListPojo>> {
        val data = MutableLiveData<List<StitchingServiesReviewListPojo>>()

        val call =RestClient.get()!!.getStitchingserviceReviewList(json)
        call.enqueue(object : RestCallback<List<StitchingServiesReviewListPojo>>(mContext) {
            override fun Success(response: Response<List<StitchingServiesReviewListPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}