package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.ApplyCouponList
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class ApplyCouponModel : ViewModel(){

    var languageresponse: LiveData<List<ApplyCouponList>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getApplyCoupon(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<ApplyCouponList>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<ApplyCouponList>> {
        val data = MutableLiveData<List<ApplyCouponList>>()

        val call =RestClient.get()!!.getApplyCouponList(json)
        call.enqueue(object : RestCallback<List<ApplyCouponList>>(mContext) {
            override fun Success(response: Response<List<ApplyCouponList>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}