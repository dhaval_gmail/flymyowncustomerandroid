package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.GetUserSettingsPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class GetUserSettingsModel : ViewModel() {

    lateinit var userSettingsResponse: LiveData<List<GetUserSettingsPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun UserSettingsApi(
        context: Context, isShowing: Boolean,
        json: String
    ): LiveData<List<GetUserSettingsPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        userSettingsResponse = UserSettingsApiFun()

        return userSettingsResponse
    }

    private fun UserSettingsApiFun(): LiveData<List<GetUserSettingsPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(this!!.mContext!!)

        val data = MutableLiveData<List<GetUserSettingsPojo>>()
        val call = RestClient.get()!!.getUserSettingsApi(json)

        call.enqueue(object : retrofit2.Callback<List<GetUserSettingsPojo>> {
            override fun onFailure(call: Call<List<GetUserSettingsPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = null
            }

            override fun onResponse(
                call: Call<List<GetUserSettingsPojo>>,
                response: Response<List<GetUserSettingsPojo>>
            ) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = response.body()

            }
        })

        return data
    }
}