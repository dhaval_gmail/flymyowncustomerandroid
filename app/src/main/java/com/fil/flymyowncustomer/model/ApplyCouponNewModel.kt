package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.ApplyCouponNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class ApplyCouponNewModel : ViewModel(){

    lateinit var getResponse: LiveData<List<ApplyCouponNewPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun apiFunction(context: Context, isShowing: Boolean,
                          json: String): LiveData<List<ApplyCouponNewPojo>> {
        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        getResponse = apiREsponse()
        return getResponse
    }
    private fun apiREsponse(): LiveData<List<ApplyCouponNewPojo>> {
        if (isShowing) {
            MyUtils.showProgressDialog(this!!.mContext!!)
        }
        val data = MutableLiveData<List<ApplyCouponNewPojo>>()
        val call = RestClient.get()!!.applyCoupon(json)
        call.enqueue(object : retrofit2.Callback<List<ApplyCouponNewPojo>> {
            override fun onResponse(call: Call<List<ApplyCouponNewPojo>>, response: Response<List<ApplyCouponNewPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = response.body()
            }
            override fun onFailure(call: Call<List<ApplyCouponNewPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }
        })
        return data
    }

}