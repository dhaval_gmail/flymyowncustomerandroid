package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.DelearListPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class DelearListModel : ViewModel(){

    var languageresponse: LiveData<List<DelearListPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getDealer(
        context: Context,
        isShowing: Boolean,
        json: String,from:String): LiveData<List<DelearListPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<DelearListPojo>> {
        val data = MutableLiveData<List<DelearListPojo>>()

        var call :Call<List<DelearListPojo>>?=null
          when(from)
          {
              "favList"->{
                  call=RestClient.get()!!.getDealerFavList(json)

              }
              ""->{
                  call=RestClient.get()!!.getDealerList(json)
              }
          }

        call?.enqueue(object : RestCallback<List<DelearListPojo>>(mContext) {
            override fun Success(response: Response<List<DelearListPojo>>) {

                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}