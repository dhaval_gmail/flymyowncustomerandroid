package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class StitchingStoreFavouriteViewModel : ViewModel(){

    lateinit var getResponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var dealerID: String = ""
    var from: String = ""
    var loginuserID: String = ""
    var languageID: String = ""
    fun apiFunction(context: Context, isShowing: Boolean,loginuserID: String,languageID: String,from: String,dealerID:String): LiveData<List<CommonPojo>> {
        this.mContext = context
        this.isShowing = isShowing
        this.dealerID = dealerID
        this.from = from
        this.loginuserID = loginuserID
        this.languageID = languageID
        getResponse = apiREsponse()
        return getResponse
    }
    private fun apiREsponse(): LiveData<List<CommonPojo>> {
        if (isShowing) {
            MyUtils.showProgressDialog(this!!.mContext!!)
        }
        val data = MutableLiveData<List<CommonPojo>>()
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("loginuserID",loginuserID)
            jsonObject.put("languageID",languageID)
            jsonObject.put("stitchingserviceID",dealerID)
            jsonObject.put("apiType",RestClient.apiType)
            jsonObject.put("apiVersion",RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        var call:Call<List<CommonPojo>>?=null

        when(from)
        {
            "AddFav"->   call = RestClient.get()!!.getAddStitchingserviceFav(jsonArray.toString())
            "RemoveFav"->   call = RestClient.get()!!.geremoveStitchingserviceFav(jsonArray.toString())
        }


        call?.enqueue(object : retrofit2.Callback<List<CommonPojo>> {
            override fun onResponse(call: Call<List<CommonPojo>>, response: Response<List<CommonPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = response.body()
            }
            override fun onFailure(call: Call<List<CommonPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }
        })
        return data
    }

}