package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.getChangePassword
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils

import retrofit2.Call
import retrofit2.Response

class UserChangePasswordModel : ViewModel() {

    lateinit var updateChangePasswordresponse: LiveData<List<getChangePassword>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun getUserUpdate(
        context: Context, isShowing: Boolean,
        json: String
    ): LiveData<List<getChangePassword>> {
        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        updateChangePasswordresponse = getUserUpdateFun()
        return updateChangePasswordresponse
    }

    private fun getUserUpdateFun(): LiveData<List<getChangePassword>> {
        if (isShowing) {
            MyUtils.showProgressDialog(this!!.mContext!!)
        }
        val data = MutableLiveData<List<getChangePassword>>()
        val call = RestClient.get()!!.UserChangePasswordApi(json)
        call.enqueue(object : retrofit2.Callback<List<getChangePassword>> {
            override fun onResponse(call: Call<List<getChangePassword>>, response: Response<List<getChangePassword>>) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = response.body()
            }

            override fun onFailure(call: Call<List<getChangePassword>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }
        })
        return data
    }
}