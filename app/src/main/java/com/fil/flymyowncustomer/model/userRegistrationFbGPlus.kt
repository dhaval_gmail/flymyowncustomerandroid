package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class userRegistrationFbGPlus : ViewModel(){
    lateinit var getResponse: LiveData<List<RegisterNewPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun apiFunction(context: Context, isShowing: Boolean,
                    json: String): LiveData<List<RegisterNewPojo>> {
        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        getResponse = apiREsponse()
        return getResponse
    }
    private fun apiREsponse(): LiveData<List<RegisterNewPojo>> {
        if (isShowing) {
            MyUtils.showProgressDialog(this!!.mContext!!)
        }
        val data = MutableLiveData<List<RegisterNewPojo>>()
        val call = RestClient.get()!!.userRegistrationApi(json)
        call.enqueue(object : retrofit2.Callback<List<RegisterNewPojo>> {
            override fun onResponse(call: Call<List<RegisterNewPojo>>, response: Response<List<RegisterNewPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = response.body()
            }
            override fun onFailure(call: Call<List<RegisterNewPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }
        })
        return data
    }
}