package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.OrderDetailsPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Response

class ReturnReplaceRequestModel : ViewModel(){
    var languageresponse: LiveData<List<OrderDetailsPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getCancelOrder(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<OrderDetailsPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<OrderDetailsPojo>> {
        val data = MutableLiveData<List<OrderDetailsPojo>>()

        val call = RestClient.get()!!.getReturnReplaceOrder(json)
        call.enqueue(object : RestCallback<List<OrderDetailsPojo>>(mContext) {
            override fun Success(response: Response<List<OrderDetailsPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })


        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }
}