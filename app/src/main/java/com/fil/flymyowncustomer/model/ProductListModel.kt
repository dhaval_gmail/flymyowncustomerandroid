package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.ProductListPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class ProductListModel : ViewModel(){

    var languageresponse: LiveData<List<ProductListPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun  getProductListList(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<ProductListPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<ProductListPojo>> {
        val data = MutableLiveData<List<ProductListPojo>>()

        var call: Call<List<ProductListPojo>>?=null
            when(from){
                "favList"->
                {
                    call = RestClient.get()!!.getProductFavList(json)
                }
                else->
                {
                    call = RestClient.get()!!.getProductList(json)
                }
            }


        call.enqueue(object : RestCallback<List<ProductListPojo>>(mContext) {
            override fun Success(response: Response<List<ProductListPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}