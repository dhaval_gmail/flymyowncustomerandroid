package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.MasterPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class MasterListModel : ViewModel(){

    var languageresponse: LiveData<List<MasterPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""



    fun  getMasterList(
        context: Context,
        isShowing: Boolean,
        json: String): LiveData<List<MasterPojo>> {
        this.json = json

        this.mContext = context

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<MasterPojo>> {
        val data = MutableLiveData<List<MasterPojo>>()




        var call = RestClient.get()!!.getMasters(json)
        call.enqueue(object : RestCallback<List<MasterPojo>>(mContext) {
            override fun Success(response: Response<List<MasterPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}