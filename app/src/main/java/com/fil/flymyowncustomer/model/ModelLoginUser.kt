package com.fil.flymyowncustomer.util

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.RegisterNewPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class ModelLoginUser : ViewModel(){

    lateinit var loginUserResponse: LiveData<List<RegisterNewPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun userLogin(context: Context,isShowing: Boolean,
                    json: String): LiveData<List<RegisterNewPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        loginUserResponse = userLoginFun()

        return loginUserResponse
    }

    private fun userLoginFun(): LiveData<List<RegisterNewPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(this!!.mContext!!)

        val data = MutableLiveData<List<RegisterNewPojo>>()
        val call = RestClient.get()!!.loginUser(json)

        call.enqueue(object : retrofit2.Callback<List<RegisterNewPojo>> {
            override fun onFailure(call: Call<List<RegisterNewPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = null
            }
            override fun onResponse(call: Call<List<RegisterNewPojo>>, response: Response<List<RegisterNewPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = response.body()

            }
        })

        return data
    }

}