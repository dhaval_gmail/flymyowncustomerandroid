package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.ProductReviewList
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class ProductReviewListModel : ViewModel(){

    var languageresponse: LiveData<List<ProductReviewList>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun getProductReviewList(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<ProductReviewList>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<ProductReviewList>> {
        val data = MutableLiveData<List<ProductReviewList>>()

        val call =RestClient.get()!!.getProductReviewList(json)
        call.enqueue(object : RestCallback<List<ProductReviewList>>(mContext) {
            override fun Success(response: Response<List<ProductReviewList>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}