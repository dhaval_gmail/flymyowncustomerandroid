package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.StichingServicePojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class StichingServiceListModel : ViewModel(){

    var languageresponse: LiveData<List<StichingServicePojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun  getStichingListList(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<StichingServicePojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<StichingServicePojo>> {
        val data = MutableLiveData<List<StichingServicePojo>>()

        var call: Call<List<StichingServicePojo>>?=null
        when(from)
        {
            "favList"->{
                call = RestClient.get()!!.getStitchingserviceFavList(json)

            }
            ""->{
                call = RestClient.get()!!.getProductStichingService(json)
            }
        }

        call?.enqueue(object : RestCallback<List<StichingServicePojo>>(mContext) {
            override fun Success(response: Response<List<StichingServicePojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}