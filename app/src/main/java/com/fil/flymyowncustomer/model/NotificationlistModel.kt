package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.pojo.NotificationListPojo1


import retrofit2.Call
import retrofit2.Response

class NotificationlistModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<NotificationListPojo1>>
    lateinit var mContext: Context

    var isShowing:Boolean = false
    lateinit var pbDialog:Dialog
    var json: String = ""

    var searchkeyword: String = ""

    fun  getNotificationList(
        context: Context,
        isShowing: Boolean,
        json: String): LiveData<List<NotificationListPojo1>> {
        this.json = json

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        languageresponse = getNotificationListApi()

        return languageresponse
    }

    private fun getNotificationListApi(): LiveData<List<NotificationListPojo1>> {
        val data = MutableLiveData<List<NotificationListPojo1>>()

        if (isShowing) {
            showPb()
        }

        var call = RestClient.get()!!.getNotificationListApi(json)
        call.enqueue(object : RestCallback<List<NotificationListPojo1>>(mContext) {
            override fun Success(response: Response<List<NotificationListPojo1>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext)
        pbDialog.show()
    }


}


