package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.ProductInventoryPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class ProductInventoryModel : ViewModel(){

    var languageresponse: LiveData<List<ProductInventoryPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""
    var from: String = ""



    fun ProductInventory(
        context: Context,
        isShowing: Boolean,
        json: String,
        from: String
    ): LiveData<List<ProductInventoryPojo>> {
        this.json = json

        this.mContext = context
        this.from = from

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<ProductInventoryPojo>> {
        val data = MutableLiveData<List<ProductInventoryPojo>>()

        var call: Call<List<ProductInventoryPojo>>?=null

         call = RestClient.get()!!.getProductInventoryApi(json)



        call.enqueue(object : RestCallback<List<ProductInventoryPojo>>(mContext) {
            override fun Success(response: Response<List<ProductInventoryPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}