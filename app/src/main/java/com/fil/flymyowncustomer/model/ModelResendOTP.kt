package com.fil.flymyowncustomer.util

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestClient

import retrofit2.Call
import retrofit2.Response

class ModelResendOTP : ViewModel(){

    lateinit var resendOTPresponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun resendOTP(context: Context, isShowing: Boolean,
                    json: String): LiveData<List<CommonPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        resendOTPresponse = resendOTPFun()

        return resendOTPresponse
    }

    private fun resendOTPFun(): LiveData<List<CommonPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(this!!.mContext!!)

        val data = MutableLiveData<List<CommonPojo>>()
        val call = RestClient.get()!!.resendOTP(json)

        call.enqueue(object : retrofit2.Callback<List<CommonPojo>> {
            override fun onFailure(call: Call<List<CommonPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = null
            }
            override fun onResponse(call: Call<List<CommonPojo>>, response: Response<List<CommonPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = response.body()

            }
        })

        return data
    }

}