package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.UserHomePojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class UserhomeModel : ViewModel(){

    var languageresponse: LiveData<List<UserHomePojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""



    fun  getUserHome(
        context: Context,
        isShowing: Boolean,
        json: String): LiveData<List<UserHomePojo>> {
        this.json = json

        this.mContext = context

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<UserHomePojo>> {
        val data = MutableLiveData<List<UserHomePojo>>()

        var call = RestClient.get()!!.getUserHome(json)
        call.enqueue(object : RestCallback<List<UserHomePojo>>(mContext) {
            override fun Success(response: Response<List<UserHomePojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}