package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient

import retrofit2.Call
import retrofit2.Response

class RechargeWalletModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context


    lateinit var pbDialog: Dialog
    var json: String = ""

    fun addRechargeWallet(
        context: Context,
        isShowing: Boolean,
        json: String,
        apiName: String
    ): LiveData<List<CommonPojo>> {
        this.json = json

        this.mContext = context

        languageresponse = rechargeWalletApi()

        return languageresponse
    }

    private fun rechargeWalletApi(): LiveData<List<CommonPojo>> {
        val data = MutableLiveData<List<CommonPojo>>()
        var call: Call<List<CommonPojo>>? = null


        call = RestClient.get()!!.getRechargeWallet(json)
        call.enqueue(object : RestCallback<List<CommonPojo>>(mContext) {
                override fun Success(response: Response<List<CommonPojo>>) {
                    data.value = response.body()
                }

            override fun failure() {
                data.value = null
            }

        })

        return data
    }

}


