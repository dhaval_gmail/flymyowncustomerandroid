package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.GetCmspagePojo
import com.fil.flymyowncustomer.retrofit.RestClient
import retrofit2.Call
import retrofit2.Response

class GetCmspageModel : ViewModel() {

    lateinit var cmspageresponse: LiveData<List<GetCmspagePojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun getCMSPage(context: Context,isShowing: Boolean,
        json: String): LiveData<List<GetCmspagePojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        cmspageresponse = getCMSPageFun()

        return cmspageresponse
    }

    private fun getCMSPageFun(): LiveData<List<GetCmspagePojo>> {
//        if (isShowing)
//            MyUtils.showProgressDialog(mContext, "Uploading")

        val data = MutableLiveData<List<GetCmspagePojo>>()
        val call = RestClient.get()!!.getCmspageApi(json)

        call.enqueue(object : retrofit2.Callback<List<GetCmspagePojo>> {
            override fun onFailure(call: Call<List<GetCmspagePojo>>, t: Throwable) {
//                if (isShowing)
//                    MyUtils.dismissProgressDialog()

                data.value = null
            }
            override fun onResponse(call: Call<List<GetCmspagePojo>>, response: Response<List<GetCmspagePojo>>) {
//                if (isShowing)
//                    MyUtils.dismissProgressDialog()

                data.value = response.body()

            }
        })
        return data
    }

}