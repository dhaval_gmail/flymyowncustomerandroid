package com.fil.R2K2.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.PaymentPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class PaymentModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var json: String = ""
    var type: String = ""
    var userID: String = ""
    var transactionTXNAMOUNT: String = ""
    var  transactionPAYMENTMODE: String= ""

    fun getPayment(context: Context,transactionTXNAMOUNT:String,transactionPAYMENTMODE: String,userID: String): LiveData<List<PaymentPojo>>
    {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", userID)
            jsonObject.put("languageID", "1")
            jsonObject.put("transactionTXNAMOUNT", transactionTXNAMOUNT)
            jsonObject.put("transactionPAYMENTMODE", transactionPAYMENTMODE)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)



        val data = MutableLiveData<List<PaymentPojo>>()

        var call: Call<List<PaymentPojo>>?=null

         call = RestClient.get()!!.paymentCard(jsonArray.toString())


        call!!.enqueue(object : RestCallback<List<PaymentPojo>>(context){
            override fun Success(response: Response<List<PaymentPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


