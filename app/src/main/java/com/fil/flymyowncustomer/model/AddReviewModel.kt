package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient

import retrofit2.Call
import retrofit2.Response

class AddReviewModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context


    lateinit var pbDialog: Dialog
    var json: String = ""
    var apiName: String = ""

    fun addReview(
        context: Context,
        isShowing: Boolean,
        json: String,
        apiName: String
    ): LiveData<List<CommonPojo>> {
        this.json = json

        this.mContext = context
        this.apiName = apiName
        languageresponse = addReviewApi()

        return languageresponse
    }

    private fun addReviewApi(): LiveData<List<CommonPojo>> {
        val data = MutableLiveData<List<CommonPojo>>()
        var call: Call<List<CommonPojo>>? = null

        if (apiName.equals("product", false))
        {
            call = RestClient.get()!!.getAddProductReview(json)
        }
        else if (apiName.equals("stitching", false))
        {
            call = RestClient.get()!!.getAddStitchingserviceRevie(json)

        }


        call!!.enqueue(object : RestCallback<List<CommonPojo>>(mContext) {
            override fun Success(response: Response<List<CommonPojo>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }

        })

        return data
    }

}


