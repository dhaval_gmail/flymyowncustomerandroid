package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.FaqListPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class FaqListModel : ViewModel() {

    lateinit var faqListResponse: LiveData<List<FaqListPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""

    fun faqListApi(
        context: Context, isShowing: Boolean,
        json: String
    ): LiveData<List<FaqListPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        faqListResponse = faqListApiFun()

        return faqListResponse
    }

    private fun faqListApiFun(): LiveData<List<FaqListPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(this!!.mContext!!)

        val data = MutableLiveData<List<FaqListPojo>>()
        val call = RestClient.get()!!.getFaqListApi(json)

        call.enqueue(object : retrofit2.Callback<List<FaqListPojo>> {
            override fun onFailure(call: Call<List<FaqListPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = null
            }

            override fun onResponse(call: Call<List<FaqListPojo>>, response: Response<List<FaqListPojo>>) {
                if (isShowing)
                    MyUtils.closeProgress()

                data.value = response.body()

            }
        })

        return data
    }
}