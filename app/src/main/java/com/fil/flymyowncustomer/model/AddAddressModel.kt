package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.BillingAddressPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient

import retrofit2.Call
import retrofit2.Response

class AddAddressModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<BillingAddressPojo>>
    lateinit var mContext: Context


    lateinit var pbDialog: Dialog
    var json: String = ""
    var apiName: String = ""

    fun address(
        context: Context,
        isShowing: Boolean,
        json: String,
        apiName: String
    ): LiveData<List<BillingAddressPojo>> {
        this.json = json

        this.mContext = context
        this.apiName = apiName
        languageresponse = addAddressApi()

        return languageresponse
    }

    private fun addAddressApi(): LiveData<List<BillingAddressPojo>> {
        val data = MutableLiveData<List<BillingAddressPojo>>()
        var call: Call<List<BillingAddressPojo>>? = null

        if (apiName.equals("addAddress", false))
        {
            call = RestClient.get()!!.addAddress(json)
        }
        else if (apiName.equals("updateAddress", false))
        {
            call = RestClient.get()!!.updateAddress(json)

        }
        else if (apiName.equals("deleteAddress", false))
        {
            call = RestClient.get()!!.deleteAddress(json)

        }
        else if (apiName.equals("setDefaultAddress", false))
        {
            call = RestClient.get()!!.setDefaultAddress(json)

        }

        call!!.enqueue(object : RestCallback<List<BillingAddressPojo>>(mContext) {
            override fun Success(response: Response<List<BillingAddressPojo>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }

        })

        return data
    }

}


