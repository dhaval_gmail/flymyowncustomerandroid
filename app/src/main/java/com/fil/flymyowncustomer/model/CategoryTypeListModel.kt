package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CategoryTypeListPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response

class CategoryTypeListModel : ViewModel(){

    var languageresponse: LiveData<List<CategoryTypeListPojo>>?=null
    var mContext: Context?=null


    lateinit var pbDialog: Dialog
    var json: String = ""



    fun  getCategoryTypeList(
        context: Context,
        isShowing: Boolean,
        json: String): LiveData<List<CategoryTypeListPojo>> {
        this.json = json

        this.mContext = context

        languageresponse = getNotificationListApi()

        return languageresponse!!
    }

    private fun getNotificationListApi(): LiveData<List<CategoryTypeListPojo>> {
        val data = MutableLiveData<List<CategoryTypeListPojo>>()




        var call = RestClient.get()!!.getCategoryTypeList(json)
        call.enqueue(object : RestCallback<List<CategoryTypeListPojo>>(mContext) {
            override fun Success(response: Response<List<CategoryTypeListPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext!!)
        pbDialog.show()

    }

}