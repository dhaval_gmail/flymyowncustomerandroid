package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.VeriFyCartPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils
import retrofit2.Call
import retrofit2.Response


class VerifyCartItemsModel : ViewModel() {

    lateinit var adddddressresponse: LiveData<List<VeriFyCartPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""
    var from: String = ""

    fun getVerifyCartItems(
        context: Context, isShowing: Boolean,
        json: String
    ): LiveData<List<VeriFyCartPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        adddddressresponse = getVerifyCartItemFun()

        return adddddressresponse
    }

    private fun getVerifyCartItemFun(): LiveData<List<VeriFyCartPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(this!!.mContext!!)
        val data = MutableLiveData<List<VeriFyCartPojo>>()
        var call: Call<List<VeriFyCartPojo>>


        call = RestClient.get()!!.verifyCart(json)


        call.enqueue(object : retrofit2.Callback<List<VeriFyCartPojo>> {
            override fun onFailure(call: Call<List<VeriFyCartPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.closeProgress()
                data.value = null
            }

            override fun onResponse(
                call: Call<List<VeriFyCartPojo>>,
                response: Response<List<VeriFyCartPojo>>
            ) {
                if (isShowing)
                    MyUtils.closeProgress()
                data?.value = response.body()
            }
        })
        return data
    }
}