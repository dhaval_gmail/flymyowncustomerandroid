package com.fil.flymyowncustomer.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.Wallethistory
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient

import retrofit2.Call
import retrofit2.Response

class RechargeWalletHistoryModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<Wallethistory>>
    lateinit var mContext: Context


    lateinit var pbDialog: Dialog
    var json: String = ""

    fun  rechargeWalletHistory(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<Wallethistory>> {
        this.json = json

        this.mContext = context

        languageresponse = rechargeWalletHistoryApi()

        return languageresponse
    }

    private fun rechargeWalletHistoryApi(): LiveData<List<Wallethistory>> {
        val data = MutableLiveData<List<Wallethistory>>()
        var call: Call<List<Wallethistory>>? = null


            call = RestClient.get()!!.getRechargeHistory(json)
        call!!.enqueue(object : RestCallback<List<Wallethistory>>(mContext) {
            override fun Success(response: Response<List<Wallethistory>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }

        })

        return data
    }

}


