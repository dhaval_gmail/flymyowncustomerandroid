package com.fil.R2K2.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.BillingAddressPojo
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.pojo.PaymentPojo
import com.fil.flymyowncustomer.retrofit.RestCallback
import com.fil.flymyowncustomer.retrofit.RestClient
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class AddMoneyModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""
    var type: String = ""
    lateinit var languageresponse: LiveData<List<CommonPojo>>


    fun addMoney(
        context: Context,
        json: String
    ): LiveData<List<CommonPojo>> {
        this.json = json
        this.mContext = context
        languageresponse = addAddressApi()
        return languageresponse
    }

    private fun addAddressApi(): LiveData<List<CommonPojo>> {
        val data = MutableLiveData<List<CommonPojo>>()
        var call: Call<List<CommonPojo>>? = null


        call = RestClient.get()!!.getRechargeWallet(json)

        call!!.enqueue(object : RestCallback<List<CommonPojo>>(mContext) {
            override fun Success(response: Response<List<CommonPojo>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }

        })

        return data
    }




}


