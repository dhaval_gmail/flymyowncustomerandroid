package com.fil.flymyowncustomer.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fil.flymyowncustomer.pojo.CommonPojo
import com.fil.flymyowncustomer.retrofit.RestClient
import com.fil.flymyowncustomer.util.MyUtils

import retrofit2.Call
import retrofit2.Response

class DeleteNotificationModel : ViewModel() {

    lateinit var deleteaddressresponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    var json: String = ""
    fun getDeleteNotification(
        context: Context, isShowing: Boolean,
        json: String
    ): LiveData<List<CommonPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.json = json
        deleteaddressresponse = getDeleteNotificationFun()

        return deleteaddressresponse
    }

    private fun getDeleteNotificationFun(): LiveData<List<CommonPojo>> {
        try {
            if (isShowing)
                MyUtils.showProgressDialog(this!!.mContext!!)
        } catch (e: Exception) {
        }
        val data = MutableLiveData<List<CommonPojo>>()
        val call = RestClient.get()!!.deleteNotificationApi(json)

        call.enqueue(object : retrofit2.Callback<List<CommonPojo>> {
            override fun onFailure(call: Call<List<CommonPojo>>, t: Throwable) {
                try {
                    if (isShowing)
                        MyUtils.closeProgress()
                } catch (e: Exception) {
                }
                data.value = null
            }

            override fun onResponse(call: Call<List<CommonPojo>>, response: Response<List<CommonPojo>>) {
                try {
                    if (isShowing)
                        MyUtils.closeProgress()
                } catch (e: Exception) {
                }
                data.value = response.body()
            }
        })
        return data
    }
}