package com.fil.flymyowncustomer.util

import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.graphics.Paint.Style
import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.ColorRes
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import org.florescu.android.rangeseekbar.RangeSeekBar
import org.florescu.android.util.BitmapUtil
import org.florescu.android.util.PixelUtil
import java.math.BigDecimal

/**
 * Created by dhaval on 01/07/2017.
 *//*
Copyright 2015 Alex Florescu
Copyright 2014 Stephan Tittel and Yahoo Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Widget that lets durationMonthList select a minimum and maximum value on a given numerical range.
 * The range value types can be one of Long, Double, Integer, Float, Short, Byte or BigDecimal.<br></br>
 * <br></br>
 * Improved [MotionEvent] handling for smoother use, anti-aliased painting for improved aesthetics.
 *
 * @param <T> The Number type of the range values. One of Long, Double, Integer, Float, Short, Byte or BigDecimal.
 * @author Stephan Tittel (stephan.tittel@kom.tu-darmstadt.de)
 * @author Peter Sinnott (psinnott@gmail.com)
 * @author Thomas Barrasso (tbarrasso@sevenplusandroid.org)
 * @author Alex Florescu (alex@florescu.org)
 * @author Michael Keppler (bananeweizen@gmx.de)
</T> */
class RangeSeekBar2<T : Number> : androidx.appcompat.widget.AppCompatImageView {
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val shadowPaint = Paint()

    private var thumbImage: Bitmap? = null
    private var thumbPressedImage: Bitmap? = null
    private var thumbDisabledImage: Bitmap? = null

    private var mThumbHalfWidth: Float = 0.toFloat()
    private var mThumbHalfHeight: Float = 0.toFloat()

    private var padding: Float = 0.toFloat()
    /**
     * Returns the absolute minimum value of the range that has been set at construction time.
     *
     * @return The absolute minimum value of the range.
     */
    var absoluteMinValue: Int? = null
        private set
    /**
     * Returns the absolute maximum value of the range that has been set at construction time.
     *
     * @return The absolute maximum value of the range.
     */
    var absoluteMaxValue: Int? = null
    private var numberType: NumberType? = null
    private var absoluteMinValuePrim: Double = 0.toDouble()
    private var absoluteMaxValuePrim: Double = 0.toDouble()
    private var normalizedMinValue = 0.0
    private var normalizedMaxValue = 1.0
    private var pressedThumb: Thumb? = null
    /**
     * Should the widget notify the listener callback while the user is still dragging a thumb? Default is false.
     */
    var isNotifyWhileDragging = false
    private var listener: OnRangeSeekBarChangeListener<Any>? = null

    private var mDownMotionX: Float = 0.toFloat()

    private var mActivePointerId = INVALID_POINTER_ID

    private var mScaledTouchSlop: Int = 0

    private var mIsDragging: Boolean = false

    private var mTextOffset: Int = 0
    private var mTextSize: Int = 0
    private var mDistanceToTop: Int = 0
    private var mRect: RectF? = null

    private var mSingleThumb: Boolean = false
    private var mAlwaysActive: Boolean = false
    private var mShowLabels: Boolean = false
    private var mShowTextAboveThumbs: Boolean = false
    private var mInternalPad: Float = 0.toFloat()
    private var mActiveColor: Int = 0
    private var mDefaultColor: Int = 0
    private var mTextAboveThumbsColor: Int = 0

    private var mThumbShadow: Boolean = false
    private var mThumbShadowXOffset: Int = 0
    private var mThumbShadowYOffset: Int = 0
    private var mThumbShadowBlur: Int = 0
    private var mThumbShadowPath: Path? = null
    private val mTranslatedThumbShadowPath = Path()
    private val mThumbShadowMatrix = Matrix()

    private var mActivateOnDefaultValues: Boolean = false

    /**
     * Returns the currently selected min value.
     *
     * @return The currently selected min value.
     */
    /**
     * Sets the currently selected minimum value. The widget will be invalidated and redrawn.
     *
     * @param value The Number value to set the minimum value to. Will be clamped to given absolute minimum/maximum range.
     */
    // in case absoluteMinValue == absoluteMaxValue, avoid division by zero when normalizing.
    var selectedMinValue: Int?
        get() = normalizedToValue(normalizedMinValue)
        set(value) = if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            setNormalizedMinValue(0.0)
        } else {
            setNormalizedMinValue(valueToNormalized(value))
        }

    /**
     * Returns the currently selected max value.
     *
     * @return The currently selected max value.
     */
    /**
     * Sets the currently selected maximum value. The widget will be invalidated and redrawn.
     *
     * @param value The Number value to set the maximum value to. Will be clamped to given absolute minimum/maximum range.
     */
    // in case absoluteMinValue == absoluteMaxValue, avoid division by zero when normalizing.
    var selectedMaxValue: Int?
        get() = normalizedToValue(normalizedMaxValue)
        set(value) = if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            setNormalizedMaxValue(1.0)
        } else {
            setNormalizedMaxValue(valueToNormalized(value))
        }


    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs)
    }

    private fun extractNumericValueFromAttributes(a: TypedArray, attribute: Int, defaultValue: Int):Int{
        val tv = a.peekValue(attribute) ?: return Integer.valueOf(defaultValue) as Int

        val type = tv.type
        return if (type == TypedValue.TYPE_FLOAT) {
            java.lang.Float.valueOf(a.getFloat(attribute, defaultValue.toFloat())) as Int
        } else {
            Integer.valueOf(a.getInteger(attribute, defaultValue)) as Int
        }
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val barHeight: Float
        val thumbNormal = org.florescu.android.rangeseekbar.R.drawable.seek_thumb_normal
        val thumbPressed = org.florescu.android.rangeseekbar.R.drawable.seek_thumb_pressed
        val thumbDisabled = org.florescu.android.rangeseekbar.R.drawable.seek_thumb_disabled
        val thumbShadowColor: Int
        val defaultShadowColor = Color.argb(75, 0, 0, 0)
        val defaultShadowYOffset = PixelUtil.dpToPx(context, 2)
        val defaultShadowXOffset = PixelUtil.dpToPx(context, 0)
        val defaultShadowBlur = PixelUtil.dpToPx(context, 2)

        if (attrs == null) {
            setRangeToDefaultValues()
            mInternalPad = PixelUtil.dpToPx(context, INITIAL_PADDING_IN_DP).toFloat()
            barHeight = PixelUtil.dpToPx(context, LINE_HEIGHT_IN_DP).toFloat()
            mActiveColor = ACTIVE_COLOR
            mDefaultColor = Color.GRAY
            mAlwaysActive = false
            mShowTextAboveThumbs = true
            mTextAboveThumbsColor = Color.WHITE
            thumbShadowColor = defaultShadowColor
            mThumbShadowXOffset = defaultShadowXOffset
            mThumbShadowYOffset = defaultShadowYOffset
            mThumbShadowBlur = defaultShadowBlur
            mActivateOnDefaultValues = false
        } else {
            val a = getContext().obtainStyledAttributes(attrs, org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar, 0, 0)
            try {
                setRangeValues(
                        extractNumericValueFromAttributes(a, org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_absoluteMinValue, DEFAULT_MINIMUM),
                        extractNumericValueFromAttributes(a, org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_absoluteMaxValue, DEFAULT_MAXIMUM)
                )
                mShowTextAboveThumbs = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_valuesAboveThumbs, true)
                mTextAboveThumbsColor = a.getColor(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_textAboveThumbsColor, Color.WHITE)
                mSingleThumb = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_singleThumb, false)
                mShowLabels = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_showLabels, true)
                mInternalPad = a.getDimensionPixelSize(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_internalPadding, INITIAL_PADDING_IN_DP).toFloat()
                barHeight = a.getDimensionPixelSize(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_barHeight, LINE_HEIGHT_IN_DP).toFloat()
                mActiveColor = a.getColor(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_activeColor, ACTIVE_COLOR)
                mDefaultColor = a.getColor(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_defaultColor, Color.GRAY)
                mAlwaysActive = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_alwaysActive, false)

                val normalDrawable = a.getDrawable(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbNormal)
                if (normalDrawable != null) {
                    thumbImage = BitmapUtil.drawableToBitmap(normalDrawable)
                }
                val disabledDrawable = a.getDrawable(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbDisabled)
                if (disabledDrawable != null) {
                    thumbDisabledImage = BitmapUtil.drawableToBitmap(disabledDrawable)
                }
                val pressedDrawable = a.getDrawable(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbPressed)
                if (pressedDrawable != null) {
                    thumbPressedImage = BitmapUtil.drawableToBitmap(pressedDrawable)
                }
                mThumbShadow = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbShadow, false)
                thumbShadowColor = a.getColor(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbShadowColor, defaultShadowColor)
                mThumbShadowXOffset = a.getDimensionPixelSize(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbShadowXOffset, defaultShadowXOffset)
                mThumbShadowYOffset = a.getDimensionPixelSize(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbShadowYOffset, defaultShadowYOffset)
                mThumbShadowBlur = a.getDimensionPixelSize(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_thumbShadowBlur, defaultShadowBlur)

                mActivateOnDefaultValues = a.getBoolean(org.florescu.android.rangeseekbar.R.styleable.RangeSeekBar_activateOnDefaultValues, false)
            } finally {
                a.recycle()
            }
        }

        if (thumbImage == null) {
            thumbImage = BitmapFactory.decodeResource(resources, thumbNormal)
        }
        if (thumbPressedImage == null) {
            thumbPressedImage = BitmapFactory.decodeResource(resources, thumbPressed)
        }
        if (thumbDisabledImage == null) {
            thumbDisabledImage = BitmapFactory.decodeResource(resources, thumbDisabled)
        }

        mThumbHalfWidth = 0.5f * thumbImage!!.width
        mThumbHalfHeight = 0.5f * thumbImage!!.height

        setValuePrimAndNumberType()

        mTextSize = PixelUtil.dpToPx(context, DEFAULT_TEXT_SIZE_IN_DP)
        mDistanceToTop = PixelUtil.dpToPx(context, DEFAULT_TEXT_DISTANCE_TO_TOP_IN_DP)
        mTextOffset = if (!mShowTextAboveThumbs)
            0
        else
            this.mTextSize + PixelUtil.dpToPx(context,
                    DEFAULT_TEXT_DISTANCE_TO_BUTTON_IN_DP) + this.mDistanceToTop

        mRect = RectF(padding,
                mTextOffset + mThumbHalfHeight - barHeight / 2,
                width - padding,
                mTextOffset.toFloat() + mThumbHalfHeight + barHeight / 2)

        // make RangeSeekBar focusable. This solves focus handling issues in case EditText widgets are being used along with the RangeSeekBar within ScrollViews.
        isFocusable = true
        isFocusableInTouchMode = true
        mScaledTouchSlop = ViewConfiguration.get(getContext()).scaledTouchSlop

        if (mThumbShadow) {
            // We need to remove hardware acceleration in order to blur the shadow
            setLayerType( View.LAYER_TYPE_SOFTWARE, null)
            shadowPaint.color = thumbShadowColor
            shadowPaint.maskFilter = BlurMaskFilter(mThumbShadowBlur.toFloat(), BlurMaskFilter.Blur.NORMAL)
            mThumbShadowPath = Path()
            mThumbShadowPath!!.addCircle(0f,
                    0f,
                    mThumbHalfHeight,
                    Path.Direction.CW)
        }
    }

    fun setRangeValues(minValue: Int, maxValue: Int) {
        this.absoluteMinValue = minValue
        this.absoluteMaxValue = maxValue
        setValuePrimAndNumberType()
    }


    fun setTextAboveThumbsColor(textAboveThumbsColor: Int) {
        this.mTextAboveThumbsColor = textAboveThumbsColor
        invalidate()
    }

    fun setTextAboveThumbsColorResource(@ColorRes resId: Int) {
        setTextAboveThumbsColor(resources.getColor(resId))
    }

    private// only used to set default values when initialised from XML without any values specified
    fun setRangeToDefaultValues() {
        this.absoluteMinValue = DEFAULT_MINIMUM as Int
        this.absoluteMaxValue = DEFAULT_MAXIMUM as Int
        setValuePrimAndNumberType()
    }

    private fun setValuePrimAndNumberType() {
        absoluteMinValuePrim = absoluteMinValue!!.toDouble()
        absoluteMaxValuePrim = absoluteMaxValue!!.toDouble()
        numberType = NumberType.fromNumber(absoluteMinValue!!)
    }

    fun resetSelectedValues() {
        selectedMinValue = absoluteMinValue
        selectedMaxValue = absoluteMaxValue
    }

    /**
     * Registers given listener callback to notify about changed selected values.
     *
     * @param listener The listener to notify about changed selected values.
     */
    fun setOnRangeSeekBarChangeListener(onRangeSeekBarChangeListener: OnRangeSeekBarChangeListener<Any>) {
        this.listener = onRangeSeekBarChangeListener
    }
//    fun setOnRangeSeekBarChangeListener(onRangeSeekBarChangeListener: RangeSeekBar2.OnRangeSeekBarChangeListener<Any>) {
//        this.listener = listener
//    }

    /**
     * Set the path that defines the shadow of the thumb. This path should be defined assuming
     * that the center of the shadow is at the top left corner (0,0) of the canvas. The
     * [.drawThumbShadow] method will place the shadow appropriately.
     *
     * @param thumbShadowPath The path defining the thumb shadow
     */
    fun setThumbShadowPath(thumbShadowPath: Path) {
        this.mThumbShadowPath = thumbShadowPath
    }

    /**
     * Handles thumb selection and movement. Notifies listener callback on certain events.
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (!isEnabled) {
            return false
        }

        val pointerIndex: Int

        val action = event.action
        when (action and MotionEvent.ACTION_MASK) {

            MotionEvent.ACTION_DOWN -> {
                // Remember where the motion event started
                mActivePointerId = event.getPointerId(event.pointerCount - 1)
                pointerIndex = event.findPointerIndex(mActivePointerId)
                mDownMotionX = event.getX(pointerIndex)

                pressedThumb = evalPressedThumb(mDownMotionX)

                // Only handle thumb presses.
                if (pressedThumb == null) {
                    return super.onTouchEvent(event)
                }

                isPressed = true
                invalidate()
                onStartTrackingTouch()
                trackTouchEvent(event)
                attemptClaimDrag()
            }
            MotionEvent.ACTION_MOVE -> if (pressedThumb != null) {

                if (mIsDragging) {
                    trackTouchEvent(event)
                } else {
                    // Scroll to follow the motion event
                    pointerIndex = event.findPointerIndex(mActivePointerId)
                    val x = event.getX(pointerIndex)

                    if (Math.abs(x - mDownMotionX) > mScaledTouchSlop) {
                        isPressed = true
                        invalidate()
                        onStartTrackingTouch()
                        trackTouchEvent(event)
                        attemptClaimDrag()
                    }
                }

                if (isNotifyWhileDragging && listener != null) {
                    listener!!.onRangeSeekBarValuesChanged(this, this.selectedMinValue!!, this.selectedMaxValue!!)
                }
            }
            MotionEvent.ACTION_UP -> {
                if (mIsDragging) {
                    trackTouchEvent(event)
                    onStopTrackingTouch()
                    setPressed(false)
                } else {
                    // Touch up when we never crossed the touch slop threshold
                    // should be interpreted as a tap-seek to that location.
                    onStartTrackingTouch()
                    trackTouchEvent(event)
                    onStopTrackingTouch()
                }

                pressedThumb = null
                invalidate()
                if (listener != null) {
                    listener!!.onRangeSeekBarValuesChanged(this, this.selectedMinValue!!, this.selectedMaxValue!!)
                }
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                val index = event.pointerCount - 1
                // final int index = ev.getActionIndex();
                mDownMotionX = event.getX(index)
                mActivePointerId = event.getPointerId(index)
                invalidate()
            }
            MotionEvent.ACTION_POINTER_UP -> {
                onSecondaryPointerUp(event)
                invalidate()
            }
            MotionEvent.ACTION_CANCEL -> {
                if (mIsDragging) {
                    onStopTrackingTouch()
                    isPressed = false
                }
                invalidate() // see above explanation
            }
        }
        return true
    }

    private fun onSecondaryPointerUp(ev: MotionEvent) {
        val pointerIndex = ev.action and ACTION_POINTER_INDEX_MASK shr ACTION_POINTER_INDEX_SHIFT

        val pointerId = ev.getPointerId(pointerIndex)
        if (pointerId == mActivePointerId) {
            // This was our active pointer going up. Choose
            // a new active pointer and adjust accordingly.

            val newPointerIndex = if (pointerIndex == 0) 1 else 0
            mDownMotionX = ev.getX(newPointerIndex)
            mActivePointerId = ev.getPointerId(newPointerIndex)
        }
    }

    private fun trackTouchEvent(event: MotionEvent) {
        val pointerIndex = event.findPointerIndex(mActivePointerId)
        val x = event.getX(pointerIndex)

        if (Thumb.MIN == pressedThumb && !mSingleThumb) {
            setNormalizedMinValue(screenToNormalized(x))
        } else if (Thumb.MAX == pressedThumb) {
            setNormalizedMaxValue(screenToNormalized(x))
        }
    }

    /**
     * Tries to claim the user's drag motion, and requests disallowing any ancestors from stealing events in the drag.
     */
    private fun attemptClaimDrag() {
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true)
        }
    }

    /**
     * This is called when the user has started touching this widget.
     */
    internal fun onStartTrackingTouch() {
        mIsDragging = true
    }

    /**
     * This is called when the user either releases his touch or the touch is canceled.
     */
    internal fun onStopTrackingTouch() {
        mIsDragging = false
    }

    /**
     * Ensures correct size of the widget.
     */
    @Synchronized
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var width = 200
        if (View.MeasureSpec.UNSPECIFIED != View.MeasureSpec.getMode(widthMeasureSpec)) {
            width = View.MeasureSpec.getSize(widthMeasureSpec)
        }

        var height = (thumbImage!!.height
                + (if (!mShowTextAboveThumbs) 0 else PixelUtil.dpToPx(context, HEIGHT_IN_DP))
                + if (mThumbShadow) mThumbShadowYOffset + mThumbShadowBlur else 0)
        if (View.MeasureSpec.UNSPECIFIED != View.MeasureSpec.getMode(heightMeasureSpec)) {
            height = Math.min(height, View.MeasureSpec.getSize(heightMeasureSpec))
        }
        setMeasuredDimension(width, height)
    }

    /**
     * Draws the widget on the given canvas.
     */
    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.textSize = mTextSize.toFloat()
        paint.style = Style.FILL
        paint.color = mDefaultColor
        paint.isAntiAlias = true
        var minMaxLabelSize = 0f

        if (mShowLabels) {
            // draw min and max labels
            val minLabel = context.getString(org.florescu.android.rangeseekbar.R.string.demo_min_label)
            val maxLabel = context.getString(org.florescu.android.rangeseekbar.R.string.demo_max_label)
            minMaxLabelSize = Math.max(paint.measureText(minLabel), paint.measureText(maxLabel))
            val minMaxHeight = mTextOffset.toFloat() + mThumbHalfHeight + (mTextSize / 3).toFloat()
            canvas.drawText(minLabel, 0f, minMaxHeight, paint)
            canvas.drawText(maxLabel, width - minMaxLabelSize, minMaxHeight, paint)
        }
        padding = mInternalPad + minMaxLabelSize + mThumbHalfWidth

        // draw seek bar background line
        mRect!!.left = padding
        mRect!!.right = width - padding
        canvas.drawRect(mRect!!, paint)

        val selectedValuesAreDefault = selectedMinValue == absoluteMinValue && selectedMaxValue == absoluteMaxValue

        val colorToUseForButtonsAndHighlightedLine = if (!mAlwaysActive && !mActivateOnDefaultValues && selectedValuesAreDefault)
            mDefaultColor
        else
        // default values
            mActiveColor   // non default, filter is active

        // draw seek bar active range line
        mRect!!.left = normalizedToScreen(normalizedMinValue)
        mRect!!.right = normalizedToScreen(normalizedMaxValue)

        paint.color = colorToUseForButtonsAndHighlightedLine
        canvas.drawRect(mRect!!, paint)

        // draw minimum thumb (& shadow if requested) if not a single thumb control
        if (!mSingleThumb) {
            if (mThumbShadow) {
                drawThumbShadow(normalizedToScreen(normalizedMinValue), canvas)
            }
            drawThumb(normalizedToScreen(normalizedMinValue), Thumb.MIN == pressedThumb, canvas,
                    selectedValuesAreDefault)
        }

        // draw maximum thumb & shadow (if necessary)
        if (mThumbShadow) {
            drawThumbShadow(normalizedToScreen(normalizedMaxValue), canvas)
        }
        drawThumb(normalizedToScreen(normalizedMaxValue), Thumb.MAX == pressedThumb, canvas,
                selectedValuesAreDefault)

        // draw the text if sliders have moved from default edges
        if (mShowTextAboveThumbs && (mActivateOnDefaultValues || !selectedValuesAreDefault)) {
            paint.textSize = mTextSize.toFloat()
            paint.color = mTextAboveThumbsColor
            // give text a bit more space here so it doesn't get cut off
            val offset = PixelUtil.dpToPx(context, TEXT_LATERAL_PADDING_IN_DP)

            val minText = selectedMinValue.toString()
            val maxText = selectedMaxValue.toString()
            val minTextWidth = paint.measureText(minText) + offset
            val maxTextWidth = paint.measureText(maxText) + offset
            var min_val = Integer.parseInt(minText)
            var max_val = Integer.parseInt(maxText)
            if (min_val <= 10) {
                min_val = 1
            } else if (min_val <= 20 && min_val > 10) {
                min_val = 2

            } else if (min_val <= 30 && min_val > 20) {
                min_val = 3

            } else if (min_val <= 40 && min_val > 30) {
                min_val = 4

            } else if (min_val <= 50 && min_val > 40) {
                min_val = 5

            } else if (min_val <= 60 && min_val > 50) {
                min_val = 6

            } else if (min_val <= 70 && min_val > 60) {
                min_val = 7

            } else if (min_val <= 80 && min_val > 70) {
                min_val = 8

            } else if (min_val <= 90 && min_val > 80) {
                min_val = 9

            } else if (min_val <= 100 && min_val > 90) {
                min_val = 10

            }

            if (max_val <= 10) {
                max_val = 1
            } else if (max_val <= 20 && max_val > 10) {
                max_val = 2

            } else if (max_val <= 30 && max_val > 20) {
                max_val = 3

            } else if (max_val <= 40 && max_val > 30) {
                max_val = 4

            } else if (max_val <= 50 && max_val > 40) {
                max_val = 5

            } else if (max_val <= 60 && max_val > 50) {
                max_val = 6

            } else if (max_val <= 70 && max_val > 60) {
                max_val = 7

            } else if (max_val <= 80 && max_val > 70) {
                max_val = 8

            } else if (max_val <= 90 && max_val > 80) {
                max_val = 9

            } else if (max_val <= 100 && max_val > 90) {
                max_val = 10

            }


            if (!mSingleThumb) {
                canvas.drawText(min_val.toString() + "",
                        normalizedToScreen(normalizedMinValue) - minTextWidth * 0.5f,
                        (mDistanceToTop + mTextSize).toFloat(),
                        paint)

            }

            canvas.drawText(max_val.toString() + "",
                    normalizedToScreen(normalizedMaxValue) - maxTextWidth * 0.5f,
                    (mDistanceToTop + mTextSize).toFloat(),
                    paint)
        }

    }

    /**
     * Overridden to save instance state when device orientation changes. This method is called automatically if you assign an id to the RangeSeekBar widget using the [.setId] method. Other members of this class than the normalized min and max values don't need to be saved.
     */
    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable("SUPER", super.onSaveInstanceState())
        bundle.putDouble("MIN", normalizedMinValue)
        bundle.putDouble("MAX", normalizedMaxValue)
        return bundle
    }

    /**
     * Overridden to restore instance state when device orientation changes. This method is called automatically if you assign an id to the RangeSeekBar widget using the [.setId] method.
     */
    override fun onRestoreInstanceState(parcel: Parcelable) {
        val bundle = parcel as Bundle
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"))
        normalizedMinValue = bundle.getDouble("MIN")
        normalizedMaxValue = bundle.getDouble("MAX")
    }

    /**
     * Draws the "normal" resp. "pressed" thumb image on specified x-coordinate.
     *
     * @param screenCoord The x-coordinate in screen space where to draw the image.
     * @param pressed     Is the thumb currently in "pressed" state?
     * @param canvas      The canvas to draw upon.
     */
    private fun drawThumb(screenCoord: Float, pressed: Boolean, canvas: Canvas, areSelectedValuesDefault: Boolean) {
        val buttonToDraw: Bitmap?
        if (!mActivateOnDefaultValues && areSelectedValuesDefault) {
            buttonToDraw = thumbDisabledImage
        } else {
            buttonToDraw = if (pressed) thumbPressedImage else thumbImage
        }

        canvas.drawBitmap(buttonToDraw!!, screenCoord - mThumbHalfWidth,
                mTextOffset.toFloat(),
                paint)
    }

    /**
     * Draws a drop shadow beneath the slider thumb.
     *
     * @param screenCoord the x-coordinate of the slider thumb
     * @param canvas      the canvas on which to draw the shadow
     */
    private fun drawThumbShadow(screenCoord: Float, canvas: Canvas) {
        mThumbShadowMatrix.setTranslate(screenCoord + mThumbShadowXOffset, mTextOffset.toFloat() + mThumbHalfHeight + mThumbShadowYOffset.toFloat())
        mTranslatedThumbShadowPath.set(mThumbShadowPath!!)
        mTranslatedThumbShadowPath.transform(mThumbShadowMatrix)
        canvas.drawPath(mTranslatedThumbShadowPath, shadowPaint)
    }

    /**
     * Decides which (if any) thumb is touched by the given x-coordinate.
     *
     * @param touchX The x-coordinate of a touch event in screen space.
     * @return The pressed thumb or null if none has been touched.
     */
    private fun evalPressedThumb(touchX: Float): Thumb? {
        var result: Thumb? = null
        val minThumbPressed = isInThumbRange(touchX, normalizedMinValue)
        val maxThumbPressed = isInThumbRange(touchX, normalizedMaxValue)
        if (minThumbPressed && maxThumbPressed) {
            // if both thumbs are pressed (they lie on top of each other), choose the one with more room to drag. this avoids "stalling" the thumbs in a corner, not being able to drag them apart anymore.
            result = if (touchX / width > 0.5f) Thumb.MIN else Thumb.MAX
        } else if (minThumbPressed) {
            result = Thumb.MIN
        } else if (maxThumbPressed) {
            result = Thumb.MAX
        }
        return result
    }

    /**
     * Decides if given x-coordinate in screen space needs to be interpreted as "within" the normalized thumb x-coordinate.
     *
     * @param touchX               The x-coordinate in screen space to check.
     * @param normalizedThumbValue The normalized x-coordinate of the thumb to check.
     * @return true if x-coordinate is in thumb range, false otherwise.
     */
    private fun isInThumbRange(touchX: Float, normalizedThumbValue: Double): Boolean {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= mThumbHalfWidth
    }

    /**
     * Sets normalized min value to value so that 0 <= value <= normalized max value <= 1. The View will get invalidated when calling this method.
     *
     * @param value The new normalized min value to set.
     */
    private fun setNormalizedMinValue(value: Double) {
        normalizedMinValue = Math.max(0.0, Math.min(1.0, Math.min(value, normalizedMaxValue)))
        invalidate()
    }

    /**
     * Sets normalized max value to value so that 0 <= normalized min value <= value <= 1. The View will get invalidated when calling this method.
     *
     * @param value The new normalized max value to set.
     */
    private fun setNormalizedMaxValue(value: Double) {
        normalizedMaxValue = Math.max(0.0, Math.min(1.0, Math.max(value, normalizedMinValue)))
        invalidate()
    }

    /**
     * Converts a normalized value to a Number object in the value space between absolute minimum and maximum.
     */
    private fun normalizedToValue(normalized: Double):Int {
        val v = absoluteMinValuePrim + normalized * (absoluteMaxValuePrim - absoluteMinValuePrim)

        return numberType!!.toNumber(Math.round(v * 100) / 100.0) as Int
    }

    /**
     * Converts the given Number value to a normalized double.
     *
     * @param value The Number value to normalize.
     * @return The normalized double.
     */
    private fun valueToNormalized(value: Int?): Double {
        return if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            // prevent division by zero, simply return 0.
            0.0
        } else (value!!.toDouble() - absoluteMinValuePrim) / (absoluteMaxValuePrim - absoluteMinValuePrim)
    }

    /**
     * Converts a normalized value into screen space.
     *
     * @param normalizedCoord The normalized value to convert.
     * @return The converted value in screen space.
     */
    private fun normalizedToScreen(normalizedCoord: Double): Float {
        return (padding + normalizedCoord * (width - 2 * padding)).toFloat()
    }

    /**
     * Converts screen space x-coordinates into normalized values.
     *
     * @param screenCoord The x-coordinate in screen space to convert.
     * @return The normalized value.
     */
    private fun screenToNormalized(screenCoord: Float): Double {
        val width = width
        if (width <= 2 * padding) {
            // prevent division by zero, simply return 0.
            return 0.0
        } else {
            val result = ((screenCoord - padding) / (width - 2 * padding)).toDouble()
            return Math.min(1.0, Math.max(0.0, result))
        }
    }

    /**
     * Thumb constants (min and max).
     */
    private enum class Thumb {
        MIN, MAX
    }

    /**
     * Utility enumeration used to convert between Numbers and doubles.
     *
     * @author Stephan Tittel (stephan.tittel@kom.tu-darmstadt.de)
     */
    private enum class NumberType {
        LONG, DOUBLE, INTEGER, FLOAT, SHORT, BYTE, BIG_DECIMAL;

        fun toNumber(value: Double): Number {
            when (this) {
                LONG -> return value.toLong()
                DOUBLE -> return value
                INTEGER -> return value.toInt()
                FLOAT -> return value.toFloat()
                SHORT -> return value.toShort()
                BYTE -> return value.toByte()
                BIG_DECIMAL -> return BigDecimal.valueOf(value)
            }
            throw InstantiationError("can't convert " + this + " to a Number object")
        }

        companion object {

            @Throws(IllegalArgumentException::class)
            fun <E : Number> fromNumber(value: E): NumberType {
                if (value is Long) {
                    return LONG
                }
                if (value is Double) {
                    return DOUBLE
                }
                if (value is Int) {
                    return INTEGER
                }
                if (value is Float) {
                    return FLOAT
                }
                if (value is Short) {
                    return SHORT
                }
                if (value is Byte) {
                    return BYTE
                }
                if (value is BigDecimal) {
                    return BIG_DECIMAL
                }
                throw IllegalArgumentException("Number class '" + value.javaClass.name + "' is not supported")
            }
        }
    }

    /**
     * Callback listener interface to notify about changed range values.
     *
     * @param <T> The Number type the RangeSeekBar has been declared with.
     * @author Stephan Tittel (stephan.tittel@kom.tu-darmstadt.de)
    </T> */
    interface OnRangeSeekBarChangeListener<Any> {

        fun onRangeSeekBarValuesChanged(bar: RangeSeekBar2<*>, minValue: Any, maxValue: Any)
    }


    companion object {
        /**
         * Default color of a [RangeSeekBar], #FF33B5E5. This is also known as "Ice Cream Sandwich" blue.
         */
        val ACTIVE_COLOR = Color.argb(0xFF, 0x33, 0xB5, 0xE5)

        /**
         * An invalid pointer id.
         */
        val INVALID_POINTER_ID = 255

        // Localized constants from MotionEvent for compatibility
        // with API < 8 "Froyo".
        val ACTION_POINTER_INDEX_MASK = 0x0000ff00
        val ACTION_POINTER_INDEX_SHIFT = 8

        val DEFAULT_MINIMUM = 0
        val DEFAULT_MAXIMUM = 100
        val HEIGHT_IN_DP = 30
        val TEXT_LATERAL_PADDING_IN_DP = 3

        private val INITIAL_PADDING_IN_DP = 8
        private val DEFAULT_TEXT_SIZE_IN_DP = 14
        private val DEFAULT_TEXT_DISTANCE_TO_BUTTON_IN_DP = 8
        private val DEFAULT_TEXT_DISTANCE_TO_TOP_IN_DP = 8

        private val LINE_HEIGHT_IN_DP = 1
    }


}
