package com.fil.flymyowncustomer.util

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.fil.flymyowncustomer.activity.MainActivity
import com.fil.flymyowncustomer.pojo.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@SuppressLint("StaticFieldLeak")
object CartCalculate /*(context: Context)*/ {
    var context: Context? = null
    var prefDb: PrefDb? = null
    var sessionManager: SessionManager? = null
    private var isIGST = true
    private var cartArrayList = ArrayList<LocalCart>()
    //    private var usersetting: UpdateDeviceToken.Usersetting? = null
    internal var orderSubTotal: Double? = 0.0
    internal var couponTotal: Double? = 0.0
    internal var orderSGST: Double? = 0.0
    internal var orderCGST: Double? = 0.0
    internal var orderIGST: Double? = 0.0
    internal var orderVisiting: Double? = 0.0
    internal var orderTotal: Double? = 0.0
    internal var shippingCharge: Double? = 0.0
    internal var appliedCouponValue: Double? = 0.0
    internal var TotalItem = 0
    private val key_Cart_StoreList = "cartList"

    fun CartCalculate(context: Context) {
        this.context = context
        prefDb = PrefDb(context)
        sessionManager = SessionManager(context)
    }

    fun getOrderIGST(): Double? {
        return orderIGST
    }

    fun setOrderIGST(orderIGST: Double?) {
        this.orderIGST = orderIGST
    }

    fun getAppliedCoponValue(): Double? {
        return appliedCouponValue
    }

    fun setAppliedCoponValue(appliedCouponValue: Double?) {
        this.appliedCouponValue = appliedCouponValue
    }

    fun getOrderVisiting(): Double? {
        return orderVisiting
    }

    fun setOrderVisiting(orderIGST: Double?) {
        this.orderVisiting = orderIGST
    }


    /*fun getUsersetting(): UpdateDeviceToken.Usersetting {

        return usersetting!!
    }

    fun setUsersetting(usersetting: UpdateDeviceToken.Usersetting) {
        this.usersetting = usersetting
    }*/

    fun getCartArrayList(): ArrayList<LocalCart> {
        return cartArrayList
    }

    fun setCartArrayList(cartArrayListt: ArrayList<LocalCart>) {
        this.cartArrayList = cartArrayListt
    }


    fun isIGST(): Boolean {
        return isIGST
    }

    fun setIGST(IGST: Boolean) {
        isIGST = IGST
    }

    fun getTotalItem(): Int {
        return TotalItem
    }

    fun setTotalItem(totalItem: Int) {
        TotalItem = totalItem
    }

    fun getOrderSubTotal(): Double? {
        return orderSubTotal
    }

    fun setOrderSubTotal(orderSubTotal: Double?) {
        this.orderSubTotal = orderSubTotal
    }

    fun getCouponPrice(): Double? {
        return couponTotal
    }

    fun setCouponPrice(couponTotal: Double?) {
        this.couponTotal = couponTotal
    }

    fun getOrderSGST(): Double? {
        return orderSGST
    }

    fun setOrderSGST(orderSGST: Double?) {
        this.orderSGST = orderSGST
    }

    fun getOrderCGST(): Double? {
        return orderCGST
    }

    fun setOrderCGST(orderCGST: Double?) {
        this.orderCGST = orderCGST
    }

    fun getOrderTotal(): Double? {
        return orderTotal
    }

    fun setOrderTotal(orderTotal: Double?) {
        this.orderTotal = orderTotal
    }

    fun getShippingCharge(): Double? {
        return shippingCharge
    }

    fun setShippingCharge(shippingCharge: Double?) {
        this.shippingCharge = shippingCharge
    }

    fun AddToCart(
        product: ProductListPojo.Data,
        isPlus: Boolean,
        isRemove: Boolean,
        cartID: String
    ): Boolean {

        var flag = true
        var isCatadded = false
        var isProductAdded = true

//        product.productPrice = product.productPrice

        if (!isRemove && (java.lang.Double.valueOf(product.productStock!!)) <= 0) {
            return false
        }

        for (i in 0 until cartArrayList.size) {


            if (cartArrayList[i].cartID.toString().equals(cartID, true)) {

                if (cartArrayList[i]?.dealerID.equals(product!!.dealerID)) {

                    if (cartArrayList[i]?.productId.equals(product!!.productID) && cartArrayList[i].addToCartSizeId.equals(
                            product.addToCartSizeId
                        ) && cartArrayList[i].addToCartColorId.equals(product.addToCartColorId) && cartArrayList[i].addToCartMaterialId.equals(
                            product.addToCartMaterialId
                        ) && cartArrayList[i]?.addToCartPatternId.equals(product.addToCartPatternId) && cartArrayList[i].addToCartStyleId.equals(
                            product.addToCartStyleId
                        ) && cartArrayList[i].addToCartBrandId.equals(product.addToCartBrandId)
                    ) {

                        isCatadded = true
                        isProductAdded = true
                        Log.d("cartadd", isCatadded.toString())

                        if (isRemove) {
                            cartArrayList[i]?.cartQuantity = 0
                            cartArrayList.removeAt(i)
                            break
                        } else {
                            if (isPlus) {
                                if (cartArrayList[i]?.cartQuantity!! < java.lang.Double.valueOf(
                                        product.cartStock.toString()
                                    )
                                ) {
                                    cartArrayList[i]?.cartQuantity =
                                            (cartArrayList[i]?.cartQuantity!! + 1)
                                } else {
                                    cartArrayList[i]?.issue = "Out of Stock"
                                    cartArrayList[i]?.issueMsg =
                                            cartArrayList[i]?.productName + " available  stock is " +
                                            MyUtils.formatPricePerCountery(
                                                cartArrayList[i]?.cartStock
                                            ) + "."
                                    flag = false
                                    return flag
                                }
                            } else {
                                if (cartArrayList[i]?.cartQuantity!! <= 0) {
                                    cartArrayList[i]?.issue = "Somthing Rong"
                                    cartArrayList[i]?.issueMsg = "Please try again later."
                                    flag = false
                                    return flag
                                } else {
                                    if (cartArrayList[i].issue.equals("Out of Stock", true)) {
                                        cartArrayList[i].issue = ""
                                        cartArrayList[i].issueMsg = ""
                                    }

                                    cartArrayList[i]?.cartQuantity =
                                            (cartArrayList[i]?.cartQuantity!!.toInt() - 1)
                                }
                            }
                        }
                    }
                }

            }

            if (cartArrayList[i]?.cartQuantity!! <= 0) {
                cartArrayList.removeAt(i)
                break
            }
        }


        var cartId = -1

        if (!cartArrayList.isNullOrEmpty()) {
            cartId = cartArrayList.size
            cartId += cartArrayList.size
        } else {
            cartId = 0
        }

        if (!isCatadded) {
            var cart = LocalCart()
            val productArrayList = ArrayList<ProductListPojo.Data>()

            product.addToCartProductId = product.productID!!
            product.cartId = cartId
            productArrayList.add(product)

            cart.cartID = cartId
            cart.dealerID = product.dealerID!!
            cart.productId = product.productID!!
            cart.addToCartDeliveryName = product.addToCartDeliveryName
            cart.addToCartDeliveryCharges = product.addToCartDeliveryCharges
            cart.addToCartDeliveryCharges = product.addToCartDeliveryCharges

            if (!product.dealerCompanyName.isNullOrEmpty()) {
                cart.dealerCompanyName = product.dealerCompanyName!!
            }
            cart.productName = product.productName!!

            cart.cartQuantity = 1
            cart.cartStock = product?.cartStock
            cart.productDiscountedPrice = product?.productDiscountedPrice

            cart.categoryGST = product?.categoryGST
            cart.isIGST = product?.isIGST
            cart.productPriceId = product?.productPriceId

            cart.addToCartSizeId = product.addToCartSizeId
            cart.addToCartColorId = product.addToCartColorId
            cart.addToCartMaterialId = product.addToCartMaterialId
            cart.addToCartStyleId = product.addToCartStyleId
            cart.addToCartBrandId = product.addToCartBrandId
            cart.addToCartPatternId = product.addToCartPatternId
            cart.addToCartCategoryTypeId = product.categorytypeID!!
            cart.addToCartCategoryId = product.categoryID!!
            cart.addToCartsubcatId = product.subcatID!!


            cart.productData = productArrayList
            cartArrayList.add(cart)
        }


        Log.d("Saved Cart Items", "${cartArrayList.toString()}")
        Log.d("Saved Cart Items", "${getCartArrayList()}")
        saveCartItems(cartArrayList)
        setCartArrayList(cartArrayList)
        return flag
    }

    fun AddToCartService(
        product: StichingServicePojo.StichingServicData,
        isPlus: Boolean,
        isRemove: Boolean,
        cartID: String
    ): Boolean {

        var flag = true
        var isCatadded = false
        var isProductAdded = true

//        product.productPrice = product.productPrice

//        if (!isRemove && (java.lang.Double.valueOf(product.productStock!!)) <= 0) {
//            return false
//        }

        for (i in 0 until cartArrayList.size) {
            if (cartArrayList[i].cartID.toString().equals(cartID, true)) {
                if (cartArrayList[i]?.dealerID.equals(product!!.dealerID)) {

                    if (cartArrayList[i]?.cartID.equals(product!!.cartId) && cartArrayList[i]?.stitchingId.equals(
                            product!!.stitchingserviceID
                        )
                    ) {

                        isCatadded = true
                        isProductAdded = true
                        Log.d("cartadd", isCatadded.toString())

                        if (isRemove) {
                            cartArrayList[i]?.cartQuantity = 0
                            cartArrayList.removeAt(i)
                            break
                        } else {
                            if (isPlus) {
                                if (cartArrayList[i].cartQuantity > 0) {
                                    cartArrayList[i].cartQuantity =
                                            (cartArrayList[i]?.cartQuantity!! + 1)
                                } else {
//                                cartArrayList[i]?.issue = "Out of Stock"
//                                cartArrayList[i]?.issueMsg = cartArrayList[i]?.productName + " available  stock is " + MyUtils.formatPricePerCountery(cartArrayList[i]?.cartStock) + "."

                                    if (!cartArrayList[i].stichingServiceData.isNullOrEmpty()) {
                                        cartArrayList[i]?.stichingServiceData!![0].issue =
                                                "Out of Stock"
                                        cartArrayList[i]?.stichingServiceData!![0].issueMsg =
                                                cartArrayList[i]?.productName + " is out of stock."
                                    }

                                    flag = false
                                    return flag
                                }
                            } else {
                                if (cartArrayList[i]?.cartQuantity!! <= 0) {
                                    cartArrayList[i]?.issue = "Somthing Rong"
                                    cartArrayList[i]?.issueMsg = "Please try again later."

                                    if (!cartArrayList[i].stichingServiceData.isNullOrEmpty()) {
                                        cartArrayList[i]?.stichingServiceData!![0].issue =
                                                "Somthing Rong"
                                        cartArrayList[i]?.stichingServiceData!![0].issueMsg =
                                                "Please try again later."
                                    }

                                    flag = false
                                    return flag
                                } else {
                                    if (cartArrayList[i].issue.equals("Out of Stock", true)) {
                                        cartArrayList[i]?.stichingServiceData!![0].issue = ""
                                        cartArrayList[i]?.stichingServiceData!![0].issueMsg = ""
                                    }

                                    cartArrayList[i]?.cartQuantity =
                                            (cartArrayList[i]?.cartQuantity!!.toInt() - 1)
                                }
                            }
                        }
                    }
                }
            }
            if (cartArrayList[i]?.cartQuantity!! <= 0) {
                cartArrayList.removeAt(i)
                break
            }
        }


        var cartId = -1

        if (!cartArrayList.isNullOrEmpty()) {
            cartId = cartArrayList.size
            cartId += cartArrayList.size
        } else {
            cartId = 0
        }

        if (!isCatadded) {
            var cart = LocalCart()
            val productArrayList = ArrayList<StichingServicePojo.StichingServicData>()

            product.addToCartProductId = product.stitchingserviceID!!
            product.cartId = cartId
            productArrayList.add(product)

            cart.cartID = cartId
            cart.dealerID = product.dealerID!!
            cart.stitchingId = product.stitchingserviceID!!
            cart.addToCartDeliveryName = product.addToCartDeliveryName
            cart.addToCartDeliveryCharges = product.addToCartDeliveryCharges

            if (!product.dealerCompanyName.isNullOrEmpty()) {
                cart.dealerCompanyName = product.dealerCompanyName!!
            }
            cart.productName = product.stitchingserviceName!!
            cart.cartQuantity = 1
            cart.cartStock = product?.cartStock
            cart.productDiscountedPrice = product?.productDiscountedPrice
            cart.categoryGST = product?.categoryGST
            cart.isIGST = product?.isIGST


            cart.addToCartSizeId = product.addToCartSizeId
            cart.addToCartColorId = product.addToCartColorId
            cart.addToCartMaterialId = product.addToCartMaterialId
            cart.addToCartStyleId = product.addToCartStyleId
            cart.addToCartBrandId = product.addToCartBrandId
            cart.addToCartPatternId = product.addToCartPatternId


            cart.stichingServiceData = productArrayList
            cartArrayList.add(cart)
        }


        Log.d("Saved Cart Items", "${cartArrayList.toString()}")
        Log.d("Saved Cart Items", "${getCartArrayList()}")
        saveCartItems(cartArrayList)
        setCartArrayList(cartArrayList)
        return flag
    }

    fun onlySaveCart(cart: LocalCart) {
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i]?.cartID.equals(cart?.cartID) && cartArrayList[i]?.productId.equals(
                        cart?.productId
                    )
                ) {
                    cartArrayList[i] = cart
                    break
                }
            }
        }
        saveCartItems(cartArrayList)
    }

    fun getCartQuantity(product: LocalCart): Int {
        var cartQuantity = 0
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {

                if (cartArrayList[i].cartID.equals(product.cartID)) {
                    cartQuantity = cartArrayList[i].cartQuantity
                }

                /*if (!cartArrayList[i].productData.isNullOrEmpty()) {
                    if (cartArrayList[i]?.productId.equals(product!!.productId) && cartArrayList[i].addToCartSizeId.equals(
                            product.addToCartSizeId
                        ) && cartArrayList[i].addToCartColorId.equals(product.addToCartColorId) && cartArrayList[i].addToCartMaterialId.equals(
                            product.addToCartMaterialId
                        ) && cartArrayList[i]?.addToCartPatternId.equals(product.addToCartPatternId) && cartArrayList[i].addToCartStyleId.equals(
                            product.addToCartStyleId
                        ) && cartArrayList[i].addToCartBrandId.equals(product.addToCartBrandId)
                    ) {
                        cartQuantity = cartArrayList[i].cartQuantity
                    }
                }*/
            }
        } else {
            cartQuantity = 0
        }
        return cartQuantity
    }

    fun checkDealerItemAlreadyItemInCartOrNotAndClear(
        product: ProductListPojo.Data,
        isClear: Boolean
    ): Boolean {
        var checkFlag = true
        if (isClear) {
            cartArrayList.clear()
            saveCartItems(cartArrayList)
            checkFlag = true
        } else {
            if (!cartArrayList?.isNullOrEmpty()) {
                for (i in 0 until cartArrayList?.size) {
                    if (!cartArrayList[i]?.dealerID.equals(product?.dealerID, true)) {
                        checkFlag = false
                        break
                    }
                }
            } else {
                checkFlag = true
            }
        }
        return checkFlag
    }

    fun checkDealerServiceAlreadyItemInCartOrNotAndClear(
        product: StichingServicePojo.StichingServicData,
        isClear: Boolean
    ): Boolean {
        var checkFlag = true
        if (isClear) {
            cartArrayList.clear()
            saveCartItems(cartArrayList)
            checkFlag = true
        } else {
            if (!cartArrayList?.isNullOrEmpty()) {
                for (i in 0 until cartArrayList?.size) {
                    if (!cartArrayList[i]?.dealerID.equals(product?.dealerID, true)) {
                        checkFlag = false
                        break
                    }
                }
            } else {
                checkFlag = true
            }
        }
        return checkFlag
    }


    //cart Calculation
    fun cartCalculation() {
        var subTotal = 0.0
        var GST = 0.0
        var totalItems = 0
        var shippingCharge = 0.0

        var subTotal1 = 0.0
//        var GST1 = 0.0
        var shippingCharge1 = 0.0
        var totalCoupon = 0.0
        var isIGST = false


        for (i in 0 until cartArrayList.size) {
            var categoryWiseTotal: Double? = 0.0

            if (cartArrayList.size > 0) {
                isIGST = cartArrayList[i]!!.isIGST
//                for (j in 0 until cartArrayList[i].productList.size) {
                var price = 0.0

                var temol = 0.0
                var temol1 = 0.0
                var y = 0.0
                var y1 = 0.0
                var x = 0.0
                var x1 = 0.0
                var ActualAmount = 0.0
                var ActualAmount1 = 0.0

                try {
                    temol = java.lang.Double.valueOf(cartArrayList[i]?.productDiscountedPrice!!)
                    y = temol * 100
                    x = java.lang.Double.valueOf(cartArrayList[i]?.categoryGST!!) + 100
                    ActualAmount = y / x
                    //                price = Double.valueOf(cartArrayList.get(j).getProductPrice()) * cartArrayList.get(j).getQty();
                    price =
                            java.lang.Double.valueOf(ActualAmount) * (cartArrayList[i]?.cartQuantity!!)
                } catch (e: Exception) {
                }


                categoryWiseTotal = categoryWiseTotal!! + price
//                val Prodcutprice =
//                    java.lang.Double.valueOf((cartArrayList[i].productDiscountedPrice)!!.toDouble()) * (cartArrayList[i].productQunity!!)
//
//                val Prodcutprice1 =
//                    java.lang.Double.valueOf((cartArrayList[i].productDiscountedPrice1)!!.toDouble()) * (cartArrayList[i].productQunity1!!)

                cartArrayList[i]?.orderTotal = price

                if (isIGST) {
                    var iGSTtax = 0.0

                    iGSTtax =
                            price!! * java.lang.Double.valueOf(cartArrayList[i]?.categoryGST!!) / 100

                    cartArrayList[i]?.IGSTPercentage =
                            java.lang.Double.valueOf(cartArrayList[i]?.categoryGST!!)

                    cartArrayList[i]?.IGST = (iGSTtax)
                    GST += (iGSTtax)
                } else {
                    val tax = java.lang.Double.valueOf(cartArrayList[i]?.categoryGST!!) / 2
                    val GSTtax =
                        price!! * java.lang.Double.valueOf(cartArrayList[i]?.categoryGST!!) / 100

                    cartArrayList[i]?.SGSTPercentage = (tax)
                    cartArrayList[i]?.SGST = (GSTtax / 2)
                    cartArrayList[i]?.CGSTPercentage = (tax)
                    cartArrayList[i]?.CGST = (GSTtax / 2)

                    GST += GSTtax
                }

                if (cartArrayList[i]?.addToCartDeliveryCharges!! > 0) {
                    shippingCharge += cartArrayList[i]?.addToCartDeliveryCharges!!
                }
            }

            cartArrayList[i]?.orderTotal = (java.lang.Double.valueOf(categoryWiseTotal!!))

            subTotal += (cartArrayList[i]?.orderTotal!!)

            totalItems += cartArrayList[i]?.cartQuantity!!
        }

        setOrderSubTotal(subTotal)

        setTotalItem(totalItems)
        setShippingCharge(shippingCharge)
//        setCouponPrice(totalCoupon)

        if (isIGST) {
            setOrderIGST(GST)
        } else {
            setOrderCGST((GST) / 2)
            setOrderSGST((GST) / 2)
        }
        Log.e(
            "System out",
            "print:== " + getOrderSubTotal()!! + getShippingCharge()!! + getOrderCGST()!! + getOrderSGST()!! + getOrderIGST()!!
        )
        Log.e("System out", "printget ShippingCharge:== " + getShippingCharge()!!)
//        Log.e("System out", "print getOrderCGST :== " + getOrderCGST()!!)
//        Log.e("System out", "print: getOrderSGST == " + getOrderSGST()!!)
//        Log.e("System out", "print: getOrderIGST == " + getOrderIGST()!!)
        setOrderTotal(getOrderSubTotal()!! + getShippingCharge()!! + GST- getAppliedCoponValue()!!)
    }


    /*fun getGrossAmountOfWhichAvaliableForApplyCoupon(): Double {
        var totalAmountOfOrder = 0.0
        if (!cartArrayList.isNullOrEmpty()) {

            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i].isFabricStitching || cartArrayList[i].isStitching) {
                    if (!cartArrayList[i].isDesigner) {
                        totalAmountOfOrder +=
                            (java.lang.Double.valueOf(cartArrayList[i].stichingProductList[0]?.stitchingserviceDiscountedPrice) * cartArrayList[i].productQunity!!) +
                                    (java.lang.Double.valueOf(cartArrayList[i].stichingFrontPrice) * cartArrayList[i].productQunity!!) +
                                    (java.lang.Double.valueOf(cartArrayList[i].stichingBackPrice) * cartArrayList[i].productQunity!!) +
                                    (java.lang.Double.valueOf(cartArrayList[i].stichingSleevePrice) * cartArrayList[i].productQunity!!) +
                                    (java.lang.Double.valueOf(cartArrayList[i].stichingAddOnPrice) * cartArrayList[i].productQunity!!) +
                                    (java.lang.Double.valueOf(cartArrayList[i].stichingAttributePrice) * cartArrayList[i].productQunity!!)

                    }
                }
            }
        }
        return totalAmountOfOrder
    }
*/

    fun applyCouponOn(data: ApplyCouponNewPojoData) {

        val qtyOfApplyCoupon = cartArrayList.size

        /*for (i in 0 until cartArrayList.size) {
            if (cartArrayList[i].s) {
                if (!cartArrayList[i].isDesigner) {
                    qtyOfApplyCoupon += 1
                }
            }
        }*/

        var typeOfDiscount = true // true = Fix false = Pesantage
        var valueOfDiscount = 0.0
        var maxDiscount = 0.0
        var totalDiscount = 0.0
        var orderValue = 0.0

        var discountPerItem = 0.0
        var valueAfterPoint = 0
        var valueAfterPointTotalWithQty = 0.0
        var addinLastProduct = 0.0

        if (data != null) {

            if (!data.offerDiscountType.isNullOrEmpty() && (data.offerDiscountType.equals(
                    "Percentage",
                    true
                ) || data.offerDiscountType.equals(
                    "%",
                    true
                )
                        )
            ) {
                typeOfDiscount = false
            } else {
                typeOfDiscount = true
            }

            if (!data.offerDiscountValue.isNullOrEmpty()) {
                valueOfDiscount = java.lang.Double.valueOf(data?.offerDiscountValue!!)
            }

            if (!data.offerMaxDiscountAmount.isNullOrEmpty()) {
                maxDiscount = java.lang.Double.valueOf(data?.offerMaxDiscountAmount!!)
            }

            cartCalculation()
            orderValue = getOrderSubTotal()!!

            Log.d("Qty for apply coupon", "is $qtyOfApplyCoupon")

            if (!typeOfDiscount) {
                totalDiscount = (orderValue * valueOfDiscount) / 100
            } else {
                totalDiscount = valueOfDiscount
            }

            if (totalDiscount > maxDiscount) {
                totalDiscount = maxDiscount
            } else {
                totalDiscount = totalDiscount
            }

            Log.e("System out","Print totalDiscount Amount : "+totalDiscount)

            discountPerItem = totalDiscount / qtyOfApplyCoupon
            discountPerItem = java.lang.Double.valueOf(String.format("%.2f", (discountPerItem)))

            if (discountPerItem.toString().contains(".")) {
                val subString = (discountPerItem.toString()).substring(
                    (discountPerItem.toString()).lastIndexOf('.') + 1
                )
                valueAfterPoint = java.lang.Integer.valueOf(subString)
                valueAfterPointTotalWithQty =
                        java.lang.Double.valueOf("0." + valueAfterPoint.toString()) * qtyOfApplyCoupon

                if (((discountPerItem.toInt().toDouble()) + valueAfterPointTotalWithQty) == discountPerItem.toInt().toDouble()) {
                    addinLastProduct = valueAfterPointTotalWithQty
                } else {
                    addinLastProduct =
                            ((Math.round(valueAfterPointTotalWithQty)).toInt()).toDouble()
                }

                if (((discountPerItem.toInt().toDouble()) + addinLastProduct) > (((Math.round(
                        valueAfterPointTotalWithQty
                    )).toInt()).toDouble())
                ) {
                    addinLastProduct - 1
                } else if (((discountPerItem.toInt().toDouble()) + addinLastProduct) < (((Math.round(
                        valueAfterPointTotalWithQty
                    )).toInt()).toDouble())
                ) {
                    addinLastProduct + 1
                }
            }

            var pos = 0
            for (i in 0 until cartArrayList.size) {
                pos += 1
                if (pos == qtyOfApplyCoupon) {
                    cartArrayList[i].appliedDiscount =
                            discountPerItem.toInt().toDouble() + addinLastProduct
                } else {
                    cartArrayList[i].appliedDiscount = discountPerItem.toInt().toDouble()
                }
                cartArrayList[i].appliedCouponCode = data.appliedCouponCode!!
                cartArrayList[i].appliedCouponCodeCreatedBy = if (data.couponCodeCreatedBy.isNullOrEmpty()) "" else data.couponCodeCreatedBy!!
            }

            setAppliedCoponValue(totalDiscount)
//            saveCartItems(cartArrayList)
        }
    }

    fun shippingCalculate(product: ProductListPojo.Data): Double {
        /*var shipping : Double = 0.0
       if (usersetting != null) {
           shipping = product.quantity * Double.valueOf(usersetting.getDeliveryChargesPerUnit()) + Double.valueOf(usersetting.getSettingsDeliveryCharges());
       }
       return shipping;*/
        return 0.0
    }

    fun getBadge(cartArrayList: ArrayList<ProductListPojo>): Int {
        return cartArrayList.size
    }

    open fun saveCartItems(arrayList: ArrayList<LocalCart>) {

        prefDb = PrefDb(context!! as MainActivity)
        val gson = Gson()
        val json = gson.toJson(arrayList) //tasks is an ArrayList instance variable
        Log.d("Add to cart", json.toString())
        prefDb!!.putString(key_Cart_StoreList, json)
    }

    open fun saveCartItems1(arrayList: ProductListPojo, p: Int) {
//        prefDb = PrefDb(context!! as MainActivity)
        val gson = Gson()
        val json = gson.toJson(arrayList) //tasks is an ArrayList instance variable
        Log.d("Add to cart", json.toString())
        prefDb!!.putString(key_Cart_StoreList, json)
    }

    open fun getCartItems(c: Context): ArrayList<LocalCart> {
        prefDb = PrefDb(c)

        val gson = Gson()
        val json = prefDb!!.getString(key_Cart_StoreList)
        if (json != null && json.length > 0)
            cartArrayList = gson.fromJson(json, object : TypeToken<ArrayList<LocalCart>>() {}.type)
        return cartArrayList
    }

    open fun clearCartItems() {
        prefDb!!.clearValue(key_Cart_StoreList)
        cartArrayList.clear()
    }

    open fun clearCartItems(pos: java.util.ArrayList<LocalCart>): Boolean {

        val iter = pos.iterator()
        while (iter.hasNext()) {
            val user = iter.next()
            if (user?.isDelete!!) {
                iter.remove()
            }
        }
        saveCartItems(pos)
        return true
    }

    fun setIsDeleteOrNot(item: Int, b: Boolean) {
        if (!cartArrayList.isNullOrEmpty()) {
            if (b) {
                cartArrayList[item].isDelete = true
            } else {
                cartArrayList[item].isDelete = false
            }
        }
        setCartArrayList(cartArrayList)
    }


    /*fun verifyItemSetIssue(productList: kotlin.collections.List<VerifyCartItemsPojo.Product>) {
        for (i in cartArrayList.indices) {
            for (i1 in 0 until cartArrayList[i].productList.size) {

                cartArrayList[i].productList.get(i1)!!.issue = ""
                cartArrayList[i].productList.get(i1)!!.issueMsg = ""
                for (i2 in productList.indices) {
                    if (cartArrayList[i].productList.get(i1)!!.productID.equals(productList[i2].productID)) {
                        cartArrayList[i].productList.get(i1)!!.issue = productList[i2].itemissue
                        if (productList[i2].itemissue.equals("outofArea", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.resources.getString(R.string.out_of_area)
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.out_of_area)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "Out of Area"
                        } else if (productList[i2].itemissue.equals("Inactive", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.inactive)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "Inactive"
                        } else if (productList[i2].itemissue.equals("orderdetailsPrice", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.orderdetailsPrice)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "order detail price"
                        } else if (productList[i2].itemissue.equals("productGST", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.productGST)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "productGST"
                        } else if (productList[i2].itemissue.equals("outofStock", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.outofStock)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "out of stock"

                        } else if (productList[i2].itemissue.equals("productStock", true)) {
//                            cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.productStock)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "product stock"

                        } else {
                            *//*if (java.lang.Double.valueOf(productList[i2].productStock) >= java.lang.Double.valueOf(
                                    cartArrayList[i].productList.get(i1).productMOQ
                                )
                            ) {
                                val qty = java.lang.Double.valueOf(productList[i2].productStock).toInt()
                                val msg = context!!.getString(R.string.availStock, qty)
                                cartArrayList[i].productList.get(i1).issueMsg = msg
                            } else {*//*
//                                cartArrayList[i].productList.get(i1).issueMsg = context!!.getString(R.string.outofStock)
                            cartArrayList[i].productList.get(i1)!!.issueMsg = "out of stock"
//                            }
                        }
//                        cartArrayList[i].productList.get(i1).setProductSnapzoPrice(productList[i2].getProductSnapzoPrice())
//                        cartArrayList[i].productList.get(i1).setProductStock(productList[i2].productStock)
//                        cartArrayList[i].productList.get(i1).setProductGST(productList[i2].getProductGST())
                        break
                    }
                }
            }
        }
    }
*/
    fun verifyItemSetNoIssue() {
        for (i in cartArrayList.indices) {
            cartArrayList[i]?.issue = ""
            cartArrayList[i]?.issueMsg = ""
        }
    }

    fun getCartId(product: ProductListPojo.Data): String {

        var oldCartId = "-1"

        if (!cartArrayList.isNullOrEmpty()) {
            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i]?.dealerID.equals(product!!.dealerID)) {
                    if (cartArrayList[i]?.productId.equals(product!!.productID) && cartArrayList[i].addToCartSizeId.equals(
                            product.addToCartSizeId
                        ) && cartArrayList[i].addToCartColorId.equals(product.addToCartColorId) && cartArrayList[i].addToCartMaterialId.equals(
                            product.addToCartMaterialId
                        ) && cartArrayList[i]?.addToCartPatternId.equals(product.addToCartPatternId) && cartArrayList[i].addToCartStyleId.equals(
                            product.addToCartStyleId
                        ) && cartArrayList[i].addToCartBrandId.equals(product.addToCartBrandId)
                    ) {
                        oldCartId = cartArrayList[i].cartID.toString()
                        break
                    }
                }
            }
        }
        return oldCartId
    }

    fun getCartQuantityPerCartId(cartId: String): Int {

        var oldCartId = 0

        if (!cartArrayList.isNullOrEmpty()) {
            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i]?.cartID.toString().equals(cartId, true)) {
                    oldCartId = cartArrayList[i].cartID
                    break
                }
            }
        }
        return oldCartId
    }

    fun getServiceCartId(product: StichingServicePojo.StichingServicData): String {

        var oldCartId = "-1"

        if (!cartArrayList.isNullOrEmpty()) {
            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i]?.dealerID.equals(product!!.dealerID) && cartArrayList[i]?.stitchingId.equals(
                        product!!.stitchingserviceID
                    )
                ) {
                    oldCartId = cartArrayList[i].cartID.toString()
                    break
                }
            }
        }
        return oldCartId
    }

    fun getServiceCartQuantityPerCartId(cartId: String): Int {

        var oldCartId = 0

        if (!cartArrayList.isNullOrEmpty()) {
            for (i in 0 until cartArrayList.size) {
                if (cartArrayList[i]?.cartID.toString().equals(cartId, true)) {
                    oldCartId = cartArrayList[i].cartID
                    break
                }
            }
        }
        return oldCartId
    }

    fun getCaSize(): Int {
        return cartArrayList.size
    }

    fun verifyItemSetIssue(product: VeriFyCartPojo) {

        for (i in cartArrayList.indices) {
            if (!product.product.isNullOrEmpty() && !cartArrayList[i].productData.isNullOrEmpty()) {
                for (i1 in 0 until cartArrayList[i].productData!!.size) {

                    cartArrayList[i].issue = ""
                    cartArrayList[i].issueMsg = ""


                    for (i2 in 0 until product.product.size) {
                        if (cartArrayList[i].productData!![i1].productID.equals(product.product[i2].productID)) {

                            if (product.product[i2].itemissue.equals("productStock", true)) {
                                cartArrayList[i].issueMsg = "Product is out of stock."
                                cartArrayList[i].issue = "productStock"
                            } else if (product.product[i2].itemissue.equals("outofStock", true)) {
                                cartArrayList[i].issueMsg = "Product is out of stock."
                                cartArrayList[i].issue = "outofStock"
                            } else if (product.product[i2].itemissue.equals("productGST", true)) {
                                cartArrayList[i].issueMsg = "Product GST changed."
                                cartArrayList[i].issue = "productGST"
                            } else if (product.product[i2].itemissue.equals("orderdetailsPrice", true)) {
                                cartArrayList[i].issueMsg = "Product price changed."
                                cartArrayList[i].issue = "orderdetailsPrice"
                            } else if (product.product[i2].itemissue.equals("outofArea", true)) {
                                cartArrayList[i].issueMsg = "Product is not avaliable for your area."
                                cartArrayList[i].issue = "outofArea"
                            } else if (product.product[i2].itemissue.equals("productInactive", true)) {
                                cartArrayList[i].issueMsg = "Product is inactive."
                                cartArrayList[i].issue = "productInactive"
                            }

                        }
                    }


                    for (i2 in 0 until product.service!!.size) {
                        if (cartArrayList[i].stichingServiceData!![i1].stitchingserviceID.equals(product.service[i2].productID)) {

                            if (product.product[i2].itemissue.equals("productStock", true)) {
                                cartArrayList[i].issueMsg = "Product is out of stock."
                                cartArrayList[i].issue = "productStock"
                            } else if (product.product[i2].itemissue.equals("outofStock", true)) {
                                cartArrayList[i].issueMsg = "Product is out of stock."
                                cartArrayList[i].issue = "outofStock"
                            } else if (product.product[i2].itemissue.equals("productGST", true)) {
                                cartArrayList[i].issueMsg = "Product GST changed."
                                cartArrayList[i].issue = "productGST"
                            } else if (product.product[i2].itemissue.equals("orderdetailsPrice", true)) {
                                cartArrayList[i].issueMsg = "Product price changed."
                                cartArrayList[i].issue = "orderdetailsPrice"
                            } else if (product.product[i2].itemissue.equals("outofArea", true)) {
                                cartArrayList[i].issueMsg = "Product is not avaliable for your area."
                                cartArrayList[i].issue = "outofArea"
                            } else if (product.product[i2].itemissue.equals("productInactive", true)) {
                                cartArrayList[i].issueMsg = "Product is inactive."
                                cartArrayList[i].issue = "productInactive"
                            }
                        }
                    }
                }
            }
        }
    }
/*fun isVerifyItem(): Boolean {
    var flag = true
    for (i in cartArrayList.indices) {
        for (i1 in 0 until cartArrayList[i].productList.size) {

            if (cartArrayList[i].productList.get(i1)!!.issue!!.length > 1 && !(cartArrayList[i].productList.get(
                    i1
                )!!.issue.equals(
                    "productSnapzoPrice",
                    true
                ) || cartArrayList[i].productList.get(i1)!!.issue.equals(
                    "productGST", true
                ))
            ) {
                flag = false
                break
            }
        }
    }
    return flag
}*/

/*fun isVerifyItemPriceTaxChanged(): Boolean {

    var flag = true
    for (i in cartArrayList.indices) {
        for (i1 in 0 until cartArrayList[i].productList.size) {

            if (cartArrayList[i].productList.get(i1)!!.issue!!.length > 1 && (cartArrayList[i].productList.get(
                    i1
                )!!.issue.equals(
                    "productSnapzoPrice",
                    true
                ) || cartArrayList[i].productList.get(i1)!!.issue.equals(
                    "productGST", true
                ))
            ) {
                flag = false
                break
            }
        }
    }
    return flag
}*/

/*internal enum class productUnitPackage {
    Unit, Grams, Ml
}*/
}