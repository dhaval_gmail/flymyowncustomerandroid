package com.fil.flymyowncustomer.iterfaces

interface RecyclerViewItemClickListner {
    fun onItemClick(position: Int, typeofOperation: String)
}